#!/usr/bin/env bash

DIR=$1

if [ ${DIR} != *"lab"* ] || [ ${DIR} != *"style"* ] || [ ${DIR} != *"venv"* ]; then
    cd ${DIR};
    rm -r "auto";
    rm "elab_data.yml" "elab_data.pkl";
    rm img/*;
    latexmk -C;
    cd ..
fi
