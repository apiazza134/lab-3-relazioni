#!/usr/bin/env python3

from lab import Data

d = Data()

d_dietro = d.get('conversione', 'd_dietro')
d_davanti = d.get('conversione', 'd_davanti')
d_mezzo = d.get('conversione', 'd_mezzo')
x = d.get('conversione', 'x')
y = d.get('conversione', 'y')

Dxe = d_davanti + d_mezzo/2
Dey = d_dietro + d_mezzo/2

gamma = Dxe/(Dxe + Dey) + x/y*(1 - Dxe/(Dxe + Dey))
kappa = d.elab_data['conversione']['delta_px']/d.get('conversione', 'delta_cm')
conv_factor = kappa/gamma

d.push(kappa, None, 'conversione', 'kappa')
d.push(gamma, None, 'conversione', 'gamma')
d.push(conv_factor, None, 'conversione', 'conv_factor')

d.save()
