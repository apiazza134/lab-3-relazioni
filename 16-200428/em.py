#!/usr/bin/env python3

import numpy as np
from uncertainties import unumpy as unp
from uncertainties import correlated_values, ufloat
from uncertainties.umath import sqrt as usqrt
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from lab import Data, loadtxtSorted, Table

plt.style.use('../style.yml')
d = Data()
datasets = np.array(range(1, 9))

# Carico i dati
I, V = loadtxtSorted('data/misure.txt')

I = I[datasets-1]
V = V[datasets-1]

I = unp.uarray(I, np.sqrt((I*0.02)**2 + 0.01**2))
V = unp.uarray(V, 1)

R = [d.get(f'dataset-{i}', 'R') for i in datasets]
R = np.array([[r.nominal_value, r.std_dev] for r in R])
R = unp.uarray(*(R.transpose()))

magic_number = 7.80e-4

# Calcolo le variabli opportune
I = I
x = 2*V
y = (magic_number*I*R)**2


# e/m come media pesata
e_m = x/y
e_mExact = 1.60217662e-19/9.10938356e-31

d.push(e_mExact, 'coulomb/\\kilogram', 'e-m-exact', precision=3)

w = 1/(unp.std_devs(e_m)**2)

e_mMean = ufloat(
    sum(unp.nominal_values(e_m)*w)/sum(w),
    1/usqrt(sum(w))
)

d.push(e_mMean, 'coulomb/\\kilogram', 'e-m-mean')

# Tabella
tab = Table()
tab.pushColumn('Dataset', None, datasets)
tab.pushColumn('\\Delta V', 'volt', V)
tab.pushColumn('I', 'ampere', I)
tab.pushColumn('e/m_{e}', None, e_m)

d.pushTable(tab, 'tab-I-V')

# Dipendenza di e/m dal raggio
plt.errorbar(
    unp.nominal_values(R), unp.nominal_values(e_m)/e_mExact,
    xerr=unp.std_devs(R), yerr=unp.std_devs(e_m)/e_mExact,
    linestyle=''
)

for i in range(len(R)):
    plt.annotate(i+1, (R[i].n, e_m[i].n/e_mExact))

plt.xlabel('$ R $\\ [m]')
plt.ylabel('$ e/m $ exp/exact')

# plt.savefig('img/em-R-em.png', bbox_inches='tight')
# plt.show()
plt.close()

# e/m come fit
def retta(x, m, q):
    return m*x + q


init = (1/e_mExact, 0)
opt, cov = curve_fit(retta, unp.nominal_values(x), unp.nominal_values(y), p0=init, sigma=unp.std_devs(y))

_opt = correlated_values(opt, cov)

print(_opt)
print(f'e/m fit: {1/_opt[0]:.2u}')
print(f'e/m fit / exact: {1/_opt[0]/e_mExact:.2u}')
print(f'e/m exp: {e_mExact:.2e}')

X = np.linspace(min(unp.nominal_values(x)), max(unp.nominal_values(x)))
plt.plot(X, retta(X, *opt), color='red')

plt.errorbar(unp.nominal_values(x), unp.nominal_values(y),
             yerr=unp.std_devs(y), xerr=unp.std_devs(x),
             linestyle='', color='black')

for i in range(len(x)):
    plt.annotate(i+1, (x[i].n, y[i].n))

plt.xlabel('$ 2 V_{\\mathrm{acc}} $ [V] ')
plt.ylabel('$ (B_{z}^{\\mathrm{max}} \\; R)^{2} \\ [\\mathrm{T}^{2} \\mathrm{m}^{2}] $')
plt.savefig('img/em-fit.png', bbox_inches='tight')
# plt.show()
plt.close()

d.save()
