\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}
\usepackage[style=alphabetic]{biblatex}
\addbibresource{reference.bib}
\usepackage{svg}

% workaround brutto per bullseye
\begin{luacode*}
   local file = io.open("/etc/debian_version", "r")
   local currVersion = file:read("*all")
   io.close(file)
   if (currVersion == "bullseye/sid\n") then
      tex.print([[\gdef\bullseye{a}]])
   end
\end{luacode*}

\ifx\bullseye\undefined
\else
\makeatletter
\ifdefined\svg@ink@ver\else
\def\svg@ink@ver{1}%
\renewcommand*\svg@ink@cmd[2]{%
    \svg@ink@exe\space"#1.\svg@file@ext"\space%
    \svg@ink@area\space%
    \ifx\svg@ink@dpi\relax\else--export-dpi=\svg@ink@dpi\space\fi%
    \if@svg@ink@latex--export-latex\space\fi%
    \ifx\svg@ink@opt\@empty\else\svg@ink@opt\space\fi%
    \ifnum\svg@ink@ver<\@ne%
    --export-\svg@ink@format="#2.\svg@ink@format"\space%
    \else%
    --export-type=\svg@ink@format\space%
    --export-filename="#2.\svg@ink@format"\space%
    \fi%
}%
\fi
\makeatother
\fi

\preambolo["../globals-fisica.yml"]

\begin{document}
\maketitle
\renewcommand{\abstractname}{Nota}

\begin{abstract}
    Questa esperienza è stata svolta a casa, collaborando in maniera telematica, tramite utilizzo di dati degli anni precedenti a causa della chiusura dei laboratori in seguito sospensione dell'attività didattica.
\end{abstract}

\section{Metodo di misura}
Lo scopo dell'esperienza è la stima del rapporto tra la carica e la massa dell'elettrone, tramite lo studio di un fascio di elettroni in moto in presenza di un campo magnetico.

Il campo magnetico è creato da due bobine in configurazione di Helmholtz (cioè poste a una distanza pari al loro raggio), di cui si può calcolare il campo magnetico nella regione di interesse (cioè vicino al centro della bobina).

Gli elettroni sono prodotti da un cannoncino per effetto termoionico, e vengono accelerati attraverso una differenza di potenziale $ \Delta V $. All'uscita dal cannoncino, nella regione in cui supponiamo assente il campo elettrico, vale quindi
\begin{equation}\label{eq:consEnergia}
    \frac{1}{2} m_{e} v^{2} = e \Delta V
\end{equation}
Il cannoncino inizializza gli elettroni con velocità iniziale ortogonale all'asse delle bobine, per cui, assumendo che il campo magnetico sia statico e uniforme, il moto si manterrà nel piano ortogonale a tale asse, e sarà circolare uniforme\footnote{
    Questo non è in realtà del tutto vero; dopo circa un giro gli elettroni si ritrovano in prossimità del cannoncino, dove è presente un campo elettrico. Quindi in prossimità dello stesso il moto verrà perturbato. Nell'acquisizione dei punti dalle foto fornite si è posta attenzione a non prendere punti in prossimità del cannoncino, cioè dove la traiettoria appare chiaramente non circolare.
}.
Dall'equazione del moto segue che, con ovvio significato dei simboli,
\[ m_{e} \frac{v^2}{R} = evB. \]
Usando la~\eqref{eq:consEnergia} e la precedente si ottiene
\begin{equation}\label{eq:fit}
    \frac{e}{m_{e}} = \frac{2 \Delta V}{(BR)^2}.
\end{equation}
Il membro di destra è noto in quanto $ \Delta V $ è la tensione applicata all'elemento acceleratore, direttamente controllabile, $ B $ è legato alla corrente che scorre nelle spire, anche questa controllabile agendo sul generatore, e $ R $ è misurabile a partire dalle fotografie fornite. \\

Le bobine in configurazione di Helmholtz hanno raggio $r$ e sono costituite da $ N $ spire. Il campo massimo si misura nel punto medio dell'asse congiungente i centri delle bobine è parallelo all'asse delle bobine. Può essere calcolato facendo ricorso alle legge di Biot-Savart dopo aver osservato che i campi in quel punto si sommano ottenendo
\begin{equation}\label{eq:B-helmholtz}
    B = \frac{\mu_0 N r^2 I\ped{coil}}{\left[r^2 + \left(\dfrac{r}{2}\right)^2\right]^\frac{3}{2}} = \left(\frac{4}{5}\right)^{\frac{3}{2}} \frac{\mu_{0} N}{r} I\ped{coil}.
\end{equation}
dove $ I\ped{coil} $ è la corrente che scorre nelle spire (collegate in serie). \\

Sulle foto sopra menzionate si è effettuato un campionamento dei punti sull'arco interno e sull'arco esterno. Le coordinate dei pixel così ricavate sono state interpolate con un \emph{fit} circolare per ottenere una stima del raggio interno ed esterno. Si è poi assunto come valore efficace del raggio dell'orbita la media del raggio del cerchio interno e di quello esterno, e si è attribuito un errore pari alla semi-dispersione degli stessi. I raggi così ottenuti sono stati poi convertiti in unità fisiche come spiegato nella Sezione~\ref{sec:conv}. \\

La stima del rapporto $ e/m_{e} $ è stata poi ottenuta in due modi diversi: come media pesata delle singole stime di tale rapporto ottenute dalla~\eqref{eq:fit} ed effettuando un \emph{fit} lineare di $ 2\Delta V $ al variare di $ (B R)^{2} $ e ottenendo $ e/m_{e} $ dal coefficiente angolare della retta di \emph{best-fit}. \\
Assumendo $ e = \SI{1.602176634e11}{\coulomb} $ e $ m_{e} = \SI{9.10938370e-31}{\kilogram} $ il valore atteso del loro rapporto è
\begin{equation}\label{eq:e-m-exact}
    \left(\frac{e}{m_{e}}\right)\ped{exp} = \SI[]{ 175.882e9 }{ \coulomb/\kilogram }
\end{equation}

\section{Conversione da pixel a \si{\centi\meter}}\label{sec:conv}

Con riferimento alla Figura~\ref{fig:conversione}, fissiamo un ``cono visivo''. Questo sottende sul righello più vicino alla fotocamera la distanza $ x $, su quello più lontano la distanza $ y $. Il rapporto $x/y$ può essere determinato confrontando le scale graduate dei due righelli (quello del Fermilab e quello dell'apparato sperimentale) nella relativa figura fornita. Chiamiamo $ D_{x} $, $ D_{y} $, $ D_{xy} $ le distanze dalla fotocamera ai/tra i vari piani. $ D_{xe} $ è invece la distanza tra il righello $ x $ e il piano del moto degli elettroni.
Per similitudine
\[ \frac{y}{x} = \frac{D_{y}}{D_{x}} = 1 + \frac{D_{xy}}{D_{x}}. \]
Quindi la relazione tra il raggio misurato dal righello $ y $ e il raggio reale è
\[
    R = R_{y} \frac{D_{x} + D_{xe}}{D_{x} + D_{xy}} =
    R_{y} \sbr{\frac{D_{xe}}{D_{xy}} + \frac{x}{y} \del{1 - \frac{D_{xe}}{D_{xy}}}}
    = \gamma R_{y}.
\]
Sia $ \kappa $ il fattore di conversione tra pixel e i centimetri del righello $ y $. Allora abbiamo
\[ R[\si{\centi\meter}] = \frac{\gamma}{\kappa} R_{y}[\mathrm{px}]. \]
Dalle misure riportate nella scheda dell'esperienza si ottiene, attribuendo un'incertezza di \SI{1}{\milli\meter} a tutte le lunghezze in gioco
\[ \gamma = \num{ 906+-5e-3 } \qquad \kappa = \num{ 9.91+-0.07e3 }. \]

\begin{figure}[h!]
    \centering
    \includesvg[scale=0.15]{img/geometria}
    \caption{\label{fig:conversione}schema della geometria usata per la conversione pixel $ \to \si{\centi\meter} $ e per la correzione dell'effetto prospettico.}
\end{figure}

\section{Fotografie e fit circolari}
Per ciascuna delle fotografie fornite si sono acquisite le coordinate in pixel di $ \sim 25 $ punti sull'arco interno e sull'arco esterno del fascio elettronico. Su tali set di dati si è eseguito un \emph{fit} circolare per determinare il raggio di \emph{best-fit} secondo quanto suggerito in~\cite{circle-fit}.

Si riportano in Figura~\ref{fig:fit-circolare-1} e in Figura~\ref{fig:fit-circolare-2} i \emph{sampling} effettuati e le circonferenze di \emph{best-fit}. In Tabella~\ref{tab:fit-circolare} si riportano invece i valori ottenuti, assieme alla media e semi-dispersione usati come stima per $ R $, opportunamente convertiti in \si{\centi\meter} come descritto nella Sezione~\ref{sec:conv}.

\begin{table}[h!]
    \centering
    \begin{tabular}{ cccc } \toprule
      Dataset & $ R\ped{int} \ [\mathrm{px}] $ & $ R\ped{out} \ [\mathrm{px}] $ & $ R\ [\si{ \centi\meter }] $ \\ \midrule
      1 & \num{ 437.5e0 } & \num{ 471.9e0 } & \SI[multi-part-units=single]{ 4.2+-0.2 }{ \nothing } \\
      2 & \num{ 460.5e0 } & \num{ 499.4e0 } & \SI[multi-part-units=single]{ 4.4+-0.2 }{ \nothing } \\
      3 & \num{ 406.7e0 } & \num{ 431.0e0 } & \SI[multi-part-units=single]{ 3.8+-0.1 }{ \nothing } \\
      4 & \num{ 420.9e0 } & \num{ 446.7e0 } & \SI[multi-part-units=single]{ 4.0+-0.1 }{ \nothing } \\
      5 & \num{ 451.6e0 } & \num{ 481.6e0 } & \SI[multi-part-units=single]{ 4.3+-0.1 }{ \nothing } \\
      6 & \num{ 462.1e0 } & \num{ 491.7e0 } & \SI[multi-part-units=single]{ 4.4+-0.1 }{ \nothing } \\
      7 & \num{ 405.4e0 } & \num{ 425.3e0 } & \SI[multi-part-units=single]{ 3.8+-0.1 }{ \nothing } \\
      8 & \num{ 371.9e0 } & \num{ 402.6e0 } & \SI[multi-part-units=single]{ 3.5+-0.1 }{ \nothing } \\
      9 & \num{ 252.7e0 } & \num{ 275.4e0 } & \SI[multi-part-units=single]{ 2.4+-0.1 }{ \nothing } \\
      10 & \num{ 215.5e0 } & \num{ 231.3e0 } & \SI[multi-part-units=single]{ 2.04+-0.07 }{ \nothing }\\ \bottomrule
    \end{tabular}
    \caption{\label{tab:fit-circolare}raggi delle orbite ricavati dalle immagini.}
\end{table}

\section{Stima di $ e/m_{e} $ tramite media pesata}
Per ognuno dei dataset si è calcolato il rapporto $ e/m_{e} $ usando l'equazione~\eqref{eq:fit} a partire dai raggi determinati in precedenza e dai valori misurati riportati in Tabella~\ref{tab:em-all}. Il campo magnetico viene determinato tramite la~\eqref{eq:B-helmholtz} dal valore della corrente e assumendo $ r = \SI{15}{\centi\meter} $ e $ N = 130 $, da cui $ {B = \num{7.8e-4} \si{\tesla\ampere^{-1}}\cdot I\ped{coil}[\si{\ampere}]} $. Nella stessa tabella si riportano inoltre i valori di $ e/m_{e} $ così determinati.

\begin{table}[h!]
    \centering
    \directlua{tex.print(data['tab-I-V'])}
    \caption{\label{tab:em-all}}
\end{table}

Da tale tabella osserviamo che i valori così ottenuti non sono compatibili tra di loro, e si possono osservare deviazioni significative nei dataset 9 e 10. Possiamo in ogni caso dare una stima di $ e/m_{e} $ come media pesata\footnote{
    Ovvero il fit con una retta costante: se i dati sono della forma $ \{x_{i} \pm \sigma_{i}\} $ si intende
    \[
        x = \frac{\sum_{i} w_{i} x_{i}}{\sum_{i} w_{i}} \pm \frac{1}{\sqrt{\sum_{i} w_{i}}}
        \qquad \text{dove} \quad w_{i} = \frac{1}{\sigma_{i}^{2}}
    \]
} dei valori ottenuti ricavando
\[
    \left(\frac{e}{m_{e}}\right)\ped{mean} = \SI[]{ 308+-8e9 }{\coulomb/\kilogram }
\]
che non è compatibile con il valore atteso, ma ci si discosta di un fattore $ \sim 1.75 $. \\

Un tentativo di migliorare la stima di cui sopra consiste nel considerare i dati relativi ai dataset 9 e 10 come \emph{outliers} e non considerarli nella media pesata. Facendo un grafico dei rapporti carica-massa precedentemente ottenuti in funzione del raggio (Figura~\ref{fig:em-R-em}) si osserva infatti che i dataset 9 e 10 sono ben distanti (in termini di deviazioni standard) dalla zona in cui sono addensati gli altri punti. Osserviamo inoltre che tali dataset sono quelli relativi ai raggi più piccoli in cui cadono molte delle ipotesi fatte nella Sezione~\ref{sec:conv} e che verranno discussi più in dettaglio nella Sezione~\ref{sec:errori}.

Escludendo i dataset 9 e 10 si ottiene quindi la seguente stima
\[
    \left(\frac{e}{m_{e}}\right)\ped{mean} = \SI[]{ 294+-8e9 }{\coulomb/\kilogram }
\]
che migliora leggermente il risultato trovato in precedenza, ma risulta sempre una sovrastima del valore atteso.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{img/em-R-em}
    \caption{\label{fig:em-R-em}rapporto tra la stima $ e/m_{e} $ ed il valore esatto in funzione del raggio del cerchio.}
\end{figure}
\section{Stima di $ e/m_{e} $ tramite \emph{fit} lineare}
Utilizzando le misure in Tabella~\ref{tab:em-all}, i raggi ottenuti precedentemente e determinando il campo magnetico secondo la \eqref{eq:B-helmholtz} e si è quindi effettuato un \emph{fit} lineare a due parametri con riferimento all'equazione \eqref{eq:fit}.

Con un modello $ y = ax + b $ ove $ x = 2\Delta V$ e $ y = (B R)^{2}$, escludendo i raggi relativi ai dataset 9 e 10 per i motivi esposti nella sezione precedente, si ottengono i seguenti parametri di \emph{best-fit}
\[
    a = \SI{6.4+-0.8e-12}{\tesla^{2}\meter^{2}/\volt} \qquad b = \SI{-1.5+-0.4e-9}{\tesla^{2}\meter^{2}}
\]
da cui si ricava la seguente stima del rapporto e/m:
\[
    \left(\frac{e}{m_{e}}\right)\ped{fit} = \frac{1}{a} = \SI[]{ 157+-19e9 }{\coulomb/\kilogram }
\]
che è compatibile entro 1 deviazione standard con il valore atteso, nonostante si sia introdotto un offset inizialmente non previsto dal modello.
L'origine del parametro $ b $ potenzialmente potrebbe segnalare, visto l'ordine di grandezza e il segno, la presenza di ulteriori campi magnetici esterni nell'apparato.
Si riportano in figura \ref{fig:em-fit} i dati sperimentali e la curva di \emph{best-fit}.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7]{img/em-fit.png}
    \caption{\label{fig:em-fit}\emph{fit} dei dati sperimentali per la determinazione del rapporto carica-massa.}
\end{figure}

\section{Commenti sugli errori sistematici}\label{sec:errori}
La misura del rapporto $ e/m_{e} $ è affetta da numerosi errori sistematici che cerchiamo di riassumere in quanto segue. Ove possibile, cerchiamo di dare una stima rozza degli effetti trattandoli separatamente.
\subsection{Distorsione del bulbo}
Un effetto geometrico di cui non abbiamo tenuto conto è dato dalla distorsione delle immagini dovute al bulbo sferico. Chiaramente tale effetto non è uniforme tra i punti all'interno del bulbo ed è più accentuato per quelli più vicini al bordo. L'effetto della distorsione può essere stimato confrontando tra di loro la foto del righello dell'apparato sperimentale con e senza bulbo: in Figura~\ref{fig:righelli} si osserva che tale effetto diventa significativo per punti che distano $ \sim \SI{5}{\centi\meter} $ dal centro del bulbo dove si osserva una contrazione delle lunghezze di $ \sim \SI{5}{\centi\meter} / \SI{5.1}{\centi\meter} \simeq 98\% $. \\

Chiaramente tale fattore \emph{non} è quello corretto da applicare ai punti all'\emph{interno} del bulbo, in quanto è stato ricavato a partire da confronto con il righello \emph{dietro} al bulbo e non è ovvio il modo in cui tale fattore sia legato a quello corretto. Tuttavia facendo una simulazione, il cui output è riportato in Figura~\ref{fig:bulbo}, si osserva che, a parità di distanza del centro della sfera, l'effetto di distorsione di un oggetto dentro alla sfera è circa la metà della distorsione dietro alla stessa.

Pertanto un $ \sim 1\% $ rappresenta una stima rozza ma ragionevole dell'errore sistematico compiuto sulla determinazione dei raggi delle orbite più vicine al bordo.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=1]{img/righelli.png}
    \caption{\label{fig:righelli}confronto del righello posteriore con (sotto) e senza (sopra) bulbo.}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.2]{img/bulbo.png}
    \caption{\label{fig:bulbo}simulazione della distorsione su un fascio di raggi paralleli provenienti da dietro il bulbo e su un fascio proveniente dall'interno del bulbo stesso (realizzato con~\cite{raySim}).}
\end{figure}

\subsection{Disunifomità del campo magnetico}
Il campo magnetico generato dalle bobine non è uniforme e presenta una dipendenza sia dalla distanza dall'asse $ \rho $ che dalla distanza dalle bobine $ z $, completamente trascurata nell'analisi precedente. I relativi grafici teorici $ B_{\mathrm{H},z}(\rho) $ e $ B_{\mathrm{H}, z}(z) $ sono riportati in Figura~\ref{fig:B-spire-nonuniforme} in rapporto al valore massimo $ B\ped{H}\ap{max} $.

\begin{figure}[h!]
    \centering
    \def\scale{0.6}
    \subfloat[$ B\ped{H, z}(\rho, z = 0)$]{
        \includegraphics[scale=\scale]{img/Bh.pdf}
    }
    \subfloat[$ B\ped{H, z}(\rho = 0, z)$]{
        \includegraphics[scale=\scale]{img/Bz.pdf}
    }
    \caption{\label{fig:B-spire-nonuniforme}non uniformità teoriche della componente assiale del campo magnetico.}
\end{figure}

Osservando le immagini fornite per l'esperienza si vede che il pennello elettronico si trova inizialmente a una distanza di $\sim \SI{4}{\centi\meter}$ dall'asse congiungente i centri delle bobine, dove $B\ped{H} \simeq 0.998 \cdot  B\ped{H}\ap{max}$.

Per quanto riguarda la dipendenza di $ B_{\mathrm{H},z} $ da $ \rho $, ipotizzando che la traiettoria si mantenga sempre su un piano ortogonale alla direzione assiale, possiamo individuare due situazioni tipiche:
\begin{enumerate}
    \item se la traiettoria si mantiene equidistante dall'asse delle bobine, il campo magnetico sarà comunque costante sulla traiettoria.
    Questo è abbastanza vero per le traiettorie di raggio più grande, come quella mostrata in Figura~\ref{fig:img6}, in cui si avrà una correzione del campo magnetico del $\sim 0.4\%$;

    \item se la traiettoria ha un raggio decisamente inferiore (questa è ad esempio la situazione del dataset 10 in Figura~\ref{fig:img10}) il centro dell'orbita elettronica non giace in prossimità dell'asse e  non possiamo più assumere che il campo sia costante sulla traiettoria. Il moto non sarà più circolare, con curvatura maggiore vicino all'asse rispetto alla parte inferiore. In questo caso non riusciamo a dare una stima dell'errore percentuale compiuto, ma è ragionevole assumere (in base alle curve teoriche) sia leggermente superiore al precedente.
\end{enumerate}

\begin{figure}[h!]
    \centering
    \def\scale{0.2}
    \subfloat[\label{fig:img6}Dataset 6.]{
        \includegraphics[scale=\scale]{img/img06.jpg}
    }
    \subfloat[\label{fig:img10}Dataset 10.]{
        \includegraphics[scale=\scale]{img/img10.jpg}
    }
    \caption{immagini rappresentative della configurazione sperimentale in cui è possibile apprezzare le dimensioni del bulbo rispetto alle bobine e la distanza del pennello elettronico dall'asse congiungente i centri delle bobine.}
\end{figure}

\subsection{Campo magnetico terrestre}
Al fine di valutare l'impatto del campo magnetico terrestre $ \vec{B}\ped{T} $ sulla misura si è fatto riferimento ai valori forniti dal \href{https://www.ngdc.noaa.gov/geomag/calculators/magcalc.shtml#igrfwmm}{database NOAA} per la città di Pisa. Per stimare l'effetto di $ \vec{B}\ped{T} $ assumiamo che il campo delle bobine $ \vec{B}\ped{H} $ sia uniforme e omogeneo, con modulo corrispondente alla corrente minima. \\

Se $ \hat{z} $ è la direzione di $ \vec{B}\ped{H} $, l'apparato sperimentale è stato orientato in modo che $ \vec{B}\ped{T} $ appartenesse al piano $ xy $ (Figura~\ref{fig:B-terrestre}): a Pisa il campo magnetico terrestre ha modulo $ B\ped{T} \sim \SI{47}{\micro\tesla} $ ed è inclinato di $ \sim 60^{\circ} $ verso il basso (rispetto alla direzione orizzontale), pertanto il campo magnetico totale è
\[
    \vec{B} = (B_{\mathrm{T}x}, B_{\mathrm{T}y}, B\ped{H}) \sim (23, -41, 780) \ \si{\micro\tesla}
\]
dove si è posto $ \hat{y} $ lungo alla normale uscente alla superficie terrestre. Gli elettroni vengono inizializzati con velocità $ \vec{v}_{0} = (v_{0}, 0, 0) $ faranno orbite elicoidali, scomponibili in un moto rettilineo uniforme nella direzione $ \vec{n} = \vec{B}/B \sim (0.03, -0.05, 0.998) $ con velocità $ v\ped{\parallel} = \vec{v}_{0} \cdot \vec{n} \sim 0.03 \cdot v_{0} $ e moto circolare uniforme di raggio $ R $ nel piano ortogonale a $ \vec{n} $ con velocità orbitale $ v_{\perp} = \sqrt{v_{0}^{2} - v_{\parallel}^{2}} \sim 0.99955 \cdot v_{0} $ (Figura~\ref{fig:piani}). \\

Se ci concentriamo solo sull'effetto ``maggiore'' di distorsione del moto dovuto alla componente $ \hat{y} $ di $ \vec{B} $ e trascuriamo il moto di traslazione, la proiezione sul piano della foto del moto dell'elettrone sarà un ellisse con asse maggiore pari a $ R $ e asse minore $ R_{y} \simeq R \cos\alpha $ dove $ \pi/2 + \alpha $ è l'angolo tra $ \vec{n} $ e $ \hat{y} $. Ora $ \alpha \simeq \sin{\alpha} = -\cos(\pi/2 + \alpha) = - \vec{n} \cdot \hat{y} \sim 0.05 $ da cui $ R_{y} \simeq R \cos\alpha \simeq 1 - \alpha^{2}/2 \sim 0.999 \cdot R $. \\

La procedura più corretta per determinare il raggi tenendo conto del campo magneti terrestre, sarebbe quella di effettuare sui punti acquisiti dalle foto un fit ellittico ed estrapolare dall'asse maggiore e dall'asse minore di questo il valore del raggio. Per quanto detto in precedenza si avrebbe una correzione sui raggi del $ \sim 0.1\% $.

\begin{figure}[h!]
    \centering
    \subfloat[\label{fig:B-terrestre}Configurazione dell'apparato sperimentale rispetto alla direzione Nord-Sud del campo magnetico terrestre.]{
        \includegraphics[scale=0.8]{img/bussola.png}
    }
    \hspace{1cm}
    \subfloat[\label{fig:piani}Rotazione del piano della trattoria (parallelepipedo) dovuto alla presenza della componente di campo terrestre ortogonale al campo delle spire.]{
        \includegraphics[scale=0.8]{img/piani.png}
    }
    \caption{effetti del campo magnetico terrestre.}
\end{figure}

\subsection{Velocità degli elettroni}
La velocità degli elettroni si discosta dal valore teorico per due motivi principali:
\begin{enumerate}
    \item non uniformità del potenziale acceleratore dovuta alla struttura dell'anodo;
    \item collisioni con gli atomi di elio nel bulbo.
\end{enumerate}
Questi effetti implicano in ultima analisi una sovrastima di $ e/m_{e} $.

Al fine di minimizzare gli effetti dovuti alle collisioni il manuale dell'apparato utilizzato suggerisce di mantenere valori elevati nel range permesso per la tensione di accelerazione ($ \SI{270}{\volt} - \SI{370}{\volt} $ a $ \SI{1.6}{\ampere} $): si nota infatti dalla Tabella~\ref{tab:em-all} che a data corrente \textit{I} aumentando \textit{V} si ottengono stime migliori (si considerino ad esempio i dataset 4, 5, 6).

\subsection{Campo elettrico}
In prossimità del cannoncino elettronico è presente un campo elettrico che non è completamente schermato al di fuori dello stesso e che devia gli elettroni vicini. Tale effetto è ben visibile in Figura~\ref{fig:img10}.

\def\scale{0.6}
\begin{figure}[p]
    \centering
    \foreach \x in {1,...,6} {
        \subfloat{
            \includegraphics[scale=\scale]{img/fit-\x}\hfill
        }
    }
    \caption{\label{fig:fit-circolare-1}\emph{sampling} delle foto e \emph{fit} circolare.}
\end{figure}
\begin{figure}[p]
    \centering
    \foreach \x in {7,...,10} {
        \subfloat{
            \includegraphics[scale=\scale]{img/fit-\x}\hfill
        }
    }
    \caption{\label{fig:fit-circolare-2}\emph{sampling} delle foto e \emph{fit} circolare.}
\end{figure}

\dichiarazione
\printbibliography
\end{document}
