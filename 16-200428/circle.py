#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from lab import Data
from uncertainties import ufloat

d = Data()
conv_factor = d.get('conversione', 'conv_factor')
plt.style.use("../style.yml")
datasets = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for dataset in datasets:
    fig, ax = plt.subplots()
    Rs = []

    for c, color in zip(['in', 'out'], ['black', 'red']):
        x, y = np.loadtxt(f'data/{dataset}-{c}.txt', unpack=True)

        u = x - x.mean()
        v = y - y.mean()

        suu = (u**2).sum()
        suv = (u*v).sum()
        svv = (v**2).sum()

        suuu = (u**3).sum()
        suvv = (u*(v**2)).sum()
        suuv = ((u**2)*v).sum()
        svvv = (v**3).sum()

        uc = (
            suv*(svvv+suuv) - svv*(suuu+suvv)
        )/(
            2*(suv**2 - suu*svv)
        )

        vc = (
            suv*(suuu+suvv) - suu*(svvv+suuv)
        )/(
            2*(suv**2 - suu*svv)
        )

        R = np.sqrt(uc**2 + vc**2 + (suu+svv)/len(u))
        C = [uc + x.mean(), vc + y.mean()]
        Rs.append(R)

        ax.plot(x, y, marker='x', linestyle='', color=color)
        ax.add_patch(Circle(C, R, fill=False, edgecolor=color))
        ax.plot(*C, marker='x', color=color)

    ax.set_xlabel('$ x $ [px]')
    ax.set_ylabel('$ y $ [px]')
    ax.set_aspect('equal')
    ax.set_title(f'Dataset {dataset}')
    plt.gca().invert_yaxis()
    # plt.show()
    plt.savefig(f'img/fit-{dataset}.png', bbox_inches='tight')
    plt.close()

    R = ufloat(sum(Rs)/2, abs(Rs[1]-Rs[0])/2)/conv_factor
    print(f'dataset {dataset}: R = {R} m')
    d.push(R, 'meter', f'dataset-{dataset}', 'R')

d.save()
