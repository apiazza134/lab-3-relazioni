#!/usr/bin/env python3

from lab import run_chain

run_chain([
    '1a.py', '1c.py', '2.py', 'laplace.py'
])
