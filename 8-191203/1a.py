#!/usr/bin/env python3

from lab.data import Data
from scipy.optimize import fsolve
import numpy as np

d = Data()

# rapporto di partizione misurato

d.push(d.get('1.a', 'Vout')/d.get('1.a', 'Vin'), None, '1.a', 'betaMeas')

# omega_0 atteso
R1 = d.get('1.z', 'R1')
R2 = d.get('1.z', 'R2')
C1 = d.get('1.z', 'C1')
C2 = d.get('1.z', 'C2')

omegaExp1 = 1/(R1*C1)
omegaExp2 = 1/(R2*C2)

d.push(omegaExp1, 'second^{-1}', '1.a', 'omegaExp1')
d.push(omegaExp2, 'second^{-1}', '1.a', 'omegaExp2')

# omega_0 misurata
d.push(1/d.get('1.a', 'tau'), 'second^{-1}', '1.a', 'omega0')

# tempo di salita atteso
tbasso = fsolve(lambda x: 0.9 - np.exp(-omegaExp1.nominal_value * x)*(1 + omegaExp1.nominal_value * x), 0)[0]
talto = fsolve(lambda x: 0.1 - np.exp(-omegaExp1.nominal_value * x)*(1 + omegaExp1.nominal_value * x), 0.0005)[0]

d.push(talto-tbasso, 'second', '1.b', 'tSalitaExp', precision=0)

d.save()
