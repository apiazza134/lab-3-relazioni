#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from uncertainties import correlated_values
import uncertainties.unumpy as unp
from scipy.optimize import curve_fit
from lab import (
    Data, Table, loadtxtSorted,
    measureMeanUncertaintyArray
)

plt.style.use('../style.yml')

d = Data()

# Risposta in frequenza
f, df, Vin, divVin, Vout, divVout, tau, dtau = loadtxtSorted('./data/1c.txt')

f = unp.uarray(f, df)
Vin = measureMeanUncertaintyArray(Vin, divVin)
Vout = measureMeanUncertaintyArray(Vout, divVout)
tau = unp.uarray(tau, dtau)
A = Vout/Vin
Adb = 20*unp.log10(A)
phi = 2*np.pi*f*tau

# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A_{v}', 'deci\\bel', Adb)
tab.pushColumn('\\tau', 'second', tau)
tab.pushColumn('\\varphi', 'radian', phi)
d.pushTable(tab, '1.c', 'tab')


# Fit
# Modello
def modello(x, a, fT):
    return a/(1 + (x/fT)**2)


# Faccio il fit
init = (19.72, 1e3)
opt, cov = curve_fit(
    modello, unp.nominal_values(f), unp.nominal_values(A),
    p0=init, sigma=unp.std_devs(A)
)
dopt = np.sqrt(cov.diagonal())

chi2 = (((unp.nominal_values(A) - modello(unp.nominal_values(f), *opt))/unp.std_devs(A))**2).sum()
ndof = len(f) - 2

# scrivo i parametri ottimali come ufloat
opt = correlated_values(opt, cov)
d.push(opt[0], None, '1.c', 'a')
d.push(opt[1], 'hertz', '1.c', 'fT')
d.recursivePush(f'{chi2:.2f}/{ndof}', '1.c', 'chi2ndof')

# Faccio i grafici
plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(Adb),
    xerr=unp.std_devs(f), yerr=unp.std_devs(Adb),
    linestyle='', color='black', elinewidth=0.8
)

X = np.logspace(
    np.log10(unp.nominal_values(f).min()),
    np.log10(unp.nominal_values(f).max())
)
Y = 20*np.log10(modello(X, *unp.nominal_values(opt)))

plt.plot(X, Y, color='red', linewidth=0.5)
plt.xscale('log')

# bellurie
plt.xlabel('$f\\ [\\mathrm{Hz}]$')
plt.ylabel('$A_{v}\\ [\\mathrm{dB}] $')
plt.savefig('img/1cBode.png')
# plt.show()
plt.close()

# frequenza di taglio attesa
fT1 = 1/(2*np.pi*d.get('1.z', 'R1')*d.get('1.z', 'C1'))
fT2 = 1/(2*np.pi*d.get('1.z', 'R2')*d.get('1.z', 'C2'))
d.push(fT1, 'hertz', '1.e', 'fT1')
d.push(fT2, 'hertz', '1.e', 'fT2')

# rapporto di partizione
R9 = d.get('1.z', 'R9')
R10 = d.get('1.z', 'R10')
d.push(R10/(R9+R10), None, '1.e', 'beta')

# Sfasamento
# Faccio il grafico
plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(phi),
    xerr=unp.std_devs(f), yerr=unp.std_devs(phi),
    linestyle='', color='black', elinewidth=0.8
)

Y = -2*unp.arctan(X/fT2.nominal_value)

plt.plot(X, Y, color='red', linewidth=0.5)

plt.xscale('log')
plt.xlabel('$f\\ [\\mathrm{Hz}]$')
plt.ylabel('$\\varphi \\ [\\mathrm{rad}] $')
plt.savefig('img/1cFase.png')
# plt.show()

d.save()
