#!/usr/bin/env python3

from uncertainties import unumpy as unp
from uncertainties import correlated_values
import numpy as np
from scipy.optimize import curve_fit
import cmath
import matplotlib.pyplot as plt

from lab import (
    Data, loadtxtSorted, cov2corr, Table,
    measureMeanUncertaintyArray
)

plt.style.use('../style.yml')

d = Data()

# Carico i dati dal txt
f, df, Vin, divVin, Vout, divVout, tau, dtau = loadtxtSorted("./data/2a.txt")

f = unp.uarray(f, df)
Vin = measureMeanUncertaintyArray(Vin, divVin)
Vout = measureMeanUncertaintyArray(Vout, divVout)
tau = unp.uarray(tau, dtau)

A = Vout/Vin
phi = 2*np.pi*tau*f

# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('\\tau', 'second', tau)
tab.pushColumn('A', None, A)
tab.pushColumn('\\varphi', 'radian', phi)

d.pushTable(tab, '2.a', 'tab')
d.save()


def Afit(x, f1, f2):
    return (1 + (x/f1)**2) / (1 + (x/f2)**2)


def phase(x, f1, f2):
    Acomplex = (1 + 1j*x/f1)**2 / (1 + 1j*x/f2)**2
    return np.array(list(
        map(lambda t: cmath.phase(t), list(Acomplex))
    ))


TAGLIO = 90e3
fbasse = unp.nominal_values(f)[unp.nominal_values(f) < TAGLIO]
Abasse = unp.nominal_values(A)[unp.nominal_values(f) < TAGLIO]
dAbasse = unp.std_devs(A)[unp.nominal_values(f) < TAGLIO]

init = (1e3, 10e3)
opt, cov = curve_fit(
    Afit, fbasse, Abasse, p0=init, sigma=dAbasse, absolute_sigma=False
)

# Plot Bode
X = np.logspace(
    np.log10(unp.nominal_values(f).min()),
    np.log10(unp.nominal_values(f).max())
)

Aexp = 20*np.log10(Afit(X, *opt))
Adb = 20*unp.log10(A)

plt.plot(X, Aexp, color='red', linewidth=0.5)
plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(Adb),
    xerr=unp.std_devs(f), yerr=unp.std_devs(Adb),
    linestyle='', color='black', elinewidth=0.8
)
plt.xscale('log')
plt.xlabel('$ f \ [\mathrm{Hz}] $')
plt.ylabel('$ A \ [\mathrm{dB}] $')

plt.savefig("./img/compensatoreBode.png")
# plt.show()
plt.close()

# Plot fase
phiexp = phase(X, *opt)

plt.plot(X, phiexp, color='red', linewidth=0.5)
plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(phi),
    xerr=unp.std_devs(f), yerr=unp.std_devs(phi),
    linestyle='', color='black', elinewidth=0.8
)

plt.xscale('log')
plt.xlabel('$ f \\ [\\mathrm{Hz}] $')
plt.ylabel('$ \\varphi \\ [\\mathrm{rad}] $')

plt.savefig("./img/compensatoreFase.png")
# plt.show()
plt.close()

# Valori attesi
d = Data()

A01 = d.get('2.z', 'R5') / d.get('2.z', 'R3')
A02 = d.get('2.z', 'R7') / d.get('2.z', 'R6')

f01 = 1/(2*np.pi*(d.get('2.z', 'R3') + d.get('2.z', 'R4')) * d.get('2.z', 'C3'))
f02 = 1/(2*np.pi*(d.get('2.z', 'R6') + d.get('2.z', 'R7')) * d.get('2.z', 'C4'))
fp1 = 1/(2*np.pi*d.get('2.z', 'R4') * d.get('2.z', 'C3'))
fp2 = 1/(2*np.pi*d.get('2.z', 'R7') * d.get('2.z', 'C3'))

# Pusho i risultati del fit e i valori attesi
opt = correlated_values(opt, cov)
corr = cov2corr(cov)[1][0]
chi2 = (((Abasse - Afit(fbasse, *unp.nominal_values(opt))) / dAbasse)**2).sum()

d = Data()
d.push(A01, None, '2.b', 'A01')
d.push(A02, None, '2.b', 'A02')
d.push(f01, 'hertz', '2.b', 'f01')
d.push(f02, 'hertz', '2.b', 'f02')
d.push(fp1, 'hertz', '2.b', 'fp1')
d.push(fp2, 'hertz', '2.b', 'fp2')
d.push(opt[0], 'hertz', '2.b', 'f0')
d.push(opt[1], 'hertz', '2.b', 'fp')

d.recursivePush(f'{corr:.3f}', '2.b', 'corr')
d.recursivePush(f'{chi2:.3f}/{len(fbasse)-len(opt)}', '2.b', 'chi2dof')

d.save()
