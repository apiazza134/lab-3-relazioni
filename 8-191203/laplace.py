#!/usr/bin/env python3

# import sympy as sp
import numpy as np
import matplotlib.pyplot as plt

# s, t = sp.symbols('s, t')
# w0 = sp.symbols('w0', real = True)
# w1 = sp.symbols('w1', real = True)
# wp = sp.symbols('wp', real = True)

# expression = (1 + s/w0)**2 /(( 1 + s/wp )**2 * ( 1 + s/w1 )**2 * s )

# print(sp.inverse_laplace_transform(expression, s, t))

# PLOT
plt.style.use('../style.yml')


# Scriviamo direttamente la formula perché sympy è lento
def f(t, w0, w1, wp):
    return -(t*w0**2*w1**3*wp*np.exp(t*w1) - t*w0**2*w1**2*wp**2*np.exp(t*w1) + t*w0**2*w1**2*wp**2*np.exp(t*wp) - t*w0**2*w1*wp**3*np.exp(t*wp) - 2*t*w0*w1**3*wp**2*np.exp(t*w1) - 2*t*w0*w1**3*wp**2*np.exp(t*wp) + 2*t*w0*w1**2*wp**3*np.exp(t*w1) + 2*t*w0*w1**2*wp**3*np.exp(t*wp) + t*w1**4*wp**2*np.exp(t*wp) + t*w1**3*wp**3*np.exp(t*w1) - t*w1**3*wp**3*np.exp(t*wp) - t*w1**2*wp**4*np.exp(t*w1) + w0**2*w1**3*np.exp(t*w1) - w0**2*w1**3*np.exp(t*(w1 + wp)) - 3*w0**2*w1**2*wp*np.exp(t*w1) + 3*w0**2*w1**2*wp*np.exp(t*(w1 + wp)) + 3*w0**2*w1*wp**2*np.exp(t*wp) - 3*w0**2*w1*wp**2*np.exp(t*(w1 + wp)) - w0**2*wp**3*np.exp(t*wp) + w0**2*wp**3*np.exp(t*(w1 + wp)) + 4*w0*w1**2*wp**2*np.exp(t*w1) - 4*w0*w1**2*wp**2*np.exp(t*wp) - w1**3*wp**2*np.exp(t*w1) + w1**3*wp**2*np.exp(t*wp) - w1**2*wp**3*np.exp(t*w1) + w1**2*wp**3*np.exp(t*wp))*np.exp(-t*(w1 + wp))/(w0**2*(w1 - wp)**3) if t > 0 else 0


X = np.linspace(-50e-6, 9*50e-6, 1000)

Y = list(map(lambda x: 1 if x > 0 else 0, X))
plt.plot(X, Y, color='black')

Y = list(map(lambda x: f(x, 2*np.pi*953, 2*np.pi*999, 2*np.pi*11.17e3), X))
plt.plot(X, Y, color='blue')

plt.xlabel('$t \\ [\\mathrm{s}]$')
plt.ylabel('$[\\mathrm{V}]$')

plt.savefig('img/rispostaGradinoSympy.png')
