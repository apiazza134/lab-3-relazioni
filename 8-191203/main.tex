\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}
\usepackage{breqn}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo

\begin{document}
\maketitle
\section{Componenti}
Si è assemblato il circuito con i seguenti valori misurati dei componenti:
\begin{gather*}
  R_{1} = \sivar{1.z}{R1}\quad
  R_{2} = \sivar{1.z}{R2}\quad
  R_{3} = \sivar{2.z}{R3}\quad
  R_{4} = \sivar{2.z}{R4}\\
  R_{5} = \sivar{2.z}{R5}\quad
  R_{6} = \sivar{2.z}{R6}\quad
  R_{7} = \sivar{2.z}{R7}\quad
  R_{8} = \sivar{2.z}{R8}\\
  R_{9} = \sivar{1.z}{R9}\quad
  R_{10} = \sivar{1.z}{R10}\quad
  R \ped{pot,1} = \sivar{1.z}{RP1}\quad
  R \ped{pot,2} = \sivar{1.z}{RP2}\\  \\
  C_{1} = \sivar{1.z}{C1}\quad
  C_{2} = \sivar{1.z}{C2}\quad
  C_{3} = \sivar{2.z}{C3}\quad
  C_{4} = \sivar{2.z}{C4}\\ \\
  V \ped{CC} = \sivar{2.z}{VCC}\quad
  V \ped{EE} = \sivar{2.z}{VEE}
\end{gather*}

\section{Analisi del circuito di test}\label{sec:circuitoTest}
Dato che il valore della resistenza $ R_{2} $ non è trascurabile rispetto all'impedenza in ingresso dell'oscilloscopio ($ \SI{1}{\mega\ohm} $), per prelevare il segnale $ V \ped{out} $ si è usata, nelle sezioni che seguono, una sonda con attenuazione \texttt{10x}. In questa sezione si sono regolati entrambi i potenziometri a zero.

\subsection{Risposta al gradino}
Per analizzare la risposta al gradino si è inviata in ingresso al partitore di tensione un'onda quadra di ampiezza picco-picco $ V \ped{in} $. In Figura~\ref{fig:rispostaGradino} si riporta il segnale in ingresso assieme al segnale in uscita dal doppio \texttt{RC}. Si osserva che l'ampiezza picco-picco del segnale in uscita è correttamente attenuata di un fattore
\[ \frac{V \ped{out}}{V \ped{in}} = \frac{\sivar{1.a}{Vout}}{\sivar{1.a}{Vin}} = \sivar{1.a}{betaMeas}. \]
rispetto al segnale in ingresso, in accordo con la~\eqref{eq:rapportoPartizione}\footnote{
  I dati sopra riportati sono diversi da quelli visibili il Figura~\ref{fig:rispostaGradino} in quanto si è usata una scala diversa per prendere tali misure (che garantisce un'incertezza minore sulle stesse misure).
}.

In Figura~\ref{fig:dettaglioGradino} si riporta un dettaglio della risposta al gradino. Si nota che la curva riproduce qualitativamente l'andamento atteso
\begin{equation}\label{eq:rispostaGradino}
  V \ped{out}(t) = V \ped{out, S} (1 - e^{- \omega_{0} t} - \omega_{0}t e^{-\omega_{0}t}).
\end{equation}

Per determinare $ \omega_{0} $ si è misurato il tempo $ \tau = 1/\omega_{0} $ impiegato dal segnale $ V \ped{out} $ a passare dal valore minimo a una frazione $ (1 - 2/e) $ del suo valore ``asintotico'' (si è scelta una frequenza del segnale in ingresso di $ \sivar{1.a}{f} $, così da garantire che l'uscita potesse raggiungere il suo valore asintotico prima di un nuovo passaggio di $ V \ped{in} $ da ``alto'' a ``basso'' e viceversa).

Si è misurato $ \tau = \sivar{1.a}{tau} $, da cui
\[ \omega_{0} = \sivar{1.a}{omega0} \]
compatibile con i valori attesi
\[
  \omega \ped{0, 1, exp} = \frac{1}{R_{1} C_{1}} = \sivar{1.a}{omegaExp1}\qquad
  \omega \ped{0, 2, exp} = \frac{1}{R_{2} C_{2}} = \sivar{1.a}{omegaExp2}
\]

Si è misurato come tempo di salita, usando l'apposita funzione dell'oscilloscopio,
\[ t \ped{sal} = \sivar{1.a}{tsalita}. \]

Per ottenere una stima teorica di tale valore si sono ottenuti numericamente (usando la funzione \texttt{fsolve} del pacchetto \texttt{scipy.optimize} di Python) i tempi per cui la funzione data dalla~\eqref{eq:rispostaGradino} raggiunge il $ 10\% $ e il $ 90\% $ del valore asintotico. Il tempo di salita atteso (valore nominale, calcolato con $ \omega \ped{0, 1, exp} $) sarà dunque la differenza di questi tempi:
\[ t \ped{sal, exp} = \sivar{1.b}{tSalitaExp}, \]
compatibile con il valore misurato.

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:rispostaGradino}Risposta a un'onda quadra: segnale in ingresso nel partitore di tensione (\texttt{CH1}) sovrapposto a quello in uscita dal doppio \texttt{RC} (\texttt{CH2}).]{\includegraphics[scale=0.8,trim={0 0 0 2cm}]{data/1a}} \hspace{1cm}
  \subfloat[\label{fig:dettaglioGradino}Dettaglio della risposta al gradino.]{\includegraphics[scale=0.45]{data/1a-cusu}}
  \caption{}
\end{figure}

\subsection{Risposta in frequenza}
Si riportano in Tabella~\ref{tab:bodeRC} i dati di $ V \ped{in} $ e $ V \ped{out} $ in funzione della frequenza per il doppio \texttt{RC} in serie al partitore, assieme alla conseguente attenuazione $ A_{v} $ e allo sfasamento in tempo $ \tau $ e angolo $ \varphi $.
Si è effettuato un fit con modello
\[ f(x) = \frac{a}{1 + \del{\dfrac{x}{f \ped{T}}}^{2}}. \]
Osserviamo che il modello ha una pendenza asintotica di $ \SI{-40}{\deci\bel/decade} $, essendo un filtro del secondo ordine (in quanto serie di due filtri del primo ordine idealmente identici e indipendenti)\footnote{
  Non si è potuta effettuare una misura diretta della pendenza asintotica del grafico di Bode, in quanto questo avrebbe richiesto di spingersi a frequenze più alte di quelle da noi esplorate, dove tuttavia il segnale era troppo debole e troppo affetto da rumore per permettere un'analisi efficace. Tuttavia il modello con la corretta pendenza asintotica risulta qualitativamente in accordo con i dati sperimentali.
}.
Si sono ottenuti come parametri di best fit
\[ a = \sivar{1.c}{a}\qquad f \ped{T} = \sivar{1.c}{fT} \qquad \chi^{2}/\mathrm{dof} = \sivar{1.c}{chi2ndof} \]
Il valore ottenuto per la frequenza di taglio è compatibile con le frequenze attese ottenute con i valori misurati delle resistenze e delle capacità:
\[ f \ped{T, 1} = \frac{1}{2\pi R_{1} C_{1}} = \sivar{1.e}{fT1} \qquad f \ped{T, 2} = \frac{1}{2\pi R_{2} C_{2}} = \sivar{1.e}{fT2} \]
Una misura diretta della frequenza di taglio come la frequenza a cui il segnale in uscita è attenuato di $ \SI{-6}{\deci\bel} $ rispetto alla massima ampiezza picco-picco di $ V \ped{out} $ dà:
\[ f_{\SI{-6}{\deci\bel}} = \sivar{1.a}{fT}, \]
ancora compatibile con le altre stime di $ f \ped{T} $.

Il parametro $ a $ è invece compatibile con il rapporto di partizione
\begin{equation}\label{eq:rapportoPartizione}
  \beta = \frac{R_{10}}{R_{9} + R_{10}} = \sivar{1.e}{beta}.
\end{equation}
In Figura~\ref{fig:bodeRC} si riporta il grafico di Bode per il doppio \texttt{RC} in serie al partitore, con sovrapposta la funzione di best fit.

In Figura~\ref{fig:faseRC} si riporta invece lo sfasamento del segnale $ V \ped{out} $ rispetto al segnale $ V \ped{in} $, con sovrapposto il grafico dello sfasamento calcolato con la $ f \ped{T, 1} $ sopra riportata (usando la $ f \ped{T, 2} $ il grafico non cambia sensibilmente). Notiamo che i dati sperimentali riproducono qualitativamente l'andamento atteso.

\begin{table}[h!]
  \centering
  \tab{1.c}{tab}
  \caption{\label{tab:bodeRC}risposta in frequenza del doppio RC in serie al partitore.}
\end{table}

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:bodeRC}Diagramma di Bode per il doppio \texttt{RC} in serie al partitore. Sovrapposto è il grafico della funzione di best fit.]{\includegraphics[scale=0.8]{img/1cBode}}\\
  \subfloat[\label{fig:faseRC}Sfasamento del segnale $ V \ped{out} $ rispetto al segnale $ V \ped{in} $.]{\includegraphics[scale=0.8]{img/1cFase}}
  \caption{risposta in frequenza del doppio filtro \texttt{RC} in serie al partitore di tensione.}
\end{figure}

\clearpage

\section{Circuito di compensazione}
\subsection{Risposta in frequenza}
Si assembla il circuito di compensazione e si invia, con il generatore di funzioni, all'ingresso di questo un segnale sinusoidale $ V\ped{in,comp} $. Si osserva in uscita $ V\ped{out,comp} $ un segnale sinusoidale della stessa frequenza ma sfasato e di ampiezza diversa.  \\

Si riportano in Tabella~\ref{tab:compBode} le misure delle ampiezze picco-picco $ V\ped{in,comp} $ e $ V\ped{out,comp} $ al variare della frequenza $ f $ del segnale in ingresso; si misura inoltre lo sfasamento tra i segnali in tempo $ \tau $. Nella stessa si riporta inoltre il guadagno $ A_{v} = V\ped{out,comp}/V\ped{in,comp} $ e lo sfasamento $ \varphi = 2\pi f \tau $ in radianti.

L'incertezza sulle misure dello sfasamento per frequenze nel range \SI{800}{\hertz}--\SI{10}{\kilo\hertz} è stata attribuita tenendo contro delle forti distorsioni presenti nel segnale in uscita, vedi Figura~\ref{fig:compDistorsione}, probabilmente attribuibili alla non monocromaticità segnale in ingresso. Per tale misura abbiamo considerato lo sfasamento tra il segnale in ingresso e un'interpolazione ``ad occhio'' della componente fondamentale del segnale in uscita. \\

In Figura~\ref{fig:compBode} e in Figura~\ref{fig:compFase} si riportano rispettivamente il diagramma di Bode e lo sfasamento in funzione della frequenza.

\begin{table}[h!]
  \centering
  \tab{2.a}{tab}
  \caption{risposta in frequenza del circuito di compensazione}
  \label{tab:compBode}
\end{table}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.9]{data/2-1}
  \caption{distorsione del segnale in uscita. \texttt{CH1} è $ V\ped{in,comp} $, \texttt{CH2} è $ V\ped{out,comp} $.}
  \label{fig:compDistorsione}
\end{figure}

\subsection{Funzione di trasferimento}
Il circuito compensatore è costituito dalla cascata di due amplificatori non invertenti con impedenze non puramente resistive. Considerando l'OpAmp come ideale e in regime lineare, la funzione di trasferimento del singolo invertente è
\[
  \tilde{A}_{1}(s) = - \frac{R_{5}}{\left(\dfrac{1}{R_{3}} + \dfrac{1}{R_{4} + \frac{1}{sC_{3}}}\right)^{-1}} = - \frac{R_{5}}{R_{3}} \frac{1 + C_{3} (R_{3} + R_{4}) s}{1 + C_{3}R_{4} s}
  \qquad
  \tilde{A}_{2}(s) = - \frac{R_{8}}{R_{6}} \frac{1 + C_{3} (R_{6} + R_{7}) s}{1 + C_{3}R_{7} s}
\]
e la funzione di trasferimento complessiva sarà data da $ \tilde{A}(s) = \tilde{A}_{1}(s) \tilde{A}_{2}(s) $ avendo il primo stadio impedenza in uscita trascurabile. Ora nel nostro caso è
\begin{align*}
  R_{5}/R_{3} & = \sivar{2.b}{A01}                                    & R_{8}/R_{6} & = \sivar{2.b}{A02}                                    \\
  f_{0,1}     & = \frac{1}{2\pi (R_{3} + R_{4}) C} = \sivar{2.b}{f01} & f_{0,2}     & = \frac{1}{2\pi (R_{6} + R_{7}) C} = \sivar{2.b}{f02} \\
  f\ped{p,1}  & = \frac{1}{2\pi R_{4} C} = \sivar{2.b}{fp1}           & f\ped{p,2}  & = \frac{1}{2\pi R_{7} C} = \sivar{2.b}{fp2}
\end{align*}
dunque possiamo approssimare la risposta del circuito complessivo considerando le rispettive resistenze e capacità uguali così da avere
\begin{equation}
  \tilde{A}(f) = \tilde{A}(2\pi j f) = \frac{\left(1 + j\frac{f}{f_{0}}\right)^{2}}{\left(1 + j\frac{f}{f\ped{p}}\right)^{2}}
\end{equation}
con $ f\ped{p} \sim 10 f_{0} $. Il modulo della funzione di trasferimento è
\begin{equation}\label{eq:compfit}
  A(f) = \frac{1 + \frac{f^{2}}{f_{0}^{2}}}{1 + \frac{f^{2}}{f\ped{p}^{2}}}
\end{equation}
pertanto nel diagramma di Bode ci aspettiamo un andamento qualitativo del tipo
\begin{equation} \label{eq:compBode}
  A[\si{\deci\bel}] =
  \begin{cases}
    0 & \text{per } f \ll f_{0} \\
    40 \log{f} - 40\log{f_{0}} & \text{per } f_{0} \ll f \ll f\ped{p} \\
    40 \log{\frac{f\ped{p}}{f_{0}}} & \text{per } f \gg f\ped{p}
  \end{cases}
\end{equation}

Dai dati sperimentali in Figura~\ref{fig:compBode} osserviamo che questi riproducono l'andamento atteso dell'equazione~\ref{eq:compBode} solo per $ f \leq \SI{100}{\kilo\hertz} $.
Al di sopra di tale frequenza si osserva una diminuzione del guadagno all'aumentare della frequenza: tale deviazione è probabilmente attribuibile alla dipendenza del guadagno dell'OpAmp dalla frequenza che è stata trascurata nel modello presentato in precedenza. \\

Si effettua un \textit{fit} numerico del guadagno in funzione della frequenza usando come funzione modello l'equazione~\eqref{eq:compfit}. I parametri di \textit{best fit} ottenuti sono
\[
  f\ped{0,best} = \sivar{2.b}{f0} \qquad f\ped{p,best} = \sivar{2.b}{fp} \qquad \mathrm{corr} = \get{2.b}{corr} \quad \chi^{2}/\mathrm{dof} = \get{2.b}{chi2dof}
\]
compatibili con quelli attesi $ f_{0,i} $ e $ f_{\text{p}, i} $ con $ i=1, 2 $ calcolati in precedenza.

In Figura~\ref{fig:compBode} riportiamo, sovrapposta ai dati sperimentali, la curva modello in corrispondenza dei parametri ottimali.
In Figura~\ref{fig:compFase} riportiamo la curva di sfasamento atteso: osserviamo che a differenza del plot di Bode, la dipendenza del guadagno dell'OpAmp dalla frequenza contribuisce in mondo non trascurabile allo sfasamento già per frequenze $ \sim \SI{10}{\kilo\hertz} $.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.75]{img/compensatoreBode}
  \caption{diagramma di Bode per il circuito compensatore.}
  \label{fig:compBode}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.75]{img/compensatoreFase}
  \caption{sfasamento in funzione della frequenza per il circuito compensatore.}
  \label{fig:compFase}
\end{figure}

\section{``Accelerazione'' del circuito di test}
Si interpone il circuito di compensazione tra l'uscita del partitore e il circuito di test \texttt{RCRC}.

\subsection{Risposta al gradino del circuito complessivo}
Si invia in ingresso un'onda quadra di frequenza $ f = \sivar{3.a}{f} $ e ampiezza picco-picco $ V_{2} = \sivar{3.a}{Vin} $ così da avere all'uscita del partitore $ V\ped{out,S} = \sivar{3.a}{VoutS} $. In Figura~\ref{fig:gradinoOscillo} si riporta l'acquisizione con l'oscilloscopio della risposta al gradino. \\

Da un punto di vista modellistico ci aspetteremmo di vedere in uscita un segnale della stessa forma di quello riportato in Figura~\ref{fig:rispostaGradino} in cui la transizione da ``basso'' a ``alto'' avviene in $ \sim 1/10 $ del tempo.
Tuttavia nella risposta si osserva la presenza di un ``overshoot'': tale comportamento è in realtà atteso se si tiene conto del fatto che la frequenza $ f_{0} $ è diversa tra il circuito di compensazione $ f\ped{0,comp} = \sivar{2.b}{f0} $ e il circuito di test \texttt{RCRC} $ f\ped{0,test} = \sivar{1.c}{fT} $.
La funzione di trasferimento complessiva della loro serie è quindi
\[
  \tilde{A}(f) = \frac{\left(1 + j\frac{f}{f\ped{0,comp}}\right)^{2}}{\left(1 + j\frac{f}{f\ped{p}}\right)^{2}\left(1 + j\frac{f}{f\ped{0,test}}\right)^{2}}
\]
In tale caso il calcolo della risposta nel dominio del tempo ad un gradino non risulta agevole. Pertanto per invertire la trasformata di Laplace abbiamo fatto uso del pacchetto di Python di calcolo simbolico \texttt{sympy}. Ponendo per semplicità $ \omega\ped{0,comp} = \omega_{0} $ e $ \omega\ped{0,test} = \omega_{1} $ si ottiene
\input{formula-brutta}
In Figura~\ref{fig:gradinoSympy} si riporta il grafico della risposta del circuito al gradino di ampiezza unitaria avendo usato per $ \omega_{0} $, $ \omega_{1} $ e $ \omega \ped{p} $ i valori derivanti dalle stime delle frequenze caratteristiche sopra riportate.
Tenendo conto del fatto che $ \omega_{0} \neq \omega_{1} $ quanto osservato risulta effettivamente qualitativamente in accordo con quanto atteso.

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:gradinoOscillo}$ V_{2} $ nel \texttt{CH1}, $ V\ped{out} $ nel \texttt{CH2}.]{\includegraphics[scale=0.75]{data/3a-1}}
  \subfloat[\label{fig:gradinoSympy}Forma attesa della risposta.]{\includegraphics[scale=0.5]{img/rispostaGradinoSympy}}
  \caption{risposta al gradino del circuito complessivo.}
\end{figure}

\subsection{Risposta al gradino del circuito di compensazione}

La funzione di trasferimento da $ V \ped{in, comp} $ all'uscita del primo OpAmp (che denotiamo con $ V \ped{I} $) è
\[ \tilde{A}_{1}(s) = - \frac{1 + \frac{s}{\omega_{0}}}{1 + \frac{s}{\omega \ped{p}}} \]
da cui segue che la risposta a un gradino di altezza $ V_{0} $ è
\begin{equation}\label{eq:schifezzaPrimoInv}
  V \ped{I}(t) = - V_{0} \sbr{ 1 + \del{\frac{\omega \ped{p}}{\omega_{0}} - 1}e^{- \omega_{p} t}} \theta(t).
\end{equation}
Si riporta in Figura~\ref{fig:gradinoPrimoInv} la risposta all'onda quadra del primo amplificatore invertente con un dettaglio dei picchi osservati, che sono qualitativamente ben descritti dalla~\eqref{eq:schifezzaPrimoInv}. L'ampiezza picco-picco attesa del segnale in uscita è $ V_{0}(\omega \ped{p} / \omega_{0} + 1) \simeq 11 V\ped{out, S} \simeq \SI{1.2}{\volt} $, in accordo con quanto riportato nella figura. \\

La funzione di trasferimento dell'intero circuito di compensazione è invece
\[ \tilde{A}(s) = \tilde{A}_{1}(s) \tilde{A}_{s}(s) \simeq \tilde{A}_{1}^{2}(s) = \frac{\del{1 + \frac{s}{\omega_{0}}}^{2}}{\del{1 + \frac{s}{\omega \ped{p}}}^{2}} \]
da cui segue che la risposta a un gradino di altezza $ V_{0} $ è
\begin{equation}\label{eq:schifezzaSecondoInv}
  V \ped{out, comp}(t) = V_{0} \sbr{1 + \del{ \frac{\omega \ped{p}^{2}}{\omega_{0}^{2}}- 1} e^{- \omega \ped{p} t}
    - \del{ 1 - \frac{\omega \ped{p}}{\omega_{0}} }^{2} \omega \ped{p} t e^{- \omega \ped{p} t} }
\end{equation}
Si riporta in Figura~\ref{fig:gradinoComp} la risposta all'onda quadra dell'intero circuito di compensazione con un dettaglio dei picchi osservati, che sono qualitativamente ben descritti dalla~\eqref{eq:schifezzaSecondoInv}\footnote{
  Fa eccezione il transiente iniziale in cui ci aspetteremmo una salita ``istantanea'' che viene però mitigata da capacità e induttanze parassite.
}. L'ampiezza picco-picco attesa del segnale in uscita, stimato numericamente, è $ V_{0}(\omega \ped{p}^{2} / \omega_{0}^{2}) - \min\{ V \ped{out}\} \simeq V \ped{out, S} ( 100 + 7.78 ) \simeq \SI{12}{\volt} $, comparabile con quanto riportato nella figura.

\begin{figure}[h!]
  \centering
  \subfloat[Risposta complessiva.]{\includegraphics[scale=0.6]{data/3b-1-1}}
  \subfloat[Zoom sul gradino.]{\includegraphics[scale=0.6]{data/3b-1-2}}
  \caption{risposta al gradino all'uscita del primo invertente.}
  \label{fig:gradinoPrimoInv}
\end{figure}

\begin{figure}[h!]
  \centering
  \subfloat[Risposta complessiva.]{\includegraphics[scale=0.6]{data/3b-2-1}}
  \subfloat[Zoom sul gradino.]{\includegraphics[scale=0.6]{data/3b-2-2}}
  \caption{risposta al gradino all'uscita del circuito di compensazione.}
  \label{fig:gradinoComp}
\end{figure}

\subsection{Dipendenza dalla posizione dei potenziometri}
Variando la posizione dei trimmer si osserva qualitativamente che l'overshoot tende ad attenuarsi fino a sparire, come si nota dalla Figura~\ref{fig:schifoPotenziometro}.
Tuttavia, per valori eccessivi delle resistenze dei potenziometri si osserva che il segnale in uscita viene distorto in forma rispetto a quello analizzato nella sezione~\ref{sec:circuitoTest}.

Scegliendo $ R \ped{pot,1} = \SI{0}{\ohm} $ e $ R \ped{pot,2} = \sivar{3.c}{Rpot2} $ si ottiene che la risposta del circuito complessivo è qualitativamente analoga in forma alla risposta del solo circuito di test, come si può notare in Figura~\ref{fig:schifoAccordato}; in presenza del compensatore, però, la risposta del circuito risulta più rapida.

Tuttavia, il rapporto tra i tempi caratteristici delle due risposte non è $ \sim 10 $ come desiderato. Infatti tale comportamento si avrebbe solo per una precisa configurazione in cui entrambi i potenziometri hanno resistenza non nulla e i due circuiti \texttt{RC} hanno la stessa frequenza di taglio, uguale alla frequenza caratteristica del circuito di compensazione.

\begin{figure}[h!]
  \centering
  \subfloat[Potenziometro 1 circa a metà.]{\includegraphics[scale=0.55]{data/3c-1}}
  \subfloat[Potenziometro 1 al massimo.]{\includegraphics[scale=0.55]{data/3c-2}}
  \subfloat[Potenziometro 2 circa a metà.]{\includegraphics[scale=0.55]{data/3c-3}}
  \caption{\label{fig:schifoPotenziometro}}
\end{figure}
\begin{figure}[h!]
  \centering
  \subfloat[Solo circuito di test.]{\includegraphics[scale=0.55]{data/3c-5}}
  \subfloat[Circuito completo.]{\includegraphics[scale=0.55]{data/3c-4}}
  \caption{\label{fig:schifoAccordato}Risposta al gradino con il potenziometro impostato a $ R\ped{pot,2} \simeq \SI{30}{\kilo\ohm} $.}
\end{figure}

\dichiarazione

\end{document}
