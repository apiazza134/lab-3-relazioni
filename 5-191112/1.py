#!/usr/bin/env python3

from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
from uncertainties import correlated_values
import matplotlib.pyplot as plt
import numpy as np
from lab import (
    Data, Table, loadtxtSorted,
    measureMeanUncertaintyArray
)

plt.style.use('../style.yml')

d = Data()

# 1.a
Aexp = - d.get('1.a', 'R2') / d.get('1.a', 'R1')
d.push(Aexp, None, '1.a', 'Aexp')

# 1.b
Vin, divVin, Vout, divVout = loadtxtSorted('data/1b.txt')
Vin = measureMeanUncertaintyArray(Vin, divVin)
Vout = measureMeanUncertaintyArray(Vout, divVout)
A = Vout/Vin

# Tabella
tab = Table()
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A_{v}', None, A)
d.pushTable(tab, '1.b', 'tab')


# Fit
def f(x, A):
    return A*x


Vinlin = Vin[Vin < 1.5]
Voutlin = Vout[Vin < 1.5]

init = 10
opt, cov = curve_fit(
    f, unp.nominal_values(Vinlin), unp.nominal_values(Voutlin),
    p0=init, sigma=unp.std_devs(Voutlin), absolute_sigma=True
)
yerr = np.sqrt(unp.std_devs(Voutlin)**2 + opt[0]**2 * unp.std_devs(Vinlin)**2)
opt, cov = curve_fit(
    f, unp.nominal_values(Vinlin), unp.nominal_values(Voutlin),
    p0=opt[0], sigma=yerr, absolute_sigma=True
)

opt = correlated_values(opt, cov)
chi2 = (((unp.nominal_values(Voutlin) - f(unp.nominal_values(Vinlin), *unp.nominal_values(opt)))/yerr)**2).sum()

plt.errorbar(unp.nominal_values(Vinlin), unp.nominal_values(Voutlin),
             xerr=unp.std_devs(Vinlin), yerr=unp.std_devs(Voutlin),
             linestyle='', color='blue', elinewidth=0.8)

X = np.linspace(min(unp.nominal_values(Vinlin))-0.1, max(unp.nominal_values(Vinlin)) + 1)
plt.plot(X, f(X, *unp.nominal_values(opt)), color='red', linewidth=0.7)

Vinother = Vin[Vin > 1.5]
Voutother = Vout[Vin > 1.5]

plt.errorbar(unp.nominal_values(Vinother), unp.nominal_values(Voutother),
             xerr=unp.std_devs(Vinother), yerr=unp.std_devs(Voutother),
             linestyle='', color='black', elinewidth=0.8)

plt.xlabel('$V_{\\mathrm{in}} [\\mathrm{V}]$')
plt.ylabel('$V_{\\mathrm{out}} [\\mathrm{V}]$')
plt.savefig('./img/ampinvert.png')

d.push(opt[0], None, '1.b', 'A')
d.recursivePush(f'{chi2:.3f}/{len(Vinlin)}', '1.b', 'chi2')

# 1.c: impedenza in ingresso
R1 = d.get('1.a', 'R1')
R2 = d.get('1.a', 'R2')

Zin = d.get('1.c', 'RL')/(d.get('1.c', 'V1')/d.get('1.c', 'V2') - 1)
d.push(Zin, 'ohm', '1.c', 'Zin')

d.save()
