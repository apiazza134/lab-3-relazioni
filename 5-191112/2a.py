#!/usr/bin/env python3

import numpy as np
from lab import (
    Data, Table, loadtxtSorted
)
import matplotlib.pyplot as plt
from lab.oscilloscope import measureMeanUncertaintyArray
from uncertainties import unumpy, correlated_values
from scipy.optimize import curve_fit

plt.style.use('../style.yml')

f, dF, Vin, divVin, Vout, divVout = loadtxtSorted('data/2a.txt')

f = unumpy.uarray(f, dF)
Vin = measureMeanUncertaintyArray(Vin, divVin)
Vout = measureMeanUncertaintyArray(Vout, divVout)

# FIT
logf = unumpy.log10(f)
A = Vout/Vin
Adb = 20*unumpy.log10(A)

tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A_{v}', 'deci\\bel', Adb)

d = Data()
d.pushTable(tab, '2.a', 'tab')

# FIT
logsoglia = 5.6

logfalte = logf[logf > logsoglia]
Adbalte = Adb[logf > logsoglia]

logfbasse = logf[logf < logsoglia]
Adbbasse = Adb[logf < logsoglia]

# disegno bode
plt.errorbar(
    unumpy.nominal_values(logfbasse), unumpy.nominal_values(Adbbasse),
    xerr=unumpy.std_devs(logfbasse), yerr=unumpy.std_devs(Adbbasse),
    linestyle='', elinewidth=0.8, color='blue'
)

plt.errorbar(
    unumpy.nominal_values(logfalte), unumpy.nominal_values(Adbalte),
    xerr=unumpy.std_devs(logfalte), yerr=unumpy.std_devs(Adbalte),
    linestyle='', color='red'
)

# indice del cazzilo massimo
indexMax = np.argmax(unumpy.nominal_values(A))
d.push(A[indexMax], None, '2.a', 'Amax')


# Modello
def retta(x, m, q):
    return m*x + q

# Fitto dei log delle cose ad alte frequenze
init = (-20, 20*7.57)
opt, cov = curve_fit(
    retta, unumpy.nominal_values(logfalte), unumpy.nominal_values(Adbalte),
    p0=init, sigma=unumpy.std_devs(Adbalte)
)
dopt = np.sqrt(cov.diagonal())

# scrivo i parametri ottimali come ufloat
opt = correlated_values(opt, cov)
print(opt)

d.push(opt[0], 'deci\\bel/decade', '2.a', 'm')
d.push(opt[1], 'deci\\bel', '2.a', 'q')

# frequenza f_{high}
logfH = (Adb[indexMax] - opt[1])/opt[0]
fH = 10**logfH

X = np.linspace(logfH.nominal_value, unumpy.nominal_values(logfalte).max())
Y = retta(X, *(unumpy.nominal_values(opt)))

plt.plot(X, Y, color='green', linewidth=0.5)  # stampo la retta fittata

# salvo i dati
d.push(fH, 'hertz', '2.a', 'fH')

# stampo la retta orizzontale
X = np.linspace(unumpy.nominal_values(logf).min(), logfH.nominal_value)
plt.plot(X, [Adb[indexMax].nominal_value]*len(X), color='green', linewidth=0.5)

# bellurie
plt.xlabel('$\\log_{10} (f/1 \\mathrm{Hz})$')
plt.ylabel('$A_{v}\\ [\\mathrm{dB}] $')

# salvo il grafico (bellissimo)
plt.savefig('img/2a.png')
# plt.show()

# salvo i dati
d.save()
