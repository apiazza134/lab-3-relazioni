\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}  %% use 1.a numbering

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo

\begin{document}
\maketitle

Scopo dell'esperienza è misurare le caratteristiche di circuiti lineari realizzati con un OpAmp \texttt{TL081} alimentati tra \SI{+15}{\volt} e \SI{-15}{\volt}.

\section{Amplificatore invertente}
Si vuole realizzare un amplificatore invertente con un'impedenza di ingresso superiore a \SI{1}{\kilo\ohm} e con un'amplificazione a centro banda di \num{10}.

\subsection{Scelta dei componenti}
Si monta il circuito secondo lo schema mostrato in Figura~\ref{fig:ampinv}, utilizzando la barra di distribuzione grigia per la tensione negativa, quella rossa per la tensione positiva, e quella nera per la massa.

In condizioni di OpAmp ideale è $ V_{-} \simeq V_{+} \simeq 0 $ e pertanto l'impedenza in ingresso è $ R\ped{in} = V\ped{in} / I\ped{in} \simeq R_{1} $: si dovrà quindi scegliere $ R_{1} \geq \SI{1}{\kilo\ohm} $. Il guadagno dell'amplificatore invertente è $ A_{v} = - R_{2}/R_{1} $ da cui deve essere $ R_{2} = 10 R_{1} $. Tuttavia per prendere misure attendibili con l'oscilloscopio deve anche essere $ R_{2} \ll R\ped{osc} \sim \SI{1}{\mega\ohm} $. Abbiamo quindi scelto come valori nominali $ R_{1} = \SI{1}{\kilo\ohm} $ e $ R_{2} = \SI{10}{\kilo\ohm} $. \\

Le resistenze selezionate hanno i seguenti valori, misurati con il multimetro digitale, con il corrispondente valore atteso
del guadagno in tensione dell'amplificatore.
\[
  R_1 = \sivar{1.a}{R1} \quad
  R_2 = \sivar{1.a}{R2} \quad
  A\ped{exp} = \sivar{1.a}{Aexp}
\]
Le tensioni di alimentazione dell'OpAmp sono
\[ V\ped{CC} = \sivar{1.a}{VCC} \qquad V\ped{EE} = \sivar{1.a}{VEE} \]

\begin{figure}[h]
  \centering
  \begin{circuitikz}
    \draw node[op amp] (opamp) {}
    (opamp.up) to ++(0,0.3) node[above]{$ V\ped{CC} $}
    (opamp.down) to ++(0,-0.3) node[below]{$ V\ped{EE} $};
    \draw (-4,0.5) node[ocirc, label=$ v\ped{in} $]{}
    to [R=$ R_{1} $] (opamp.-);
    \draw (opamp.-)
    to ++(0,1.2)
    to [R=$ R_{2} $]  ($ (opamp.out) + (0,1.7) $)
    to (opamp.out);
    \draw (opamp.out) to ++(0.5,0) node[ocirc, label=$v\ped{out}$] {};
    \draw (opamp.+) to ++(-0.5,0) node[ground] {};
  \end{circuitikz}
  \caption{schema di un amplificatore invertente.}
  \label{fig:ampinv}
\end{figure}

\subsection{Linearità e misura del guadagno}
Si invia all'ingresso dell'amplificatore un'onda sinusoidale di frequenza fissata $ f = \sivar{1.b}{f} $.
L'uscita dell'amplificatore è mostrata qualitativamente in Figura~\ref{fig:oscinv} per due differenti ampiezze di $ V\ped{in} $, circa \SI{480}{\milli\volt} e \SI{10}{\volt} (ampiezze picco-picco).
Nel primo caso l'OpAmp si comporta in modo lineare mentre nel secondo caso si osserva clipping del segnale in uscita. \\

Variando l'ampiezza di $V\ped{in}$ si misura $V\ped{out}$ ed il relativo guadagno $A_{v}=V\ped{out}/V\ped{in}$, riportando i dati ottenuti in Tabella~\ref{tab:guadagno} e mostrandone un grafico in Figura~\ref{fig:lin}. \\

Per stimare il guadagno dell'amplificatore invertente abbiamo effettuato un \textit{fit} numerico con la funzione \texttt{curve\_fit}\footnote{Si è usata l'opzione \texttt{absolute\_sigma = True}.} del pacchetto \texttt{scipy} di Python sulle coppie $ (V\ped{in}, V\ped{out}) $ per cui $ V\ped{in} \leq \SI{1.5}{\volt} $. Essendo gli errori relativi su $ V\ped{in} $ e $ V\ped{out} $ confrontabili tra di loro, abbiamo effettuato un \textit{fit} iterato usando errori ``efficaci'' su $ V\ped{out} $. Usando come modello $ y = A x $, si è ottenuto
\[
  A\ped{best} = \sivar{1.b}{A} \qquad  \chi^2/\mathrm{dof} = \get{1.b}{chi2}
\]
Il valore ottenuto per il guadagno è compatibile con quanto atteso. Il $ \chi^{2} $ è decisamente inferiore a quanto atteso, discrepanza attribuibile a una sovrastima degli errori nelle misure (come l'errore di calibrazione dell'oscilloscopio del 3\%). \\

Idealmente ci aspettiamo che per $ V\ped{in} \geq V\ped{out}\ap{max}/A \sim 2V\ped{CC}/A \sim \SI{3}{\volt} $ (ampiezze picco-picco) l'amplificatore esca dalla zona lineare e l'output si saturi a $ 2V\ped{CC} \sim \SI{30}{\volt} $. Come si può osservare in Figura~\ref{fig:lin} l'amplificatore devia dal comportamento lineare già per $ V\ped{in} \simeq \SI{2}{\volt} $ e per $ V\ped{in} \geq \SI{3}{\volt} $ si satura ad un valore di circa $ \SI{27}{\volt} $, leggermente minore di quanto atteso\footnote{
  Il manuale dell'OpAmp riporta un \emph{voltage swing} (cioè la massima semi-ampiezza realmente erogabile in saturazione) a $ \SI{15}{\volt} $ di $ \sim\SI{13.5}{\volt} $, da cui una massima ampiezza picco-picco di $ \sim\SI{27}{\volt} $, in accordo con quanto osservato.
}.

\begin{figure}[h!]
  \centering
  \subfloat[regime lineare]{\includegraphics[scale=0.85]{data/1b-lin}}
  \subfloat[clipping/saturazione]{\includegraphics[scale=0.85]{data/1b-clip}}
  \caption{segnale in ingresso (\texttt{CH1}) e in uscita (\texttt{CH2}) dall'amplificatore invertente.}
  \label{fig:oscinv}
\end{figure}

\begin{table}[h!]
  \centering
  \get{1.b}{tab}
  \caption{$V\ped{out}$ in funzione di $V\ped{in}$ e relativo rapporto.}
  \label{tab:guadagno}
\end{table}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.85]{img/ampinvert.png}
  \caption{$ V\ped{out} $ in funzione di $ V\ped{in} $ (ampiezze picco-picco) per l'amplificatore invertente. In blu i dati su cui è stato fatto il \textit{fit} con sovrapposta la retta \textit{best fit}.}
  \label{fig:lin}
\end{figure}

\subsection{Misura dell'impedenza di ingresso}
Fissato $ V\ped{in} $, si misura l'impedenza di ingresso dell'amplificatore confrontando l'ampiezza della tensione di uscita in presenza ($ V_{2} $) e in assenza ($ V_{1} $) di una resistenza $R\ped{L}$ (confrontabile con il valore atteso per $\abs{Z\ped{in}}$) tra l'uscita del generatore e l'ingresso dell'amplificatore.

Usando una resistenza $R\ped{L} = \sivar{1.c}{RL}$, si è misurato
\[
  V\ped{in} = \sivar{1.c}{Vin} \qquad V_{1} = \sivar{1.c}{V1} \quad V_{2} = \sivar{1.c}{V2}
\]
da cui si ricava
\[
  Z\ped{in} = \frac{R\ped{L}}{\dfrac{V_{1}}{V_{2}} - 1} = \sivar{1.c}{Zin}
\]
compatibile con quanto atteso $ Z\ped{in} \simeq R_{1} $.

\section{Risposta in frequenza e \textit{slew rate}}
\subsection{Risposta in frequenza del circuito}\label{subsec:cusumano}
Si misura la risposta in frequenza del circuito, riportando i dati in Tabella~\ref{tab:bodeinv} e in un grafico di Bode in Figura~\ref{fig:bodeinv}.
Dal grafico si nota subito che l'amplificatore non attenua le frequenze ``basse'', ossia presenta frequenza di taglio inferiore nulla.
L'andamento è invece quello di un passa-basso, per cui possiamo stimare la frequenza di taglio superiore facendo un \textit{fit} con un modello lineare per valori delle frequenze maggiori di $ \SI{400}{\kilo\hertz} $.
Questo ha restituito come parametri di \textit{best fit}, per il modello ($ y = mx + q $):
\[ m = \sivar{2.a}{m} \qquad q = \sivar{2.a}{q} \]
Si nota che $ m $ è compatibile con il valore atteso di $ \SI{-20}{\deci\bel/decade} $.

In Figura~\ref{fig:bodeinv} sono riportati in rosso i dati usati per il fit, in verde la retta di \emph{best-fit} e la retta orizzontale corrispondente alla massima amplificazione ($ A \ped{max} = \sivar{2.a}{Amax} $). Dall'intersezione tra queste rette si ricava la frequenza di taglio superiore:
\[
  f\ped{H} = \frac{A \ped{max} - q}{m} = \sivar{2.a}{fH}.
\]

\begin{table}[h!]
  \centering
  \tab{2.a}{tab}
  \caption{guadagno dell'amplificatore invertente in funzione della frequenza.}
  \label{tab:bodeinv}
\end{table}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.85]{img/2a.png}
  \caption{plot di Bode in ampiezza per l'amplificatore invertente.}
  \label{fig:bodeinv}
\end{figure}

\subsection{Misura dello \textit{slew rate}}
Si misura direttamente lo \textit{slew rate} dell'OpAmp inviando in ingresso un'onda quadra di frequenza $ f = \sivar{2.b}{f} $ e di ampiezza $ \sim \SI{15}{\volt} $.

In Figura~\ref{fig:slew-rate-diretto} si riporta uno screenshot dell'oscilloscopio.
Si nota che il segnale in uscita si satura ad una ampiezza picco-picco di $ \sim\SI{30}{\volt} $ e che, nella transizione tra il valore ``alto'' e il valore ``basso'' dell'onda quadra in ingresso, l'uscita dell'OpAmp satura lo \emph{slew-rate}.
Si ottiene, misurando direttamente gli incrementi del segnale in tempo e in differenza di potenziale:
\[
  \mathrm{SR} = \frac{\Delta V}{\Delta t} = \frac{\sivar{2.b}{deltaV}}{\sivar{2.b}{deltaT}}
  = \SI{11.7 +- 0.4}{\volt/\micro\second}
\]
comparabile con il valore tipico indicato nel datasheet di $ \SI{13}{\volt/\micro\second} $.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.9]{data/2b.png}
  \caption{segnale in ingresso (\texttt{CH1}) e in uscita (\texttt{CH2}) dall'amplificatore invertente per la misura diretta dello slew rate.}
  \label{fig:slew-rate-diretto}
\end{figure}

\section{Amplificatore non invertente}
Si è montato il circuito dell'amplificatore non invertente con
\[
  R_{3} = \sivar{3.a}{R3}.
\]
Per il partitore si sono usate le stesse utilizzate in precedenza $ R_{1} \sim \SI{10}{\kilo\ohm} $ e $ R_{2} \sim \SI{1}{\kilo\ohm} $, mentre per disaccoppiare la tensione in ingresso si è utilizzato un condensatore da $ C_{1} = \sivar{3.a}{C1} $.

\subsection{Misura del prodotto banda-guadagno}
Per alcune posizioni del potenziometro si sono misurati il valore della resistenza del potenziometro $ R $, il guadagno del circuito $A\ped{max} = V\ped{out}\ap{max} / V_{+}$, e la frequenza di taglio superiore $f\ped{H}$, riportando i dati ottenuti in Tabella~\ref{tab:bandaguadagno} e indicando il valore del prodotto banda-guadagno $\mathrm{GBW} = A\ped{max} \cdot f\ped{H}$. \\

La resistenza del potenziometro è stata misurata con il multimetro digitale. Fissato un valore di $ V\ped{in} $ (e quindi di $ V\ped{+} $\footnote{A patto di rimanere al di sopra della frequenza di taglio del passa-alto nel sotto-circuito di input.}) abbiamo variato la frequenza cercando di massimizzare $ V\ped{out} $, che equivale a trovare la massima amplificazione (se $ V_{+} $ si mantiene costante come verificato). La frequenza di taglio superiore $ f\ped{H} $ è stata stimata in modo più rozzo determinando la frequenza per cui si avesse un'amplificazione di \SI{-3}{\deci\bel} rispetto a $ A\ped{max} $, ovvero la frequenza per cui $ A = A\ped{max} / \sqrt{2} $.

\begin{table}[h]
  \centering
  \get{3.a}{tab}
  \caption{misure relative al calcolo del rapporto banda-guadagno.}
  \label{tab:bandaguadagno}
\end{table}

Si verifica, entro l'errore sperimentale, la costanza del prodotto banda-guadagno al variare di $ A\ped{max} $ e $ f\ped{H} $. Tale valore è circa la metà di quanto riportato sul datasheet dell'OpAmp \texttt{TL081}, pari a $ \mathrm{GBW} = \SI{4}{\mega\hertz} $, che è tuttavia riportato come valore \emph{tipico}.

\section{Circuito integratore}
Si monta il circuito integratore con i seguenti valori  dei componenti indicati:
\[
  R_1 = \sivar{4.z}{R1} \qquad R_2 = \sivar{4.z}{R2} \qquad C = \sivar{4.z}{C}
\]

\subsection{Risposta in frequenza}

Si invia un'onda sinusoidale e si misura la risposta in frequenza dell'amplificazione e della fase riportandoli nella Tabella~\ref{tab:bodeinte} e in un diagramma di Bode in Figura~\ref{fig:bodeinte}. Tutte le misure sono state effettuate con l'oscilloscopio. \\

La misura di sfasamento è stata effettuata misurando la differenza di tempo tra l'intersezione della ``discesa'' di $ V\ped{out} $ con la linea di riferimento di terra e l'intersezione della ``discesa'' di $ V\ped{in} $ con la stessa linea. Essendo presente un offset nell'onda sinusoidale prodotta del generatore è stato necessario accoppiare l'oscilloscopio in \texttt{AC} su entrambi i canali per poter misurare in modo corretto lo sfasamento. \\

Si ricava una stima delle caratteristiche principali dell'andamento (guadagno a bassa frequenza $ A\ped{max} $, frequenza di taglio $ f\ped{H} $, e pendenza ad alta frequenza) e si confrontano con quanto atteso. Non si effettua la stima degli errori, trattandosi di misure qualitative.

Il guadagno massimo è stato ricavato come il massimo guadagno a bassa frequenza. La frequenza di taglio è stata stimata rozzamente determinando il punto di attenuazione di \SI{-3}{\deci\bel} rispetto al guadagno massimo. La pendenza ad alta frequenza è stata stimata come media della pendenza su 3 coppie di dati consecutivi ($ f \geq \SI{1}{\kilo\hertz} $)
\[
  A\ped{max} = \SI{\get{4.a}{Amax}}{\deci\bel} \qquad
  f\ped{H} = \SI{347}{\hertz} \qquad
  \dod{A_{v}[\si{\deci\bel}]}{\, \log_{10}{f}} = \SI{\get{4.a}{m}}{\deci\bel/decade}
\]
comparabili con i valori attesi
\[
  A\ped{max}\ap{exp} = 20\log_{10}{\frac{R_{2}}{R_{1}}} = \SI{\get{4.a}{Aexp}}{\deci\bel} \qquad
  f\ped{H}\ap{exp} = \frac{1}{2\pi R_{2} C} = \SI{\get{4.a}{fHexp}}{\kilo\hertz} \qquad
  \left(\dod{A_{v}[\si{\deci\bel}]}{\, \log_{10}{f}}\right)\ap{exp} = \SI{-20}{\deci\bel/decade}
\]
calcolati dalla funzione di trasferimento
\[
  A_{v} = -\frac{Z_{2}}{R_{1}} = - \frac{R_{2}}{R_{1}} \frac{1}{1 + j \dfrac{f}{f\ped{H}}}.
\]

\begin{table}[h!]
  \centering
  \get{4.a}{tab}
  \caption{guadagno e fase dell'integratore invertente in funzione della frequenza. $ V\ped{in} $ e $ V\ped{out} $ sono ampiezze picco-picco.}
  \label{tab:bodeinte}
\end{table}

\begin{figure}[h!]
  \centering
  \subfloat[diagramma di Bode]{\includegraphics[scale=0.51]{img/integratoreBode}}
  \subfloat[fase in funzione della frequenza]{\includegraphics[scale=0.51]{img/integratoreFase}}
  \caption{risposta in frequenza per il circuito integratore.}
  \label{fig:bodeinte}
\end{figure}

\subsection{Risposta ad un'onda quadra}
Si invia all'ingresso un'onda quadra di frequenza $ f = \sivar{4.b}{f} $ e ampiezza picco-picco $ V\ped{in} = \sivar{4.b}{Vin} $. Si ottiene un'onda triangolare di ampiezza $ V\ped{out} = \sivar{4.b}{Vout} $. Si riporta in Figura~\ref{fig:integratore} le forme d'onda acquisite all'oscilloscopio per l'ingresso e l'uscita. \\

Qualitativamente il comportamento coincide con quanto atteso: essendo la frequenza dell'onda quadra molto maggiore della frequenza di taglio, all'uscita troviamo un segnale proporzionale all'integrale dell'ingresso, ovvero un'onda triangolare. \\

Detta $ V_{0} = V\ped{in} $ l'ampiezza picco-picco del segnale in ingresso, la componente dominante nello sviluppo in serie di Fourier dell'onda quadra ha ampiezza $ \frac{4}{\pi} V_{0} $ e frequenza pari a quella della portante $ f_{1} = f $. Dalla funzione di trasferimento sviluppata per frequenze molto maggiori della frequenza di taglio, la portante dell'onda in uscita ha ampiezza $ \frac{4}{\pi} V_{0} \frac{1}{2\pi R_{1} C f_{1}} $. Pertanto ci aspettiamo che l'onda triangolare in uscita abbia ampiezza picco-picco scalata di un fattore $ \pi^{2}/8 $ dato dallo sviluppo in serie di Fourier di un'onda triangolare. Quindi
\[
  V\ped{out}\ap{exp} = \frac{\pi^{2}}{8} \frac{4}{\pi} V_{0} \frac{1}{2\pi R_{1} C f_{1}} = \frac{V\ped{in}}{4 \pi R_{1} C f} = \sivar{4.b}{Voutexp}
\]
compatibile con quanto misurato. \\

Abbassando la frequenza del segnale in ingresso, si osserva un aumento dell'ampiezza del segnale in uscita e un cambiamento della forma da onda triangolare a pinna di squalo, caratteristiche del processo di carica e scarica del condensatore.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.85]{data/4b.png}
  \caption{}
  \label{fig:integratore}
\end{figure}

\subsection{Commenti}
In assenza della resistenza $ R_{2} $ il circuito avrebbe idealmente una funzione di trasferimento della forma $ A(f) = - 1/(2\pi f R_{1} C) $, ovvero nel diagramma di Bode una retta a pendenza costante pari a \SI{-20}{\deci\bel/decade}. Pertanto per basse frequenze si avrebbe una divergenza del segnale in uscita che porterebbe l'OpAmp in regime non lineare di saturazione: a fissata $ V \ped{in} $, il diagramma di Bode reale avrebbe quindi un andamento qualitativamente simile a quello di un integratore in cui però non abbiamo controllo sulla frequenza di taglio e il segnale a basse frequenze sarebbe affetto da clipping.

L'inserimento della resistenza in parallelo al condensatore ha lo scopo di poter controllare la frequenza di taglio, ora fissata a $ 1/(2\pi R_{2} C) $, e di limitare il guadagno massimo a $ R_{2}/R_{1} $ (valori indipendenti dai parametri costruttivi dell'OpAmp). Chiaramente anche tale modello si basa su alcune approssimazioni: in particolare abbiamo trascurato la dipendenza del guadagno dell'OpAmp dalla frequenza (che si manifesta per frequenza molto alte) e abbiamo supposto che l'OpAmp lavori in regime lineare, ovvero che si abbia $ V\ped{in} R_{2} / R_{1} \leq 2 V\ped{CC} $.

Si osserva infine dal grafico dello sfasamento che questo riproduce il corretto andamento qualitativo per un passa-basso.

\section{Circuito derivatore}
Si monta il circuito derivatore utilizzando gli stessi componenti ($ C $, $ R_{1} $ e $ R_{2} $) dell'integratore.

\subsection{Risposta in frequenza}
Si invia un'onda sinusoidale e si misura la risposta in frequenza dell'amplificazione e della fase riportandoli
nella Tabella~\ref{tab:bodederi} e in un diagramma di Bode in Figura~\ref{fig:bodederi}.

\begin{table}[h!]
  \centering
  \get{5.a}{tab}
  \caption{guadagno e fase del derivatore invertente in funzione della frequenza. $ V\ped{in} $ e $ V\ped{out} $ sono ampiezze picco-picco.}
  \label{tab:bodederi}
\end{table}

\begin{figure}[h!]
  \centering
  \subfloat[diagramma di Bode]{\includegraphics[scale=0.51]{img/derivatoreBode}}
  \subfloat[fase in funzione della frequenza]{\includegraphics[scale=0.51]{img/derivatoreFase}}
  \caption{risposta in frequenza per il circuito derivatore.}
  \label{fig:bodederi}
\end{figure}


Si ricava una stima delle caratteristiche principali dell'andamento (guadagno ad alta frequenza, frequenza di taglio, e pendenza a bassa frequenza). Non si effettua la stima degli errori, trattandosi di misure qualitative.

Il guadagno massimo è stato ricavato come il guadagno di centro banda. La frequenza di taglio inferiore è stata stimata rozzamente determinando il punto di attenuazione di \SI{-3}{\deci\bel} rispetto al guadagno massimo. La pendenza a bassa frequenza è stata stimata come media della pendenza su 2 coppie di dati consecutivi ($ f \leq \SI{5}{\kilo\hertz} $).

\[
  A\ped{max} = \get{5.a}{Amax} \qquad
  f\ped{L} = \SI{2.95}{\kilo\hertz} \qquad
  \dod{A_{v} [\si{\deci\bel}]}{\, \log_{10}f} = \get{5.a}{m}
\]

\[
  A \ped{max}\ap{exp} = 20\log_{10}\del{\frac{R_{2}}{R_{1}}} = \get{5.a}{Aexp} \qquad
  f\ped{L}\ap{exp} = \SI{3.1}{\kilo\hertz} \qquad
  \del{\dod{A_{v} [\si{\deci\bel}]}{\, \log_{10}f}}\ap{exp} = \SI{20}{\deci\bel/decade}
\]

Il guadagno massimo e la frequenza di taglio sono comparabili con i valori attesi. La pendenza a bassa frequenza è del giusto ordine di grandezza, ma si discosta significativamente da quella attesa, probabilmente perché non si sono esplorati intervalli di frequenze abbastanza bassi.

Per i valori attesi si è usata la funzione di trasferimento
\[
  A_{v} = -\frac{R_{2}}{Z_{1}} = - \frac{R_{2}}{R_{1}} \frac{1}{1 - j \dfrac{f_{L}}{f}}.
\]


\subsection{Risposta ad un'onda triangolare}
Si invia all'ingresso un'onda triangolare di frequenza $ f = \sivar{5.b}{f} $ e ampiezza $ V\ped{in} = \sivar{5.b}{Vin} $. Si misura in uscita (con i cursori) un'onda quadra di ampiezza picco-picco $ V \ped{out} = \sivar{5.b}{Vout} $. Si riportano in Figura~\ref{fig:derivatore} le forme d'onda acquisite all'oscilloscopio per l'ingresso e l'uscita.

Qualitativamente il comportamento coincide con quanto atteso: essendo la frequenza dell'onda triangolare molto minore della frequenza di taglio, all'uscita troviamo un segnale proporzionale alla derivata dell'ingresso, ovvero un'onda quadra.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.9]{data/5b.png}
  \caption{ingresso (\texttt{CH1}) ed uscita (\texttt{CH2}) del circuito derivatore per un'onda triangolare.}
  \label{fig:derivatore}
\end{figure}

Con ragionamenti analoghi a quelli del punto precente si ottiene il valore atteso per l'uscita
\[
  V \ped{out} \ap{exp} = \frac{8}{\pi^{2}} V\ped{in} 2 \pi R_{2} C f \frac{\pi}{4} = 4 V \ped{in} R_{2} C f = \sivar{5.b}{Voutexp}
\]
che è confrontabile, seppur non compatibile, con quanto misurato.

Alzando la frequenza del segnale in ingresso, si osserva un aumento dell'ampiezza del segnale in uscita e un cambiamento della forma da onda quadra a pinna di squalo, caratteristiche del processo di carica e scarica del condensatore.

Inoltre, per frequenze maggiori di $ \sim \SI{100}{\kilo\hertz} $, l'ampiezza torna nuovamente a diminuire, a causa della dipendenza del guadagno dell'OpAmp dalla frequenza, come si è osservato nella sottosezione~\ref{subsec:cusumano}.

Zoomando e aumentando la frequenza, si osserva che la pendenza assunta dall'onda quadra nella transizione da ``basso'' a ``alto'' è limitata superiormente (e inferiormente) dallo slew rate dell'OpAmp.

\subsection{Commenti}
In assenza della resistenza $ R_{1} $ il circuito avrebbe idealmente una funzione di trasferimento della forma $ A(f) = - 2\pi f R_{2} C $, ovvero nel diagramma di Bode una retta a pendenza costante pari a \SI{20}{\deci\bel/decade}. Pertanto per alte frequenze si avrebbe una divergenza del segnale in uscita che porterebbe l'OpAmp in regime non lineare di  saturazione: a fissato $ V \ped{in} $, il diagramma di Bode reale avrebbe quindi un andamento qualitativamente simile a quello di un derivatore in cui però non abbiamo controllo sulla frequenza di taglio e il segnale ad alte frequenze sarebbe affetto da clipping.

L'inserimento della resistenza in serie al condensatore ha lo scopo di poter controllare la frequenza di taglio, ora fissata a $ 1/(2\pi R_{1} C) $, e di limitare il guadagno massimo a $ R_{2}/R_{1} $ (valori indipendenti dai parametri costruttivi dell'OpAmp). Chiaramente anche tale modello si basa su alcune approssimazioni: in particolare abbiamo trascurato la dipendenza del guadagno dell'OpAmp dalla frequenza (che si è osservata, come già detto, per frequenze molto alte) e abbiamo supposto che l'OpAmp lavori in regime lineare, ovvero che si abbia $ V\ped{in} R_{2} / R_{1} \leq 2 V\ped{CC} $.

Si osserva infine dal grafico dello sfasamento che questo riproduce il corretto andamento qualitativo per un passa-banda.

\dichiarazione

\end{document}
