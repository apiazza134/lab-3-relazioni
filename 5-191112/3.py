#!/usr/bin/env python3

from uncertainties import unumpy as unp
from lab import (
    Data, Table, loadtxtSorted,
    measureMeanUncertaintyArray
)

R, dR, Vp, divVp, Vout, divVout, fH, dfH = loadtxtSorted("./data/3a.txt")

R = unp.uarray(R, dR)
Vp = measureMeanUncertaintyArray(Vp, divVp)
Vout = measureMeanUncertaintyArray(Vout, divVout)
fH = unp.uarray(fH, dfH)

A = Vout/Vp
GBW = A*fH

print(R)

tab = Table()
tab.pushColumn('R', 'ohm', R)
tab.pushColumn('V_{+}', 'volt', Vp)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A\\ped{max}', None, A)
tab.pushColumn('f\\ped{H}', 'hertz', fH)
tab.pushColumn('\\mathrm{GWB}', 'hertz', GBW)

d = Data()
d.pushTable(tab, '3.a', 'tab')
d.save()
