#!/usr/bin/env python3

from uncertainties import unumpy as unp
import matplotlib.pyplot as plt
from numpy import pi
from lab import (
    Data, Table, loadtxtSorted,
    measureMeanUncertaintyArray
)

plt.style.use('../style.yml')

d = Data()

f, df, Vin, divVin, Vout, divVout, tau, dtau = loadtxtSorted("./data/4a.txt")

f = unp.uarray(f, df)
Vin = measureMeanUncertaintyArray(Vin, divVin)
Vout = measureMeanUncertaintyArray(Vout, divVout)
tau = - unp.uarray(tau, dtau)

A = Vout/Vin
phi = 2*pi*f*tau

# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A', None, A)
tab.pushColumn('\\tau', 'second', tau)
tab.pushColumn('\\phi', 'radian', phi)

d.pushTable(tab, '4.a', 'tab')

# Grafici
A = 20*unp.log10(Vout/Vin)

plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(A),
    xerr=unp.std_devs(f), yerr=unp.std_devs(A),
    linestyle='', elinewidth=1, color='black'
)
plt.xscale('log')
plt.xlabel('$ f [\\mathrm{Hz}]$')
plt.ylabel('$ A [\\mathrm{dB}]$')
plt.savefig('./img/integratoreBode.png')

plt.close()

plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(phi),
    xerr=unp.std_devs(f), yerr=unp.std_devs(phi),
    linestyle='', elinewidth=1, color='black'
)
plt.xscale('log')
plt.xlabel('$ f [\\mathrm{Hz}]$')
plt.ylabel('$ \\phi [\\mathrm{rad}]$')
plt.savefig('./img/integratoreFase.png')


# Pendenza ad alta frequenza
Alin = A[f > 1e3]
flin = f[f > 1e3]

m = []
for i in range(len(flin)-1):
    Dx = unp.log10(flin[i+1]) - unp.log10(flin[i])
    Dy = Alin[i+1] - Alin[i]
    m += [Dy/Dx]

Amax = max(unp.nominal_values(A))
m = (sum(m)/len(m)).nominal_value

Aexp = (20*unp.log10(d.get('4.z', 'R2') / d.get('4.z', 'R1'))).nominal_value
fHexp = 1/(2*pi*d.get('4.z', 'R2')*d.get('4.z', 'C')).nominal_value

Voutexp = d.get('4.b', 'Vin')/(4 * d.get('4.z', 'R1') * d.get('4.z', 'C') * d.get('4.b', 'f'))

d.recursivePush(f'{Amax:.1f}', '4.a', 'Amax')
d.recursivePush(f'{m:.1f}', '4.a', 'm')
d.recursivePush(f'{Aexp:.0f}', '4.a', 'Aexp')
d.recursivePush(f'{fHexp:.0f}', '4.a', 'fHexp')
d.push(Voutexp, 'volt', '4.b', 'Voutexp')

d.save()
