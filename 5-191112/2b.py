#!/usr/bin/env python3

from lab import Data

# slew rate
d = Data()

slew = d.get('2.b', 'deltaV')/d.get('2.b', 'deltaT')
d.push(slew, 'volt/\\second', '2.b', 'SR')
d.save()
