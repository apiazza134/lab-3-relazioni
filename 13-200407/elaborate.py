#!/usr/bin/env python3

from lab import run_chain

run_chain([
    'preamp.py', 'bandpass-postamp.py', 'boltz.py'
])
