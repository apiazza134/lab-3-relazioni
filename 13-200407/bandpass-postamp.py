#!/usr/bin/env python3

from lab.spice import parse_spice
from lab.data import Data
import matplotlib.pyplot as plt
import numpy as np

plt.style.use("../style.yml")

f, A, phi = parse_spice('data/bandpass-postamp-universal-zoom.txt')

# Picco e larghezza a -3dB
peak = np.argmax(A)
idx1 = np.argmin(np.abs(A[:peak] - A[peak] + 3))
idx2 = np.argmin(np.abs(A[peak:] - A[peak] + 3)) + peak

d = Data()
d.push(f[peak], 'hertz', 'bandpass-postamp', 'f0')
d.push(A[peak], 'decibel', 'bandpass-postamp', 'Atot')
d.push(np.abs(f[idx1] - f[idx2]), 'hertz', 'bandpass-postamp', 'Df')

f, A, phi = parse_spice('data/bandpass-postamp-universal.txt')

phi *= np.pi/180

# Fix orriblie per le fasi
for i in range(len(phi)-1, 0, -1):
    if phi[i] > phi[i-1]:
        phi[i:] = phi[i:] - 2*np.pi
        break

# plt.plot([f[idx1], f[idx2]], [A[idx1], A[idx2]], marker='x', color='red')
plt.plot(f, A)
plt.xlabel('$f$ [Hz]')
plt.ylabel('$A$ [dB]')
plt.xscale('log')
plt.savefig('img/bandpass-postamp-bode.png')
# plt.show()
plt.close()

plt.plot(f, phi)
plt.xlabel('$f$ [Hz]')
plt.ylabel('$\\phi$ [rad]')
plt.xscale('log')
plt.savefig('img/bandpass-postamp-fase.png')


# valori previsti

R1 = d.get('bandpass-postamp', 'R1').nominal_value
R2 = d.get('bandpass-postamp', 'R2').nominal_value
R3 = d.get('bandpass-postamp', 'R3').nominal_value
R4 = d.get('bandpass-postamp', 'R4').nominal_value
R5 = d.get('bandpass-postamp', 'R5').nominal_value
C = d.get('bandpass-postamp', 'C').nominal_value

f0exp = 1/(2*np.pi*C)*np.sqrt((R1 + R2)/(R1*R2*R3))
Dfexp = 1/(np.pi*R3*C)
A0exp = 20*np.log10(R3/(2*R1))
A1exp = 20*np.log10(1 + R5/R4)
Atotexp = A0exp + A1exp

d.push(f0exp, 'hertz', 'bandpass-postamp', 'f0exp')
d.push(Dfexp, 'hertz', 'bandpass-postamp', 'Dfexp')
d.push(A0exp, 'decibel', 'bandpass-postamp', 'A0exp')
d.push(A1exp, 'decibel', 'bandpass-postamp', 'A1exp')
d.push(Atotexp, 'decibel', 'bandpass-postamp', 'Atotexp')

d.save()
