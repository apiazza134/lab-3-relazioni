#!/usr/bin/env python3

import numpy as np
from uncertainties import unumpy as unp, correlated_values
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from lab import loadtxtSorted, Data, Table


def modello(x, A, B, C):
    return A*np.sqrt(1 + x/B + (x**2)/(C**2))


plt.style.use("../style.yml")
d = Data()

inits = [
    [60e-3, 2e3, 49e3],
    [60e-3, 2e3, 49e3],
    [100e-3, 1e3, 49e3],
]

_A0 = []
_Df = []
opts = []

datasets = [1, 3]

for dataset in datasets:
    # Carico i dati
    R, dR, V, dV = loadtxtSorted(f'./data/boltz{dataset}.txt')

    R = unp.uarray(R, dR)
    V = unp.uarray(V, dV)

    # Tabella
    tab = Table()
    tab.pushColumn('R', 'ohm', R)
    tab.pushColumn('V\\ped{RMS}', 'volt', V)
    d.pushTable(tab, 'boltz', f'set{dataset}', 'tab')

    # Computo cose
    A0 = d.get('boltz', f'set{dataset}', 'A0')
    Df = d.get('boltz', f'set{dataset}', 'Df')

    d.push(20*unp.log10(A0), 'decibel', 'boltz', f'set{dataset}', 'A0dB')
    _A0.append(A0)
    _Df.append(Df)

    # Fit
    init = inits[dataset-1]

    opt, cov = curve_fit(
        modello, unp.nominal_values(R), unp.nominal_values(V),
        p0=init, sigma=unp.std_devs(V)
    )
    print(f'Best fit for dataset {dataset}: {opt}')

    try:
        opt = correlated_values(opt, cov)
    except:
        print('vaffanculo')

    opts.append(opt)

    kT = opt[0]**2/(4*A0**2*Df*opt[1])
    print(f'kT/25meV = {kT/(1.602176634e-19/40)}')

    plt.errorbar(
        unp.nominal_values(R), unp.nominal_values(V),
        xerr=unp.std_devs(R), yerr=unp.std_devs(V),
        linestyle='', color='black', elinewidth=0.8
    )

    # Plot
    x = np.linspace(R[0].nominal_value, R[-1].nominal_value, 1000)
    # plt.plot(x, modello(x, *init), color='red')
    plt.plot(x, modello(x, *unp.nominal_values(opt)), color='gray')

    plt.xlabel('$ R\\ [\\Omega] $')
    plt.ylabel('$ V_\\mathrm{RMS}\\ [\\mathrm{V}] $')

    plt.savefig(f'img/boltz{dataset}.png')
    # plt.show()
    plt.close()


# Ampiezza dalla simulazione
A0 = d.get('preamp', 'A0') + d.get('bandpass-postamp', 'Atot')
d.push(A0, 'decibel', 'boltz', 'simulazione', 'A0')

# Parametri di best fit
V0ninit, RTinit, Rninit = np.array(inits).transpose()
V0n, RT, Rn = np.array(opts).transpose()

tab = Table()
tab.pushColumn('V\\ped{0,n}', 'volt', V0n)
tab.pushColumn('R\\ped{T}', 'ohm', RT)
tab.pushColumn('R\\ped{n}', 'ohm', Rn)

d.pushTable(tab, 'boltz', 'bestfit')

# kT
A0 = np.array(_A0)
Df = np.array(_Df)
kT = V0n**2/(2*(A0**2)*np.pi*Df*RT)
# kT = V0n**2/(4*(A0**2)*Df*RT)

tab = Table()
tab.pushColumn('V\\ped{0,n}', 'volt', V0n)
tab.pushColumn('A_{0}', None, A0)
tab.pushColumn('\\Delta f', 'hertz', Df)
tab.pushColumn('k\\ped{B}T \\ [\\SI{25}{\\milli\\electronvolt}]',
               None, kT/(1.602176634e-19/40))

d.pushTable(tab, 'boltz', 'kT')

# k
T = d.get('boltz', 'T')
k = kT / T

for i in range(len(datasets)):
    d.push(k[i], 'joule/\\kelvin', 'boltz', f'set{datasets[i]}', 'k', offset=1)

d.save()
