\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\preambolo
\newcommand{\vartab}[3]{\directlua{texprintStripNL(data['#1']['#2']['#3'])}}
\newcommand{\varsivar}[3]{\directlua{tex.print(data['#1']['#2']['#3'])}}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\begin{document}
\maketitle

\renewcommand{\abstractname}{Nota}
\begin{abstract}
    Questa esperienza è stata svolta a casa, collaborando in maniera telematica, tramite simulazioni numeriche e utilizzo di dati degli anni precedenti, a causa della chiusura dei laboratori in seguito sospensione dell'attività didattica.
\end{abstract}

\section{Funzionamento del circuito}\label{sec:noooise}
L'obiettivo dell'esperienza è di stimare il valore della costante di Boltzmann $ k\ped{B} $ a partire dalla misura della differenza di potenziale quadratica media ai capi di una resistenza dovuta al rumore Johnson-Nyquist e della temperatura ambientale.

Il rumore Johnson-Nyquist è un \emph{rumore bianco}, cioè la sua densità spettrale non dipende dalla frequenza. In particolare questa è legata ai parametri fisici tramite
\[ S_{V \ped{n,r}}(f) = 4 k\ped{B} T R \]
dove $ T $ è la temperatura del resistore e $ R $ è la sua resistenza.

Il segnale dovuto al rumore termico, tuttavia, è tipicamente molto piccolo rispetto alla risoluzione del multimetro e deve pertanto essere fortemente amplificato. Se l'amplificatore utilizzato ha un guadagno in frequenza $ G(f) $ abbiamo che la tensione quadratica media in uscita dall'amplificatore è
\[
    V \ped{RMS} = \int_{0}^{+\infty} S_{V \ped{n,r}}(f) \abs{G(f)}^{2}\dif{f}
\]
In virtù del fatto che la densità spettrale è costante abbiamo
\begin{equation}\label{eq:noooiseCusumanoResistenza}
    V \ped{RMS} = \abs{G(f_{0})}^{2} S_{V \ped{n.r}} \int_{0}^{+\infty}\abs{\frac{G(f)}{G(f_{0})}}^{2} \dif{f} = A B_{n} \cdot 4 k\ped{B} T R
\end{equation}
dove $ A $ è l'amplificazione al picco $ f_{0} $ e $ B_{n} $ è la \emph{banda equivalente}.
Nota la risposta in frequenza dell'amplificatore, la resistenza $ R $ e la temperatura ambientale $ T $, possiamo stimare $ k\ped{B} $ misurando il segnale quadratico medio in uscita dall'amplificatore.

Tuttavia, gli amplificatori reali introducono a loro volta un rumore che non è trascurabile rispetto a quello termico che vogliamo quantificare.
Possiamo costruire un modello efficace di un amplificatore rumoroso considerando un amplificatore ideale il cui ingresso sia collegato in serie a un generatore di tensione $ V\ped{n,a} $ (che modellizza il rumore in tensione), e in parallelo a un generatore di corrente $ i\ped{n,a} $ (che modellizza il rumore in corrente).
Tenendo conto anche della resistenza di test $ R $ come sorgente di rumore in tensione $ V \ped{n,r} $ in serie a $ V \ped{n,a} $ abbiamo che la tensione quadratica media \emph{noise-equivalent} in ingresso nell'amplificatore è
\[
    \overline{V \ped{ne}^{2}} = \overline{(V \ped{n,a} + R i\ped{n,a} + V \ped{n,r})^{2}}
    = \overline{V \ped{n,a}^{2}} + R^{2}\overline{i \ped{n,a}^{2}} + \overline{V \ped{n,r}^{2}}
    + 2 R \overline{V \ped{n,a} i \ped{n,a}} + 2 \overline{V \ped{n,a} V \ped{n, res}} + 2R\overline{i \ped{n,a} V \ped{n,r}}.
\]
Ora, nell'ipotesi che le sorgenti di rumore siano scorrelate, abbiamo
\[
    \overline{V \ped{ne}^{2}} = \overline{V \ped{n,a}^{2}} + R^{2}\overline{i \ped{n,a}^{2}} + \overline{V \ped{n,r}^{2}}
    + 2 R \overline{V \ped{n,a}}\cdot\overline{i \ped{n,a}} + 2 \overline{V \ped{n,a}}\cdot\overline{V \ped{n, res}} + 2R\overline{i \ped{n,a}}\cdot\overline{V \ped{n,r}}.
\]
Usando infine che hanno media nulla
\[ \overline{V \ped{ne}^{2}} = \overline{V \ped{n,a}^{2}} + R^{2}\overline{i \ped{n,a}^{2}} + \overline{V \ped{n,r}^{2}}. \]
Ora la tensione quadratica media \emph{noise equivalent} in uscita $ V \ped{RMS}^{2} $ è $ A^{2} $ volte quella in ingresso (dove $ A $ è l'amplificazione complessiva a centro banda), per cui, usando la~\eqref{eq:noooiseCusumanoResistenza} e manipolando l'espressione precedente si ottiene infine
\[ V \ped{RMS} = V_{0,n} \sqrt{1 + \frac{R}{R_{T}} + \frac{R^{2}}{R^{2}\ped{n}}} \]
dove
\[
    V_{0, n} = A V \ped{n,a} \qquad
    R_{T} = \frac{\overline{V\ped{n,a}^{2}}}{4 k\ped{B} T B_{n}} \qquad
    R \ped{n} = \frac{\overline{V \ped{n,a}^{2}}}{\overline{i \ped{n,a}^{2}}}.
\]

Nella pratica si usa la serie di un pre-amplificatore, un passa-banda e un post-amplificatore. Il passa-banda utilizzato, realizzato con un OpAmp, ha banda equivalente $ B_{n} = \frac{\pi}{2} \Delta f $, dove $ \Delta f $ è la larghezza di $ \abs{G(f)} $ a $ \SI{-3}{\decibel} $ dal picco.
L'alimentazione agli OpAmp è inoltre fornita tramite un \emph{power filter} per ridurre il rumore dovuto all'alimentazione in continua.
Il segnale in uscita viene infine convertito nel suo valore \texttt{RMS} tramite il \emph{true RMS-to-DC converter} \texttt{AD736} e quindi misurato con il multimetro.

\section{Pre-amplificatore}
Si è realizzato con \texttt{LTspice} il circuito pre-amplificatore, la cui schematica utilizzata è riportata in Figura~\ref{fig:preamp-schema}. Il circuito è costituito dalla cascata di un \emph{precision instrumental amplifier} e di un amplificatore invertente. L'\texttt{INA114}, in base a quanto riportato nel datasheet, e l'invertente hanno un'amplificazione
\[
    A_{\texttt{INA114}}  =  1 + \frac{\SI{50}{\kilo\ohm}}{R_{1} \, [\si{\ohm}]}
    \qquad
    A\ped{invertente} = - \frac{R_{3}}{R_{2}}
\]
Con i valori scelti delle resistenze, il modulo delle amplificazioni attese dei due ``blocchi'' del pre-amplificatore sono, in decibel,
\begin{equation}\label{eq:preamp-amp-prev}
    A_{\texttt{INA114}} = \sivar{preamp}{Aina} \qquad
    A\ped{invertente} = \sivar{preamp}{Ainvert} \qquad
    A\ped{preamp} = \sivar{preamp}{A}
\end{equation}
\begin{figure}[h!]
    \centering
    \includegraphics[scale=1.2]{data/preamp.png}
    \caption{\label{fig:preamp-schema}schema del pre-amplificatore come realizzato tramite \texttt{LTspice}.}
\end{figure}

\subsection{Risposta nel dominio delle frequenze}
Si è eseguita una simulazione della risposta in frequenza tramite la direttiva \texttt{.ac dec 100 0.1 10meg} con la quale si ottiene un plot di Bode e dello sfasamento tra input e output nel range di frequenza $ \SI{0.1}{\hertz} - \SI{10}{\mega\hertz} $. I grafici prodotti della simulazione sono riportati in Figura~\ref{fig:preamp}.

Dalla simulazione si ricava che l'amplificazione a bassa frequenza è $ \sivar{preamp}{A0} $ in accordo con la previsione~\eqref{eq:preamp-amp-prev}.
Dal grafico possiamo inoltre notare che la frequenza di taglio dell'amplificatore è $ \sim\SI{15}{\kilo\hertz} $, in accordo con quanto si può dedurre dal plot di Bode riportato nel datasheet del \texttt{INA114} per un \emph{gain} di $ \sim 51 $.
Oltre i $ \sim\SI{15}{\kilo\hertz} $ si osserva la tipica dipendenza dalla frequenza dovuta al comportamento non ideale dell'OpAmp.

\begin{figure}[h!]
    \centering
    \def\scale{0.5}
    \subfloat[\label{fig:cusumano}Plot di Bode]{
        \includegraphics[scale=\scale]{img/preamp-bode}
    }
    \subfloat[Sfasamento]{
        \includegraphics[scale=\scale]{img/preamp-fase}
    }
    \caption{\label{fig:preamp}risposta in frequenza del pre-amplificatore.}
\end{figure}

\subsection{Risposta nel dominio dei tempi}
Si è eseguita una simulazione della risposta nel dominio dei tempi tramite la direttiva \texttt{.tran 2m}, per ottenere una simulazione della durata di $ \SI{2}{\milli\second} $.
Si è preso come segnale in ingresso $ V \ped{in} $ una sinusoide di ampiezza $ \SI{800}{\micro\volt} $ e frequenza $ \SI{1}{\kilo\hertz} $.
Si sono esportati i dati per $ V \ped{in} $ e per la tensione in uscita $ V \ped{out} $ dal pre-amplificatore in funzione del tempo. I relativi grafici sono riportati in Figura~\ref{fig:preamp-transient}. Si osserva che l'amplificatore è correttamente invertente; vi è inoltre un notevole offset del segnale in uscita, probabilmente dovuto alla elevata amplificazione e a errori numerici nella simulazione.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.55]{img/preamp-transient.png}
    \caption{\label{fig:preamp-transient}risposta del pre-amplificatore nel dominio del tempo.}
\end{figure}

\section{Passa banda e post-amplificatore}
Si è realizzato con \texttt{LTspice} il circuito costituito dalla cascata di un passa-banda (realizzato con un OpAmp), un amplificatore non invertente e infine un amplificatore a \emph{gain} unitario usato per disaccoppiare l'uscita dal circuito precedente.

La frequenza di picco, la larghezza di banda (definita come la differenza della frequenze a $ \SI{-3}{\decibel} $ rispetto al massimo) e l'amplificazione a centro banda del passa-banda sono date da
\[
    f_{0} = \frac{1}{2\pi C}  \sqrt{ \frac{R_{1} + R_{2}}{R_{1}R_{2}R_{3}}} \qquad
    \Delta f = \frac{1}{2 \pi} \frac{2}{R_{3} C} \qquad
    A\ped{bandpass} = \frac{R_{3}}{2 R_{1}}
\]
mentre l'amplificatore non invertente ha amplificazione
\[
    A\ped{postamp} = 1 + \frac{R_{5}}{R_{4}}.
\]
Con i valori scelti delle resistenze e delle capacità ($ C_1 = C_2 = C $) si ottengono i seguenti valori attesi
\[
    f_{0} = \sivar{bandpass-postamp}{f0exp} \qquad
    \Delta f = \sivar{bandpass-postamp}{Dfexp} \qquad
    A\ped{bandpass} = \sivar{bandpass-postamp}{A0exp} \qquad
    A\ped{postamp} = \sivar{bandpass-postamp}{A1exp} \qquad
    A_{0} = \sivar{bandpass-postamp}{Atotexp}
\]
dove $ A_{0} $ è l'amplificazione massima totale.
\begin{figure}[h!]
    \centering
    \includegraphics[scale=1.5]{data/bandpass-postamp.png}
    \caption{\label{fig:bandpass-postamp-schema}schema del passa banda e del post-amplificatore come realizzati tramite \texttt{LTspice}.}
\end{figure}

Si è eseguita una simulazione della risposta in frequenza tramite la direttiva \texttt{.ac dec 1000 50 500k} con la quale si ottiene un plot di Bode e dello sfasamento tra input e output nel range di frequenza $ \SI{50}{\hertz} - \SI{500}{\kilo\hertz} $. I grafici prodotti della simulazione sono riportati in Figura~\ref{fig:bandpass-postamp}.

Nella simulazione si osserva il tipico andamento di un passa-banda con un frequenza di picco, larghezza di banda e amplificazione massima pari a
\[
    f_{0} = \sivar{bandpass-postamp}{f0} \qquad
    \Delta f = \sivar{bandpass-postamp}{Df} \qquad
    A_{0} = \sivar{bandpass-postamp}{Atot}
\]
La frequenza di picco $ f_{0} $, la larghezza a \SI{-3}{\decibel} e l'ampiezza a centro banda sono confrontabili con quelli previsti dal modello. Per queste simulazioni si è deciso\footnote{Sentita anche la professoressa Chiara Roda.} di adoperare l'amplificatore \texttt{LT1002A}, invece dell'\texttt{AD708} fornito, in quanto il primo riproduce più fedelmente le caratteristiche di quello usato in laboratorio e riportate sul datasheet.

\begin{figure}[h!]
    \centering
    \def\scale{0.515}
    \subfloat[Bode]{
        \includegraphics[scale=\scale]{img/bandpass-postamp-bode}
    }
    \subfloat[Sfasamento]{
        \includegraphics[scale=\scale]{img/bandpass-postamp-fase}
    }
    \caption{\label{fig:bandpass-postamp}risposta in frequenza del passa-banda e del post-amplificatore in sequenza.}
\end{figure}


\section{Misura di $ k\ped{B} $}
In base a quanto detto nella Sezione~\ref{sec:noooise} ci aspettiamo una dipendenza di $ V\ped{RMS} $ dalla resistenza $ R $ della forma
\begin{equation}\label{eq:modello}
    V \ped{RMS}(R) = V_{0,n} \sqrt{1 + \frac{R}{R_{T}} + \frac{R^{2}}{R^{2}_{n}}}.
\end{equation}
Si è deciso di utilizzate il ``primo'' e il ``terzo'' set di dati forniti in quanto i dati del ``secondo'' set si discostano notevolmente dall'andamento qualitativo atteso. I dati utilizzati sono riportati nelle seguenti tabelle
\begin{center}
    \vartab{boltz}{set1}{tab}\hspace{1cm}
    \vartab{boltz}{set3}{tab}
\end{center}

Abbiamo quindi eseguito 2 \emph{fit} sui questi set di dati con la funzione \texttt{curve\_fit} della libreria \texttt{scipy} di Python usando il modello a 3 parametri liberi~\eqref{eq:modello}.
Per ciascun set di dati riportiamo i parametri di \emph{best-fit} nella seguente tabella
\begin{center}
    \tab{boltz}{bestfit}
\end{center}
I dati sperimentali forniti con le relative curve di \emph{best-fit} sono riportati in Figura~\ref{fig:bestfit}. \\

Dato che
\[  R_{T} = \frac{\overline{V\ped{0,n}^{2}}}{4 k\ped{B} T A_{0}^{2} B_{n}}, \]
essendo note $ V\ped{0,n} $, l'amplificazione a centro banda $ A_{0} $ e la banda equivalente dell'amplificatore $ {B_{n} = \pi \Delta f / 2} $, possiamo ricavare il valore di $ k\ped{B}T $ per ciascuno dei set di dati forniti:
\begin{equation}\label{eq:formula-kT}
    k\ped{B} T = \frac{\overline{V\ped{0,n}^{2}}}{2\pi \Delta f \, R_{T} A_{0}^{2}}.
\end{equation}
Riportiamo il valori così ottenuti nella seguente tabella, assieme ai valori forniti per $ A_{0} $ e $ \Delta f $ (larghezza a \SI{-3}{\decibel}) per i due set di dati. Il valore di $ k\ped{B} T $ è riportato in unità di $ 1/40 \, \si{\electronvolt} $, per un confronto più agevole con il valore ``convenzionale'' di $ k\ped{B} T $ a temperatura ambiente.
\begin{center}
    \tab{boltz}{kT}
\end{center}
Assumendo una temperatura ``ragionevole'' di $ T = \sivar{boltz}{T} $ possiamo ottenere le seguenti stime della costante di Boltzmann
\[
    k\ped{B}^{(1)} = \varsivar{boltz}{set1}{k} \qquad
    k\ped{B}^{(3)} = \varsivar{boltz}{set3}{k}
\]

Il valore atteso della costante di Boltzmann è $ \SI{1.380649e-23}{\joule/\kelvin} $. Osserviamo quindi che $ k\ped{B}^{(3)} $ è compatibile entro due deviazioni standard con il valore atteso, mentre $ k\ped{B}^{(1)} $ riproduce solamente il corretto ordine di grandezza. Non essendo stati presenti alla presa delle misure possiamo solo fare alcune ipotesi su tali discrepanze:
\begin{itemize}
    \item il risultato ricavato sul rumore Johnson-Nyquist assume che il sistema sia all'equilibrio termodinamico e pertanto bisognerebbe assicurasi che il circuito con il quale sono state effettuate le misure fosse termalizzato (il che vuol dire anche stimare il tempo con cui il sistema raggiunge l'equilibrio);
    \item la stima di $ k\ped{B} $ è stata fatta assumendo una temperatura ambiente ragionevole, tuttavia, come si è detto sopra, la resistenza non necessariamente è all'equilibrio termico con l'ambiente;
    \item il modello effettivo di amplificatore rumoroso come amplificatore ideale in cascata con un generatore di tensione e di corrente rumorosi potrebbe non essere sufficientemente accurato.

    Ad esempio il rumore dell'amplificatore include il rumore Johnson delle resistenze contenute nello stesso: se si volesse tenere conto di tale contribuito, le sorgenti di rumore $ V\ped{n,a} $ e $ i\ped{n,a} $ effettive avrebbero una dipendenza da $ k\ped{B} T $ e quindi nella relazione~\eqref{eq:formula-kT} la grandezza $ \overline{V\ped{0,n}^{2}} $ avrebbe essa stessa una dipendenza da $ k\ped{B}T $;
    \item il rumore Johnson non è sicuramente l'unica sorgente di rumore che interessa il circuito. Dato che l'ordine di grandezza di $ k\ped{B} $ ottenuto è in accordo con quello previsto, possiamo supporre a posteriori che l'ordine di grandezza di altri eventuali rumori sia confrontabile ovvero inferiore a quello del rumore termico.
\end{itemize}

\begin{figure}[h!]
    \centering
    \subfloat[Dataset 1]{\includegraphics[scale=0.7]{img/boltz1.png}}\\
    \subfloat[Dataset 3]{\includegraphics[scale=0.7]{img/boltz3.png}}
    \caption{\label{fig:bestfit}dati sperimentali con sovrapposte le curve di \emph{best-fit}.}
\end{figure}

\dichiarazione

\end{document}