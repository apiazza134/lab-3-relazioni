#!/usr/bin/env python3

from lab.spice import parse_spice
from lab import loadtxtSorted, Data
import matplotlib.pyplot as plt
import numpy as np

plt.style.use("../style.yml")

d = Data()

# Amplificazioni nominali
R1 = d.get('preamp', 'R1').nominal_value
R2 = d.get('preamp', 'R2').nominal_value
R3 = d.get('preamp', 'R3').nominal_value

Aina = 20*np.log10(1 + (50e3/R1))
Ainvert = 20*np.log10(R3/R2)

d.push(Aina, 'decibel', 'preamp', 'Aina')
d.push(Ainvert, 'decibel', 'preamp', 'Ainvert')
d.push(Aina + Ainvert, 'decibel', 'preamp', 'A')

# Plot di bode e della fase
f, A, phi = parse_spice('data/preamp.txt')

d.push(A[50], 'decibel', 'preamp', 'A0')
d.save()

phi *= np.pi/180

# Fix orribile per le fasi
for i in range(len(phi)-1, 0, -1):
    if phi[i] > phi[i-1]:
        phi[i:] = phi[i:] - 2*np.pi
        break

# Volendo si potebbero stimare le pendenze dei vari pezzi ma guardando
# la derivata non sembra che abbia una pendenza lineare a tratti
plt.plot(f, np.gradient(A, np.log10(f)))
plt.xscale('log')
# plt.show()
plt.close()

plt.plot(f, A)
plt.xlabel('$f$ [Hz]')
plt.ylabel('$A$ [dB]')
plt.xscale('log')
plt.savefig('img/preamp-bode.png', bbox_inches='tight')
# plt.show()
plt.close()

plt.plot(f, phi)
plt.xlabel('$f$ [Hz]')
plt.ylabel('$\\phi$ [rad]')
plt.xscale('log')
plt.savefig('img/preamp-fase.png', bbox_inches='tight')
plt.close()

# transient cusumano
t, Vout, Vin, Vinmezzo = loadtxtSorted('./data/preamp-transient.txt')


fig, ax1 = plt.subplots()

color = 'tab:red'
ax1.set_xlabel('$ t $\\ [s]')
ax1.set_ylabel('$ V_\\mathrm{in} $', color=color)
ax1.plot(t, Vin, color=color)
ax1.tick_params(axis='y', labelcolor=color)
ax1.ticklabel_format(style='sci', scilimits=(0, 0))

ax2 = ax1.twinx()
color = 'tab:blue'
ax2.set_ylabel('$ V_\\mathrm{out} $', color=color)
ax2.plot(t, Vout, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # boh ma il tizio lo usava

plt.savefig('img/preamp-transient.png', bbox_inches='tight')
# plt.show()
