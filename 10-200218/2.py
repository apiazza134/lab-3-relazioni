#!/usr/bin/env python3

from uncertainties import unumpy as unp
from uncertainties import correlated_values
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from numpy import linspace

from lab.data import Data
from lab.utils import loadtxtSorted
from lab.oscilloscope import measureMeanUncertaintyArray
from lab.table import Table

plt.style.use('../style.yml')

# 2a
Vin, dVin, Vout, dVout = loadtxtSorted("./data/2a.txt")

Vin = unp.uarray(Vin, dVin)
Vout = unp.uarray(Vout, dVout)


# Valori operazionali
def retta(x, m, q):
    return m*x + q


def intersection(r1, r2):
    (m1, q1), (m2, q2) = r1, r2
    x = - (q1 - q2)/(m1 - m2)
    return (x, retta(x, *r1))


low = (unp.nominal_values(Vin) < 900e-3)
high = (unp.nominal_values(Vin) > 1.09)
tran = (unp.nominal_values(Vin) > 900e-3) & (unp.nominal_values(Vin) < 1.09)

VinL = Vin[low]
VinT = Vin[tran]
VinH = Vin[high]

VoutH = Vout[low]
VoutT = Vout[tran]
VoutL = Vout[high]

optL, covL = curve_fit(
    retta, unp.nominal_values(VinL), unp.nominal_values(VoutH),
    sigma=unp.std_devs(VoutH), absolute_sigma=False
)
optL = correlated_values(optL, covL)

optT, covT = curve_fit(
    retta, unp.nominal_values(VinT), unp.nominal_values(VoutT),
    sigma=unp.std_devs(VoutT), absolute_sigma=False
)
optT = correlated_values(optT, covT)

d = Data()
d.push(optL[0], 'volt', '2.a', 'mL')
d.push(optL[1], 'volt', '2.a', 'qL')
d.push(optT[0], 'volt', '2.a', 'mT')
d.push(optT[1], 'volt', '2.a', 'qT')

d.push(intersection(optL, optT)[0], 'volt', '2.a', 'VinL')
d.push(intersection(optL, optT)[1], 'volt', '2.a', 'VoutH')
d.push(VinH[0], 'volt', '2.a', 'VinH')
d.push(VoutL.sum() / len(VoutL), 'volt', '2.a', 'VoutL')

# Tabella
tab = Table()
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
d.pushTable(tab, '2.a', 'tab')

low = linspace(0, intersection(optL, optT)[0].nominal_value)
tran = linspace(intersection(optL, optT)[0].nominal_value, unp.nominal_values(VinT)[-1])

plt.errorbar(
    unp.nominal_values(Vin), unp.nominal_values(Vout),
    xerr=unp.std_devs(Vin), yerr=unp.std_devs(Vout),
    linestyle='', color='black', elinewidth=1
)

style_args = dict(linestyle='-', color='red', marker='', linewidth=0.5)
plt.plot(low, retta(low, *unp.nominal_values(optL)), **style_args)
plt.plot(tran, retta(tran, *unp.nominal_values(optT)), **style_args)

plt.xlabel('$ V_\\mathrm{in} \\ [\\mathrm{V}] $')
plt.ylabel('$ V_\\mathrm{out} \\ [\\mathrm{V}] $')

plt.savefig("./img/2a.png", bbox_inches="tight")
# plt.show()
plt.close()


# 2b
Vin, divVin, Iin, dIin = loadtxtSorted("./data/2b.txt")

Vin = measureMeanUncertaintyArray(Vin, divVin)
Iin = unp.uarray(Iin, dIin)

tab = Table()
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('I\\ped{in}', 'ampere', Iin)
d.pushTable(tab, '2.b', 'tab')
d.save(overwrite=True)

plt.errorbar(
    unp.nominal_values(Vin), unp.nominal_values(Iin),
    xerr=unp.std_devs(Vin), yerr=unp.std_devs(Iin),
    linestyle='', color='black', elinewidth=1
)
plt.xlabel('$ V_\\mathrm{in} \\ [\\mathrm{V}] $')
plt.ylabel('$ I_\\mathrm{in} \\ [\\mathrm{A}] $')

plt.ticklabel_format(style='sci', scilimits=(0, 0))
plt.savefig("./img/2b.png", bbox_inches="tight")
# plt.show()
plt.close()

d.push(Iin[2], 'ampere', '2.b', 'IinH')
d.push(Vin[2], 'volt', '2.b', 'VinIinH')
d.push(Iin[10], 'ampere', '2.b', 'IinL')
d.push(Vin[10], 'volt', '2.b', 'VinIinL')

d.save()
