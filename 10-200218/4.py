#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

from lab.data import Data
from lab.oscilloscope import readCSV


def indiceIntersezione(lst: np.array, threshold: float) -> int:
    return (np.abs(lst - threshold)).argmin()


plt.style.use("../style.yml")

# Tempo tPHL (CANALE 1 SALE), tempo di salita in e tempo di discesa out
# Valori asintotici finali
tin, Vin, tout, Vout = readCSV('data/4-HL-1.csv')
VinAsymptD, VoutAsymptD = list(map(lambda x: x[tin > 5e-7].mean(), [Vin, Vout]))

# Plot lungo
fig, axes = plt.subplots(2, 1, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [1,1]})
axes[0].plot(tin, Vin, color='red')
axes[0].set_ylabel("$ V_\\mathrm{in} $ \\ [V]")
axes[1].plot(tout, Vout, color='blue')
axes[1].set_xlabel("$ t $ \\ [s]")
axes[1].set_ylabel("$ V_\\mathrm{out} $ \\ [V]")

# plt.show()
plt.savefig('img/4a-HL.png', bbox_inches="tight")
plt.close()


# Valori asintotici iniziali
tin, Vin, tout, Vout = readCSV('data/4-HL-2.csv')

VinAsymptS, VoutAsymptS = list(map(lambda x: x[tin < -1e-8].mean(), [Vin, Vout]))

VinMiddle = (VinAsymptD + VinAsymptS)/2
VoutMiddle = (VoutAsymptD + VoutAsymptS)/2

Vin10 = VinAsymptS*0.9 + VinAsymptD*0.1
Vin90 = VinAsymptS*0.1 + VinAsymptD*0.9

Vout10 = VoutAsymptS*0.1 + VoutAsymptD*0.9
Vout90 = VoutAsymptS*0.9 + VoutAsymptD*0.1

inIndexMiddle = indiceIntersezione(Vin, VinMiddle)
tinMiddle = tin[inIndexMiddle]

outIndexMiddle = indiceIntersezione(Vout, VoutMiddle)
toutMiddle = tout[outIndexMiddle]

tPHL = toutMiddle - tinMiddle

tinSalita = tin[indiceIntersezione(Vin, Vin90)] - \
    tin[indiceIntersezione(Vin, Vin10)]
toutDiscesa = tout[indiceIntersezione(Vout, Vin10)] - \
    tin[indiceIntersezione(Vout, Vin90)]

# Plot
fig, axes = plt.subplots(2, 1, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [1,1]})
axes[0].plot(tin, Vin, color='red')
axes[0].plot(2*[tinMiddle], [VinAsymptS, VinAsymptD], color='black', linestyle='--')
axes[0].plot([tin[0], tin[-1]], 2*[VinAsymptD], color='red', linestyle='--')
axes[0].plot([tin[0], tin[-1]], 2*[VinAsymptS], color='red', linestyle='--')
axes[0].set_ylabel("$ V_\\mathrm{in} $ \\ [V]")

axes[1].plot(tout, Vout, color='blue')
axes[1].plot(2*[toutMiddle], [VoutAsymptS, VoutAsymptD], color='black', linestyle='--')
axes[1].plot([tout[0], tout[-1]], 2*[VoutAsymptD], color='blue', linestyle='--')
axes[1].plot([tout[0], tout[-1]], 2*[VoutAsymptS], color='blue', linestyle='--')
axes[1].set_xlabel("$ t $ \\ [s]")
axes[1].set_ylabel("$ V_\\mathrm{out} $ \\ [V]")

# plt.show()
plt.savefig('img/4a-tPHL.png', bbox_inches="tight")
plt.close()

# Tempo tPLH (CANALE 1 SCENDE), tempo di discesa in e tempo di salita out
# Valori asintotici finali
tin, Vin, tout, Vout = readCSV('data/4-LH-1.csv')

VinAsymptD, VoutAsymptD = list(map(lambda x: x[tin > 2.2e-6].mean(), [Vin, Vout]))

# plot lungo
fig, axes = plt.subplots(2, 1, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [1,1]})
axes[0].plot(tin, Vin, color='red')
axes[0].set_ylabel("$ V_\\mathrm{in} $ \\ [V]")
axes[1].plot(tout, Vout, color='blue')
axes[1].set_xlabel("$ t $ \\ [s]")
axes[1].set_ylabel("$ V_\\mathrm{out} $ \\ [V]")
plt.ticklabel_format(style='sci', scilimits=(0, 0))
plt.savefig('img/4a-LH.png', bbox_inches="tight")
plt.close()

# Valori asintotici iniziali
tin, Vin, tout, Vout = readCSV('data/4-LH-2.csv')
VinAsymptS, VoutAsymptS = list(map(lambda x: x[tin < -1.6e-8].mean(), [Vin, Vout]))

VinMiddle = (VinAsymptD + VinAsymptS)/2
VoutMiddle = (VoutAsymptD + VoutAsymptS)/2

Vin10 = VinAsymptS*0.1 + VinAsymptD*0.9
Vin90 = VinAsymptS*0.9 + VinAsymptD*0.1

Vout10 = VoutAsymptS*0.9 + VoutAsymptD*0.1
Vout90 = VoutAsymptS*0.1 + VoutAsymptD*0.9

inIndexMiddle = indiceIntersezione(Vin, VinMiddle)
tinMiddle = tin[inIndexMiddle]

outIndexMiddle = indiceIntersezione(Vout, VoutMiddle)
toutMiddle = tout[outIndexMiddle]

tPLH = toutMiddle - tinMiddle

# Tempo di discesa di Vin
tinDiscesa = tin[indiceIntersezione(Vin, Vin10)] - \
    tin[indiceIntersezione(Vin, Vin90)]

# Le intersezioni di Vout vanno prese con i dati lunghi
tin, Vin, tout, Vout = readCSV('data/4-LH-1.csv')

toutSalita = tout[indiceIntersezione(Vout, Vout90)] - \
    tin[indiceIntersezione(Vout, Vout10)]

# Plot
tin, Vin, tout, Vout = readCSV('data/4-LH-2.csv')
fig, axes = plt.subplots(2, 1, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [1,1]})
axes[0].plot(tin, Vin, color='red')
axes[0].plot(2*[tinMiddle], [VinAsymptS, VinAsymptD], color='black', linestyle='--')
axes[0].plot([tin[0], tin[-1]], 2*[VinAsymptD], color='red', linestyle='--')
axes[0].plot([tin[0], tin[-1]], 2*[VinAsymptS], color='red', linestyle='--')
axes[0].set_ylabel("$ V_\\mathrm{in} $ \\ [V]")

axes[1].plot(tout, Vout, color='blue')
axes[1].plot(2*[toutMiddle], [VoutAsymptS, VoutAsymptD], color='black', linestyle='--')
axes[1].plot([tout[0], tout[-1]], 2*[VoutAsymptD], color='blue', linestyle='--')
axes[1].plot([tout[0], tout[-1]], 2*[VoutAsymptS], color='blue', linestyle='--')
axes[1].set_xlabel("$ t $ \\ [s]")
axes[1].set_ylabel("$ V_\\mathrm{out} $ \\ [V]")

# plt.show()
plt.savefig('img/4a-tPLH.png', bbox_inches="tight")
# plt.close()

# Carico i dati
d = Data()

d.push(tPHL, 'second', '4.a', 'tPHL')
d.push(tPLH, 'second', '4.a', 'tPLH')

d.push(tinSalita, 'second', '4.b', 'tinSalita')
d.push(toutDiscesa, 'second', '4.b', 'toutDiscesa')
d.push(tinDiscesa, 'second', '4.b', 'tinDiscesa')
d.push(toutSalita, 'second', '4.b', 'toutSalita')

d.save()
