\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo

\begin{document}
\maketitle

\section{Caratteristiche statiche}
Si è assemblato il circuito con i valori misurati delle resistenze
\[ R_{1} = \sivar{2.z}{R1} \qquad R_{2} = \sivar{2.z}{R2} \]
e alimentazione dell'integrato
\[ V \ped{CC} = \sivar{2.z}{VCC}. \]

Si è quindi misurata la tensione in uscita $ V \ped{out} $ al variare di quella in ingresso $ V \ped{in} $, regolata agendo sul potenziometro (entrambe misurate con il multimetro digitale). Si riportano i dati in Tabella~\ref{tab:VinVout} e il grafico in Figura~\ref{fig:VinVout}.

Si è infine misurata la corrente in ingresso $ I \ped{in} $ (con il multimetro digitale) al variare della tensione in ingresso $ V \ped{in} $ (misurata con l'oscilloscopio). La corrente è considerata positiva quando ``entrante'' nell'input. Si riportano i dati in Tabella~\ref{tab:VinIin} e il grafico in Figura~\ref{fig:VinIin}.
\begin{table}[h!]
    \centering
    \subfloat[\label{tab:VinVout}]{\tab{2.a}{tab}}\hspace{2cm}
    \subfloat[\label{tab:VinIin}]{\tab{2.b}{tab}}
    \caption{dati relativi alla porta \texttt{NOT}.}
\end{table}
\begin{figure}[h!]
    \centering
    \subfloat[\label{fig:VinVout}Tensione in uscita in funzione di quella in ingresso.]{\includegraphics[scale=0.6]{img/2a}}\\
    \subfloat[\label{fig:VinIin}Corrente in ingresso in funzione della tensione in ingresso.]{\includegraphics[scale=0.6]{img/2b}}
    \caption{curve caratteristiche della porta \texttt{NOT}.}
\end{figure}

\subsection{Misura delle tensioni di operazione}
L'andamento delle tensioni e delle correnti riportati mette in luce il carattere non ideale della porta logica utilizzata. Per una porta \texttt{NOT} ideale il grafico di $ V\ped{in}$--$V\ped{out} $ dovrebbe essere un gradino: in Figura~\ref{fig:VinVout} si osserva un andamento lineare a tratti con una zona di transizione attorno a $ V\ped{in} \simeq \SI{1}{\volt} $.

Si sono effettuati dei \emph{fit} lineari nelle zone $ V\ped{in} < \SI{0.9}{\volt} $ e $ \SI{0.9}{\volt} < V\ped{in} < \SI{1.09}{\volt}$. Dall'intersezione delle rette così ottenute\footnote{
  Le coordinate dell'intersezione tra due rette $ y = m_1 x + q_1 $ e $ y = m_2 x + q_2 $ (con $ m_1 \neq m_2 $) sono date da \[ x = - \frac{q_1 - q_2}{m_1 - m_2} \qquad y = - \frac{q_1 m_2 - q_2 m_1}{m_1 - m_2} \]
} si è ottenuta una stima di $ V \ped{in, L} $ e di $ V \ped{out, H} $.
Come stima di $ V \ped{out, L} $ è stata presa la media delle tensioni in uscita corrispondenti a $ V\ped{in} > \SI{1.09}{\volt} $; come stima di $ V \ped{in, H} $ si è invece preso il $ V \ped{in} $ minore tra tali dati.
I valori così ottenuti sono i seguenti
\[
    V\ped{in,L} = \sivar{2.a}{VinL} \quad
    V\ped{in,H} = \sivar{2.a}{VinH} \quad
    V\ped{out,L} = \sivar{2.a}{VoutL} \quad
    V\ped{out,H} = \sivar{2.a}{VoutH}
\]
Nella maggior parte dei casi, questi sono comparabili, seppur non sempre compatibili con i valori tipici (o range di valori) riportati nel \emph{datasheet} dell'\texttt{SN74LS04}.

\subsection{Misura della corrente in ingresso}
Anche in questo caso la corrente in ingresso $ I\ped{in} $ in funzione della tensione in ingresso $ V\ped{in} $ ha un andamento lineare a tratti, con la corrente sostanzialmente costante ed ``entrante'' nel dispositivo per $ V\ped{in} > \SI{1.5}{\volt} $. Facendo riferimento alle condizioni di test riportate nel \emph{datasheet} della porta \texttt{SN74LS04}, possiamo ricavare le seguenti stime:
\[
    I\ped{in,L} = \sivar{2.b}{IinL} \qquad I\ped{in,H} = \sivar{2.b}{IinH}
\]
compatibili con i valori massimi riportati nel \emph{datasheet} ($ I\ped{in,L}\ap{max} = \SI{-1.6}{\milli\ampere} $ a $ V\ped{in} = \SI{0.4}{\volt} $ e $ I\ped{in,H}\ap{max} = \SI{40}{\micro\ampere} $ a $ V\ped{in} = \SI{2.4}{\milli\volt} $).

Facendo ora riferimento allo schema della porta in Figura~\ref{fig:circuitoNOT} possiamo dare una descrizione qualitativa dell'andamento della corrente in ingresso al variare in $ V\ped{in} $ (i.e. ``Input $ A $'').
\begin{itemize}
    \item Quando $ A $ è ``alto'' (e conseguentemente l'output $ Y $ è ``basso'') entrambi i transistor $ T_{1} $ e $ T_{2} $ sono in saturazione e pertanto la differenza di potenziale ai capi delle rispettive giunzioni BE è fissata a $ \sim \SI{0.6}{\volt} $. Così $ V\ped{P} \sim \SI{1.2}{\volt} $ e fintanto che $ V\ped{A} > V\ped{P} $ il diodo zener $ D $ risulta in interdizione. Si avrà quindi una corrente entrante nel circuito (positiva con le convenzioni di cui sopra) pari alla corrente di \emph{reverse-bias} del diodo zener.
    \item Quando $ V\ped{A} < \SI{1.2}{\volt} $, il diodo $ D $ entra in conduzione, la corrente è uscente e quindi negativa. In Figura~\ref{fig:VinIin} si osserva che effettivamente la transizione avviene in corrispondenza di $ V\ped{in} \sim \SI{1}{\volt} $.
    \item L'andamento della corrente al variare della tensione in ingresso al di sotto della soglia sopra citata risulta difficile da modellare a causa della presenza di numerosi componenti non lineari. Ci limitiamo ad osservare che al di sotto di $ V\ped{in} \sim \SI{1}{\volt} $ l'andamento della corrente non è esponenziale come ci si potrebbe aspettare in prima battuta dalla curva caratteristica del diodo $ D $: questo è dovuto al fatto che sotto la soglia i transistor $ T_{1} $ e $ T_{2} $ vanno in interdizione e pertanto $ V\ped{P} $ diminuisce al diminuire di $ V\ped{in} $, diventando minore di $ \sim \SI{1.2}{\volt} $.
\end{itemize}

\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
    \node at (0,0) {\includegraphics[scale=0.35]{data/NOTttl}};
    \node at (-2.6,0.7) {$ D $};
    \node at (-1.7,0.4) {$ P $};
    \node at (-0.9,0.21) {$ T_{1} $};
    \node at (2.5,-1.3) {$ T_{2} $};
    \end{tikzpicture}
    \caption{\label{fig:circuitoNOT}schema circuitale della porta \texttt{SN74LS04}.}
\end{figure}

\section{Montaggio di Arduino}
I valori misurati delle resistenze e delle capacità utilizzate sono
\begin{gather*}
  R_{1} = \sivar{3.z}{R1} \quad
  R_{2} = \sivar{3.z}{R2} \quad
  R_{3} = \sivar{3.z}{R3} \quad
  R_{4} = \sivar{3.z}{R4} \quad
  R_{5} = \sivar{3.z}{R5} \\
  C_{1} = \sivar{3.z}{C1} \quad
  C_{2} = \sivar{3.z}{C2}
\end{gather*}

\section{Caratteristiche dinamiche}

Si è regolato il potenziometro in modo da ottenere, in uscita dal pin \texttt{Y1}, un'onda quadra di frequenza $ f = \sivar{4.z}{f} $. Si è utilizzata una resistenza di \emph{pull-up} verso $ V \ped{CC} $ di $ \sivar{4.z}{Rpu} $.

Con l'oscilloscopio abbiamo acquisito le forme d'onda all'entrata e all'uscita della porta \texttt{NOT}, rispettivamente $ V\ped{in} $ e $ V\ped{out} $, prendendo in considerazione una scala di tempi sufficientemente vicina alle zone di transizione.

In Figura~\ref{fig:HL} e in Figura~\ref{fig:LH} riportiamo il grafico dei punti acquisiti per una finestra maggiore di tempo: osserviamo che i segnali presentano sia \emph{overshoot} che notevoli oscillazioni dopo la transizione, attribuibili al comportamento non ideale della porta; inoltre la transizione di $ V\ped{out} $ da ``basso'' a ``alto'' risulta particolarmente lenta. Per questo motivo la definizione stressa dei tempi di propagazione (e di salita) non è ben posta: le stime riportate successivamente sono da considerarsi quindi come indicative e pertanto riportate senza incertezza.

Per la determinazione del punti al 10\%, 50\% e 90\% abbiamo deciso di utilizzare l'ampiezza picco-picco dei segnali ricavata dai valori asintotici dei segnali (ottenuti come media delle tensioni per tempi rispettivamente sufficientemente precedenti alla transizione e sufficientemente successivi), ignorando quindi le ``impurità'' dovute all'overshoot e alle oscillazioni.

\begin{figure}[h!]
    \centering
    \subfloat[\label{fig:HL}Transizione low-high.]{\includegraphics[scale=0.55]{img/4a-HL}}
    \subfloat[\label{fig:LH}Transizione high-low.]{\includegraphics[scale=0.55]{img/4a-LH}}
    \caption{acquisizione lunga delle transizioni della porta \texttt{NOT}.}
\end{figure}

Per stimare i tempi di transizione, si è determinato il punto campionato più vicino alla soglia del 50\% del segnale in ingresso e di uscita, sia per la transizione alto-basso che per la transizione basso-alto. Per ciascuna delle due transizioni, la differenza tra tali tempi costituisce (dato che l'acquisizione dei due canali è simultanea e riferita a un comune trigger) una stima per tempo caratteristico di propagazione della porta: si è ottenuto
\[
    t\ped{PHL} \simeq \sivar{4.a}{tPHL} \qquad
    t\ped{PLH} \simeq \sivar{4.a}{tPLH}
\]
Il tempo di propagazione alto-basso è compatibile con quello riportato nel \emph{datasheet}, entro il range dichiarato. Il tempo di propagazione basso-alto risulta superiore al massimo dichiarato nel \emph{datasheet}, incompatibilità probabilmente attribuibile alla salita particolarmente ``lenta'' di $ V\ped{out} $ in tale transizione, già osservata in precedenza.

In Figura~\ref{fig:propagazione} si riportano i grafici dei dati acquisiti in ingresso e in uscita vicino alla transizione, utilizzati per la determinazione dei tempi di propagazione.

\begin{figure}[h!]
  \centering
  \subfloat[Transizione low-high.]{\includegraphics[scale=0.8]{img/4a-tPHL}} \\
  \subfloat[Transizione high-low.]{\includegraphics[scale=0.8]{img/4a-tPLH}}
  \caption{\label{fig:propagazione}i valori asintotici sono tratteggiati in orizzontale, mentre la linea tratteggiata verticalmente corrisponde al tempo di 50\% del segnale.}
\end{figure}

Si è seguito un procedimento analogo per la determinazione del tempo di salita e discesa del segnale (il tempo necessario a passare dal 10\% al 90\% dell'incremento asintotico e viceversa). Si è ottenuto
\begin{gather*}
  t \ped{in, sal} = \sivar{4.b}{tinSalita} \qquad t \ped{in, dis} = \sivar{4.b}{tinDiscesa} \\
  t \ped{out, sal} = \sivar{4.b}{toutSalita} \qquad t \ped{out, dis} = \sivar{4.b}{toutDiscesa}
\end{gather*}


\section{Costruzione di circuiti logici elementari}
La tensione misurata di alimentazione degli integrati è $ V \ped{CC} = \sivar{5.a}{VCC} $.

\subsection{Porta \texttt{NAND}}
Si osserva che si alternano due pin a $ \sivar{5.a}{VinS} $ e un pin a $ \sivar{5.a}{VoutS} $ su ciascun lato. Dato che un ingresso non collegato di un  \texttt{TTL} corrisponde a un ingresso ``alto'', si conclude che sono presenti due \texttt{NAND} su ciascun lato i cui input corrispondono ai pin con il potenziale maggiore sopra menzionato. Questo rispecchia quanto riportato nel \emph{datasheet} dell'integrato.
Non si nota una variazione significativa dei potenziali sopra citati tra le diverse porte dello stesso integrato, entro gli errori sperimentali.

La tabella di verità del \texttt{NAND} è
\begin{center}
  \begin{tabular}{ccc}
    \toprule
    $ A $ & $ B $ & $ \nand $ \\ \midrule
    1 & 0 & 1 \\
    1 & 1 & 0 \\
    0 & 1 & 1 \\
    0 & 0 & 1 \\ \bottomrule
  \end{tabular}
\end{center}
Si verifica il corretto funzionamento della porta; in particolare, se almeno uno degli input è collegato a massa, si misura
\[ V \ped{out, H} = \sivar{5.b}{VoutH}. \]
Altrimenti, se entrambi gli input sono collegati a $ V \ped{CC} $, si misura
\[ V \ped{out, L} = \sivar{5.b}{VoutL}. \]
Il corretto comportamento della porta è stato verificando anche collegando un \texttt{LED} all'output.

Il comportamento della porta logica può essere analizzato anche alimentando gli input con i segnali prodotti dal circuito impulsatore e osservando con l'oscilloscopio input e output. In particolare il circuito impulsatore produce nei suoi pin \texttt{Y1} e \texttt{Y2} onde quadre sfasate di $ \pi / 2 $. Queste si riportano in Figura~\ref{fig:impulsatore}. In Figura~\ref{fig:uscitaNAND} si riporta invece l'uscita della porta \texttt{NAND}; si verifica immediatamente che la relazione tra input e output coincide con la tabella di verità sopra riportata (l'ordine corrisponde a quello temporale).

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:impulsatore}Output del circuto impulsatore. Il \texttt{CH1} (risp. \texttt{CH2}) corrisponde all'ingresso A (risp. B) della porta.]{\includegraphics[scale=0.6]{data/5d-1}}\hspace{0.5cm}
  \subfloat[\label{fig:uscitaNAND}Il \texttt{CH1} corrisponde all'ingresso A, il \texttt{CH2} corrisponde all'uscita del \texttt{NAND}.]{\includegraphics[scale=0.6]{data/5d-2}}
  \caption{}
\end{figure}

\subsection{Circuito \texttt{OR}}
Un circuito \texttt{OR} si può realizzare con una porta \texttt{NAND} e due porte \texttt{NOT} notando che
\[ A + B = \overline{\overline{A}\cdot \overline{B}}. \]
Si è quindi assemblato il circuito il cui schema è riportato in Figura~\ref{fig:circuitoOR}.
\begin{figure}[h!]
    \centering
    \begin{circuitikz}
        \draw (0,0) node (nand) [nand port] {};
        \draw (nand.in 1) ++(-0.5, 0) node (not1) [not port, scale=0.4] {};
        \draw (nand.in 2) ++(-0.5, 0) node (not2) [not port, scale=0.4] {};
        \draw (not1.out) -- (nand.in 1);
        \draw (not2.out) -- (nand.in 2);
        \draw (not1.in) -- ++(-0.5, 0) node[anchor=east] {$ A $};
        \draw (not2.in) -- ++(-0.5, 0) node[anchor=east] {$ B $};
        \node[anchor=west] at (nand.out) {$ A + B $};
    \end{circuitikz}
    \caption{\label{fig:circuitoOR}Circuito \texttt{OR}.}
\end{figure}

In Figura~\ref{fig:outputOR} si riporta l'output del circuito, acquisito con l'oscilloscopio. Qui e nel seguito intendiamo che gli input $ A $ e $ B $ sono sempre generati dall'impulsatore, come in Figura~\ref{fig:impulsatore}. Si osserva che l'output riproduce correttamente la tabella di verità:
\begin{center}
  \begin{tabular}{ccc}
    \toprule
    $ A $ & $ B $ & $ A + B $ \\ \midrule
    1 & 0 & 1 \\
    1 & 1 & 1 \\
    0 & 1 & 1 \\
    0 & 0 & 0 \\ \bottomrule
  \end{tabular}
\end{center}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{data/5e-or}
  \caption{\label{fig:outputOR}il \texttt{CH1} corrisponde all'ingresso A, il \texttt{CH2} corrisponde all'uscita dell'\texttt{OR}.}
\end{figure}

\subsection{Circuito \texttt{XOR}}
Un circuito \texttt{XOR} si può realizzare con 4 porte \texttt{NAND} notando che
\begin{align*}
  A \oplus B &= (A \cdot \overline{B}) + (\overline{A} \cdot B)
             =  (A \cdot \overline{A} + A \cdot \overline{B}) + (B \cdot \overline{B} + \overline{A} \cdot B) \\
             &= A \cdot (\overline{A} + \overline{B}) + B \cdot (\overline{B} + \overline{A})
             = A \cdot \overline{(A \cdot B)} + B \cdot \overline{(B \cdot A)} \\
             &= \overline{ \overline{A \cdot \nand(A, B)} \cdot \overline{B \cdot \nand(A, B)} } \\
             &= \nand\sbr{ \nand\del{A, \nand(A, B)}, \nand\del{B, \nand(A, B)}}.
\end{align*} %todo: ridefinire nand con degli spazi decenti.

\begin{figure}[h!]
    \centering
    \begin{circuitikz}
        \draw (0, 1) node (nandSopra) [nand port] {};
        \draw (0, -1) node (nandSotto) [nand port] {};
        \draw (3, 0) node (nandDestra) [nand port] {};
        \draw (-3, 0) node (nandSinistra) [nand port] {};

        \coordinate (biforcazione) at ($ (nandSinistra.out |-, 0) + (0.5, 0) $);
        \draw (nandSinistra.out) -- (biforcazione) |- (nandSopra.in 2);
        \draw (nandSinistra.out) -- (biforcazione) |- (nandSotto.in 1);

        \draw (nandSopra.out) -- ++(0.5,0) |- (nandDestra.in 1);
        \draw (nandSotto.out) -- ++(0.5,0) |- (nandDestra.in 2);
        \draw (nandDestra.out) node [anchor=west] {$ A \oplus B $};
        \draw (nandSopra.in 1) -- ++(-4, 0) node[anchor=east] (inputA) {$ A $};
        \draw (nandSotto.in 2) -- ++(-4, 0) node[anchor=east] (inputB) {$ B $};

        \draw (nandSinistra.in 1) -| (-4.7, |- inputA);
        \draw (nandSinistra.in 2) -| (-4.7, |- inputB);
    \end{circuitikz}
    \caption{\label{fig:circuitoXOR}circuito \texttt{XOR}.}
\end{figure}

Si è quindi assemblato il circuito il cui schema è riportato in Figura~\ref{fig:circuitoXOR}.
In Figura~\ref{fig:outputXOR} si riporta l'output del circuito, acquisito con l'oscilloscopio. Si osserva che l'output riproduce correttamente la tabella di verità:
\begin{center}
  \begin{tabular}{ccc}
    \toprule
    $ A $ & $ B $ & $ A \oplus B $ \\ \midrule
    1 & 0 & 1 \\
    1 & 1 & 0 \\
    0 & 1 & 1 \\
    0 & 0 & 0 \\ \bottomrule
  \end{tabular}
\end{center}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{data/5e-xor}
  \caption{\label{fig:outputXOR}il \texttt{CH1} corrisponde all'ingresso $ A $, il \texttt{CH2} corrisponde all'uscita dello \texttt{XOR}.}
\end{figure}

\subsection{Circuito sommatore a 1 bit}
Un circuito sommatore a 1 bit ha la seguente tabella di verità

\begin{center}
  \begin{tabular}{cccc}
    \toprule
    $ A $ & $ B $ & $ S $ & $ R $ \\ \midrule
    1 & 0 & 1 & 0 \\
    1 & 1 & 0 & 1 \\
    0 & 1 & 1 & 0 \\
    0 & 0 & 0 & 0 \\ \bottomrule
  \end{tabular}
\end{center}
ove $ S $ è la somma $ \mathrm{mod} \, 2 $ (\texttt{XOR}) e $ R $ è il resto (\texttt{AND}).
Si è quindi assemblato un circuito costituito dallo \texttt{XOR} dei due input e dal loro \texttt{AND} (ottenuto con una porta \texttt{NOT} in cascata a una porta \texttt{NAND}).

In Figura~\ref{fig:outputSommatore} si riporta l'output del circuito, acquisito con l'oscilloscopio. Si osserva che l'output riproduce correttamente la tabella di verità.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{data/5e-sommatore}
  \caption{\label{fig:outputSommatore}il \texttt{CH1} corrisponde all'uscita $ S $, il \texttt{CH2} corrisponde all'uscita $ R $.}
\end{figure}

\subsection{Circuito selettore}
Un circuito selettore (cioè tale che l'output è $ A $ se $ C $ è 0 e $ B $ se $ C $ è 1) si può realizzare con 3 porte \texttt{NAND} e una porta \texttt{NOT} notando che
\[
  \sel(A,B;C) = A \cdot \overline{C} + B \cdot C
  = \overline{(\overline{A \cdot \overline{C}}) \cdot (\overline{B \cdot C})}
  = \nand\sbr{\nand(A, \overline{C}), \nand(B, C)}.
\]
Si è quindi assemblato il circuito il cui schema è riportato in Figura~\ref{fig:circuitoSelettore}.

\begin{figure}[h!]
    \centering
    \begin{circuitikz}
        \draw (0, 1) node (nandSopra) [nand port] {};
        \draw (0, -1) node (nandSotto) [nand port] {};
        \draw (3, 0) node (nandDestra) [nand port] {};

        \draw (nandSopra.in 2) ++(-0.5,0) node (not) [not port, scale=0.5] {};
        \draw (nandDestra.out) node [anchor=west] {$ \sel(A,B;C) $};
        \draw (nandSopra.in 1) -- ++(-2, 0) node[anchor=east] (input) {$ A $};
        \draw (nandSotto.in 2) -- ++(-2, 0) node[anchor=east] {$ B $};

        \draw (not.out) -- (nandSopra.in 2);
        \coordinate (biforcazione) at ($ (not.out |-, 0) + (-1, 0) $);
        \draw (input |-, 0) node[anchor=east] {$ C $} -- (biforcazione) |- (not.in);
        \draw (biforcazione) |- (nandSotto.in 1);

        \draw (nandSopra.out) -| ++(0.5,0) |- (nandDestra.in 1);
        \draw (nandSotto.out) -| ++(0.5,0) |- (nandDestra.in 2);
    \end{circuitikz}
    \caption{\label{fig:circuitoSelettore}circuito Selettore.}
\end{figure}

In Figura~\ref{fig:outputSelettore} si riporta l'output del circuito, per i due valori di $ C $. Si osserva che l'output riproduce correttamente la tabella di verità:
\begin{center}
  \begin{tabular}{cccc}
    \toprule
    $ A $ & $ B $ & $ C $ & $ \sel $ \\ \midrule
    1 & 0 & 0 & 1 \\
    1 & 1 & 0 & 1 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 0 & 0 \\ \midrule
    1 & 0 & 1 & 0 \\
    1 & 1 & 1 & 1 \\
    0 & 1 & 1 & 1 \\
    0 & 0 & 1 & 0 \\ \bottomrule
  \end{tabular}
\end{center}

\begin{figure}[h!]
  \centering
  \subfloat[$ V \ped{C} \simeq \SI{0}{\volt} $.]{\includegraphics[scale=0.8]{data/5e-select-cL}}
  \hspace{0.5cm}
  \subfloat[$ V \ped{C} \simeq \SI{5}{\volt} $.]{\includegraphics[scale=0.8]{data/5e-select-cH}}
  \caption{\label{fig:outputSelettore}output del circuito selettore.}
\end{figure}


\dichiarazione

\end{document}
