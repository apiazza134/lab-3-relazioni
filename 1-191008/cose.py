#!/usr/bin/env python3

from uncertainties import unumpy
from lab import Data, Table, loadtxtSorted

d = Data()

# partitore di tensione
for sec in ['2.b', '2.c']:
    # valore atteso
    R1 = d.get(sec, 'R1')
    R2 = d.get(sec, 'R2')
    A = R2 / (R1 + R2)
    d.push(A, None, sec, 'A')

    # valore misurato
    Vin, dVin, Vout, dVout = loadtxtSorted(f'data/{sec}.txt')
    Vin = unumpy.uarray(Vin, dVin)
    Vout = unumpy.uarray(Vout, dVout)
    Ameas = Vout/Vin
    Abar = Ameas.mean()
    d.push(Abar, None, sec, 'Abar')
    tab = Table()
    tab.pushColumn('V\\ped{in}', 'volt', Vin)
    tab.pushColumn('V\\ped{out}', 'volt', Vout)
    tab.pushColumn('A', None, Ameas)
    d.pushTable(tab, sec, 'tab')


# 2.d (resistenza in ingesso multimetro)
d.push(d.get('2.b', 'R1')*d.get('2.b', 'A'), 'ohm', '2.b', 'RTsogliab')
Abar = d.get('2.c', 'Abar')
A = d.get('2.c', 'A')
d.push(
    d.get('2.c', 'R1')*(Abar*A)/(A-Abar), 'ohm',
    '2.d', 'RTsogliac'
)


# 2.e (partitore di corrente)
R1 = d.get('2.e', 'R1')
R2 = d.get('2.e', 'R2')
R3 = d.get('2.e', 'R3')
Vin = d.get('2.e', 'Vin')
VR3 = d.get('2.e', 'VR3')
I1dig = d.get('2.e', 'I1-dig')
I2dig = d.get('2.e', 'I2-dig')

Itot = 1000*Vin/R3
I1 = Itot*R2/(R1+R2)
I2 = Itot*R1/(R1+R2)
Itotmeas = 1000*VR3/R3

r1 = (Itotmeas/I1dig)*R2 - R1 - R2
r2 = (Itotmeas/I2dig)*R1 - R1 - R2

d.push(Itot, 'ampere', '2.e', 'Itot')
d.push(I1, 'ampere', '2.e', 'I1')
d.push(I2, 'ampere', '2.e', 'I2')
d.push(Itotmeas, 'ampere', '2.e', 'Itotmeas')
d.push(r1, 'ohm', '2.e', 'r1')
d.push(r2, 'ohm', '2.e', 'r2')


# 3.b (oscilloscopio per misure di tensione)
# valore atteso
R1 = d.get('3.b', 'R1')
R2 = d.get('3.b', 'R2')
A = R2 / (R1 + R2)
d.push(A, None, '3.b', 'A')

Vin, dVin, Vout, dVout = loadtxtSorted("data/3.b.txt")

Vin = unumpy.uarray(Vin, dVin)
Vout = unumpy.uarray(Vout, dVout)
Ameas = Vout/Vin
tab = Table()
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A', None, Ameas)
d.pushTable(tab, '3.b', 'tab')

# 3.c
Amean = Ameas.mean()
d.push(Amean, None, '3.c', 'Amean')

R1 = d.get('3.b', 'R1')
R2 = d.get('3.b', 'R2')
A = d.get('3.b', 'A')
Rin = (R1 * A * Amean)/(A - Amean)
d.push(Rin, 'ohm', '3.c', 'Rin')

# 4.b
T, sT, cu, su, TOsc, dTOsc, fTrig, dfTrig = loadtxtSorted('data/4.b.txt')
dT = sT/250 + 1e-4*T + 0.6e-9

T = unumpy.uarray(T, dT)
TOsc = unumpy.uarray(TOsc, dTOsc)
f = 1/T
fOsc = 1/TOsc

tab = Table()
tab.pushColumn('T', 'second', T)
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('f\\ped{osc}', 'hertz', fOsc)
tab.pushColumn('f - f\\ped{osc}', 'hertz', f - fOsc)
tab.pushColumn('f\\ped{trg}', 'hertz', fTrig)
tab.pushColumn('f - f\\ped{trg}', 'hertz', f - fTrig)
d.pushTable(tab, '4.b', 'tab')

# 4.c
tH, scala, T, scalabis = loadtxtSorted('data/4.c.txt')
dT = scala/250 + 1e-4*T + 0.6e-9
dtH = scala/250 + 1e-4*tH + 0.6e-9

T = unumpy.uarray(T, dT)
tH = unumpy.uarray(tH, dtH)

tab = Table()
tab.pushColumn('T', 'second', T)
tab.pushColumn('T_H', 'second', tH)
tab.pushColumn('\\mathfrak{D}', None, tH/T * 100)
d.pushTable(tab, '4.c', 'tab')

d.save()
