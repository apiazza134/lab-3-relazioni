\documentclass[a4paper, 11pt]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\preambolo

\begin{document}
\maketitle

\stepcounter{section}
\section{Misure di tensione e corrente}

\stepcounter{subsection}
\subsection{Partitore con resistenze da circa \SI{1}{\kilo\ohm}}\label{sec:1kiloohm}
Valori misurati di $R_1$ e $R_2$ e valore atteso di $A\ped{exp}$:
\[
    R_1 = \SiVar{2.b}{R1} \qquad
    R_2 = \SiVar{2.b}{R2} \qquad
    A\ped{exp} = \SiVar{2.b}{A}
\]

\begin{table}[h!]
    \centering
    \tab{2.b}{tab}
    \caption{partitore di tensione con resistenze da circa \SI{1}{\kilo\ohm}.}
\end{table}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.75]{img/2.b.png}
    \caption{grafico $V\ped{out}$ in funzione di $V\ped{in}$ con resistenze di circa \SI{1}{\kilo\ohm}.}
\end{figure}

I valori ottenuti per $ A $ sono compatibili con il valore atteso $ A\ped{exp} $ ricavato dalla misura delle resistenze con il multimetro.

\subsection{Partitore con resistenze da circa \SI{4}{\mega\ohm}}\label{sec:4megaohm}
Valori misurati di $R_1$ e $R_2$ e valore atteso di $A_\mathrm{exp}$:
\[
    R_1 = \SiVar{2.c}{R1} \qquad
    R_2 = \SiVar{2.c}{R2} \qquad
    A\ped{exp} = \SiVar{2.c}{A}
\]
In questo caso, a differenza di quanto accadeva nel punto precedente, il valori misurati di $ A $ non sono compatibili con il valore atteso $ A\ped{exp} $, e in particolare risultano minori.
Questo è imputabile al fatto che le resistenze $ R_{1} $ e $ R_{2} $ sono ora comparabili con il valore della resistenza interna del multimetro usato come voltmetro (che da manuale risulta essere \SI{10}{\mega\ohm}). L'effetto di questa resistenza è quello di abbassare la resistenza equivalente vista dal generatore (il multimetro è in parallelo a $ R_{2} $), e quindi di aumentare la corrente che scorre attraverso di esso e conseguentemente la caduta di potenziale ai capi di $ R_{1} $. Quindi, in conclusione, $ V\ped{out} $ sarà minore e così pure $ A $.

\begin{table}[h!]
    \centering
    \tab{2.c}{tab}
    \caption{partitore di tensione con resistenze da circa \SI{4}{\mega\ohm}.}
\end{table}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.75]{img/2.c.png}
    \caption{grafico $V\ped{out}$ in funzione di $V\ped{in}$ con resistenze di circa \SI{4}{\mega\ohm}.}
\end{figure}

\subsection{Resistenza di ingresso del tester}
\label{sec:resTester}
Usando il modello mostrato nella scheda si ottiene
\[
    \frac{R_1}{R_T} = \frac{V\ped{in}}{V\ped{out}} - \left(1 +  \frac{R_1}{R_2} \right)
    = \frac{1}{A} - \frac{1}{A\ped{exp}}.
\]
Nel caso del punto~\ref{sec:1kiloohm} si ha $ A \simeq A \ped{exp} $ pertanto l'errore su $ R_{T} $ ottenuto dalla propagazione delle incertezze su $ A $  e $ A \ped{exp} $ non consente di fornire una stima ragionevole di $ R_{T} $. Possiamo invece dare una rozza stima inferiore
\[ \frac{R_{1}}{R_{T}} < \frac{1}{A} \quad\Rightarrow\quad R_{T} > R_{1} A. \]
Con i dati del punto~\ref{sec:1kiloohm} si ottiene
\[ R_{T} > \SiVar{2.b}{RTsogliab}. \]
Per quanto riguarda il punto~\ref{sec:4megaohm} possiamo invece usare la relazione esatta
\[ R_{T} = R_{1} \frac{\bar{A}A\ped{exp}}{A\ped{exp} - \bar{A}} = \SiVar{2.d}{RTsogliac}. \]
dove abbiamo utilizzato la media degli $ A $ misurati
\[ \bar{A} = \SiVar{2.c}{Abar}. \]

\subsection{Partitore di corrente}
Valori misurati:
\[
    V\ped{in} = \SiVar{2.e}{Vin} \quad
    R_1 = \SiVar{2.e}{R1} \quad
    R_2 = \SiVar{2.e}{R2} \quad
    R_3 = \SiVar{2.e}{R3}
\]
Valori attesi sulla base dei valori dei componenti:
\[
    I\ped{tot,exp} = \SiVar{2.e}{Itot} \quad
    I\ped{1,exp} = \SiVar{2.e}{I1} \quad
    I\ped{2,exp} = \SiVar{2.e}{I2}
\]
Valori delle correnti misurate con amperometro digitale:
\[
    I_1 = \SiVar{2.e}{I1-dig} \quad
    I_2 = \SiVar{2.e}{I2-dig}
\]
Dalla caduta su $ R_{3} $, $ \Delta V_{R_{3}} = \SiVar{2.e}{VR3} $, si ottiene
\[ I\ped{tot} = \frac{\Delta V_{R_{3}}}{R_{3}} = \SiVar{2.e}{Itotmeas} \]
La discrepanza tra correnti attese e correnti misurate si può ascrivere all'impedenza interna degli amperometri. Infatti da queste misure possiamo ricavare una stima della resistenza interna del multimetro in modalità amperometro. Tenendo conto della resistenza $ r $ del tester quando messo in serie con $ R_{1} $, vale la relazione
\[
    I_{1} = I\ped{tot} \frac{R_2}{R_1 + R_2 + r}
\]
da cui si ottiene
\[
    r = R_{2} \frac{I\ped{tot}}{I_{1}} - R_1 - R_2 = \SiVar{2.e}{r1}.
\]
Similmente se l'amperometro viene messo in serie con $ R_{2} $ si ricava
\[
    I_{2} = I\ped{tot} \frac{R_1}{R_1 + R_2 + r'} \quad \Rightarrow \quad r' = R_{1} \frac{I\ped{tot}}{I_{2}} - R_1 - R_2 = \SiVar{2.e}{r2}
\]
Osserviamo che $ r $ ed $ r' $ risultano quindi compatibili (non abbiamo cambiato scala tra le due misure), ma sono affetti da incertezze di oltre il $ 20\% $.

\section{Uso dell'oscilloscopio}
\stepcounter{subsection}
\subsection{Misure di tensione}
Vengono ripetute le misure del punto~\ref{sec:4megaohm}\footnote{Ci siamo resi conto a posteriori di aver preso queste misure con l'oscilloscopio con la scala impostata su \texttt{x10} e pertanto abbiamo solo esplorato tensioni fino ad $ \sim \SI{1}{\volt} $.}, con pochi punti e senza grafico, e calcolato il guadagno del partitore.
Le resistenze misurate e il valore di $ A\ped{exp} $ sono
\[
    R_1 = \SiVar{3.b}{R1} \qquad
    R_2 = \SiVar{3.b}{R2} \qquad
    A\ped{exp} = \SiVar{3.b}{A}
\]
I risultati sono riportati in Tabella~\ref{tab:4megaohmoOsc}. Osserviamo che i rapporti tra le tensioni sono tra di loro compatibili ma non con il valore di $ A\ped{exp} $.

\begin{table}[h]
    \centering
    \tab{3.b}{tab}
    \caption{\label{tab:4megaohmoOsc}partitore di tensione con resistenze da circa \SI{4}{\mega\ohm}, misura con oscilloscopio.}
\end{table}

\stepcounter{subsection}
\subsection{Impedenza di ingresso dell'oscilloscopio} Si ripete l'analisi del punto~\ref{sec:resTester} da cui si ottiene
\[
    R\ped{in} = R_{1} \frac{\bar{A}A\ped{exp}}{A\ped{exp} - \bar{A}} = \SiVar{3.c}{Rin}
\]
dove abbiamo utilizzato la media degli $ A $ misurati
\[ \bar{A} = \SiVar{3.c}{Amean}. \]
Il valore dell'impedenza in ingresso dell'oscilloscopio è comparabile con quello riportato nel manuale, pari a $ \SI{1}{\mega\ohm} \pm 2\% $. Come nel punto~\ref{sec:4megaohm} non possiamo trascurare la resistenza interna dell'oscilloscopio nella previsione di $ V\ped{out} / V\ped{in} $ in quanto la resistenza in ingresso $ R\ped{in} $ risulta addirittura minore di $ R_{1} $ e $ R_{2} $, da cui la marcata incompatibilità tra $ A\ped{exp} $ e $ \bar{A} $.

\section{Misure di frequenza e tempo}
\stepcounter{subsection}
\subsection{Misure di frequenza}
Misure con onda sinusoidale:
\begin{itemize}
\item Periodo $T \pm \Delta T$;
\item Frequenza misurata con i cursori: $f \pm \Delta f$;
\item Frequenza misurata con la funzione misura dell'oscilloscopio: $f\ped{osc}$;
\item Frequenza di trigger: $f\ped{trg}$.
\end{itemize}

\begin{table}[h]
    \centering
    \tab{4.b}{tab}
    \caption{misura di frequenza di onde sinusoidali e confronto con misurazioni interne dell'oscilloscopio.}
\end{table}

\subsection{Misure di duty cyle}
Misure con onda quadra:
\begin{itemize}
\item Periodo $ T \pm \Delta T $;
\item Tempo ``alto'' $ t\ped{H} \pm \Delta t\ped{H} $;
\item Duty cycle $ \mathfrak{D} = t\ped{H} / T $.
\end{itemize}
\begin{table}[h]
    \centering
    \tab{4.c}{tab}
    \caption{misura di duty cycle per onde quadre.}
\end{table}

\section{Trigger dell'oscilloscopio}
\stepcounter{subsection}
\subsection{Segnale pulse}
Spostando il livello di trigger il segnale si sposta orizzontalmente, in quanto il livello di trigger corrisponde all'intersezione del segnale con l'asse verticale che passa per il centro dello schermo.

L'onda quadra è sfasata di $ \pi $ rispetto al segnale pulse; l'onda triangolare è crescente quando il segnale pulse è alto e viceversa; l'onda sinusoidale è decrescente quando il segnale pulse è alto e viceversa.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=1]{data/4b.png}
    \caption{relazione temporale tra il segnale pulse e l'onda principale.}
\end{figure}

\subsection{Tempo di salita e di discesa}
Misure su onda quadra
\[
    f = \SiVar{5.c}{freq} \quad
    t\ped{sal} = \SiVar{5.c}{tsal} \quad
    t\ped{dis} = \SiVar{5.c}{tdis}
\]
Si osserva chiaramente che l'onda misurata non è perfettamente quadra (cosa che tra l'altro implica tempo di salita e discesa non trascurabili), ma presenta il caratteristico andamento ``a pinna di squalo''. Questo si può attribuire a impedenze non reali certamente presenti, sia nei fili che all'interno dell'oscilloscopio.

\section{Conclusioni e commenti finali}
Dalle misure sul partitore di tensione si evince, come atteso, che il multimetro operante come voltmetro, poiché non è uno strumento ideale, non fornisce una stima attendibile delle misure di differenze di potenziale quando l'impedenza interna ha modulo comparabile con quella in uscita dal circuito. Questo è ancora più evidente nel caso dell'oscilloscopio, che ha impedenza in ingresso minore di quella in uscita del circuito.

Analogamente, il multimetro in modalità di amperometro diverge dal comportamento ideale quando l'impedenza interna diventa comparabile con quella del circuito a cui viene messo in serie.

\dichiarazione

\end{document}
