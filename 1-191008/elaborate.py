#!/usr/bin/env python3

from lab import run_chain

run_chain(
    ['cose.py', 'plot.py data/2.b.txt', 'plot.py data/2.c.txt']
)
