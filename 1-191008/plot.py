#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import sys
from os.path import splitext, basename
from os import sys

plt.style.use('../style.yml')

x, dx, y, dy = np.loadtxt(sys.argv[1], unpack=True)

plt.errorbar(x, y, xerr=dx, yerr=dy, linestyle='', marker='')
plt.xlabel("$ V_\\mathrm{in} [\\mathrm{V}] $")
plt.ylabel("$ V_\\mathrm{out} [\\mathrm{V}] $")

plt.savefig(f'img/{splitext(basename(sys.argv[1]))[0]}.png')
