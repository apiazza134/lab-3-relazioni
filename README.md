![build status](https://gitlab.com/apiazza134/lab-3-relazioni/badges/master/pipeline.svg)
# Relazioni di Laboratorio 3 (A.A. 2019-2020)

## Installazione
Sono richiesti
- `python 3.7.5`
- `lua 5.3.3`
- `lua-yaml`

Siccome `lualatex` fa schifo bisogna aggiungere [queste cose](https://gitlab.com/marco-venuti/python-latex/raw/master/texmf.cnf) al file `/usr/local/texlive/2019/texmf.cnf` affinché lua veda i path delle cose.

## Pacchetti python
Al solito
```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

## Lab Agile
Per usare il pacchetto nel sottomodulo
```
git submodule update --init --recursive
```
E poi
```
cd lab
pip install -r requirements.txt
python setup.py install
cd ..
```

## PDF

### Esperienze di elettronica
- Relazione 1 - 08/10/2019: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E01.1.AB.pdf?job=rel-1)
- Relazione 2 - 15/10/2019: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E02.1.AB.pdf?job=rel-2)
- Relazione 3 - 29/10/2019: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E03.1.AB.pdf?job=rel-3)
- Relazione 4 - 05/11/2019: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E04.1.AB.pdf?job=rel-4)
- Relazione 5 - 12/11/2019: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E05.1.AB.pdf?job=rel-5)
- Relazione 6 - 19/11/2019: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E06.1.AB.pdf?job=rel-6)
- Relazione 7 Marco - 26/11/19: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E07.1.AB.pdf?job=rel-7-marco)
- Relazione 7 Ale - 26/11/19: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E07.1.AB.pdf?job=rel-7-ale)
- Relazione 8 - 03/12/19: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E08.1.AB.pdf?job=rel-8)
- Relazione 10 - 20/02/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E10.1.AB.pdf?job=rel-10)
- Relazione 11 - 25/02/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E11.1.AB.pdf?job=rel-11)
- Relazione 12 - 03/03/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E12.1.AB.pdf?job=rel-12)
- Relazione 13 - 07/04/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E13.1.AB.pdf?job=rel-13)

### Esperienze di fisica
*Attenzione*: i nomi dei _.pdf_ sono sbagliati
- Relazione 14 - 17/04/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E14.1.AB.pdf?job=rel-14)
- Relazione 15 - 17/04/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E15.1.AB.pdf?job=rel-15)
- Relazione 16 - 28/04/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E16.1.AB.pdf?job=rel-16)
- Relazione 17 - 28/04/20: [PDF](https://gitlab.com/apiazza134/lab-3-relazioni/-/jobs/artifacts/master/raw/E17.1.AB.pdf?job=rel-17)
