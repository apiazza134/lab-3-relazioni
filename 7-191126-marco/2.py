#!/usr/bin/env python3

from uncertainties import unumpy as unp
import numpy as np
import matplotlib.pyplot as plt
from cmath import phase
from lab import (
    Data, loadtxtSorted, Table,
    measureMeanUncertaintyArray
)

plt.style.use('../style.yml')

d = Data()

f, df, Vs, divVs, Va, divVa, tau, dtau = loadtxtSorted("./data/2.txt")

f = unp.uarray(f, df)
Vs = measureMeanUncertaintyArray(Vs, divVs)
Va = measureMeanUncertaintyArray(Va, divVa)
tau = unp.uarray(tau, dtau)

L = Va/Vs
phi = 2*np.pi*f*tau

# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{S}', 'volt', Vs)
tab.pushColumn('V\\ped{A}', 'volt', Va)
tab.pushColumn('\\abs{L}', None, L)
tab.pushColumn('\\tau', 'second', tau)
tab.pushColumn('\\phi', 'radian', phi)

d.pushTable(tab, '2', 'tab')

# BODE
Ldb = 20*unp.log10(L)


# previsione

def trasf(x, R1, R2, R3, R4, R5, R6, RP, C1, C2):
    return (
        1/(1/R2 + 2j*np.pi*x*C2)
    )/(
        R1 + 1/(2j*np.pi*x*C1) + 1/(1/R2 + 2j*np.pi*x*C2)
    )*(1 + (R6+R4+R3)/(RP-R6 + R5))


R1 = d.get('1', 'R1')
R2 = d.get('1', 'R2')
R3 = d.get('1', 'R3')
R4 = d.get('1', 'R4')
R5 = d.get('1', 'R5')
RP = d.get('1', 'Rpot')
R6 = d.get('1', 'Rpotsopra')
C1 = d.get('1', 'C1')
C2 = d.get('1', 'C2')


# calcolo le cose
X = np.logspace(np.log10(min(unp.nominal_values(f))), np.log10(max(unp.nominal_values(f))))

ampiezza = 20*np.log10(abs(trasf(X, *unp.nominal_values([R1, R2, R3, R4, R5, R6, RP, C1, C2]))))
fase = np.array(list(
    map(lambda x: phase(trasf(x, *unp.nominal_values([R1, R2, R3, R4, R5, R6, RP, C1, C2]))), X)
))

# disegno le cose

## ampiezza
plt.xscale('log')
plt.xlabel('$ f [\\mathrm{Hz}]$')
plt.ylabel('$ L [\\mathrm{dB}]$')

plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(Ldb),
    xerr=unp.std_devs(f), yerr=unp.std_devs(Ldb),
    linestyle='', elinewidth=1, color='black'
)

plt.plot(X, ampiezza)
plt.savefig('./img/bodeAmpiezza.png')

# plt.show()
plt.close()

## fase
plt.xscale('log')
plt.xlabel('$ f [\\mathrm{Hz}]$')
plt.ylabel('$ \\phi [\\mathrm{rad}]$')

plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(phi),
    xerr=unp.std_devs(f), yerr=unp.std_devs(phi),
    linestyle='', elinewidth=1, color='black'
)

plt.plot(X, fase)

plt.savefig('./img/bodeFase.png')
# plt.show()

# frequenza per cui la fase si annulla
d.push(1/(2*np.pi*unp.sqrt(R1*R1*C1*C2)), 'hertz', '2', 'f0exp')
d.save()
