#!/usr/bin/env python3

from uncertainties import ufloat
import numpy as np
import matplotlib.pyplot as plt
from lab.data import Data

plt.style.use('../style.yml')

d = Data()

# resistenza che non ho misurato
RP = d.get('1', 'Rpot')
RPsotto = d.get('5', 'Rpotsotto')
Rp = RP.nominal_value - RPsotto.nominal_value
dRp = Rp*0.008
d.push(ufloat(Rp, dRp), 'ohm', '5', 'Rp')

# guadagno misurato
Vout = d.get('5', 'Vout')
Vs = d.get('5', 'Vs')

A = Vout/Vs
d.push(A, None, '5', 'A')

# plot dell'innesco
t, v = np.loadtxt('data/innesco.txt', unpack=True)
plt.plot(t, v, linewidth=0.5)
plt.xlabel('$ t [\\mathrm{s}]$')
plt.ylabel('$ V [\\mathrm{V}]$')
plt.savefig('./img/innesco.png')
# plt.show()

d.save()
