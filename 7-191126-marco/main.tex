\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo
\author{Marco Venuti} % lo riaffermo a mano così non devo cambiarlo di là

\begin{document}
\maketitle

\section{Misura del loop gain}
Si è innazi tutto assemblato il circuito con i seguenti valori misurati:
\begin{gather*}
  R_{1} = \sivar{1}{R1} \quad R_{2} = \sivar{1}{R2} \quad R_{3} = \sivar{1}{R3} \\
  R_{4} = \sivar{1}{R4} \quad R_{5} = \sivar{1}{R5} \quad R \ped{P} = \sivar{1}{Rpot} \\
  C_{1} = \sivar{1}{C1} \quad C_{2} = \sivar{1}{C2}
\end{gather*}
Si è inoltre misurato
\[ V \ped{CC} = \sivar{1}{VCC} \qquad V \ped{EE} = \sivar{1}{VEE} \]
In Tabella~\ref{tab:bode} si riportano, al variare della frequenza, le misure di $ V \ped{S} $, $ V \ped{A} $ e del tempo $ \tau $ intercorso tra le intersezioni di $ V \ped{S} $ e $ V \ped{A} $ con l'asse $ V = 0 $ (misurate nelle ``salite'').

\begin{table}[h!]
  \centering
  \tab{2}{tab}
  \caption{\label{tab:bode}risposta in frequenza del circuito e loop gain in funzione della frequenza.}
\end{table}

Si riportano in Figura~\ref{fig:bodeAmpiezza} il diagramma di Bode per il loop gain e in Figura~\ref{fig:bodeFase} lo sfasamento tra $ V \ped{S} $ e $ V \ped{A} $ in funzione della frequenza. Si riportano inoltre, sovrapposti, i grafici per il loop gain atteso e relativa fase:
\[ L(f) = A_{v} \beta(f) \]
dove $ A_{v} $ è il guadagno dell'amplificatore non invertente
\begin{equation}\label{eq:guadagnoOpAmp}
  A_{v} = 1 + \frac{R_{3} + R_{4} + R\ped{p}}{R \ped{P} - R \ped{p} + R_{5}}
\end{equation}
essendo $ R \ped{p} $ la resistenza del potenziometro tra il ramo di $ V_{-} $ e il ramo con $ R_{4} $ e $ R \ped{P} $ la resistenza totale del potenziometro; $ \beta $ è invece il rapporto di partizione del partitore dato dalla serie di $ R_{1} $ e $ C_{1} $ e dal parallelo di $ R_{2} $ e $ C_{2} $:
\[
  \beta = \frac{
    \dfrac{1}{\frac{1}{R_{2}} + 2 \pi f C_{2}}
  }{
    R_{1} + \dfrac{1}{2 \pi f C_{1}} + \dfrac{1}{\frac{1}{R_{2}} + 2 \pi f C_{2}}
  }.
\]
Le misure in Tabella~\ref{tab:bode} sono state effettuate con
\[ R \ped{p} = \sivar{1}{Rpotsopra}. \]
Si osserva dal grafico un buon accordo tra i valori attesi e il loop gain misurato. \\

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:bodeAmpiezza}In nero i dati $ \abs{V \ped{A}/V \ped{S}} $ in \si{\deci\bel}, in blu il loop gain atteso.]{\includegraphics[scale=0.7]{img/bodeAmpiezza}} \\
  \subfloat[\label{fig:bodeFase}In nero i dati della fase di $ V \ped{A}/V \ped{S} $, in blu lo sfasamento atteso.]{\includegraphics[scale=0.7]{img/bodeFase}}
  \caption{risposta in frequenza.}
\end{figure}

Si è determinata con l'oscilloscopio la frequenza per cui lo sfasamento è nullo
\[ f_{0} = \sivar{2}{f0}. \]
Per ottenere una stima teorica di tale valore, dato che $ A_{v} $ è sempre reale, basterà imporre che anche $ \beta $ sia reale:
\[
  \beta = \frac{1}{1 + \del{R_{1} + \dfrac{1}{2\pi f C_{1}}}\del{\dfrac{1}{R_{2}} + 2\pi f C_{2}}}
  = \frac{1}{\del{ \dfrac{R_{1}}{R_{2}} + \dfrac{C_{2}}{C_{1}} + 1 } + j \del{ 2 \pi f R_{1} C_{2} - \dfrac{1}{2 \pi f C_{1} R_{2}}}}
\]
\begin{equation}\label{eq:freqFaseNullaTeorica}
  \beta\in\R \quad\Rightarrow\quad \del{ 2 \pi f R_{1} C_{2} - \dfrac{1}{2 \pi f C_{1} R_{2}}} = 0 \quad\Rightarrow\quad f_{0}
  = \frac{1}{2\pi \sqrt{R_{1} R_{2} C_{1} C_{2}}}
\end{equation}
da cui
\begin{equation}\label{eq:freqFaseNulla}
  f \ped{0, exp} = \sivar{2}{f0exp},
\end{equation}
compatibile con quanto misurato. \\

Qualitativamente, si osserva che l'ampiezza del segnale $ V \ped{A} $ cresce con l'aumentare di $ R\ped{p} $ (e di conseguenza il diminuire della resistenza tra $ V_{-} $ e massa). Ciò è in accordo con quanto previsto dalla~\eqref{eq:guadagnoOpAmp} (infatti $ \beta $ dipende solo dalla frequenza, mantenuta costante mentre si ruota il potenziometro, e dai parametri costruttivi del circuito, quindi il loop gain è proporzionale ad $ A_{v} $). \\

Se i diodi non sono collegati, il loop gain non dipende, entro gli errori di misura ed entro i limiti dovuti all'ampiezza massima erogabile dall'OpAmp, dall'ampiezza $ V \ped{S} $.
Al contrario, se i diodi sono collegati, si osserva che il guadagno diminuisce quando l'ampiezza di $ V \ped{S}$ diventa troppo elevata. Questo è dovuto agli effetti non lineari del diodo che diminuiscono la resistenza efficace quando l'ampiezza diventa troppo elevata (si veda la sezione~\ref{sec:diodi}).

\section{Oscillatore}
Si è collegata l'uscita $ V_{+} $ dell'OpAmp al punto $ A $ del circuito. Prelevando con l'oscilloscopio il segnale $ V \ped{out} $, si osserva una forma d'onda oscillante.
Variando la posizione del potenziometro si regola l'ampiezza del segnale uscente; in particolare, sotto una certa soglia di $ R \ped{p} $ (data dalla~\eqref{eq:resPotInnesco}) non si osserva alcun segnale in uscita, oltre tale soglia si osserva un segnale qualitativamente sinusoidale (si veda la Figura~\ref{fig:sin}); aumentando ancora il valore di $ R \ped{p} $ il segnale aumenta di ampiezza.
Per ampiezze $ \gtrsim \SI{6}{\volt} $, il segnale inizia ad essere visibilmente distorto rispetto a una sinusoide, come si nota in Figura~\ref{fig:dist}. Infine, oltre un certo valore di $ R \ped{p} $ il segnale viene troncato al valore del \emph{voltage swing} (circa $ \SI{27}{\volt} $ picco-picco).

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:sin}]{\includegraphics[scale=0.85]{data/3-nodist}}
  \subfloat[\label{fig:dist}]{\includegraphics[scale=0.85]{data/3-diodi}}
  \caption{segnale $ V \ped{out} $ in uscita dall'oscillatore a ponte di Wien.}
\end{figure}

\section{Frequenza}
Dalla condizione $ A_{v} \beta(s) = 1 $ segue, ponendo per semplicità $ R_{1} = R_{2} \equiv R $ e $ C_{1} = C_{2} \equiv C $
\[
  s = \frac{(A_{v}-3) \pm i\sqrt{4 - (A_{v}-3)^{2}}}{2RC}
\]
cioè la frequenza di oscillazione in funzione di $ A_{v} $ è
\[
  f(A_{v}) = \frac{\sqrt{4-(A_{v}-3)^{2}}}{4\pi RC}
\]
che si verifica essere stazionaria per $ A_{v} = 3 $, cioè nella condizione di lavoro ideale (per cui $ \beta(s) $ è immaginario puro). Dunque al primo ordine in $ (A_{v}-3) $ non ci si aspettano variazioni nella frequenza.
Cercando la frequenza di oscillazione per $ R_{1} $, $ R_{2} $, $ C_{1} $, $ C_{2} $ generici si ottiene la stessa condizione espressa nella~\eqref{eq:freqFaseNullaTeorica}.

Si misura come frequenza di oscillazione del circuito
\[ f_{0} = \sivar{4}{f}, \]
compatibile con quella attesa data dalla~\eqref{eq:freqFaseNulla}.

In Figura~\ref{fig:fft} si riporta la \texttt{FFT} del segnale in uscita per due diverse scelte di $ V \ped{p} $ e quindi due diverse ampiezze di $ V \ped{out} $. Si nota che per ``grandi'' ampiezze sono presenti più picchi che per ``basse'' ampiezze, segno che il segnale è ``meno monocromatico'' e quindi più distorto.

Si osserva infine che il valore misurato per la frequenza rimane costante, entro gli errori di misura, al variare della posizione del potenziometro, cosa che si nota anche dalla vista \texttt{FFT}, mentre aumentando $ R \ped{p} $ aumenta l'ampiezza del segnale in uscita $ V \ped{out} $.

\begin{figure}[h!]
  \centering
  \subfloat[$ V \ped{out} = \SI{3.3 +- 0.1}{\volt} $]{\includegraphics[scale=0.85]{data/4-fft-1}}
  \subfloat[$ V \ped{out} = \SI{14.2 +- 0.7}{\volt} $]{\includegraphics[scale=0.85]{data/4-fft-2}}
  \caption{\label{fig:fft}\texttt{FFT} del segnale $ V \ped{out} $ in uscita dall'oscillatore a ponte di Wien.}
\end{figure}

\section{Innesco}
Il valore misurato della resistenza $ R \ped{p} $ nella posizione di innesco è\footnote{
  Nella pratica il punto di innesco è risultato difficilmente identificabile con precisione data l'elevata sensibilità della risposta del circuito alla posizione del potenziometro quando questo si trova attorno alla posizione che rende $ A \simeq 3 $.
}
\begin{equation}\label{eq:resPotInnesco}
  R \ped{p,min} = \sivar{5}{Rp}
\end{equation}
In tale condizione si è misurato
\[
  V \ped{S} = \sivar{5}{Vs} \qquad \sivar{5}{Va}
\]
da cui
\[
  A_{v} = \frac{V \ped{out}}{V \ped{S}} = \sivar{5}{A},
\]
compatibile con il valore atteso $ A_{v,\mathrm{exp}} = 3 $.
\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.85]{img/innesco}
  \caption{innesco dell'oscillatore (plot dei dati acquisiti con l'oscilloscopio).}
\end{figure}

\section{Funzione dei diodi}\label{sec:diodi}
Si riporta in Figura~\ref{fig:no-diodi} uno screenshot dell'oscilloscopio che mostra il segnale in uscita $ V \ped{out} $ dall'oscillatore a ponte di Wien con i diodi scollegati.
Si nota subito che l'ampiezza del segnale è maggiore rispetto ai casi precedenti (a parità di scelta di $ R \ped{p} $) e presenta ``clipping'' a un valore di $ \sim\SI{28}{\volt} $ picco-picco circa pari al voltage swing dell'OpAmp (da confrontare con il ``valore tipico'' dato dal datasheet di $ \SI{27}{\volt} $).

Si deduce che la funzione dei diodi è proprio quella di limitare l'ampiezza del segnale in uscita;
infatti, in presenza dei diodi in parallelo a $ R_{3} $, la curva tensione/corrente del sottocircuito costituito dai diodi e da $ R_{3} $ è come quella riportata in Figura~\ref{fig:caratteristica-wien} (simulata con LTspice con il valore nominale di $ R_{3} $) da cui consegue una ridotta ``resistenza efficace'' per valori della caduta ai capi di $ R_{3} $ maggiori di $ \sim V_{\gamma} \simeq\SI{0.6}{volt} $; la ridotta resistenza nel loop che va dal terminale ``$ - $'' al terminale ``out'' dell'OpAmp causa una minore amplificazione, in accordo con la~\eqref{eq:guadagnoOpAmp} e dunque un minore $ V \ped{out} $\footnote{
  Questi ragionamenti sono da intendersi in modo esclusivamente qualitativo; infatti, in presenza di componenti non lineari nel circuito, l'analisi nel dominio di Laplace perde di significato e non è più nemmeno possibile dare una buona definizione ad $ A_{v} $ e $ \beta(s) $, in quanto questi valori dipenderebbero dall'ampiezza dei segnali.
}. Si sottolinea infine che la scelta di usare due diodi e in versi opposti è necessaria per rendere simmetrica la caratteristica sopra citata e di conseguenza rendere simmetrico l'output del circuito.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{data/6-no-diodi}
  \caption{\label{fig:no-diodi}segnale $ V \ped{out} $ in uscita dall'oscillatore a ponte di Wien con i diodi scollegati.}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.7]{img/caratteristica-wien}
  \caption{\label{fig:caratteristica-wien}caratteristica tensione-corrente del sottocircuito costituito dai diodi e da $ R_{3} $. Si nota una regione centrale lineare in cui i diodi non entrano in azione, e due code che riproducono qualitativamente un andamento alla Shockley.}
\end{figure}


\dichiarazioneSingolo

\end{document}
