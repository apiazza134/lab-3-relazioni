#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

plt.style.use('../style.yml')

cusu, DV, I = np.loadtxt('data/caratteristica-wien.txt', unpack=True)

plt.plot(I, DV)
plt.xlabel('$ I\\ [\\mathrm{A}] $')
plt.ylabel('$ V\\ [\\mathrm{V}] $')
plt.savefig('./img/caratteristica-wien.png')
# plt.show()
