#!/usr/bin/env python3

from lab.data import Data

d = Data()

# 2.a
R1 = d.get('2.a', 'R1')
R2 = d.get('2.a', 'R2')

VCC = d.get('2.a', 'Vout')/2  # vogliamo usare V_{swing}

VS = R2*VCC/(R1+R2)
d.push(VS, 'volt', '2.a', 'VS')

# 2.c
d.push(2*VS, 'volt', '2.c', 'VSperdue')

d.save()
