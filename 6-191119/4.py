#!/usr/bin/env python3

from lab.data import Data
from uncertainties import unumpy

d = Data()

Vgamma = 0.7
VL = d.get('4.b', 'Vout')/2

R = d.get('3.b', 'R')
C = d.get('3.b', 'C')

R1 = d.get('3.b', 'R1')
R2 = d.get('3.b', 'R2')

DT = R*C*unumpy.log((1+Vgamma/VL)*(1+R2/R1))

d.recursivePush(f'{1e6*DT.nominal_value:.0f}', '4.c', 'DTexp')

d.save()
