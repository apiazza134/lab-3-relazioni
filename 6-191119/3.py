#!/usr/bin/env python3

from lab.data import Data
from uncertainties import unumpy

d = Data()

# 3.b periodo
R1 = d.get('3.b', 'R1')
R2 = d.get('3.b', 'R2')
R3 = d.get('3.b', 'R3')
R = d.get('3.b', 'R')
C = d.get('3.b', 'C')

T = 2*R*C*unumpy.log(1 + 2*R2/R1)
d.push(T, 'second', '3.b', 'Texp')

# 3.f slew rate
d.push(d.get('3.f', 'Dy')/d.get('3.f', 'Dx'), 'volt/\\second', '3.f', 'SR')

d.save()
