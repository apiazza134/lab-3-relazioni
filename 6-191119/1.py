#!/usr/bin/env python3

import matplotlib.pyplot as plt
import uncertainties.unumpy as unp
from uncertainties import correlated_values
import numpy as np
from scipy.optimize import curve_fit

from lab import (
    Data, Table, loadtxtSorted, cov2corr,
    measureMeanUncertaintyArray
)

plt.style.use('../style.yml')

# 1.b
d = Data()
Vshexp = 2 * d.get('1.b', 'Vin') * d.get('1.a', 'CT') / d.get('1.a', 'CF')
d.push(Vshexp, 'volt', '1.b', 'Vshexp')

# 1.c
Vin, divVin, h, dh = loadtxtSorted('data/1c.txt')

Vin = measureMeanUncertaintyArray(Vin, divVin)
h = unp.uarray(h, dh)

# Tabella
tab = Table()
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('h', 'second', h)
d.pushTable(tab, '1.c', 'tab')


# Fit
def f(x, a, b):
    return a*np.log(x) + b


init = (1, 1)
opt, cov = curve_fit(
    f, unp.nominal_values(Vin), unp.nominal_values(h),
    p0=init, sigma=unp.std_devs(h), absolute_sigma=True
)
yerr = np.sqrt(unp.std_devs(h)**2 + (opt[0]/unp.nominal_values(Vin))**2 * unp.std_devs(Vin)**2)
opt, cov = curve_fit(
    f, unp.nominal_values(Vin), unp.nominal_values(h),
    p0=tuple(opt), sigma=yerr, absolute_sigma=True
)

print(cov)
print(cov2corr(cov))

# Grafici

X = np.logspace(
    np.log10(min(unp.nominal_values(Vin))),
    np.log10(max(unp.nominal_values(Vin))),
    num=100
)

R1 = d.get('1.a', 'R1')
CF = d.get('1.a', 'CF')
CT = d.get('1.a', 'CF')
Vt = d.get('1.a', 'Vt')

A = R1 * CF
B = R1 * CF * unp.log(CF/(CT * Vt))
Y = A*unp.log(X) + B

plt.errorbar(
    unp.nominal_values(Vin), unp.nominal_values(h),
    xerr=unp.std_devs(Vin), yerr=unp.std_devs(h),
    linestyle='', color='black', elinewidth=0.8
)

confidence_plotstyle = dict(color='green', marker='', linestyle='dashed', linewidth='0.5')
plt.plot(X, unp.nominal_values(Y) + unp.std_devs(Y), **confidence_plotstyle)
plt.plot(X, unp.nominal_values(Y) - unp.std_devs(Y), **confidence_plotstyle)

plt.plot(X, f(X, *opt), color='red', linewidth='0.5')

plt.xscale('log')
plt.xlabel('$V_{\\mathrm{in}} [\\mathrm{V}]$')
plt.ylabel('$h [\\mathrm{s}]$')
plt.savefig("./img/1c.png")
# plt.show()

# Risultati del fit
chi2 = (((unp.nominal_values(h) - f(unp.nominal_values(Vin), *opt))/yerr)**2).sum()
opt = correlated_values(opt, cov)

d.push(A, 'second', '1.c', 'a')
d.push(B, 'second', '1.c', 'b')
d.push(opt[0], 'second', '1.c', 'afit')
d.push(opt[1], 'second', '1.c', 'bfit')
d.push(opt[1]-B, 'second', '1.c', 'bdiff')
d.recursivePush(f'{chi2:.1f}/{len(Vin)}', '1.c', 'chi2_ndof')

d.save()
