\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo

\begin{document}
\maketitle

\section{Amplificatore di carica}
\subsection{Funzionamento del circuito}\label{subsec:tot-previsioni}
\paragraph{Circuito di formazione}
Consideriamo il sotto-circuito formato dal condensatore $ C\ped{T} $ e dal circuito di formazione \texttt{X1}. La funzione di trasferimento che lega $ V\ped{in} $ a $ V\ped{sh} $ è data di fatto da quella di un amplificatore invertente con impedenze complesse: in trasformata di Laplace
\[
  \tilde{A}(s) = - \dfrac{\left(\dfrac{1}{R_{1}} + s C\ped{F}\right)^{-1}}{\dfrac{1}{s C\ped{T}}} = - \frac{C\ped{T}}{C\ped{F}} \frac{s}{s + \dfrac{1}{\tau}}
\]
con $ \tau \coloneqq R_{1} C\ped{F} $. In ingresso abbiamo un'onda quadra di periodo $ 2T $ (che prendiamo nulla per tempi negativi) che possiamo scrivere come
\[
  V\ped{in}(t) = \sum_{k=0}^{+\infty} (-1)^{k}f(t - kT)
  \qquad \text{dove} \quad
  f(t) = V_{0} \left[\theta(t) - \theta(t - T)\right]
\]
In trasformata di Laplace si ha
\[
  \tilde{f}(s) = V_{0}\left[\frac{1}{s} - \frac{e^{-sT}}{s}\right]
\]
da cui
\[
  \tilde{V}\ped{in}(s) = \sum_{k=0}^{+\infty} (-1)^{k} \tilde{f}(s) e^{-kTs} = \tilde{f}(s) \sum_{k=0}^{+\infty} (-1)^{k} e^{-kTs}
\]
La risposta del circuito in trasformata è
\[
  \tilde{V}\ped{sh}(s) = \tilde{A}(s) \tilde{V}\ped{in}(s) =  \tilde{A}(s)\tilde{f}(s) \sum_{k=0}^{+\infty} (-1)^{k}e^{-kTs} = \tilde{g}(s) \sum_{k=0}^{+\infty} (-1)^{k}e^{-kTs} = \mathcal{L}\left[\sum_{k=0}^{+\infty} (-1)^{k}g(t - kT)\right](s)
\]
Ora
\[
  \tilde{g}(s) = - V_{0} \frac{C\ped{T}}{C\ped{F}} \left[\frac{1}{s + \frac{1}{\tau}} - \frac{e^{-sT}}{s + \frac{1}{\tau}}\right]
\]
da cui, anti-trasformando
\[
  g(t) = - V_{0} \frac{C\ped{T}}{C\ped{F}} \left[e^{-t/\tau}\theta(t) - e^{-(t-T)/\tau} \theta(t - T) \right]
\]
Ma allora la risposta del circuito nel dominio del tempo è
\begin{align*}
  V\ped{sh}(t) &= - V_{0} \frac{C\ped{T}}{C\ped{F}} \cbr{ \sum_{k=0}^{+\infty} (-1)^{k}e^{-\frac{t - kT}{\tau}}\theta(t - kT) - \sum_{k=0}^{+\infty} (-1)^{k} e^{-\frac{t- (k+1) T }{\tau}} \theta(t - (k+1)T ) } \\
               &= - V_{0} \frac{C\ped{T}}{C\ped{F}} \cbr{ e^{-\frac{t}{\tau}} \theta(t) - 2 \sum_{k=1}^{+\infty} (-1)^{k} e^{-\frac{t - kT}{\tau}} \theta(t - kT) }
\end{align*}
ovvero, ignorando il transiente iniziale e supponendo $ \tau\ll T $\footnote{
  La buona definizione della somma è assicurata dal fatto che (tralasciando le costanti fisiche)
  \[ \sum_{k \geq 0} \theta(t-k) e^{-(t-k)} = \sum_{k=0}^{\lfloor t \rfloor} e^{-(t-k)} \leq \frac{e}{e-1} e^{-\{t\}}. \]
},
\begin{align}
  \label{eq:Vsh}
  V\ped{sh}(t) &\simeq 2 V_{0} \frac{C\ped{T}}{C\ped{F}} \sum_{k=1}^{+\infty} (-1)^{k} e^{-\frac{t - kT}{\tau}} \theta(t - kT) \nonumber\\
               &\simeq  2 V_{0} \frac{C\ped{T}}{C\ped{F}} \sum_{k=1}^{+\infty} (-1)^{k} e^{-\frac{t - kT}{\tau}} \chi_{[kT, (k+1)T]}(t)
\end{align}
Per il circuito in esame i valori nominali sono $ C\ped{T} = C\ped{F} = \SI{1}{\nano\farad} $, $ R_{1} = \SI{100}{\kilo\ohm} $, da cui $ \tau = \SI{100}{\micro\second} $, e $ 2T = \SI{10}{\milli\second} $. Dopo un transiente $ t\ll\tau $ in uscita ci aspettiamo quindi di osservare dei picchi esponenzialmente decrescenti di segno alterno e di ampiezza pari al doppio del segnale in ingresso. \\

\paragraph{Discriminatore}
Ignorando ora il condensatore $ C_{1} $ che ha il solo scopo di rimuovere il rumore ad alte frequenze dal generatore, il sotto-circuito \texttt{X2} è un discriminatore con tensione di soglia $ V\ped{t} $ data dal partitore di tensione collegato al terminale ``--'' dell'OpAmp
\[
  V\ped{t} = (1 - 2\alpha) V\ped{G}
\]
dove $ 0\leq \alpha \leq 1 $ è la ``frazione'' di resistenza data dal potenziometro e $ V\ped{G} = \SI{15}{\volt} $ (valore nominale) è la tensione di alimentazione. In altri termini la tensione di soglia è variabile da $ \SI{-15}{\volt} $ a $ \SI{15}{\volt} $. Supponendo di essere sempre in regime di saturazione, l'uscita del circuito è data da
\[
  V\ped{out} = V\ped{G} \sgn\left(V\ped{sh} - V\ped{t}\right).
\]
Più esplicitamente, usando $ V\ped{in} = \SI{6}{\volt} $ ci aspettiamo $ V\ped{sh} = \SI{12}{\volt} $ (ampiezze picco-picco), pertanto:
\begin{itemize}
\item se $ V\ped{t} > \SI{6}{\volt} $, $ V\ped{out} = -V\ped{G} $ costante.
\item se $ \SI{0}{\volt} < V\ped{t} < \SI{6}{\volt} $, ci aspettiamo (in un periodo)
  \[
    V\ped{out}(t) =
    \begin{cases}
      V\ped{G} & 0 < t < h \\
      - V\ped{G} & h < t < 2T
    \end{cases}
  \]
  dove $ h $ è il tempo in cui il picco esponenzialmente decrescente è maggiore della tensione di soglia, ovvero
  \begin{equation} \label{eq:tot-ampiezza-durata}
    h = \tau \log\left(\frac{C\ped{F}}{C\ped{T}} \frac{V\ped{in}}{V\ped{t}}\right).
  \end{equation}
\item se $ \SI{-6}{\volt} < V\ped{t} < \SI{0}{\volt} $, ci aspettiamo (in un periodo)
  \[
    V\ped{out}(t) =
    \begin{cases}
      V\ped{G} & 0 < t < T \\
      - V\ped{G} & T < t < T + h' \\
      V\ped{G} & T + h' < t < 2T
    \end{cases}
  \]
  dove $ h' $ è il tempo in cui il picco esponenzialmente crescente è minore della tensione di soglia, ovvero
  \[
    h' = \tau \log\left(\frac{C\ped{F}}{C\ped{T}} \frac{V\ped{in}}{\abs{V\ped{t}}}\right).
  \]
\item se $ V\ped{t} < \SI{-6}{\volt} $, $ V\ped{out} = -V\ped{G} $ costante.
\end{itemize}

\subsection{Confronto con le misure}
Si riportano i valori misurati relativi ai componenti utilizzato
\begin{gather*}
  R_{1} = \sivar{1.a}{R1} \qquad R_{2} = \sivar{1.a}{R2} \\
  C\ped{T} = \sivar{1.a}{CT} \qquad C\ped{F} = \sivar{1.a}{CF} \qquad C_{1} = \sivar{1.a}{C1}
\end{gather*}
Gli amplificatori operazionali utilizzati sono dei \texttt{TL081}, entrambi alimentati con
\[
  V\ped{E} = \sivar{1.a}{VE} \qquad V\ped{C} = \sivar{1.a}{VC}
\]
La posizione scelta del potenziometro è tale da fornire una tensione di soglia
\[
  V\ped{t} = \sivar{1.a}{Vt}.
\]
Si invia all'ingresso del circuito un'onda quadra di frequenza $ f = \sivar{1.b}{f} $ e ampiezza picco-picco $ V\ped{in} = \sivar{1.b}{Vin} $. In Figura~\ref{fig:tot-insh} si riportano il segnale in ingresso e il segnale prelevato all'uscita del primo OpAmp: $ V\ped{sh} $ rispecchia qualitativamente l'andamento atteso (equazione~\eqref{eq:Vsh}); con i cursori si misura un'ampiezza picco-picco del segnale
\[
  V\ped{sh} = \sivar{1.b}{Vsh}
\]
compatibile con il valore previsto
\[
  V\ped{sh, exp} = \sivar{1.b}{Vshexp}.
\]
In uscita si osserva il segnale riportato in Figura~\ref{fig:tot-shout} che rispecchia qualitativamente l'andamento atteso da quanto detto nella sezione~\ref{subsec:tot-previsioni}. Il segnale in uscita ha ampiezza picco-picco $ V\ped{out} = \sivar{1.b}{Vout} $, compatibile con quando atteso tenendo conto del fatto che, in realtà, in regime di saturazione l'operazionale produce tensioni pari a $ \pm \SI{13.6}{\volt} $ (\textit{voltage swing}, inferiore alla tensione di alimentazione). Inoltre come si osserva in Figura~\ref{fig:tot-shoutzoom} il segnale in uscita è ``alto'' quando $ V\ped{sh}(t) > V\ped{t} \sim \SI{200}{\milli\volt} $, e ``basso'' altrimenti.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1]{data/1b-in_sh}
  \caption{segnale in ingresso (\texttt{CH1}) sovrapposto a quello in uscita dal primo OpAmp (\texttt{CH2}).}
  \label{fig:tot-insh}
\end{figure}

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:tot-shout}]{\includegraphics[scale=0.85]{data/1b-sh_out}}
  \subfloat[\label{fig:tot-shoutzoom}]{\includegraphics[scale=0.85]{data/1b-sh_out-zoom}}
  \caption{segnale in uscita (\texttt{CH1}) sovrapposto a quello in ingrasso dal secondo OpAmp (\texttt{CH2}). A destra un'ingrandimento sulla  transizione dell'uscita da ``alto'' a ``basso''.}
\end{figure}

\subsection{Time over threshold}
Tenendo fissa la frequenza del segnale in ingresso e la tensione di riferimento del discriminatore $ V\ped{t} \simeq \SI{200}{\milli\volt} $, si varia l'ampiezza del segnale in ingresso $ V\ped{in} $ e si misura (con la funzione ``tempo alto'' dell'oscilloscopio) il tempo in cui il segnale in uscita è ``alto''. Si riportano in Tabella~\ref{tab:tot-ampiezza-durata} le misure fatte.
In Figura~\ref{fig:tot-ampiezza-durata} si riportano invece i dati sperimentali in scala semi-logaritmica.\\

Dall'equazione~\eqref{eq:tot-ampiezza-durata} ci aspettiamo una relazione del tipo $ y = a\cdot\log{x} + b $ tra $ V\ped{in} $ e $ h $, come possiamo effettivamente constatare dalla figura stessa.

Tuttavia tali valori non rispecchiano l'andamento atteso usando $ a = R_{1} C\ped{F} = \sivar{1.c}{a} $ e $ b = a\cdot\log{C\ped{T}/(C\ped{F} V\ped{t})} = \sivar{1.c}{b} $: osserviamo infatti che i dati sperimentali stanno ben al di fuori della banda di confidenza attesa.

Facendo un \textit{fit} numerico (iterato e con errori efficaci) usando come funzione modello $ y = a\cdot\log{x} + b $ si trovano i seguenti parametri di \textit{best-fit}
  \[
    a\ped{best} = \sivar{1.c}{afit} \qquad b\ped{best} = \sivar{1.c}{bfit} \qquad \mathrm{cov} = -0.54 \qquad \chi^{2}/\mathrm{dof} = \get{1.c}{chi2_ndof}
  \]
Osserviamo che $ a\ped{best} $ è compatibile con il valore atteso, mentre risulta $ b\ped{best} - b = \sivar{1.c}{bdiff} $. Tale discrepanza si può attribuire al fatto che le misure di $ h $ sono state effettuate usando la funzione ``misura tempo positivo/negativo'' dell'oscilloscopio: poiché la transizione da ``alto'' a ``basso'' (e viceversa) non è istantanea, quanto misurato dall'oscilloscopio risulta essere una sovrastima di $ h $ (stimabile a circa $ \sivar{1.c}{bdiff} $, comparabile con  quanto si può dedurre dalla Figura~\ref{fig:tot-shoutzoom}). \\

Tenendo ora fissato il segnale in ingresso a $ V\ped{in} \simeq \SI{6}{\volt} $ (ampiezza picco-picco), si modifica la posizione del potenziometro $ R_{3} $, così da cambiare la tensione di soglia $ V\ped{t} $ tra $ -\SI{15}{\volt} $ e $ \SI{+15}{\volt} $ (valori nominali). Qualitativamente osserva quanto previsto nella sotto-sezione~\ref{subsec:astabile-funzionamento}: se $ V\ped{t} $ è compreso tra $ \SI{-6}{\volt} $ e $ \SI{6}{\volt} $ si osserva in uscita un'onda quadra di frequenza fissa con duty-cyle\footnote{Rapporto tra il tempo in cui $ V\ped{out} $ è positivo e il tempo in cui $ V\ped{out} $ è negativo in un periodo.} che aumenta al diminuire di $ V\ped{t} $; se $ V\ped{t} $ è maggiore di $ \SI{6}{\volt} $ il segnale in uscita è sempre ``basso''; se $ V\ped{t} $ è minore di $ \SI{-6}{\volt} $ il segnale in uscita è sempre ``alto''.

\begin{table}[h!]
  \centering
  \get{1.c}{tab}
  \caption{durata del picco ``positivo'' del segnale in uscita in funzione dell'ampiezza del segnale in ingresso.}
  \label{tab:tot-ampiezza-durata}
\end{table}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.8]{img/1c}
  \caption{tempo per cui il segnale in uscita rimane ``alto'' in funzione dell'ampiezza picco-picco del segnale in ingresso; in nero sono riportati i dati sperimentali, in verde tratteggiato la banda di confidenza, in rosso la retta di \textit{best fit}.}
  \label{fig:tot-ampiezza-durata}
\end{figure}

\section{Trigger di Schmitt}
\subsection{Risposta ad un segnale sinusoidale}
Si è assemblato il circuito con i seguenti valori misurati delle resistenze
\[
  R_{1} = \sivar{2.a}{R1} \qquad R_{2} = \sivar{2.a}{R2}
\]

Si riporta in Figura~\ref{fig:2a-risposta-t} l'andamento temporale del segnale in uscita $ V \ped{out} $ quando il trigger viene alimentato con un segnale $ V \ped{in} $ sinusoidale. Si nota che la soglia che fa cambiare il valore di $ V \ped{out} $ è diversa per la ``salita'' e la ``discesa'' del segnale $ V \ped{in} $ (le soglie sono messe in evidenza con i cursori).

In Figura~\ref{fig:2a-risposta-xy} si riporta invece la vista dell'oscilloscopio in modalità \texttt{XY} nelle stesse condizioni, il che riproduce la figura di isteresi per il circuito (percorsa in senso orario).

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:2a-risposta-t}Risposta a un segnale sinusoidale]{
    \includegraphics[scale=0.85]{data/2a}
  }
  \subfloat[\label{fig:2a-risposta-xy}Visualizzazione XY ($ V \ped{in} $-$ V \ped{out} $)]{
    \includegraphics[scale=0.85]{data/2a-xy}
  }
  \caption{\label{fig:2a-risposta}segnale in ingresso (\texttt{CH1}) e in uscita (\texttt{CH2}) del Trigger di Schmidt.}
\end{figure}

\subsection{Funzionamento del circuito}
Supponiamo che l'OpAmp funzioni come discriminatore, cioè che la differenza tra $ V_{+} $ e $ V_{-} $ sia tale da farlo lavorare sempre in regime non lineare. Allora vale
\[ V \ped{out} = V \ped{CC} \sgn(V_{+} - V_{-}). \]
Inoltre, poiché $ V_{+} $ è collegato all'uscita del partitore, è
\[ V_{+} = \frac{R_{2}}{R_{1} + R_{2}} V \ped{out} = \beta V \ped{out} \]
e quindi
\[ V \ped{out} = V \ped{CC} \sgn(\beta V \ped{out} - V \ped{in}). \]
Da questa si conclude che il valore di $ V \ped{out} $ sarà ``alto'' (cioè pari a $ V \ped{CC} $\footnote{Più correttamente, pari al \emph{voltage swing}.}) nel semipiano $ V \ped{out} > \frac{V \ped{in}}{\beta} $ e ``basso'' nel semipiano $ V \ped{out} < \frac{V \ped{in}}{\beta} $. La transizione avviene quindi quando è
\[
  V \ped{in} = \pm V \ped{S} \qquad\text{con}\qquad V \ped{S} = \beta V \ped{CC} = \frac{R_{2}}{R_{1} + R_{2}} V \ped{CC}
\]
Il valore atteso per la soglia è
\[ V \ped{S,exp} = \sivar{2.a}{VS} \]
dove si è tenuto conto della non-idealità dell'OpAmp usando la semiampiezza picco-picco di $ V \ped{out} $ (pari al \emph{voltage swing}) in luogo di $ V \ped{CC} $:
\[ V \ped{out} = \sivar{2.a}{Vout}. \]
I valori misurati sono
\[
  V \ped{S,+} = \sivar{2.a}{Vs+}\qquad
  V \ped{S,-} = \sivar{2.a}{Vs-}
\]
compatibili con quanto atteso.

\subsection{Commenti}
\begin{itemize}
\item Si osserva che il circuito si comporta come discriminatore con isteresi per frequenze sufficientemente basse. Per frequenze maggiori di $ \sim\SI{10}{\kilo\hertz} $ la pendenza della transizione del segnale in uscita da ``alto'' a ``basso'' e viceversa comincia ad essere limitata dallo slew rate dell'OpAmp.

  Il regime in cui l'OpAmp si comporta da discriminatore con isteresi è quello in cui il tempo minimo di salita e discesa è molto minore del periodo del segnale in ingresso (e in uscita)
  \begin{equation}\label{eq:condizione-no-slew-rate}
    T \gg 2 t \ped{min} \qquad t \ped{min} = \frac{V \ped{out}}{\mathrm{SR}} \simeq \SI{2}{\micro\second} \quad\Rightarrow\quad f \ll \SI{250}{\kilo\hertz}.
  \end{equation}
  Effettivamente in Figura~\ref{fig:3-slewrate} si osserva che il tempo di salita è $ \sim \SI{2}{\micro\second} $, segno che la pendenza delle transizioni tra ``basso'' e ``alto'' sono limitate dallo slew rate.

  \begin{figure}[h!]
    \centering
    \includegraphics[scale=0.9]{data/2c-sr}
    \caption{\label{fig:3-slewrate}effetto dello \emph{slew-rate} sul segnale in uscita $ V \ped{out} $ ad alte frequenze.}
  \end{figure}

\item Variando l'ampiezza picco-picco $ V \ped{in} $ si osserva che l'ampiezza del segnale in uscita rimane eguale fino a una soglia inferiore, al di sotto della quale diventa costante.
  Infatti, affinché ciò non accada il circuito deve poter compiere il ciclo di isteresi, e dunque l'ampiezza picco-picco del segnale in ingresso deve essere maggiore della base del rettangolo di isteresi raffigurato in Figura~\ref{fig:2a-risposta-xy}:
  \[ V \ped{in} > 2 V \ped{S} = \sivar{2.c}{VSperdue} \eqqcolon V \ped{in,exp}\ap{min}. \]
  Sperimentalmente, si misura come ampiezza in ingresso minima
  \[ V \ped{in}\ap{min} = \sivar{2.c}{Vinmin} \]
  in accordo con quanto atteso.
\end{itemize}

\section{Multivibratore astabile}
\subsection{Funzionamento del circuito}\label{subsec:astabile-funzionamento}
La presenza del doppio \emph{clamp} con diodi Zener tra l'uscita del circuito e massa garantisce che i possibili valori di output siano
\[ V \ped{out} = \pm (V_{\gamma} + V_{z}) \simeq \pm \SI{7}{\volt} \]
purché il segnale in uscita superi tale valore (condizione certamente verificata se la differenza tra $ V_{+} $ e $ V_{-} $ è tale da far lavorare l'OpAmp in regime di saturazione).

Supponiamo allora di essere in un istante in cui $ V \ped{out} $ è ``alto'', cioè $ V \ped{out} = V_{\gamma} + V_{z} $ e il condensatore è scarico. Allora è
\[ V_{+} = (V_{\gamma} + V_{z}) \frac{R_{2}}{R_{1} + R_{2}} = (V_{\gamma} + V_{z}) \beta \qquad V_{-} = 0 \]
In questa situazione inizia a fluire corrente per la resistenza $ R $ e il condensatore si carica (con tempo caratteristico $ RC $) finché si raggiunge la condizione
\[ V_{-} = V_{+}. \]
Nonappena $ V_{-} $ diventa maggiore di $ V_{+} $\footnote{Questo è possibile grazie al fatto che il massimo valore raggiungibile da $ V_{-} $ è $ V_{\gamma} + V_{z}$, mentre il massimo valore raggiungibile da $ V_{+} $ è $ \beta (V_{\gamma} + V_{z}) $, con $ \beta < 1 $.}, infatti, $ V \ped{out} $ passa da ``alto'' a ``basso'' (dato che l'OpAmp sta agendo da discriminatore), così $ V_{-} < V \ped{out} $ e il condensatore inizia a scaricarsi. La scarica (sempre con tempo caratteristico $ RC $) continua finché $ V_{-} $ diventa minore di $ V_{+} $, quando $ V \ped{out} $ torna a essere alto e il ciclo ricomincia.
Il periodo di oscillazione è quindi il doppio del tempo in cui al condensatore è ``permesso'' caricarsi/scaricarsi.

Consideriamo ad esempio una scarica, e fissiamo l'origine del tempo nell'istante in cui il condensatore comincia a scaricarsi. Allora abbiamo
\[
  V \ped{out} = -(V_{\gamma} + V_{z}) \qquad
  V_{+} = -\beta (V_{\gamma} + V_{z}) \qquad
  V_{-}(t) = (V_{\gamma} + V_{z})\del{(1+\beta)e^{-t/(RC)} - 1}
\]
Imponendo $ V_{-}(t_{0}) = V_{+} = -\beta(V_{\gamma} + V_{z}) $ si ottiene $ t_{0} = RC \log\del{\frac{1+\beta}{1-\beta}} $ e quindi infine
\begin{equation}\label{eq:periodo-astabile}
  T = 2RC\log\del{1+ 2 \frac{R_{2}}{R_{1}}}.
\end{equation}


\subsection{Valori dei componenti}
I valori misurati delle resistenze sono
\[ R_{1} = \sivar{3.b}{R1} \qquad R_{2} = \sivar{3.b}{R2} \qquad R_{3} = \sivar{3.b}{R3} \]
Si sono inoltre scelti
\[ R = \sivar{3.b}{R} \qquad C = \sivar{3.b}{C} \]
così è, in base alla~\eqref{eq:periodo-astabile},
\[ T \ped{exp} = \sivar{3.b}{Texp}. \]
Si è infine misurato
\[ V \ped{CC} = \sivar{3.f}{VC} \qquad V \ped{EE} = \sivar{3.f}{VE} \]

\subsection{Osservazione del circuito}
Osservando il segnale in uscita $ V \ped{out} $ ci si attende, sulla base di quanto detto nella sotto-sezione~\ref{subsec:astabile-funzionamento}, un'onda quadra di ampiezza picco-picco $ 2(V_{\gamma} + V_{z}) \simeq \SI{14}{\volt} $ e periodo $ \sivar{3.b}{Texp} $, che è quanto effettivamente si è osservato (si riporta lo screenshot dell'oscilloscopio nella Figura~\ref{fig:astabile-out}).

Osservando $ V_{+} $ ci si aspetta un segnale identico a $ V \ped{out} $ ma ridotto di un fattore $ \beta \simeq 0.5 $; ciò si può verificare dalla Figura~\ref{fig:astabile-out-piu}.

Osservando $ V_{-} $, infine, ci si aspetta di vedere un segnale ``a pinna di squalo'', caratteristico del processo di carica/scarica di un condensatore, con la stessa ampiezza picco-picco di $ V_{+} $; questa previsione è confermata dalla Figura~\ref{fig:astabile-piu-meno}.

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:astabile-out}Vista del segnale $ V \ped{out} $]{
    \includegraphics[scale=0.9]{data/3c-out}
  }
  \subfloat[\label{fig:astabile-out-piu}Vista del segnale $ V \ped{out} $ (\texttt{CH1}) e del segnale $ V_{+} $ (\texttt{CH2})]{
    \includegraphics[scale=0.9]{data/3c-out_piu}
  }\\
  \subfloat[Vista del segnale $ V \ped{out} $ (\texttt{CH1}) e del segnale $ V_{-} $ (\texttt{CH2})]{
    \includegraphics[scale=0.9]{data/3c-out_meno}
  }
  \subfloat[\label{fig:astabile-piu-meno}Vista del segnale $ V_{+} $ (\texttt{CH1}) e del segnale $ V_{-} $ (\texttt{CH2})]{
    \includegraphics[scale=0.9]{data/3c-piu_meno}
  }
  \caption{\label{fig:astabile-segnali}segnali $ V \ped{out} $, $ V_{+} $ e $ V_{-} $ prodotti dal multivibratore astabile.}
\end{figure}

\subsection{Commenti}
\begin{itemize}
\item La funzione dei diodi Zener, già anticipata nella sotto-sezione~\ref{subsec:astabile-funzionamento}, è quella di troncare il valore di $ V \ped{out} $ a $ \pm (V_{\gamma} + V_{z}) $, così da riprodurre un segnale qualitativamente simile a un'onda quadra. La resistenza $ R_{3} $ è funzionale proprio al ``clamping'' del segnale, in quanto questa agisce limitando la corrente che scorre attraverso i diodi Zener quando sono uno in conduzione e l'altro in \emph{breakdown} (cioè quando il \emph{clamp} sta limitando il modulo di $ V \ped{out} $).
\item Variando l'ampiezza dell'alimentazione ($ V \ped{CC} $ e $ V \ped{EE} $) il periodo rimane approssimativamente costante fintanto che si mantiene $ V \ped{CC} = - V \ped{EE} \gtrsim V_{\gamma} + V_{z} $. Al di sotto di tale soglia i diodi zener non entrano in azione, per cui l'uscita $ V \ped{out} $ non sarà più un'onda quadra\footnote{
    In realtà tale effetto si verifica anche poco al di sopra della soglia indicata: a causa della caduta ai capi di $ R_{3} $, $ V \ped{out} $ può avere un valore inferiore a $ V_{\gamma}  + V_{z} $ anche se è $ V \ped{CC} > V_{\gamma} + V_{z} $; in tale caso si ha $ V \ped{out} = V \ped{CC} - R_{3} I $ (dove $ I $ è la corrente in uscita dall'OpAmp) e quindi ci si aspetta una correzione alla pura onda quadra con lo stesso andamento della corrente, in questo caso esponenziale per via della carica/scarica del condensatore. Questo si è effettivamente osservato ed è qualitativamente analogo a quanto è visibile nella Figura~\ref{fig:astabile-ampiezza-1} (anche se in quel caso era effettivamente $ V \ped{CC} < V_{\gamma} + V_{z} $).
  }. In questo caso, non essendo più $ V \ped{out} $ costante a tratti, la carica del condensatore non avverrà più solo sulla resistenza $ R $, ma sulla serie $ R + R_{3} $\footnote{
    Questo perché l'uscita dell'OpAmp rimane, al contrario, costante a tratti, nell'ipotesi (nel nostro caso sempre verificata) che la differenza tra $ V_{+} $ e $ V_{-} $ sia sempre tale da far lavorare l'OpAmp in regime di saturazione.
  }, con una correzione dovuta al fatto che parte della corrente fluisce a terra attraverso la serie $ R_{1} + R_{2} $. Inoltre, anche il valore di $ V_{+} $ non è più costante a tratti, ma ha una dipendenza non banale dal tempo, cosa che rende più complesso il calcolo del periodo (il calcolo del semiperiodo segue sempre dalla condizione $ V_{+}(t_{0}) = V_{-}(t_{0}) $). In generale, ci si aspetta comunque un aumento del periodo dato che aumenta la resistenza effettiva su cui il condensatore si carica/scarica; questo si è osservato sperimentalmente e se ne riporta un esempio nella Figura~\ref{fig:astabile-ampiezza-1}.

  Infine, se si alimenta l'OpAmp in modo asimmetrico (cioè $ V \ped{CC} \neq -V \ped{EE} $), sono diversi i moduli del valori di ``basso'' e ``alto'' che può assumere $ V \ped{out} $ e di conseguenza $ V_{+} $; ciò comporta che la transizione da ``basso'' ad ``alto'' avvenga per un valore soglia di $ V_{+} $ diverso rispetto alla soglia della transizione da ``alto'' a ``basso'' e di conseguenza i tempi di carica e scarica saranno diversi. In conclusione $ V \ped{out} $ riprodurrà un andamento qualitativamente simile a un'onda quadra (ma come prima, con una correzione esponenziale) con duty cycle diverso dal $ 50\% $ (si veda la Figura~\ref{fig:astabile-ampiezza-2}).

  \begin{figure}[h!]
    \centering
    \subfloat[\label{fig:astabile-ampiezza-1}$ V\ped{CC} = \SI{4.04 +- 0.02}{\volt} $, $ V\ped{EE} = \SI{-4.03 +- 0.02}{\volt} $]{\includegraphics[scale=0.9]{data/3e-1}}
    \subfloat[\label{fig:astabile-ampiezza-2}$ V\ped{CC} = \SI{15.06 +- 0.07}{\volt} $, $ V\ped{EE} = \SI{-4.25 +- 0.02}{\volt} $]{\includegraphics[scale=0.9]{data/3e-2}}
    \caption{\label{fig:astabile-ampiezza}vista del segnale $ V \ped{out} $ (\texttt{CH1}) e del segnale $ V_{-} $ (\texttt{CH2}) prodotti dal multivibratore astabile.}
  \end{figure}

\item Se si diminuisce il valore della resistenza $ R $, cosa che, in accordo con la~\eqref{eq:periodo-astabile}, comporta una diminuzione del periodo, si osserva un output come da Figura~\ref{fig-astabile-resistenza}. Questo differisce notevolmente da un'onda quadra a causa della caduta ai capi di $ R_{3} $, massima all'inizio di ogni carica/scarica, che ora è maggiore rispetto ai casi precedenti dato il basso valore della resistenza $ R $. Già a frequenze dell'ordine di qualche $ \si{\kilo\hertz} $, si osserva che il segnale viene fortemente attenuato.

  Se invece si mantiene il valore precedente di $ R $ e si diminuisce la capacità $ C $, si osserva un output come da Figura~\ref{fig:astabile-slew-rate}. In particolare abbiamo usato $ C = \sivar{3.f}{C} $. Notiamo che la scelta di abbassare $ C $ invece di $ R $ consente di andare a frequenze ben maggiori; tuttavia per frequenze di $ \sim\SI{100}{\kilo\hertz} $ entra in gioco lo slew rate finito dell'OpAmp.
  Misurando gli incrementi nella regione di transizione da ``alto'' a ``basso''
  \[ \Delta t = \sivar{3.f}{Dx} \qquad \Delta V = \sivar{3.f}{Dy} \]
  si stima
  \[ \mathrm{SR} = \sivar{3.f}{SR}. \]
  che è comparabile con il valore nominale dato dal \emph{datasheet} $ \mathrm{SR} = \SI{13}{\mega\volt/\second} $. Ciò è segno del fatto che sia lo slew rate a limitare la pendenza massima del segnale in uscita $ V \ped{out} $, come si vede dalla figura.
  La condizione da porre affinché il circuito funzioni a dovere è dunque analoga a quella indicata nella~\eqref{eq:condizione-no-slew-rate}, con in questo caso $ V \ped{out} \simeq \SI{7}{\volt} $:
  \[ T \gg 2t\ped{min} \simeq \SI{1}{\micro\second} \quad\Rightarrow\quad f \ll \SI{1}{\mega\hertz}. \]

  \begin{figure}[h!]
    \centering
    \subfloat[\label{fig-astabile-resistenza}$ R = \SI{730 +- 6}{\ohm} $]{\includegraphics[scale=0.9]{data/3f-1}}
    \subfloat[\label{fig:astabile-slew-rate}Effetto dello slew rate ad alte frequenze]{\includegraphics[scale=0.9]{data/3f-2}}
    \caption{\label{fig:astabile-frequenza}vista del segnale $ V \ped{out} $ (\texttt{CH1}) e del segnale $ V_{-} $ (\texttt{CH2}) prodotti dal multivibratore astabile abbassando la resistenza o la capacità.}
  \end{figure}
\end{itemize}

\section{Multivibratore monostabile}

Si è assemblato il circuito e si sono misurati:
\begin{gather*}
  V \ped{CC} = \sivar{4.b}{VC} \qquad V \ped{EE} = \sivar{4.b}{VE}\\
  V \ped{in} = \sivar{4.b}{Vin} \qquad V \ped{out} = \sivar{4.b}{Vout}\\
  f = \sivar{4.b}{f}
\end{gather*}
Si riportano in Figura~\ref{fig:monostabile} le acquisizioni con l'oscilloscopio dei segnali $ V \ped{trig} $, $ V_{+} $, $ V_{-} $ e $ V \ped{out} $.
I segnali osservati riproducono qualitativamente quanto atteso. In particolare, per $ V \ped{out} $ si osserva un'onda quadra con duty cycle, per $ V_{-} $ un segnale qualitativamente analogo a quello di ``carica e scarica'' di un condensatore, per $ V_{+} $ un segnale analogo a $ V \ped{out} $ ma ridotto di un fattore $ \frac{R_{2}}{R_{1} + R_{2}} \simeq 0.5 $.

Si è misurato il tempo per cui il segnale $ V \ped{out} $ rimane ``basso'' in ogni periodo:
\[ \Delta t = \sivar{4.c}{Dt}. \]
Il valore atteso è
\begin{equation} \label{eq:monostabile-tempopiccolomacaratteristico}
  \Delta t \ped{exp} = RC \log\sbr{\del{1+\frac{V_{\gamma}}{V \ped{out}/2}}  \del{1+\frac{R_{2}}{R_{1}}}} = \SI{\get{4.c}{DTexp}}{\micro\second},
\end{equation}
riportato senza incertezza data la natura nominale e indicativa del parametro $ V_{\gamma} \simeq \SI{0.7}{\volt} $. Il valore $ \Delta t \ped{exp} $ risulta comparabile con quello misurato.

\begin{figure}[h!]
  \centering
  \subfloat[Vista del segnale $ V \ped{trig} $ (\texttt{CH1}) e del segnale $ V \ped{out} $ (\texttt{CH2})]{\includegraphics[scale=0.9]{data/4b-in_out}}\\
  \subfloat[Vista del segnale $ V_{-} $ (\texttt{CH1}) e del segnale $ V \ped{out} $ (\texttt{CH2})]{\includegraphics[scale=0.9]{data/4b-meno_out}}
  \subfloat[Vista del segnale $ V \ped{trig} $ (\texttt{CH1}) e del segnale $ V_{+} $ (\texttt{CH2})]{\includegraphics[scale=0.9]{data/4b-in_piu}}
  \caption{\label{fig:monostabile}screenshot dell'oscilloscopio per i segnali generati dal multivibratore monostabile.}
\end{figure}

\subsection{Funzionamento del circuito}
In assenza del sotto-circuito di trigger, il sistema presenta un solo stato stabile in cui $ V\ped{out} =  V_{\gamma} + V_{z} $.
Come nel caso del multivibratore astabile, $ V\ped{out} $ può assumere solo come valori $ \pm V_{\gamma} + V_{z} $.
Se il condensatore è inizialmente scarico e se è $ V\ped{out} = V_{\gamma} + V_{z} $ allora il condensatore $ C $ si carica fino a quando la caduta ai suoi capi è $ V_{\gamma} $ essendo in parallelo ad un diodo; essendo quindi $ V\ped{+} \sim V_{\gamma} \simeq \SI{0.7}{\volt} $ e $ V_{-} = \frac{R_{2}}{R_{1} + R_{2}} V\ped{out} \simeq 0.5 \times \SI{7}{\volt} = \SI{3.5}{\volt} $, l'uscita rimane ``alta''.
Se il condensatore è inizialmente carico (con caduta ai capi pari a $ V_{\gamma} $) e se è $ V\ped{out} = -(V_{\gamma} + V_{z}) $, allora si scarica fintanto che è $ V_{-} > V_{+} \simeq -\SI{3.5}{\volt} $: a questo, essendo l'OpAmp in regime di saturazione, l'uscita passa a $ V\ped{out} = V_{\gamma} + V_{z} $ e il condensatore si carica come descritto sopra, ripristinando la condizione $ V_{+} \sim V_{\gamma} $. \\

Il sotto-circuito di trigger, formato dal passa-alto con il diodo in cascata, ha la funzione di ``sovrapporre'' a $ V_{+} $ dei picchi di differenza di potenziale negativa che portano il circuito nella configurazione instabile sopra descritta, che, dopo un tempo caratteristico dato dall'equazione~\eqref{eq:monostabile-tempopiccolomacaratteristico} torna nella configurazione stabile. \\

Come si può vedere in Figura~\ref{fig:monostabile}, quanto osservato sperimentalmente riproduce qualitativamente quanto descritto sopra.

\subsection{Commenti}
\begin{itemize}
\item La discesa di $ V\ped{trig} $ causa l'impulso in $ V\ped{out} $. La risalita in $ V\ped{out} $ accade quando $ V\ped{trig} $ è costante, prima che torni ``alto''. Tale risalita avviene infatti nel momento in cui $ V_{-} $ assume il valore minimo che fa ``scattare'' il discriminatore.

\item Si tiene fissa l'ampiezza di $ V\ped{trig} $ e se ne varia la frequenza. Si osserva che la durata dell'impulso in $ V \ped{out} $ risulta (entro gli errori di misura) indipendente dalla frequenza del segnale in ingresso, fino a quando la durata di tale impulso è minore del periodo di $ V\ped{trig} $. Al di sopra di tale frequenza critica (sperimentalmente $ f \sim \SI{974}{\hertz} $) il circuito smette di funzionare correttamente.
\item Si tiene la frequenza di $ V\ped{trig} $ e se ne varia l'ampiezza. La durata dell'impulso in $ V\ped{out} $ risulta (entro gli errori di misura) indipendente dall'ampiezza del segnale in ingresso fintanto $ V\ped{trig} $ è maggiore di una certa soglia. Si osserva sperimentalmente che al di sotto di $ V\ped{trig} \sim \SI{3.7}{\volt} $ il circuito rimane nella configurazione stabile. Infatti se $ V\ped{trig} $ è minore di una certa soglia ci aspettiamo che $ V_{+} $, dato dalla sovrapposizione dell'impulso prodotto dal sotto-circuito di trigger e del segnale uscente dal partitore, non sia abbastanza grande da rendere $ V_{+} - V_{-} $ negativo e far scattare il discriminatore.
\end{itemize}

\dichiarazione

\end{document}
