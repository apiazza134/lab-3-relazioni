\documentclass[a4paper, 11pt]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}
\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo

\begin{document}
\maketitle

\stepcounter{section}
\section{Funzionamento del \texttt{JFET} in continua}\label{sec:funzionamento}
Abbiamo utilizzato come resistenza di drain $ R\ped{D} = \sivar{2.z}{RD} $ e fissato le differenze di potenziale a $ V\ped{DD} = \sivar{2.z}{VDD} $ e $ V\ped{SS} = \sivar{2.z}{VSS} $.
\begin{enumerate}[label=\alph*.]
\item Per stimare la corrente che attraversa il \texttt{JFET} abbiamo misurato la caduta di potenziale ai capi della resistenza di drain, da cui $ I\ped{D} = V_{R\ped{D}} / R\ped{D} $. I valori misurati di $ V\ped{GS} $ e $ V_{R\ped{D}} $ al variare della posizione del potenziometro sono riportati in Tabella~\ref{tab:funzionamento}. $ V_{R\ped{D}} $ è stata misurata con il multimetro digitale, mentre $ V\ped{GS} $ con l'oscilloscopio.

    \begin{table}[h!]
        \centering
        \tab{2.a}{tab}
        \caption{\label{tab:funzionamento}}
    \end{table}

\item In regime di saturazione, cioè quando $ V\ped{DS} > V\ped{GS} - V\ped{P} $, ci aspettiamo che la corrente di drain dipenda in modo quadratico dalla differenza di potenziale tra gate e source, in particolare secondo la relazione
  \begin{equation}\label{eq:saturazione}
    I\ped{D} =
    \begin{dcases}
      \dfrac{I\ped{DSS}}{V\ped{P}^{2}} (V\ped{GS} - V\ped{P})^{2} & \text{se } V\ped{GS} > V\ped{P} \\
      0 & \text{se } V\ped{GS} \leq V\ped{P}
    \end{dcases}
  \end{equation}
  dove $ V\ped{P} $ è tensione di \textit{pinch-off} e $ I\ped{DSS} $ è la massima corrente di drain. Tuttavia per un \texttt{JFET} reale tale relazione non è ben verificata per ogni valore di $ V\ped{GS} $ tra $ 0 $ e $ V\ped{P} $ e quindi non è possibile stimare $ V\ped{P} $ e $ I\ped{DSS} $ con un \textit{fit} su tutti i dati acquisiti. Pertanto:
  \begin{itemize}
  \item Per $ V\ped{GS} > \SI{\get{2.a}{VH}}{\volt} $ abbiamo effettuato un \texttt{fit} lineare $ I\ped{D} = mV\ped{GS} + q $. I parametri di \textit{best fit} sono
    \[ m = \sivar{2.a}{m} \qquad q = \sivar{2.a}{Idss} \qquad \mathrm{corr} = \get{2.a}{corrlin} \]
    Da cui si ottiene la stima $ I\ped{DSS} = q = \sivar{2.a}{Idss} $.
    Tale stima è compatibile con quanto indicato sul datasheet: $ \SI{2}{\milli\ampere} \leq I\ped{DSS} \leq \SI{20}{\milli\ampere} $ nelle condizioni di test $ V\ped{DS} = \SI{15}{\volt} $.

  \item Per $ V\ped{GS} < \SI{\get{2.a}{VL}}{\volt} $ abbiamo effettuato un \textit{fit} con una funzione definita a tratti $ I\ped{D} = a(V\ped{GS} - b)^{2} $ per $ V\ped{GS} > b $ e $ I\ped{D} = 0 $ per $ V\ped{GS} \leq b $. I parametri ottenuti sono
    \[ a = \sivar{2.a}{A} \qquad b = \sivar{2.a}{Vpfit} \qquad \mathrm{corr} = \get{2.a}{corrpar} \]
    da cui si ottiene la stima $ V\ped{P} = b = \sivar{2.a}{Vpfit} $.
    Tuttavia, come è evidente dal grafico dei dati sperimentatali, non possiamo considerare questi valori come delle stime accurate della tensione di \textit{pinch-off}.
    Per tensioni vicine a $ V\ped{P} $ la tensione $ V_{R\ped{D}} $ diventa comparabile con la risoluzione del multimetro: risulta quindi difficile riuscire ad esplorare in modo efficace questa regione e l'incertezza ottenuta su $ V\ped{P} $ è sicuramente sottostimata\footnote{Facendo un \textit{fit} singolo con la funzione \texttt{curve\_fit} del pacchetto \texttt{scipy} di Python stiamo ignorando l'incertezza su $ V\ped{GS} $.} .

    Per le stime di seguito che coinvolgono il valore della tensione di \textit{pinch-off} abbiamo quindi deciso di assumere come errore quello associato a una ipotetica misura della tensione di \textit{pinch-off} effettuato con l'oscilloscopio: $ V\ped{P} = \sivar{2.a}{Vp} $.
    Tale stima è ragionevolmente compatibile con quanto riportato sul datasheet nel grafico $ V\ped{GS}-I\ped{DS} $ per $ I\ped{DSS} \simeq \SI{7}{\milli\ampere} $.

  \end{itemize}
  In Figura~\ref{fig:funzionamento} si riportano i dati sperimentali con sovrapposte le rispettive curve di \textit{best fit}.

  La condizione di saturazione è $ V\ped{DD} - V_{R\ped{D}} = V\ped{DS} > V\ped{GS} - V\ped{P} $ ovvero \[ I\ped{D} < - \frac{V\ped{GS}}{R\ped{D}} + \frac{V\ped{DD} + V\ped{P}}{R\ped{D}} \]
  che è quindi sempre verificata se $ I\ped{DSS} < (V\ped{DD} + V\ped{P})/R\ped{D} $. Nel nostro casi si ha $ I\ped{DSS} = \sivar{2.a}{Idss} $ e $ (V\ped{DD} + V\ped{P})/R\ped{D} = \sivar{2.a}{Ibound} $, quindi si verifica che il \texttt{JFET} è effettivamente in saturazione nelle misure effettuate.
\end{enumerate}

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\linewidth]{img/2a.png}
  \caption{corrente di drain in funzione della tensione tra il gate e il source. Sovrapposti i grafici delle funzioni di \textit{best fit} nelle regioni considerate.}
  \label{fig:funzionamento}
\end{figure}

\section{Punto di lavoro del \texttt{JFET} come amplificatore}
\begin{enumerate}[label=\alph*.]
\item Un limite superiore alla corrente si ottiene ponendo $ R \ped{S} = \SI{0}{\ohm} $ e $ V \ped{DS} = \SI{0}{\volt} $:
  \[ I \ped{D}\ap{max} = \frac{V \ped{DD}}{R \ped{D}}. \]
  La condizione desiderata sulla corrente quiesciente di drain si traduce quindi in una condizione sulla caduta di potenziale ai capi di $ R_{D} $:
  \[
    I \ped{D} \ap{Q} = \frac{I \ped{D}\ap{max}}{2} \quad\Rightarrow\quad
    \Delta V_{R_{1}} = R_{D} I \ped{D} \ap{Q} = \frac{V \ped{DD}}{2} \simeq \SI{7.5}{\volt}.
  \]
\item La posizione scelta del trimmer corrispondente a $ R\ped{S} = \sivar{3.a}{RS} $ ha prodotto $ \Delta V_{R_{1}} = \sivar{3.a}{VR1} $, da cui
  \[ I \ped{D} \ap{Q} = \frac{\Delta V_{R_{1}}}{R_{1}} = \sivar{3.b}{Idq}. \]
  dove $ R_{1} = \sivar{3.z}{R1} $. Si è inoltre misurato:
  \[ V \ped{DS} = \sivar{3.a}{VDS} \qquad V \ped{G} = \sivar{3.a}{VR2} \qquad V \ped{S} = \sivar{3.a}{VRS} \]
  da cui
  \[ V \ped{GS} = V \ped{G} - V \ped{S} = \sivar{3.b}{VGS} \]
  La differenza di potenziale $ V\ped{GS} $ è stata ottenuta come differenza tra $ V\ped{G} $ e $ V\ped{S} $ (misurate rispetto a massa) in quanto, essendo l'impedenza tra G ed S molto maggiore dell'impedenza in ingresso del multimetro digitale, una misura diretta di $ V\ped{GS} $ non sarebbe attendibile. Al contrario, la misura ai capi di $ R\ped{S} $ è attendibile poiché è $ R\ped{S} \ll \SI{10}{\mega\ohm} $, mentre la misura ai capi di $ R_{2} $ è una stima per eccesso di $ V\ped{G} $ (essendo $ R\ped{2} $ confrontabile con $ \SI{10}{\mega\ohm} $) che comunque risulta essere prossima a $ \SI{0}{\volt} $ e molto minore di $ V\ped{S} $, e quindi trascurabile nel calcolo di $ V\ped{GS} $.
\item
  Usando il valore sopra ottenuto per $ I \ped{D} \ap{Q} $ e i valori di $ I \ped{DSS} $ e $ V \ped{P} $ ottenuti nella sezione~\ref{sec:funzionamento} si ottiene
  \[ g \ped{m} = \frac{2}{\abs{V \ped{P}}} \sqrt{I \ped{DSS} I \ped{D} \ap{Q}} = \sivar{3.c}{gm}. \]
\end{enumerate}

\section{Misure a frequenza fissa}
Abbiamo scelto
\[ f = \sivar{4.z}{f} \]

\subsection{Configurazione Common Source}\label{subsec:commonSource}
\begin{enumerate}[label=\roman*.]
\item Si è verificata l'inversione di fase del segnale in uscita $ V \ped{drain} $ rispetto a quello in ingresso $ V \ped{in} $ (si veda la Figura~\ref{fig:4.a.inversione}).
  \begin{figure}[h!]
    \centering
    \includegraphics[scale=1]{data/4.a/inversione}
    \caption{inversione di fase di $ V \ped{drain} $ rispetto a $ V \ped{in} $ nella configurazione Common Source.}
    \label{fig:4.a.inversione}
  \end{figure}
\item\label{it:commonSourceGuadagnoPiccoliSegnali}
  Riportiamo in Tabella~\ref{tab:commonSource} i dati sperimentali di $ V \ped{drain} $ in funzione di $ V \ped{in} $ (picco--picco) misurati con l'oscilloscopio e dei rispettivi guadagni $ A_{v} = V \ped{drain}/V \ped{in} $.

  I dati sono inoltre riportati in Figura~\ref{fig:commonSource}; si nota che per valori di $ V \ped{in} \lesssim \SI{2.5}{\volt} $ l'andamento è approssimativamente lineare per poi saturarsi attorno a un valore di $ V \ped{drain} \sim \SI{13}{\volt} $.

  Abbiamo effettuato un fit con una retta $ V\ped{drain} = A V\ped{in} $ passante per l'origine per valori di $ V \ped{in} \leq \SI{1.7}{\volt} $, cioè per i quali l'andamento è approssimativamente lineare. Il parametro di best fit, che è il guadagno per ``piccoli segnali'', è
  \[ A = \sivar{4.a}{m}. \]
  che risulta compatibile con quanto atteso
  \[ \abs{A_{v}} = \frac{g\ped{m}R\ped{D}}{1 + g\ped{m} R\ped{S}} = \sivar{4.a}{Aexp}. \]

  \begin{table}[h!]
    \centering
    \tab{4.a}{tab}
    \caption{$ V \ped{drain} $ in funzione di $ V \ped{in} $ e conseguente guadagno $ A = \dfrac{V \ped{drain}}{V \ped{in}} $.}
    \label{tab:commonSource}
  \end{table}

  \begin{figure}[h!]
    \centering
    \includegraphics[scale=0.65]{img/commonSource.png}
    \caption{ampiezza picco--picco del segnale in uscita $ V \ped{drain} $ in funzione di quello in ingresso $ V \ped{in} $ (configurazione common source). Sono riportati i dati sperimentali e la retta di best fit per valori di $ V \ped{in} \leq \SI{1.7}{\volt} $.}
    \label{fig:commonSource}
  \end{figure}

\item
  In corrispondenza della regione non lineare descritta nel punto~\ref{it:commonSourceGuadagnoPiccoliSegnali}, cioè per valori $ V \ped{in} \gtrsim \SI{2.5}{\volt} $ si inizia a osservare all'oscilloscopio una distorsione del segnale in uscita, come si vede dalla Figura~\ref{fig:commonSourceDistorsione}.

  \begin{figure}[h!]
    \centering
    \subfloat{\includegraphics[scale=0.85]{data/4.a/nonlineare1}}
    \subfloat{\includegraphics[scale=0.85]{data/4.a/nonlineare2}}
    \caption{distorsione del segnale in uscita $ V \ped{drain} $ per valori troppo ``grandi'' di $ V \ped{in} $.}
    \label{fig:commonSourceDistorsione}
  \end{figure}

\item\label{it:clipping}
  La distorsione del segnale in uscita si manifesta soprattutto come ``clipping'' dei picchi dell'onda.

  Dato che la retta di carico resta fissata al variare di $ V \ped{in} $, dal grafico delle curve caratteristiche del transistor (nel piano $ I \ped{D} $ -- $ V \ped{DS} $) si evince che per valori ``troppo alti'' di $ V \ped{in} $ il punto di lavoro è vincolato a essere in zona ohmica e deve valere
  \[ I \ped{D} \simeq \frac{V \ped{DD}}{R \ped{D} + R \ped{S}} \qquad V \ped{DS} \simeq \SI{0}{\volt} \]
  per cui
  \[ V \ped{D} \simeq V \ped{S} = I \ped{D} R \ped{D} = \frac{V \ped{DD}}{1 + \dfrac{R \ped{D}}{R \ped{S}}} \]

  Per valori ``troppo bassi'' di $ V \ped{in} $ invece il punto di lavoro è vincolato a essere
  \[ I \ped{D} \simeq \SI{0}{\ampere} \qquad V \ped{DS} \simeq V \ped{DD} \]
  per cui
  \[
    V \ped{D} \simeq V \ped{DD} \qquad V \ped{S} \simeq \SI{0}{\volt}.
  \]
  Nella configurazione \emph{common source} ci aspettiamo quindi che il segnale abbia ampiezza massima picco--picco
  \[ \Delta V = \frac{V \ped{DD}}{1 + \dfrac{R \ped{S}}{R \ped{D}}} = \sivar{4.a}{DeltaVCS}. \]
  che è una buona stima di quanto misurato (si veda la Figura~\ref{fig:commonSourceDistorsione}).

\end{enumerate}

\subsection{Configurazione Common Drain}
\begin{enumerate}[label=\roman*.]
\item Osserviamo che in configurazione \emph{common drain} non è presente l'inversione del segnale. Abbiamo misurato $ V\ped{in} = \sivar{4.b}{Vin} $ e $ V\ped{source} = \sivar{4.b}{VSource} $ da cui si ricava (questa volta con una sola misura) una stima del guadagno
  \[ A_{v} = \frac{V\ped{source}}{V\ped{in}} = \sivar{4.b}{A}. \]
  che risulta comparabile (seppur non compatibile) con quanto atteso
  \[ A_{v} = \frac{g\ped{m}R\ped{S}}{1 + g\ped{m} R\ped{S}} = \sivar{4.b}{Aexp} \]
\item Per $ V\ped{in} \gtrsim \SI{2.5}{\volt} $ si segnale in uscita presenta delle distorsioni rispetto ad un segnale sinusoidale, come si può vedere in Figura~\ref{fig:commonDrainDistorsione}, segno di un comportamento non lineare del \texttt{JFET}. Si osserva inoltre un aumento dell'attenuazione del segnale in uscita al crescere di $ V\ped{in} $.
  \begin{figure}[h!]
    \centering
    \includegraphics[scale=0.85]{data/4.b/nonlineare1}
    \caption{distorsione del segnale in uscita $ V \ped{source} $ per valori troppo ``grandi'' di $ V \ped{in} $.}
    \label{fig:commonDrainDistorsione}
  \end{figure}

\item In base a quanto detto nel punto~\ref{it:clipping} della sottosezione~\ref{subsec:commonSource}, si ha che l'ampiezza massima picco--picco è
  \[ \Delta V \ped{S} = \frac{V \ped{DD}}{1 + \dfrac{R \ped{D}}{R \ped{S}}} = \sivar{4.b}{DeltaVCD} \]
  in accordo con le misure (si veda la Figura~\ref{fig:commonDrainDistorsione}).


\end{enumerate}


\section{Impedenza di ingresso dell'amplificatore}
\begin{enumerate}[label=\alph*.]
\item Data l'impedenza in ingresso ``infinita'' del transistor (nel senso di molto maggiore di ogni altra impedenza in gioco), l'impedenza in ingresso dell'amplificatore sarà semplicemente data dalla serie della capacità in ingresso e della resistenza di gate
  \begin{equation}\label{eq:ZinExp}
    Z \ped{in} = R_{2} + \frac{1}{2\pi j f C \ped{in}}\quad\Rightarrow\quad
    \abs{Z \ped{in,exp}}\equiv \abs{Z \ped{in}}
    = \sqrt{ R_{2}^{2} + \frac{1}{(2\pi f C \ped{in})^{2}}}
  \end{equation}
  Per ottenere una misura di $ \abs{Z \ped{in}} $ si può inserire una resistenza $ R \ped{L} $ in serie al generatore di $ V \ped{in} $ dello stesso ordine dell'impedenza in ingresso attesa. L'impedenza in ingresso sarà allora
  \[
    \abs{Z \ped{in}} = R \ped{L} \abs{\dfrac{V_{1}}{V_{2}} - 1}^{-1} = R \ped{L} \abs{  \abs{\dfrac{V_{1}}{V_{2}}}e^{i\phi} - 1 }^{-1}
  \]
  dove $ V_{1} $ è la tensione picco-picco in uscita (nella configurazione \emph{common source}\footnote{Il rapporto $ V_{1}/V_{2} $ non sarebbe cambiato se si fosse usata la configurazione \emph{source follower}.}) senza $ R \ped{L} $ e $ V_{2} $ quella con $ R \ped{L} $ e $ \phi $ è la fase relativa tra $ V_{1} $ e $ V_{2} $ \footnote{Il segno dello sfasamento è irrilevante ai fini del calcolo del modulo.}.
  Per due diverse frequenze abbiamo ottenuto
  \begin{align*}
    f_{1} &= \sivar{5.a}{f}     &\Rightarrow&& \abs{Z \ped{in,exp}} &= \sivar{5.a}{ZinExp} \\
    f_{2} &= \sivar{5.a.bis}{f} &\Rightarrow&& \abs{Z \ped{in,exp}} &= \sivar{5.a.bis}{ZinExp}
  \end{align*}
  per cui abbiamo scelto una resistenza da
  \[ R \ped{L} = \sivar{5.a}{RL}. \]
  Rispettivamente per $ f_{1} $ e per $ f_{2} $ abbiamo ottenuto
  \[ \abs{Z \ped{in,1}} = \sivar{5.a}{Zin} \quad\text{e}\quad\abs{Z \ped{in,2}} = \sivar{5.a.bis}{Zin}. \]
  Il valore ottenuto non risulta compatibile con quello atteso nel caso di $ f_{1} $; nel ricavare la~\eqref{eq:ZinExp} si è infatti trascurata la capacità efficace in parallelo ai terminali G e S\footnote{Questa è anche la causa dello sfasamento non banale tra $ V_{1} $ e $ V_{2} $.}, che diventa trascurabile solo a basse frequenze, quando la sua impedenza diventa molto maggiore delle altre impedenze in gioco; già per frequenze comparabili con $ f_{1} $, invece, la capacità in parallelo fa (coerentemente con quanto ci si aspetta) diminuire l'impedenza equivalente in ingresso.
\item
  Per $ f_{3} = \sivar{5.b}{f} $ si è ottenuto
  \[ \abs{Z \ped{in,exp}} = \sivar{5.b}{ZinExp} \qquad \abs{Z \ped{in}} = \sivar{5.b}{Zin} \]
  che presenta gli stessi problemi (ma molto più accentuati data la bassa impedenza della capacità in parallelo a questa frequenza) della misura alla frequenza $ f_{1} $ sopra descritta.
\end{enumerate}


\section{Aumento del guadagno}
Lavorando a frequenza fissa $ f = \sivar{6.a}{f} $ abbiamo cambiato la posizione del trimmer al fine di massimizzare il guadagno del \texttt{JFET} in configurazione \emph{common source}.
Ciò si è verificato per $ R\ped{S} = \sivar{6.a}{RS} $, valore per cui si è ottenuto $ V\ped{in} = \sivar{6.a}{Vin} $ e $ V\ped{drain} = \sivar{6.a}{Vdrain} $; il guadagno è
\[ A_{v} = \frac{V\ped{drain}}{V\ped{in}} = \sivar{6.a}{A}. \]
In corrispondenza di tale valore della resistenza di source si è misurato $ V\ped{DS} = \sivar{6.a}{VDS} $ e $ V\ped{S} = \sivar{6.a}{VS} $.

Essendo $ V\ped{GS} \simeq - V\ped{S} = - R\ped{S} I\ped{D} $ possiamo ricavare la dipendenza di $ I\ped{D} $ da $ R\ped{S} $ a partire dall'equazione~\eqref{eq:saturazione}. Si ottene
\[
  I\ped{D}(R\ped{S}) = -\frac{V\ped{P}}{R\ped{S}} + \frac{V\ped{P}^{2}}{2I\ped{DSS} R\ped{S}^{2}} \left[1 \pm \sqrt{1 - \frac{4I\ped{DSS}}{V\ped{P}} R\ped{S}}\right].
\]
Tale relazione è valida fintanto che siamo in regime di saturazione. La condizione da imporre è $ V\ped{DS} > V\ped{GS} - V\ped{P} $: usando sempre $ V\ped{GS} \simeq -V\ped{S} $ si ottiene
\[
  I\ped{D}(R\ped{S}) < \frac{V\ped{DD} + V\ped{P}}{R\ped{D}}.
\]
Come si vede in Figura~\ref{fig:correnteRS} la corrente $ I\ped{D} $ è monotona decrescente in $ R\ped{S} $. La resistenza che satura la disuguaglianza può essere ricavata numericamente\footnote{Abbiamo usato la funzione \texttt{fsolve} del pacchetto \texttt{scipy} di Python.}: usando i valori di $ V\ped{D} $, $ R\ped{D} $, $ I\ped{DSS} $ e $ V\ped{P} $ (senza incertezza) si ottiene
\[
  R\ped{S}\ap{min} = \sivar{6.a}{RSmin}.
\]

Essendo ora $ g\ped{m} = g\ped{m}(I\ped{D}) $ e essendo nota la dipendenza di $ I\ped{D} $ da $ R\ped{S} $ possiamo fare un grafico della dipendenza del guadagno dalla resistenza $ R\ped{S} $. Come si vede in Figura~\ref{fig:guadagnoRS} tale grandezza è monotona decrescente in $ R\ped{S} $ ed è quindi massima al limite della zona di saturazione, ovvero per $ R\ped{S} = R\ped{S}\ap{min} $. Si ottene quindi
\[
  A\ped{exp}\ap{max} = \sivar{6.a}{Amaxexp}.
\]

Tale valore non risulta compatibile con quanto misurato effettivamente. Ci si poteva effettivamente aspettare di ottenere una stima per eccesso del guadagno massimo in quanto il limite della zona di saturazione non è affatto netto, ma anzi è presente una zona di transizione che comincia ben prima del momento in cui $ I\ped{D} = (V\ped{DD} + V\ped{P})/R\ped{D} $. La corrente massima prima di uscire dalla zona di saturazione è pertanto minore, il che porta ad un aumento della resistenza minima e un conseguente abbassamento del guadagno massimo atteso.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.7]{img/correnteRS}
  \caption{corrente che attraversa il \texttt{JFET} in funzione della resistenza di source. In rosso la corrente massima affinché il \texttt{JFET} sia in saturazione.}
  \label{fig:correnteRS}
\end{figure}


\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.7]{img/guadagnoRS}
  \caption{guadagno dell'amplificatore in funzione della resistenza di source. In rosso la resistenza minima per essere in saturazione.}
  \label{fig:guadagnoRS}
\end{figure}

\dichiarazione

\end{document}
