#!/usr/bin/env python3

from lab.data import Data

d = Data()

VDD = d.get('3.a', 'VDD')
RS = d.get('3.a', 'RS')
RD = d.get('3.z', 'R1')

DeltaVCS = VDD/(1 + RS/RD)
DeltaVCD = VDD/(1 + RD/RS)

d.push(DeltaVCS, 'volt', '4.a', 'DeltaVCS')
d.push(DeltaVCD, 'volt', '4.b', 'DeltaVCD')

d.save()
