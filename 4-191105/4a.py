#!/usr/bin/env python3

from uncertainties import unumpy, correlated_values
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from lab import (
    Data, Table, loadtxtSorted,
    measureMeanUncertaintyArray
)


plt.style.use('../style.yml')

d = Data()
Vin, divVin, Vout, divVout = loadtxtSorted('data/4a.txt')

Vin = measureMeanUncertaintyArray(Vin, divVin)
Vout = measureMeanUncertaintyArray(Vout, divVout)

tab = Table()
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{drain}', 'volt', Vout)
tab.pushColumn('A', None, Vout/Vin)
d.pushTable(tab, '4.a', 'tab')


# Modello
def retta(x, m):
    return m*x


# faccio il fit per bassi Vin
soglia = 1.8
Vinbassi = Vin[Vin < soglia]
Voutbassi = Vout[Vin < soglia]

# fit iterato, ma cambia poco poco
init = (4)
opt, cov = curve_fit(
    retta, unumpy.nominal_values(Vinbassi), unumpy.nominal_values(Voutbassi),
    p0=init, sigma=unumpy.std_devs(Voutbassi)
)
opt, cov = curve_fit(
    retta, unumpy.nominal_values(Vinbassi), unumpy.nominal_values(Voutbassi),
    p0=init, sigma=opt[0]*unumpy.std_devs(Vinbassi)
)

dopt = np.sqrt(cov.diagonal())

# scrivo i parametri ottimali come ufloat
opt = correlated_values(opt, cov)
d.push(opt[0], None, '4.a', 'm')

# disegno i dati
plt.errorbar(
    unumpy.nominal_values(Vin), unumpy.nominal_values(Vout),
    xerr=unumpy.std_devs(Vin), yerr=unumpy.std_devs(Vout),
    linestyle='', elinewidth=0.8
)

# disegno la retta di best fit
X = np.linspace(unumpy.nominal_values(Vinbassi).min(),
                unumpy.nominal_values(Vinbassi).max())
Y = retta(X, *unumpy.nominal_values(opt))

plt.plot(X, Y, linewidth=1)

# bellurie
plt.xlabel('$ V_{\\mathrm{in}}\\ [\\mathrm{V}] $')
plt.ylabel('$ V_{\\mathrm{drain}}\\ [\\mathrm{V}] $')

# salvo il grafico (bellissimo)
plt.savefig("img/commonSource.png")
# plt.show()

d.save()
