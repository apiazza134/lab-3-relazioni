#!/usr/bin/env python3

from lab.data import Data
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

plt.style.use('../style.yml')

d = Data()

A = d.get('6.a', 'Vdrain') / d.get('6.a', 'Vin')
d.push(A, None, '6.a', 'A')

VDD = 15.33
Vp = -2.13
Idss = 6.2e-3
RD = 9.90e3

IDmax = (VDD + Vp)/RD


def ID(x, Vp, Idss):
    return - Vp/x + Vp**2/(2*Idss*x**2) * (1 - np.sqrt(1 - (4*Idss/Vp) * x))


def gm(I, Vp, Idss):
    return 2*np.sqrt(I * Idss)/abs(Vp)


def A(x, Vp, Idss, Rd):
    return gm(ID(x, Vp, Idss), Vp, Idss)*Rd/(1 + gm(ID(x, Vp, Idss), Vp, Idss) * x)


def func(x):
    return IDmax - ID(x, Vp, Idss)


RSmin = fsolve(func, 836)
Amax = A(RSmin, Vp, Idss, RD)

d.recursivePush(f'\\SI{{ {RSmin[0]:.0f} }}{{ \\ohm }}', '6.a', 'RSmin')
d.recursivePush(f'\\num{{ {Amax[0]:.1f} }}', '6.a', 'Amaxexp')

X = np.linspace(100, 10e3, num=500)

plt.plot(X, len(X)*[IDmax], color='red', linewidth=0.5)
plt.plot(X, ID(X, Vp, Idss), color='black', linewidth=0.5)
plt.xlabel('$R_{\\mathrm{S}} [\\Omega]$')
plt.ylabel('$I_{\\mathrm{D}} [\\mathrm{A}]$')
plt.savefig("./img/correnteRS.png")

plt.close()

X = np.linspace(RSmin[0]-100, 10e3, num=500)
plt.axvline(x=RSmin[0], ymin=0, ymax=10, color='red', linewidth=0.5)
plt.plot(X, A(X, Vp, Idss, RD), color='black', linewidth=0.5)
plt.xlabel('$R_{\\mathrm{S}} [\\Omega]$')
plt.ylabel('$A$')
plt.savefig("img/guadagnoRS.png")

d.save()
