#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from uncertainties import unumpy as unp
from uncertainties import correlated_values, ufloat
from scipy.optimize import curve_fit
from lab import (
    Data, Table, loadtxtSorted, cov2corr, matrixToTex
)

plt.style.use('../style.yml')

d = Data()

# Carico i dati
VRD, dVRD, VGS, dVGS = loadtxtSorted("./data/2a.txt", column=2)

VRD = unp.uarray(VRD, dVRD)
VGS = unp.uarray(VGS, dVGS)

RD = d.get('2.z', 'RD')
ID = VRD/RD

# Tabella
tab = Table()
tab.pushColumn("V\\ped{GS}", "volt", VGS)
tab.pushColumn("V\\ped{GS}", "volt", VGS)
tab.pushColumn("V_{R\\ped{D}}", "volt", VRD)
tab.pushColumn("I\\ped{D}", "ampere", ID)
d.pushTable(tab, '2.a', 'tab')

# Grafico
plt.errorbar(
    unp.nominal_values(VGS), unp.nominal_values(ID),
    yerr=unp.std_devs(ID), xerr=unp.std_devs(VGS),
    linestyle='', color='black'
)


# Fit parabolico
def f(x, A, x0):
    return np.piecewise(
        x, [x < x0, x >= x0],
        [lambda x: 0, lambda x: A*((x-x0)**2)]
    )


VL = -1.3
IDbasse = ID[VGS < VL]
VGSbasse = VGS[VGS < VL]

init = (1e-3, -2.3)
opt, cov = curve_fit(
    f, unp.nominal_values(VGSbasse), unp.nominal_values(IDbasse),
    p0=init, sigma=unp.std_devs(VGSbasse)
)
opt = correlated_values(opt, cov)

X = np.linspace(unp.nominal_values(VGSbasse), VL)
plt.plot(X, f(X, *unp.nominal_values(opt)), color='blue', linewidth=0.5)

# Salvo i parametri di best fit
d.recursivePush(str(VL), '2.a', 'VL')
d.push(opt[0], 'ampere/\\volt^{2}', '2.a', 'A')
d.push(opt[1], 'volt', '2.a', 'Vpfit')

d.recursivePush(
    matrixToTex(cov2corr(cov)), '2.a', 'corrpar'
)

Vp = ufloat(opt[1].nominal_value,
            np.sqrt((opt[1].nominal_value*0.03)**2 + (0.1*0.5)**2))
d.push(Vp, 'volt', '2.a', 'Vp')
A = opt[0]


# Fit lineare
def f(x, m, q):
    return m*x + q


VH = -0.5
IDalte = ID[VGS > -0.5]
VGSalte = VGS[VGS > -0.5]

init = (1, 1)
opt, cov = curve_fit(
    f, unp.nominal_values(VGSalte), unp.nominal_values(IDalte),
    p0=init, sigma=unp.std_devs(VGSalte)
)
opt = correlated_values(opt, cov)

X = np.linspace(-0.5, 0.1)
plt.plot(X, f(X, *unp.nominal_values(opt)), color='blue', linewidth=0.5)

# Salvo i parametri di best fit
d.recursivePush(str(VH), '2.a', 'VH')
d.push(opt[0], 'ampere/\\volt', '2.a', 'm')
Idss = opt[1]
d.push(Idss, 'ampere', '2.a', 'Idss')
d.recursivePush(
    matrixToTex(cov2corr(cov)), '2.a', 'corrpar'
)

Ibound = (d.get('2.z', 'VDD') + Vp)/d.get('2.z', 'RD')
d.push(Ibound, 'ampere', '2.a', 'Ibound')

plt.xlabel("$V_{\\mathrm{GS}} [\\mathrm{V}]$")
plt.ylabel("$I_{\\mathrm{D}} [\\mathrm{A}]$")
plt.savefig("img/2a.png")

# 3. punto di lavoro
Idq = d.get('3.a', 'VR1')/d.get('3.z', 'R1')
d.push(Idq, 'ampere', '3.b', 'Idq')

VGS = d.push(d.get('3.a', 'VR2') - d.get('3.a', 'VRS'), 'volt', '3.b', 'VGS')

# transcondutanza
gm = 2/abs(Vp)*unp.sqrt(Idss*Idq)
d.push(gm, 'ohm^{-1}', '3.c', 'gm')

# 4. stima del guadagno
Aexp = (gm * d.get('3.z', 'R1'))/(1 + gm * d.get('3.a', 'RS'))
d.push(Aexp, None, '4.a', 'Aexp')

A = d.get('4.b', 'VSource')/d.get('4.b', 'Vin')
Aexp = (gm * d.get('3.a', 'RS'))/(1 + gm * d.get('3.a', 'RS'))

d.push(A, None, '4.b', 'A', offset=3)
d.push(Aexp, None, '4.b', 'Aexp', offset=3)

d.save()
