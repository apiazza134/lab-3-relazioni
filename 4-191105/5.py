#!/usr/bin/env python3

from lab.data import Data
from uncertainties import umath
import numpy as np

# 5. impedenza in ingresso
d = Data()

Cin = d.get('3.z', 'C1')
R2 = d.get('3.z', 'R2')
RL = d.get('5.a', 'RL')

# 1 kHz
# attesa
f = d.get('5.a', 'f')
ZinExp = umath.sqrt(R2**2 + 1/(2*np.pi*f*Cin)**2)

# misurata
A = d.get('5.a', 'V1') / d.get('5.a', 'V2')
phi = 2*np.pi*f*d.get('5.a', 'DT')
Zin = RL/umath.sqrt((A*umath.cos(phi) - 1)**2 + (A*umath.sin(phi))**2)

d.push(ZinExp, 'ohm', '5.a', 'ZinExp')
d.push(Zin, 'ohm', '5.a', 'Zin')

# 100 Hz
# attesa
f = d.get('5.a.bis', 'f')
ZinExp = umath.sqrt(R2**2 + 1/(2*np.pi*f*Cin)**2)

# misurata
A = d.get('5.a.bis', 'V1') / d.get('5.a.bis', 'V2')
phi = 2*np.pi*f*d.get('5.a.bis', 'DT')
Zin = RL/umath.sqrt((A*umath.cos(phi) - 1)**2 + (A*umath.sin(phi))**2)

d.push(ZinExp, 'ohm', '5.a.bis', 'ZinExp')
d.push(Zin, 'ohm', '5.a.bis', 'Zin')

# 10 kHz
# attesa
f = d.get('5.b', 'f')
ZinExp = umath.sqrt(R2**2 + 1/(2*np.pi*f*Cin)**2)

# misurata
A = d.get('5.b', 'V1') / d.get('5.b', 'V2')
phi = 2*np.pi*f*d.get('5.b', 'DT')
Zin = RL/umath.sqrt((A*umath.cos(phi) - 1)**2 + (A*umath.sin(phi))**2)

d.push(ZinExp, 'ohm', '5.b', 'ZinExp')
d.push(Zin, 'ohm', '5.b', 'Zin')

d.save()
