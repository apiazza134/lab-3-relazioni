\documentclass[a4paper, 11pt]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\renewcommand{\thesubsection}{\thesection.\alph{subsection}}
\counterwithin{equation}{subsection}

\preambolo

\begin{document}
\maketitle

\stepcounter{section}
\section{Progettazione del circuito e verifica del punto di lavoro}
Per scegliere i valori di $ R_{1} $ e $ R_{2} $ teniamo conto delle seguenti osservazioni:
\begin{itemize}
\item la condizione di partitore ``stiff'' è $ I \ped{P} > 10 I \ped{B} $, dove $ I \ped{P} $ è la corrente che scorre attraverso $ R_{1} $ e $ R_{2} $. Tale condizione si traduce in
  \[ R_{1} + R_{2} < \frac{1}{10} \frac{V \ped{CC} \, h \ped{FE}}{I \ped{C} \ap{Q}}. \]
  Usando come $ V\ped{CC} = \SI{20}{\volt} $, $ I\ped{C}\ap{Q} = \SI{1}{\milli\ampere} $  e per $ h \ped{FE} \simeq \num{100} $ come dedotto dal datasheet del transitor otteniamo
  \[ R_{1} + R_{2} < \SI{200}{\kilo\ohm}; \]

\item dall'equazione di maglia $ V \ped{B} = I \ped{E} \ap{Q} R \ped{E} + V \ped{BE} \simeq I \ped{C} \ap{Q} R \ped{E} + V \ped{BE} $ si ricava
  \[ \frac{R_{1}}{R_{2}} \simeq \frac{V \ped{CC}}{V \ped{BE} + R \ped{E} I \ped{C} \ap{Q}} -1 \]
  dove abbiamo usato il fatto che, grazie alla condizione di partitore ``stiff'' vale
  \[ V \ped{B} \simeq \frac{V \ped{CC}}{1+ \dfrac{R_{1}}{R_{2}}}. \]
  Dalle condizioni sul punto di lavoro e assumendo $ V \ped{BE} \sim V_{\gamma} = \SI{0.7}{\volt} $ (il transistor lavora in regime attivo) otteniamo
  \[ \frac{R_{1}}{R_{2}} \sim \num{10}; \]

\item affinché per frequenze $ f \gtrsim f_{0} = \SI{1}{\kilo\hertz} $ sia possibile trascurare la capacità in ingresso $ C\ped{in} = \SI{220}{\nano\farad} $ deve valere $ R\ped{in} \gg 1/(2\pi f_{0} C\ped{in}) \simeq \SI{720}{\ohm} $. La resistenza in ingresso è data dal parallelo tra $ R_{1} $, $ R_{2} $ e $ h\ped{ie} + h\ped{fe} R\ped{E} \simeq \SI{4.4}{\kilo\ohm} + 135 \times \SI{1}{\kilo\ohm} \simeq \SI{140}{\kilo\ohm} $: essendo dalle prime due condizioni $ R_{2} < \SI{18}{\kilo\ohm} $ e $ R_{1} \sim 10 R_{2} $ abbiamo che $ R\ped{in} \sim R_{2} $ e pertanto deve essere
    \[ R_{2} \gg \SI{720}{\ohm}. \]
\end{itemize}
Abbiamo quindi scelto i valori nominali
\[ R_{1} = \SI{150}{\kilo\ohm} \qquad R_{2} = \SI{15}{\kilo\ohm} \]
I valori misurati delle resistenze effettivamente utilizzate sono
\begin{align*}
  R \ped{C} &= \sivar{2.z}{RC} & R \ped{E} &= \sivar{2.z}{RE} \\
  R_{1} &= \sivar{2.z}{R1} & R_{2} &= \sivar{2.z}{R2}
\end{align*}

\subsection{Misura del punto di lavoro}
Con il multimetro digitale abbiamo misurato
\[ V \ped{CC} = \sivar{2.a}{VCC}\qquad V \ped{CE} = \sivar{2.a}{VCE}\qquad V_{R \ped{C}} = \sivar{2.a}{VRC} \]
da cui si ottiene
\[ I \ped{C} \ap{Q} = \frac{V_{R \ped{C}}}{R \ped{C}} = \sivar{2.a}{IC}. \]
I valori attesi sulla base dei valori misurati delle resistenze e di $ V \ped{CC} $ sono
\begin{align*}
  I \ped{C,exp} \ap{Q} & = \frac{\frac{V\ped{CC}}{1 + R_{1}/R_{2}} - V_{\gamma}}{R\ped{E}} = \sivar{2.a}{IQexp} \\
  V \ped{CE,exp} & = V\ped{CC} - I\ped{C}\ap{exp}(R\ped{C} + R\ped{E}) = \sivar{2.a}{VCEexp}
\end{align*}
Osserviamo che tali valori non sono compatibili con quelli misurati, anche se non troppo distanti. Tale discrepanza si può ascrivere all'assunzione fatta di stimare $ V\ped{BE} \simeq V_{\gamma} = \SI{0.7}{\volt} $ (valore tipico ma arbitrario) indipendentemente dal punto di lavoro considerato, fintanto che siamo in zona attiva, e di considerare $ I\ped{E} = I\ped{C} \frac{h\ped{FE}}{h\ped{FE} + 1} \simeq I\ped{C} $. Il fatto che la stima di $ I\ped{C} $ ecceda il valore ottenuto dalle misure si può attribuire al fatto che, per via della corrente di base non trascurabile, $ V\ped{B} $ è minore di quanto atteso dato dalla~\eqref{eq:VBexp}. Un $ V\ped{B} $ minore comporta in effetti (fissato $ V\ped{CC} $) una corrente minore nel ramo di base.

\subsection{Misura delle tensioni}
Con il multimetro digitale abbiamo misurato
\begin{align*}
  V\ped{B} &= \sivar{2.b}{VB} & V\ped{E} &= \sivar{2.b}{VE} \\
  V\ped{BE} &= \sivar{2.b}{VBE} & V\ped{C} &= \sivar{2.b}{VC}
\end{align*}
Nel limite $ I\ped{P} \gg I\ped{B} $ ci aspetteremmo
\begin{equation} \label{eq:VBexp}
  V\ped{B, exp} = \frac{V\ped{CC}}{1 + R_{1}/R_{2}} = \sivar{2.b}{VBexp}
\end{equation}
che, seppur non compatibile, è comunque una stima ragionevole di quanto misurato. Si ha inoltre
\[ V\ped{E, exp} = I\ped{C, exp}\ap{Q} R\ped{E} = \sivar{2.b}{VEexp} \qquad V\ped{C, exp} = V\ped{CC} - I\ped{C,exp}\ap{Q} R\ped{C} = \sivar{2.b}{VCexp} \]
che anche in questo caso non sono compatibili a causa della stima inaccurata di $ I\ped{C}\ap{Q} $.

I valori tipici della caduta di tensione ai capi della giunzione \texttt{BE} in regime attivo sono compresi tra i $ \SI{0.6}{\volt} $ e i $ \SI{0.7}{\volt} $; quanto misurato è compatibile con tali valori ma inferiore a quanto utilizzato nelle stime precedenti.
Usando i valori misurati e l'equazione di maglia esatta $ I\ped{E} R\ped{E} + V\ped{BE} = V\ped{B} $ si ricava
\[ I\ped{E} = \frac{V\ped{B} - V\ped{BE}}{R\ped{E}} = \sivar{2.b}{IE1} \]
compatibile con la stima alternativa $ I\ped{E} = V\ped{E} / R\ped{E} = \sivar{2.b}{IE2} $. Si ricava inoltre il rapporto tra la corrente di collettore e quella di emettitore
\[ \alpha\ped{F} = \frac{I\ped{C}}{I\ped{E}} = \sivar{2.b}{aF}. \]


\subsection{Verifica della condizione di partitore ``stiff''}
Dalle misure fatte ricaviamo
\[ I_{R_{1}} = \frac{V\ped{CC} - V\ped{B}}{R_{1}} = \sivar{2.c}{IR1} \qquad I_{R_{2}} = \frac{V\ped{B}}{R_{2}} = \sivar{2.c}{IR2} \]
da cui si ottiene la stima della corrente di base
\[ I\ped{B} = I_{R_{1}} - I_{R_{2}} = \sivar{2.c}{IB}. \]
Osserviamo che è verificata la condizione di partitore ``stiff'' per cui $ I_{R_{1}} \simeq I_{R_{2}} > 10 I\ped{B} $. Possiamo inoltre ricavare una delle grandezze caratteristiche del transistor
\[ h\ped{FE} = \frac{I\ped{C}}{I\ped{B}} = \sivar{2.c}{hFE}. \]


\section{Risposta a segnali sinusoidali di frequenza fissa}
La frequenza è stata fissata a $ f = \sivar{3.z}{f} $. Inoltre per le capacità si sono misurati i valori $ C \ped{in} = \sivar{2.z}{Cin} $ e $ C \ped{out} = \sivar{2.z}{Cout} $

\subsection{Guadagno in tensione}
\begin{enumerate}[label=\roman*.]
\item Si verifica l'inversione di fase del segnale, come mostrato in Figura~\ref{fig:inversione}.
  \begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\linewidth]{data/3a1}
    \caption{confronto tra il segnale in ingresso \texttt{CH1} e quello in uscita \texttt{CH2}.}
    \label{fig:inversione}
  \end{figure}

\item Abbiamo misurato $ V\ped{in} = \sivar{3.a}{Vin} $ e $ V\ped{out} = \sivar{3.a}{Vout} $ da cui si ottiene
  \[ A_{v} = \frac{V\ped{out}}{V\ped{in}} = \sivar{3.a}{Av} \]
  comparabile con il valore atteso di circa 10.
\item A partire da circa $ V\ped{in} \sim \SI{1.5}{\volt} $ diventano visibili effetti non lineari del circuito, con la presenza di effetti di ``clipping'' asimmetrico del segnale in uscita (Figura~\ref{fig:clipping-i}).
  Arrivando a $ V\ped{in} \sim \SI{3}{\volt} $ anche la parte alta di $ V\ped{out} $ viene tagliata (Figura~\ref{fig:clipping-ii}).
  \begin{figure}[h!]
    \centering
    \subfloat[]{
      \label{fig:clipping-i}
      \includegraphics[width=0.4\textwidth]{data/3a4i}
    }
    \subfloat[]{
      \label{fig:clipping-ii}
      \includegraphics[width=0.4\textwidth]{data/3a4ii}
    }
    \caption{segnale in entrata \texttt{CH1} e in uscita \texttt{CH2} per valori della differenza di potenziale in ingresso non sufficientemente ``piccoli''.}
  \end{figure}

\item Al di fuori del regime lineare, in prossimità dei massimi dell'input (cioè in corrispondenza dei minimi dell'output) $ V\ped{C} - V\ped{B} $ diventa minore della tensione necessaria a garantire la polarizzazione inversa della giunzione \texttt{BC}; il transistor va in regime di saturazione e, come si evince dalle curve di collettore, il punto di lavoro rimane fissato ad una differenza di potenziale costante indipendente da $ I\ped{B} $ e quindi da $ V\ped{B} $.

  In prossimità dei minimi dell'input (cioè dei massimi dell'output) è la giunzione \texttt{BE} a cambiare polarizzazione; il transistor va in regime di interdizione, comportandosi quindi come un circuito aperto, come si deduce sempre dalle curve di collettore.

  Fissato punto di lavoro oltre il regime di amplificazione lineare risulta più pronunciato il ``clipping'' sulle semionde positive in quanto questo fenomeno inizia a presentarsi per ampiezze minori di $ V\ped{in} $.

\end{enumerate}

\subsection{Impedenza in ingresso}
Una stima dell'impedenza in ingresso è data da
\[
  Z \ped{in,exp} = \left(\frac{1}{h \ped{ie} + h \ped{fe} R \ped{E}} + \frac{1}{R_{1}} + \frac{1}{R_{2}}\right)^{-1} \simeq \SI{12}{\kilo\ohm}
\]
dove si sono usati i i valori misurati delle resistenze e i valori tipici (nominali, dal datasheet) $ h \ped{ie} = \SI{4.4}{\kilo\ohm} $ e $ h \ped{fe} = 135 $.

Il valore di $ Z \ped{in,exp} $ è riportato senza incertezza data la natura nominale e indicativa dei parametri usati (che, in base a quanto riportato sul manuale, sono stati ottenuti in condizioni di lavoro e frequenze diverse da quelle da noi usate).
Tuttavia questa rozza stima ci consente di scegliere una resistenza $ R \ped{S} \sim\SI{10}{\kilo\ohm} $ del giusto ordine di grandezza da mettere in serie al generatore $ V \ped{in} $. Abbiamo quindi scelto
\[ R \ped{S} = \sivar{3.b}{RS}. \]
Dette $ V_{1} $ la tensione $ V \ped{out} $ misurata in assenza di $ R \ped{S} $ e $ V_{2} $ quella misurata in presenza di $ R \ped{S} $, si è ottenuto
\[
  Z \ped{in} = \frac{R \ped{S}}{\dfrac{V_{1}}{V_{2}} - 1} = \sivar{3.b}{Zin}
\]
in accordo con la previsione.

\subsection{Impedenza in uscita}
La resistenza attesa in uscita è invece eguale alla resistenza di collettore $ R \ped{C} = \sivar{2.z}{RC} $. Inserendo una resistenza di carico comparabile con quest'ultima tra uscita e massa:
\[ R \ped{L} = \sivar{3.c}{RL}, \]
si ottiene
\[
  Z \ped{out} = R_{2} \left( \frac{V_{1}}{V_{2}} - 1\right) = \sivar{3.c}{Zout}
\]
dove $ V_{1} $ e $ V_{2} $ hanno lo stesso significato della sezione precedente. Il valore è in accordo con la previsione.

\section{Risposta in frequenza}
In Tabella~\ref{tab:rispostaInFrequenza} si riporta quanto misurato e l'attenuazione calcolata; in Figura~\ref{fig:rispostaInFrequenza} si riporta invece il corrispondente diagramma di Bode con sovrapposti le curve di \textit{best fit}. Abbiamo effettuato due \textit{fit} separatamente ad alte e basse frequenze, dove l'andamento del diagramma di Bode è qualitativamente lineare.

Per quanto riguarda le basse frequenze $ < \SI{30}{\hertz} $ si è ottenuto, per un modello lineare $ f(x) = mx+q $:
\[ m = \sivar{4.c}{mL} \qquad q = \sivar{4.c}{qL} \qquad \mathrm{corr}(m, q) = -0.998 \]
Dall'intersezione della retta di \textit{best fit} con la retta di massimo guadagno si è ottenuto infine
\[ f \ped{L} = 10^{(A \ped{max} - q)/m} = \sivar{4.c}{fL}. \]
Nella stima dell'errore si è tenuto conto della correlazione tra i parametri di \textit{best fit}.

Per quanto riguarda le alte frequenze $ > \SI{140}{\kilo\hertz} $ si è ottenuto invece
\[ m = \sivar{4.c}{mH} \qquad q = \sivar{4.c}{qH} \qquad \mathrm{corr}(m, q) = -0.970 \]
da cui, analogamente al caso precedente
\[ f \ped{H} = \sivar{4.c}{fH}. \]

L'attenuazione del segnale in uscita a basse frequenze si spiega con il fatto che l'amplificatore è in serie a un passa--alto, costituito da $ C \ped{in} $ e dal parallelo $ R_{1}\parallel R_{2} $.
La frequenza $ f \ped{L} $ è compatibile con la frequenza di taglio di tale filtro
\[
  f \ped{T} = \frac{1}{2\pi (R_{1}\parallel R_{2}) C \ped{in}} = \sivar{4.c}{fT}.
\]

La presenza di una attenuazione ad alte frequenze può essere attribuita al comportamento reale del transistor, il quale presenta una capacità nella giunzione \texttt{BC} quando polarizzata inversamente. L'uscita alternata $ V \ped{out} $ è presa tra il collettore e la resistenza $ R \ped{C} $, quindi effettivamente quel ramo del circuito si comporta come un passa--basso, del quale però risulta difficile stimare la frequenza di taglio.

\section{Aumento del guadagno}
Abbiamo inserito il condensatore $ C \ped{E} $ da $ \SI{100}{\micro\farad} $ nominali in serie a una resistenza
\[ R \ped{es} = \sivar{5.a}{Res}. \]
Alla frequenza $ f = \sivar{5.a}{f} $ si è ottenuto il guadagno in tensione
\[
  A_{v} = \frac{V \ped{out}}{V \ped{in}} = \sivar{5.a}{Av}.
\]
Questo risulta in disaccordo con la previsione data da
\[
  \abs{A_{v}} = \frac{R \ped{C}}{\abs{Z \ped{E}}}
  \simeq R \ped{C} \abs{ \frac{1}{R \ped{E}} + \frac{1}{R \ped{es} + \frac{1}{j \omega C \ped{E}}}}
  \simeq \frac{R \ped{C}}{R \ped{es}} \simeq 100
\]
dove si è usato il fatto che $ R \ped{es} \ll R \ped{E} $ e che alla frequenza scelta $ \frac{1}{2\pi f C \ped{E}} \ll R \ped{es} $.
La non compatibilità è dovuta al fatto che nella precedente formula si è implicitamente fatta l'assunzione che sia anche $ \abs{Z \ped{E}} \gg h \ped{ie}/h \ped{fe}$.
Questa non è più verificata in quanto in questo caso si ha
\[
  \abs{Z \ped{E}} = \abs{R \ped{E} \parallel \left( R \ped{es} + \frac{1}{2\pi j \omega C \ped{E}}\right)} \simeq \SI{90}{\ohm}
  \qquad
  \frac{h \ped{ie}}{h \ped{fe}} = \SI{33}{\ohm}
\]
Anche qui i valori sono riportati senza incertezza perché si sono usati i valori nominali per $ h \ped{ie} $, $ h \ped{fe} $ e $ C \ped{E} $.
Otteniamo quindi un valore in accordo con quello misurato e riportato sopra:
\[ \abs{A_{v}} = \frac{R \ped{C}}{\abs{Z \ped{E} + \dfrac{h \ped{ie}}{h \ped{fe}}}} \simeq 80. \]

\begin{table}[h!]
    \centering
    \tab{4.a}{tab}
    \caption{\label{tab:rispostaInFrequenza}risposta in frequenza.}
\end{table}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.9]{img/bode.png}
  \caption{\label{fig:rispostaInFrequenza}diagramma di Bode.}
\end{figure}

\dichiarazione

\end{document}
