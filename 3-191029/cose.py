#!/usr/bin/env python3

from lab import Data

d = Data()

# calcoliamo la corrente quiescente di collettore
IC = d.get('2.a', 'VRC') / d.get('2.z', 'RC')
d.push(IC, 'ampere', '2.a', 'IC')

# calcoliamo il punto di lavoro atteso
VCC = d.get('2.a', 'VCC')
R1 = d.get('2.z', 'R1')
R2 = d.get('2.z', 'R2')
RC = d.get('2.z', 'RC')
RE = d.get('2.z', 'RE')

VBexp = VCC/(1+R1/R2)
IQexp = (VBexp - 0.7)/RE
VCEexp = VCC - IQexp*(RC+RE)
Vgamma = VBexp - RE*IC

d.push(IQexp, 'ampere', '2.a', 'IQexp')
d.push(VCEexp, 'volt', '2.a', 'VCEexp')
d.push(Vgamma, 'volt', '2.a', 'Vgamma')

# Misure di tensione
VB = d.get('2.b', 'VB')
VE = d.get('2.b', 'VE')
VBE = d.get('2.b', 'VBE')
VC = d.get('2.b', 'VC')

VEexp = IC * RE
VCexp = VCC - IQexp * RC

IE1 = (VB - VBE)/RE
IE2 = VE/RE

aF = IC/IE1

d.push(VBexp, 'volt', '2.b', 'VBexp')
d.push(VEexp, 'volt', '2.b', 'VEexp')
d.push(VCexp, 'volt', '2.b', 'VCexp')
d.push(IE1, 'ampere', '2.b', 'IE1')
d.push(IE2, 'ampere', '2.b', 'IE2')
d.push(aF, None, '2.b', 'aF', offset=3)

# Partitore stiff
IR2 = VB / R2
IR1 = (VCC - VB) / R1
IB = IR1 - IR2
hFE = IC / IB

d.push(IR1, 'ampere', '2.c', 'IR1')
d.push(IR2, 'ampere', '2.c', 'IR2')
d.push(IB, 'ampere', '2.c', 'IB')
d.push(hFE, None, '2.c', 'hFE')

# 3 Misure a frequenza fissa
# 3.a.ii
d.push(d.get('3.a', 'Vout') / d.get('3.a', 'Vin'), None, '3.a', 'Av')

# 3.b
d.push(d.get('3.b', 'RS')/(d.get('3.b', 'V1')/d.get('3.b', 'V2') - 1), 'ohm', '3.b', 'Zin')

# 3.c
d.push(d.get('3.c', 'RL') * (d.get('3.b', 'V1')/d.get('3.c', 'V2') - 1), 'ohm', '3.c', 'Zout')

# 5.a
d.push(d.get('5.a', 'Vout')/d.get('5.a', 'Vin'), None, '5.a', 'Av')

d.save()
