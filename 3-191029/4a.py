#!/usr/bin/env python3

import numpy as np
from uncertainties import unumpy, correlated_values
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from lab import Data, Table

d = Data()

plt.style.use('../style.yml')

f, df, Vin, dVin, Vout, dVout = np.loadtxt('./data/4a.txt', unpack=True)

f = unumpy.uarray(f, df)

Vin = unumpy.uarray(Vin, dVin)
Vout = unumpy.uarray(Vout, dVout)

A = Vout/Vin
Adb = 20*unumpy.log10(A)

# TABELLA
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A', None, A)
d.pushTable(tab, '4.a', 'tab')

# FIT
logf = unumpy.log10(f)

# Frequenze alte (> 140kHz perché sì)
logfalte = logf[logf > 5.14]
Adbalte = Adb[logf > 5.14]

# Frequenze basse
logfbasse = logf[logf < 1.44]
Adbbasse = Adb[logf < 1.44]

# Frequenze mancanti
logfcentro = logf[(logf > 1.44) & (logf < 5.14)]
Adbcentro = Adb[(logf > 1.44) & (logf < 5.14)]

# disegno bode
plt.errorbar(
    unumpy.nominal_values(logfbasse), unumpy.nominal_values(Adbbasse),
    xerr=unumpy.std_devs(logfbasse), yerr=unumpy.std_devs(Adbbasse),
    linestyle='', color='red'
)

plt.errorbar(
    unumpy.nominal_values(logfcentro), unumpy.nominal_values(Adbcentro),
    xerr=unumpy.std_devs(logfcentro), yerr=unumpy.std_devs(Adbcentro),
    linestyle='', color='black'
)

plt.errorbar(
    unumpy.nominal_values(logfalte), unumpy.nominal_values(Adbalte),
    xerr=unumpy.std_devs(logfalte), yerr=unumpy.std_devs(Adbalte),
    linestyle='', color='blue'
)

indexMax = np.argmax(unumpy.nominal_values(A))
d.push(A[indexMax], None, '4.c', 'Amax')


# Modello
def retta(x, m, q):
    return m*x + q


# Fitto dei log delle cose ad alte frequenze
init = (-20, 20*7.57)
opt, cov = curve_fit(
    retta, unumpy.nominal_values(logfalte), unumpy.nominal_values(Adbalte),
    p0=init, sigma=unumpy.std_devs(Adbalte)
)
dopt = np.sqrt(cov.diagonal())

# scrivo i parametri ottimali come ufloat
opt = correlated_values(opt, cov)
d.push(opt[0], 'deci\\bel/decade', '4.c', 'mH')
d.push(opt[1], 'deci\\bel', '4.c', 'qH')

# TODO: corr = covToCorr(cov)

# frequenza f_{high}
logfH = (Adb[indexMax] - opt[1])/opt[0]
fH = 10**logfH

X = np.linspace(logfH.nominal_value, unumpy.nominal_values(logfalte).max())
Y = retta(X, *(unumpy.nominal_values(opt)))

plt.plot(X, Y, color='green', linewidth=0.5)  # stampo la retta fittata

# salvo i dati
d.push(fH, 'hertz', '4.c', 'fH')

# Fitto i log delle frequenze basse
init = (20, -20*np.log10(400))
opt, cov = curve_fit(
    retta, unumpy.nominal_values(logfbasse), unumpy.nominal_values(Adbbasse),
    p0=init, sigma=unumpy.std_devs(Adbbasse)
)
dopt = np.sqrt(cov.diagonal())

# scrivo i parametri ottimali come ufloat
opt = correlated_values(opt, cov)
d.push(opt[0], 'deci\\bel/decade', '4.c', 'mL')
d.push(opt[1], 'deci\\bel', '4.c', 'qL')

# corr = covToCorr(cov)

# frequenza f_{low}
logfL = (Adb[indexMax] - opt[1])/opt[0]
fL = 10**logfL

# salvo i dati
d.push(fL, 'hertz', '4.c', 'fL')

X = np.linspace(unumpy.nominal_values(logfbasse).min(), logfL.nominal_value)
Y = retta(X, *(unumpy.nominal_values(opt)))

plt.plot(X, Y, color='green', linewidth=0.5)  # stampo la retta fittata

# stampo la retta orizzontale
X = np.linspace(logfL.nominal_value, logfH.nominal_value)

plt.plot(X, [unumpy.nominal_values(Adb).max()]*len(X), color='green', linewidth=0.5)

# bellurie
plt.xlabel('$\\log_{10} (f/1 \\mathrm{Hz})$')
plt.ylabel('$A\\ [\\mathrm{dB}] $')

# salvo il grafico (bellissimo)
plt.savefig('img/bode.png')

# passa banda
R = 1/(1/d.get('2.z', 'R1') + 1/d.get('2.z', 'R2'))
C = d.get('2.z', 'Cin')
d.push(1/(2*np.pi*R*C), 'hertz', '4.c', 'fT')

d.save()
