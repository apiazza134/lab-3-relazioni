#!/bin/bash

wget -q ftp://ftp.math.utah.edu/pub/tex/historic/systems/texlive/2019/tlnet-final/archive/tkz-graph.tar.xz
apt-get update -qq && apt-get install xz-utils
tar xvf tkz-graph.tar.xz
mv tex/latex/tkz-graph/tkz-graph.sty tkz-graph.sty
