#!/usr/bin/env python3

import matplotlib.pyplot as plt

from lab.oscilloscope import readCSV

plt.style.use("../style.yml")

fig, axes = plt.subplots(
    4, 1, sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios': 4*[1]},
)

t1, v1, t2, v2 = readCSV('data/1-en-V-CLK.csv')
axes[3].plot(t1, v1, color='green')  # verde
axes[0].plot(t2, v2, color='black')  # clock

t1, v1, t2, v2 = readCSV('data/1-en-V-R.csv')
axes[1].plot(t2, v2, color='red')  # rosso

t1, v1, t2, v2 = readCSV('data/1-en-V-G.csv')
axes[2].plot(t2, v2, color='orange')  # giallo

axes[-1].set_xlabel("$ t $ \\ [s]")
axes[0].set_ylabel("\\texttt{CLK} \\ [V]")
axes[1].set_ylabel("$ R $ \\ [V]")
axes[2].set_ylabel("$ G $ \\ [V]")
axes[3].set_ylabel("$ V $ \\ [V]")

# plt.show()
plt.savefig('img/1-enabled.png')

plt.close()

fig, axes = plt.subplots(
    2, 1, sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios': 2*[1]},
)

t1, v1, t2, v2 = readCSV('data/1-dis-G-CLK.csv')
axes[0].plot(t2, v2, color='black')  # clock
axes[1].plot(t1, v1, color='orange')  # giallo

axes[1].set_xlabel("$ t $ \\ [s]")
axes[0].set_ylabel("\\texttt{CLK} \\ [V] ")
axes[1].set_ylabel("$ G $ \\ [V]")

# plt.show()
plt.savefig('img/1-disabled.png')
