\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\usepackage{karnaugh-map}
\usepackage{minted}
\setminted[arduino]{frame=lines,framesep=2mm,linenos}

\newenvironment{longlisting}{\captionsetup{type=listing}}{}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\usepackage{tkz-graph}
\usetikzlibrary{arrows}
\tikzset{
    LabelStyle/.style = {
        rectangle, rounded corners, draw,
        minimum width = 2em,
        font = \bfseries
    },
    VertexStyle/.append style = {
        inner sep=5pt,
        font = \Large\bfseries
    },
    EdgeStyle/.append style = {-stealth}
}

\preambolo

\begin{document}
\maketitle

\renewcommand{\abstractname}{Nota}
\begin{abstract}
    Gli sketch sono stati testati a casa su un \texttt{Arduino Uno} senza il buffer a causa della chiusura dei laboratori in seguito sospensione dell'attività didattica.
\end{abstract}

\section{Semaforo con circuiti integrati}

\subsection{Implementazione della macchina a stati finiti}
Nel caso di semaforo ``abilitato'' i possibili output sono 3 e abbiamo quindi deciso di implementarli con 3 stati interni della macchina; nel caso di semaforo ``disabilitato'' i possibili output sono 2, ma possono essere ottenuti riutilizzando due degli stati precedenti e l'input ``enable'' tramite un'opportuno circuito combinatorio (\texttt{FSM} di Mealy).

Si è deciso di codificare in due bit di memoria i tre stati $ (00) $, $ (01) $ e $ (10) $ che corrispondono, quando $ E = 1 $ (abilitato), ai tre output ``verde'', ``verde-giallo'', ``rosso'' (che si susseguono in modo ciclico). Quando invece $ E = 0 $ (disabilitato) vengono usati solamente $ (00) $ e $ (01) $, che corrispondono agli output del semaforo ``spento'' e ``giallo''.

È tuttavia necessario specificare anche cosa succede se la macchina si trova nello stato $ (10) $ e l'input $ E $ viene messo a zero\footnote{
    Notiamo incidentalmente che lo stato $ (10) $ non è proibito per $ E = 0 $, ma è semplicemente uno stato in cui la macchina ``a regime'' non si viene spontaneamente a trovare se l'input $ E $ non viene cambiato.
}. Questo comporta un cambiamento asincrono dell'output (``spento'' diventa ``verde'', ``giallo'' diventa ``verde-giallo'') ma un cambiamento sincrono dello stato interno della \texttt{FSM}. Si è deciso che lo stato successivo dev'essere lo $ (00) $.

\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
        \SetGraphUnit{4}
        \Vertex{01}
        \WE(01){00}
        \EA(01){10}

        \Edge[label = E](00)(01)
        \Edge[label = E](01)(10)

        \tikzset{EdgeStyle/.append style = {-stealth, bend right = 30}}
        \Edge[label = D](01)(00)

        \tikzset{EdgeStyle/.append style = {-stealth, bend right = 30}}
        \Edge[label = D](00)(01)

        \tikzset{EdgeStyle/.append style = {bend right = 50}}
        \Edge[label = E](10)(00)

        \tikzset{EdgeStyle/.append style = {bend left = 50}}
        \Edge[label = D](10)(00)
    \end{tikzpicture}
    \caption{\label{fig:semaforoHardwareStati}diagramma degli stati.}
\end{figure}

Si riporta in Figura~\ref{fig:semaforoHardwareStati} il diagramma degli stati e in Tabella~\ref{tab:semaforoHardwareTabelle} le tabelle di verità e le mappe di Karnaugh necessarie al calcolo delle equazioni maestre.
Si è ottenuto, come riportato nelle didascalie:
\begin{equation}\label{eq:evoluzioneHardware}
    Q_{0}^{n+1} = E \cdot Q_{1}^{n} \qquad Q_{1}^{n+1} = \overline{Q_{0}^{n}} \cdot \overline{Q_{1}^{n}}
\end{equation}
Osserviamo che nei casi di \emph{don't care} ``\dontcare'', queste equazioni implicano che la macchina torni spontaneamente in uno degli stati permessi: se si parte dallo stato proibito per $ E = 0 $ sarà $ Q_{0}^{n+1} = Q_{1}^{n+1} = 0 $; se invece si è nello stato proibito per $ E = 1 $ allora sarà $ Q_{0}^{n+1} = 1 $ e $ Q_{1}^{n+1} = 0 $.

Dalla Tabella~\ref{tab:statiLed} otteniamo inoltre
\begin{equation}\label{eq:ledHardware}
    G = E \cdot \overline{Q_{0}} \qquad Y = Q_{1} \qquad R = Q_{0}
\end{equation}
Osserviamo che nel caso $ E = 0 $, $ Q_{0} = 1 $ e $ Q_{1} = 0 $ che è riportato come ``\dontcare'' nella tabella, produce l'output ``rosso''. Questo vale a dire che se il semaforo è ``rosso'' con $ E = 1 $ e questo viene messo a zero, rimarrà ``rosso'' fino al raising successivo del clock, dopo il quale diventa ``spento''.

\begin{table}[p]
    \centering
    \subfloat[\label{tab:statiLed}Corrispondenza tra gli stati della \texttt{FSM} e gli stati dei \texttt{LED}.]{
        \begin{tabular}{cccccc}
          \toprule
          $ E $ & $ Q_{0} $ & $ Q_{1} $ &   $ G $   &   $ Y $   &   $ R $   \\ \midrule
          1   &     0     &     0     &     1     &     0     &     0     \\
          1   &     0     &     1     &     1     &     1     &     0     \\
          1   &     1     &     0     &     0     &     0     &     1     \\
          1   &     1     &     1     & \dontcare & \dontcare & \dontcare \\
          0   &     0     &     0     &     0     &     0     &     0     \\
          0   &     0     &     1     &     0     &     1     &     0     \\
          0   &     1     &     0     & \dontcare & \dontcare & \dontcare \\
          0   &     1     &     1     & \dontcare & \dontcare & \dontcare \\ \bottomrule
        \end{tabular}
    }\hspace{2cm}
    \subfloat[Transizioni di stato.]{
        \begin{tabular}{ccccc}
          \toprule
          $ E $ & $ Q_{0}^{n} $ & $ Q_{1}^{n} $ &   $ Q_{0}^{n+1} $   &   $ Q_{1}^{n+1} $ \\ \midrule
          1   &     0     &     0     &     0     &     1     \\
          1   &     0     &     1     &     1     &     0     \\
          1   &     1     &     0     &     0     &     0     \\
          1   &     1     &     1     & \dontcare & \dontcare \\
          0   &     0     &     0     &     0     &     1     \\
          0   &     0     &     1     &     0     &     0     \\
          0   &     1     &     0     &     0     &     0     \\
          0   &     1     &     1     & \dontcare & \dontcare \\ \bottomrule
        \end{tabular}
    }\vspace{0.5cm}
    \subfloat[$ Q_{0}^{n+1} = E \cdot Q_{1}^{n} $]{
        \begin{karnaugh-map}[2][4][1][$Q_{1}^{n}$][$E \ Q_{0}^{n}$]
            \minterms{5}
            \maxterms{0,1,2,4,6}
            \implicant{7}{5}
            \autoterms[\dontcare]
        \end{karnaugh-map}
    }
    \hspace{2cm}
    \subfloat[$ Q_{1}^{n+1} = \overline{Q_{0}^{n}} \cdot \overline{Q_{1}^{n}} $]{
        \begin{karnaugh-map}[2][4][1][$Q_{1}^{n}$][$E \ Q_{0}^{n}$]
            \minterms{0,4}
            \maxterms{1,2,5,6}
            \implicantedge{0}{0}{4}{4}
            \autoterms[\dontcare]
        \end{karnaugh-map}
    }
    \caption{\label{tab:semaforoHardwareTabelle}tabelle di verità di output e stati e mappe di Karnaugh per il passaggio allo stato successivo.}
\end{table}

\subsection{Schema del circuito}
In virtù della~\eqref{eq:evoluzioneHardware} e della~\eqref{eq:ledHardware}, si è assemblato il circuito riportato in Figura~\ref{fig:semaforoSchema} e si sono collegati i pin \texttt{preset} e \texttt{clear} dei D-Latch a $ V \ped{CC} $. Si è inoltre fornita la tensione di alimentazione $ V \ped{CC} = \sivar{1.z}{VCC} $.

\begin{figure}[h!]
    \centering
    \begin{circuitikz}
        \def\andScale{0.7};
        \def\cusu{-1};
        \def\mano{0.5}
        \def\yep{2}

        % componenti
        %% flip flop
        \draw (0,0) node[flipflop D] (ffSinistra) {};
        \draw (5,0) node[flipflop D] (ffDestra) {};

        %% and
        \node (andSinistra) at (-2, |- ffSinistra.pin 1) [and port, scale=\andScale] {};
        \node (andDestra) at (3, |- ffSinistra.pin 1) [and port, scale=\andScale] {};
        \node [and port, scale=\andScale] (andSopra) at (5,4) {};

        % per i crossing
        \node at ($ (ffSinistra.pin 4) + (\mano,0) $) (ancilla) {};

        %% led
        \node (G) at ($ (andSopra.out) + (3,0) $) {$ G $};
        \node (Y) at ($ (G) + (0,-0.5) $) {$ Y $};
        \node (R) at ($ (G) + (0,-1) $) {$ R $};

        % input
        \node (E) at ($ (andSopra.in 1) + (-9,0) $) {$ E $};
        \node (CLK) at ($ (E)+(0,-7) $) {\texttt{CLK}};

        % fili
        %% clock
        \draw (CLK) -| (ffDestra.pin 3);
        \draw (ffSinistra.pin 3) -- (ffSinistra.pin 3 |-, |- CLK);

        % and e ff
        \draw (andSinistra.out) -- (ffSinistra.pin 1);
        \draw (andDestra.out) -- (ffDestra.pin 1);
        \draw (E) -- (andSopra.in 1);
        \draw (ffSinistra.pin 4) -- ++ (\mano,0) |- (andSopra.in 2);
        \draw (andSinistra.in 2) -| (-3.5, |- E);
        \draw (andDestra.in 1) -- ($ (ffSinistra.pin 4 |-, |- andDestra.in 1) + (\mano,0) $);
        %% cancdo del crossing pt 1
        \node at (ffSinistra.pin 4 |-, \yep) [jump crossing] (X1) {};
        \node at (ancilla |-, \yep) [jump crossing] (X2) {};
        \draw (andSinistra.in 1) |- (X1.west);
        \draw (X1.east) -- (X2.west);
        \draw (X2.east) -- (ffDestra.pin 6 |-, \yep);
        %% cancro del crossing pt 2
        \node at ($ (ffDestra.pin 3) + (0,\cusu) $) [jump crossing] (X) {};
        \draw (ffDestra.pin 4) |- (X.east);
        \draw (X.west) -| (andDestra.in 2);

        %% led
        \draw (andSopra.out) -- (G);
        \draw (ffDestra.pin 6) |- (Y);
        %% cancro del crossing
        \node at (ancilla |-, |- R) [jump crossing] (X1) {};
        \node at (ffDestra.pin 6 |-, |- R) [jump crossing] (X2) {};
        \draw (ffSinistra.pin 6) |- (X1.west);
        \draw (X1.east) -- (X2.west);
        \draw (X2.east) -- (R);

        % numeri dei pin
        \draw (ffSinistra.pin 1) node[above] {2};
        \draw (ffSinistra.pin 3) node[above] {3};
        \draw (ffSinistra.pin 4) node[above] {6};
        \draw (ffSinistra.pin 6) node[below] {5};

        \draw (ffDestra.pin 1) node[above] {12};
        \draw (ffDestra.pin 3) node[above] {11};
        \draw (ffDestra.pin 4) node[above] {8};
        \draw (ffDestra.pin 6) node[below] {9};

    \end{circuitikz}
    \caption{\label{fig:semaforoSchema}schema del semaforo Mealey con enable.}
\end{figure}

\clearpage

\subsection{Verifica del funzionamento}
Si è verificato il corretto funzionamento del semaforo. In Figura~\ref{fig:oscilloscopioHardware} si riportano le acquisizioni con l'oscilloscopio delle uscite del semaforo (collegate ai \texttt{LED}), nei due casi in cui $ E = 0 $ e $ E = 1 $.

\begin{figure}[h!]
    \centering
    \def\scale{0.51}
    \subfloat[$ E = 1 $]{
        \includegraphics[scale=\scale]{img/1-enabled}
    }
    \subfloat[$ E = 0 $]{
        \includegraphics[scale=\scale]{img/1-disabled}
    }
    \caption{\label{fig:oscilloscopioHardware}grafico delle acquisizioni, fatte con l'oscilloscopio, delle tre uscite del semaforo collegate ai \texttt{LED}, assieme al Clock.}
\end{figure}

\section{Semaforo via software con Arduino}
Si sono collegati i led e il pin di enable come richiesto, con la corrispondenza indicata nei successivi listati.

\begin{itemize}
\item \emph{Implementazione con modello di Moore}: si riporta nel Listing~\ref{lst:semaforoMoore} il codice dell'implementazione della \texttt{FSM} in questione con Arduino.

\item \emph{Implementazione con modello di Mealy}: si riporta nel Listing~\ref{lst:semaforoMoore} il codice dell'implementazione della \texttt{FSM} in questione con Arduino. Rispetto all'implementazione hardware è stata fatta la seguente identificazione: $ A \leftrightarrow (00) $, $ B \leftrightarrow (01) $ e $ C \leftrightarrow (10) $. Pertanto la corrispondenza tra gli output e gli stati è
    \[
        R = \delta_{S, C} \qquad Y = \delta_{S, B} \qquad G = E \cdot \overline{\delta_{S, C}}
    \]
    dove $ S $ è lo stato corrente e $ \delta_{X, Y} $ è la delta di Kronecker tra gli stati $ X $ e $ Y $.

\item \emph{Semaforo ``svizzero''}: si è deciso di implementare la \texttt{FSM} in questione con modello di Moore. Nel Listing~\ref{lst:semaforoSvizzero} si riportano solo le modifiche necessarie rispetto al primo caso: in particolare bisogna solamente aggiungere lo stato $ RY $, i suoi stati successivi in funzione dell'input, il tempo di attesa e la sua codifica in termini di led.
\end{itemize}

\begin{listing}[p]
    \inputminted{arduino}{arduino/semaforo-moore/semaforo-moore.ino}
    \caption{\label{lst:semaforoMoore}semaforo con il modello di Moore.}
\end{listing}

\begin{listing}[p]
    \inputminted{arduino}{arduino/semaforo-mealy/semaforo-mealy.ino}
    \caption{\label{lst:semaforoMealy}semaforo con il modello di Mealy.}
\end{listing}

\begin{listing}
\begin{minted}{arduino}
enum state { G, GY, R, RY, OFF, Y } currState;
const unsigned wait[] = { 1000, 1000, 1000, 1000, 1000, 1000 };
const unsigned ledStates[][3] = {
    // R   Y   G
    {LOW, LOW, HIGH},
    {LOW, HIGH, HIGH},
    {HIGH, LOW, LOW},
    {HIGH, HIGH, LOW},
    {LOW, LOW, LOW},
    {LOW, HIGH, LOW}
};
const state nextEnabled[] = { GY, R, RY, G, R, R };
const state nextDisabled[] = { OFF, OFF, OFF, OFF, Y, OFF };
\end{minted}
    \caption{\label{lst:semaforoSvizzero}sostituzioni minimali al Listing~\ref{lst:semaforoMoore}.}
\end{listing}

\dichiarazione

\end{document}
