#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

from lab.data import Data

d = Data().elab_data['3.c.rit']

freq = np.array([2**n for n in range(len(d))])
rit = np.array([time.value.nominal_value for key, time in d.items()])

plt.plot(freq, rit, linestyle='', color='black', marker='.', markersize='20')

plt.savefig('img/3.png')
# plt.show()
