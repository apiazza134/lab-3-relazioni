#!/usr/bin/env python3

from lab.data import Data
from lab.measure import Measure

d = Data()

# low high
ritLH = d.get('2.d', 'LH', 'tQ50') - d.get('2.d', 'LH', 'tD50')

# high low
ritHL = d.get('2.d', 'HL', 'tQ50') - d.get('2.d', 'HL', 'tD50')

d.elab_data['2.d'] = {
    'ritLH': Measure(ritLH, 'second'),
    'ritHL': Measure(ritHL, 'second'),
}

d.save()
