#!/usr/bin/env python3

import matplotlib.pyplot as plt
from lab.oscilloscope import readCSV

plt.style.use("../style.yml")

fig, axes = plt.subplots(
    6, 1, sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios': 6*[1]}
)

shift = 0.042846

td, D, tQ0, Q0 = readCSV('data/4b/D-Q0.csv')
td -= shift
tQ0 -= shift
axes[1].plot(td, D, color='green')  # data
axes[2].plot(tQ0, Q0, color='red')  # Q0

tQ0, dummy, tclock, clock = readCSV('data/4b/Q0-clock.csv')
axes[0].plot(tclock, clock, color='blue')  # clock

tQ0, dummy, tQ1, Q1 = readCSV('data/4b/Q0-Q1.csv')
axes[3].plot(tQ1, Q1, color='red')  # Q1

tQ0, dummy, tQ2, Q2 = readCSV('data/4b/Q0-Q2.csv')
axes[4].plot(tQ2, Q2, color='red')  # Q2

tQ0, dummy, tQ3, Q3 = readCSV('data/4b/Q0-Q3.csv')
axes[5].plot(tQ3, Q3, color='red')  # Q3

axes[-1].set_xlabel("$ t $ \\ [s]")
axes[0].set_ylabel(" \\texttt{CLK}")
axes[1].set_ylabel(" $ D $")
axes[2].set_ylabel(" $ Q_0 $")
axes[3].set_ylabel(" $ Q_1 $")
axes[4].set_ylabel(" $ Q_2 $")
axes[5].set_ylabel(" $ Q_3 $")

# plt.show()
plt.savefig('img/4b.png')
