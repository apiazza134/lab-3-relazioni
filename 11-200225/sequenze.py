#!/usr/bin/env python3

def succ(curr):
    return [curr[2] ^ curr[3]] + curr[:4]

init = [0, 1, 1, 1, 1]

sequence = [init]

for i in range(0, 15):
    sequence.append(succ(sequence[-1]))


for row in sequence:
    row = (map(lambda x: str(x), row))
    print(' & '.join(row) + ' \\\\')
