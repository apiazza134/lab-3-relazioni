#!/usr/bin/env python3

import matplotlib.pyplot as plt

from lab.oscilloscope import readCSV

plt.style.use("../style.yml")

fig, axes = plt.subplots(
    4, 1, sharex=True,
    gridspec_kw={'hspace': 0, 'height_ratios': 4*[1]}
)

tQ0, Q0, tQ1, Q1 = readCSV('data/5b/Q0-Q1.csv')
axes[0].plot(tQ0, Q0, color='red')  # Q0
axes[1].plot(tQ1, Q1, color='red')  # Q1

tdummy, dummy, tQ2, Q2 = readCSV('data/5b/Q0-Q2.csv')
axes[2].plot(tQ2, Q2, color='red')  # Q2

tdummy, dummy, tQ3, Q3 = readCSV('data/5b/Q0-Q3.csv')
axes[3].plot(tQ3, Q3, color='red')  # Q3

axes[-1].set_xlabel("$ t $ \\ [s]")
axes[0].set_ylabel(" $ Q_0 $")
axes[1].set_ylabel(" $ Q_1 $")
axes[2].set_ylabel(" $ Q_2 $")
axes[3].set_ylabel(" $ Q_3 $")

# plt.show()
plt.savefig('img/5b.png')
