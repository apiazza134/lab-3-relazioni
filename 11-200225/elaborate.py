#!/usr/bin/env python3

from lab import run_chain

run_chain([
    '2.py',
    '3.py',
    '4b.py',
    '4d.py',
    '5b.py'
])
