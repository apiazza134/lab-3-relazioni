\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo

\begin{document}
\maketitle

\section{D-Latch con enable}
Si è assemblato il circuito e si sono alimentati gli integrati con
\[ V \ped{CC} = \sivar{2.z}{VCC}. \]

Il circuito, mostrato in Figura~\ref{fig:DlatchSchema}, è composto da un $ RS $-latch i cui ingressi sono collegati a due porte \texttt{NAND}; due ingressi di queste ultime sono collegati tra di loro e all'input $ E $ (\emph{enable}), mentre gli altri due sono forzati ad essere l'uno l'opposto dell'altro da un gate \texttt{NOT} (e uno di questi è collegato all'input $ D $).
L'equazione maestra è
\[ Q(t + \Delta t) = \overline{\nand(D,E)} + \nand(\overline{D}, E) \cdot Q(t) = E \cdot D + \overline{E} \cdot Q(t) \]
che corrisponde alla tabella di verità
\begin{center}
    \begin{tabular}{cccc}
      \toprule
      $ E $ & $ D $     & $ Q(t) $  & $ Q(t + \Delta t) $ \\ \midrule
      0     & \dontcare & 0         & 0 \\
      0     & \dontcare & 1         & 1 \\
      1     & 0         & \dontcare & 0 \\
      1     & 1         & \dontcare & 1 \\ \bottomrule
    \end{tabular}
\end{center}
L'uscita $ Q $ si comporta dunque come una memoria da un bit se $ E $ è ``basso'', mentre è eguale all'input se $ E $ è ``alto''. Questo è rispecchiato dalle osservazioni fatte con l'oscilloscopio, i cui screenshot si riportano in Figura~\ref{fig:DlatchTruth}.

\begin{figure}[p]
    \centering
    \subfloat[\label{fig:DlatchEnabled}$ E $ ``alto''.]{\includegraphics[scale=0.8]{data/2c-enabled}}\\
    \subfloat[\label{fig:DlatchDisabled0}$ E $ ``basso'' con $ Q $ iniziale ``basso''.]{\includegraphics[scale=0.8]{data/2c-disabled-0}}\hspace{1cm}
    \subfloat[\label{fig:DlatchDisabled1}$ E $ ``basso'' con $ Q $ iniziale ``alto''.]{\includegraphics[scale=0.8]{data/2c-disabled-1}}
    \caption{\label{fig:DlatchTruth}output dell D-Latch con enable. Il \texttt{CH1} corrisponde all'ingresso $ D $, mentre il \texttt{CH2} corrisponde all'uscita $ Q $.}
\end{figure}

Si è infine misurato il ritardo tra il segnale $ Q $ e il segnale $ D $ nella transizione ``basso''-``alto'' e ``alto''-``basso''. Le misure ottenute sono rispettivamente
\[ t\ped{PLH} = \sivar{2.d}{ritLH} \qquad t\ped{PHL} = \sivar{2.d}{ritHL} \]
Tali ritardi sono confrontabili con quanto riportanto sul datasheet del singolo \texttt{NAND}. Notiamo tuttavia che è $ t \ped{PLH} < t \ped{PHL} $; questo è attributibile al fatto che (con riferimento alla Figura~\ref{fig:DlatchSchema}) la transizione ``basso''-``alto'' deve propagarsi solamente attraverso i due gate \texttt{NAND} superiori per arrivare fino all'output $ Q $, indipendentemente dal valore che assume l'uscita del \texttt{NAND} in basso a sinistra. Viceversa, la transizione ``alto''-``basso'' dipende dal particolare valore che assume che il gate \texttt{NAND} in basso a sinistra; quindi la transizione di $ Q $ avviene solo dopo che tutti i gate in gioco hanno cambiato i proprî output.

\begin{figure}[p]
    \centering
    \begin{circuitikz}
        \draw (0,1) node[nand port] (nandSopra) {};
        \draw (0,-1) node[nand port] (nandSotto) {};
        \draw (nandSopra.out) node[anchor=west] {$ Q $};
        \draw (nandSotto.out) node[anchor=west] {$ \overline{Q} $};

        \draw (-2.5,1) node[nand port] (nandD) {};
        \draw (-2.5,-1) node[nand port] (nandNotD) {};

        \node (E) at (nandD.in 1 |-, 0) [anchor=east] {$ E $};

        \draw ($ (nandD.in 1) +(-2,0) $) node[anchor=east] (D) {$ D $} -- (nandD.in 1);
        \node (not) at (-5.5, 0) [rotate=-90, not port, scale=0.5] {};

        \draw (-5.5, |- D) -- (not.in);
        \draw (not.out) |- (nandNotD.in 2);

        \draw (nandSopra.out) -- ++(0,-0.5) -- ($(nandSotto.in 1) +(0,0.5)$) -- (nandSotto.in 1);
        \draw (nandSotto.out) -- ++(0,+0.5) -- ($(nandSopra.in 2) +(0,-0.5)$)--(nandSopra.in 2);

        \draw (nandD.out) -| (nandSopra.in 1);
        \draw (nandNotD.out) -| (nandSotto.in 2);

        \draw (nandD.in 2) -| (E);
        \draw (nandNotD.in 1) -| (E);
    \end{circuitikz}
    \caption{\label{fig:DlatchSchema}schema del D-Latch con enable.}
\end{figure}

\clearpage

\section{Divisori di frequenze}
Si è assemblato il divisore di frequenze e si sono collegate le uscite del contatore a 4 bit sincrono all'input del display esadecimale. Inviando un clock a bassa frequenza al contatore, se ne è verificato il corretto funzionamento tramite il display.

Inviando ora un segnale di frequenza $ f\ped{clock} = \sivar{3.c.freq}{fclock} $, si verifica con l'oscilloscopio che nell'output $ Q_{i} $ è presente un segnale a frequenza comparabile con $ f\ped{clock}/2^{i+1} $. Si riportano in Figura~\ref{fig:divisore} le acquisizioni delle forme d'onda con oscilloscopio. I valori misurati delle frequenze sono
\begin{align*}
  f_{Q_{0}} &= \sivar{3.c.freq}{fQ0} &
                                       f_{Q_{1}} &= \sivar{3.c.freq}{fQ1} \\
  f_{Q_{2}} &= \sivar{3.c.freq}{fQ2} &
                                       f_{Q_{3}} &= \sivar{3.c.freq}{fQ3}
\end{align*}
che confermano quanto atteso.

Si misurano inoltre i tempi di ritardo nella transizione basso-alto tra il clock e $ Q_{i} $\footnote{
    Inteso come la differenza dei tempi al 50\% dell'ampiezza asintotica dei due segnali.
}, ottenendo i seguenti valori:
\[
    t_{Q_{0}} = \sivar{3.c.rit}{t2} \quad
    t_{Q_{1}} = \sivar{3.c.rit}{t4} \quad
    t_{Q_{2}} = \sivar{3.c.rit}{t8} \quad
    t_{Q_{3}} = \sivar{3.c.rit}{t16}
\]
dove nell'errore sulla misura si è tenuto conto del fatto che in tali transizioni il segnale risultava affetto da overshoot e oscillazioni. In un contatore asincrono l'incremento del tempo di ritardo tra $ t_{Q_{i}} $ e $ t_{Q_{i+1}} $ dovrebbe essere costante, in quanto la transizione di ogni $ JK $-Latch è causata dal precedente. Questo non si riscontra nelle misure effettuate, in cui i $ t_{Q_{i}} $ sono comparabili tra loro: infatti in un contatore sincrono la transizione di ogni $ JK $-Latch è causata direttamente dal clock (nonché dallo stato del Latch precedente).

\begin{figure}[h!]
    \centering
    \def\scalaPitture{0.57}
    \subfloat[Clock (\texttt{CH1}) e output $ Q_{0} $ (\texttt{CH2}) a frequenza $ 1/2 $ del clock.]
    {\includegraphics[scale=\scalaPitture]{data/3c-2}}\hspace{1cm}
    \subfloat[Clock (\texttt{CH1}) e output $ Q_{1} $ (\texttt{CH2}) a frequenza $ 1/4 $ del clock.]
    {\includegraphics[scale=\scalaPitture]{data/3c-4}}\\
    \subfloat[Clock (\texttt{CH1}) e output $ Q_{2} $ (\texttt{CH2}) a frequenza $ 1/8 $ del clock.]
    {\includegraphics[scale=\scalaPitture]{data/3c-8}}\hspace{1cm}
    \subfloat[Clock (\texttt{CH1}) e output $ Q_{3} $ (\texttt{CH2}) a frequenza $ 1/16 $ del clock.]
    {\includegraphics[scale=\scalaPitture]{data/3c-16}}

    \caption{\label{fig:divisore}divisore di frequenze.}
\end{figure}

Il divisore di frequenza 1/10 risulta un caso particolare del divisore programmabile. Quest'ultimo è stato realizzato collegando l'uscita \texttt{RCO} (``ripple carry output'') del \texttt{SN74LS163} al pin \texttt{LOAD} dello stesso tramite una porta \texttt{NOT}. Inoltre le porte di input $ A $, $ B $, $ C $ e $ D $ sono state collegate tramite il DIP switch a terra. Dato che \texttt{RCO} è ``alto'' quando tutte le uscite $ Q_{i} $ sono ``alte'', il \texttt{LOAD} (che è normalmente ``alto'') viene attivato quando l'output corrisponde alla \texttt{F}. Al clock successivo le uscite vengono impostate in accordo con gli ingressi $ A \leftrightarrow Q_{0} $, $ B \leftrightarrow Q_{1} $, $ C \leftrightarrow Q_{2} $, $ D \leftrightarrow Q_{3} $ e il conteggio ricomincia. Pertanto se si vuole realizzare un divisore di frequenza $ 1/N $ (con $ 1\leq N \leq 15 $), cioè un dispositivo a $ N $ stati, si imposta l'input in modo che il conteggio parta da $ 16 - N $ (ovvero si imposta $ ABCD $ in modo che questo corrisponda alla scrittura binaria di $ 16 - N $). Sul display si osservano quindi in loop i numeri da $ (16 - N)_{16} $ a \texttt{F}.

Si riporta in Figura~\ref{fig:divisore1-10} l'output dell'oscilloscopio nel caso del divisore di frequenza 1/10, realizzato come detto sopra.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=1]{data/3d-load-rco}
    \caption{\label{fig:divisore1-10}divisore $ 1/10 $. In \texttt{CH1} il \texttt{CLK}, in \texttt{CH2} l'output $ Q_{3} $ (che ha frequenza 1/10 rispetto al clock).}
\end{figure}

\section{Shift register}
Si è assemblato il circuito come richiesto, collegando le uscite $ \{Q_{i}\} $ ai led per verificarne il funzionamento. Si osserva che alla pressione del pulsante (che scollega l'input $ D $ dal ground) si introduce nel primo flip-flop un segnale ``alto'', che viene trasferito al successivo a ogni ciclo di clock. Questo è stato anche confermato dall'osservazione con l'oscilloscopio, le cui acquisizioni, riferite a un comune trigger\footnote{
    L'oscilloscopio è stato configurato in modo da triggerare sull'impulso corrispondente all'input $ D $ per l'acquisizione sui canali $ D $-$ Q_{0} $, e sull'impulso corrispondente all'output $ Q_{0} $ per le acquisizioni $ Q_{0} $-$ Q_{1} $, $ Q_{0} $-$ Q_{2} $ e $ Q_{0} $-$ Q_{3} $. Al fine di avere un comune asse dei tempi nel grafico, la prima acquisizione è stata perciò traslata in modo da far coincidere il singolo impulso in $ Q_{0} $ (della durata di un clock) con lo zero dei tempi.
}, sono riportate in Figura~\ref{fig:shiftRegisterD}.

Alla chiusura dello switch che collega il \texttt{PRESET} al ground, tutti gli output diventano ``alti'' a prescindere dallo stato del clock. In altri termini il \texttt{PRESET} è asincrono.

Collegando l'uscita $ \overline{Q_{3}} $ all'input $ D $ si osserva in ogni output un'onda quadra con frequenza $ 1/8 $ rispetto alla frequenza del clock, ciascuna traslata temporalmente del periodo di un clock rispetto alla precedente. Questo è dovuto al fatto che nel registro scorre in modo ciclico la concatenazione dello stato iniziale degli output e del suo negato. In particolare se si parte dallo stato di \texttt{PRESET}, la sequenza atomica sarà $ 11110000 $ che si ripeterà ogni 8 cicli di clock. Si riporta in Figura~\ref{fig:shiftRegisterLoop} l'acquisizione del clock e dei 4 output in funzione del tempo.

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{img/4b}
    \caption{\label{fig:shiftRegisterD}acquisizione del clock, dell'input $ D $ e dei 4 output $ \{Q_{i}\} $ in funzione del tempo.}
\end{figure}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{img/4d}
    \caption{\label{fig:shiftRegisterLoop}acquisizione del clock e dei 4 output $ \{Q_{i}\} $ in funzione del tempo con $ D = \overline{Q_{3}} $.}
\end{figure}

\section{Generatore di sequenze pseudocasuali}
Si è assemblato il circuito come richiesto. Si riporta in Figura~\ref{fig:randomGenerator} l'acquisizione dei 4 output in funzione del tempo. Si osserva che questi riproducono correttamente la sequenza periodica ``pseudocasuale'' come riportato in Tabella~\ref{tab:pseudo}.

Una sequenza di numeri ``pseudocasuali'', come quella di cui sopra, può essere ottenuta tramite una porta $ \mathtt{XNOR} = \overline{\mathtt{XOR}} $, sempre collegata ai pin $ Q_{2} $ e $ Q_{3} $. La sequenza prodotta utilizzando questa porta risulta essere semplicemente la negazione di quella riportata in Figura~\ref{tab:pseudo}; pertanto in tale caso il numero non presente nella sequenza sarà $ 1111 $. Si verifica inoltre che alimentando lo \texttt{XOR} (\texttt{XNOR}) con le porte $ Q_{0} $ e $ Q_{3} $ si può ottenere una sequenza ``pseudocasuale'' di lunghezza massima (ma ordinata differentemente dalla precedente), riportata in Tabella~\ref{tab:pseudocusu}. Si verifica inoltre che utilizzando altre combinazioni di ingresso allo \texttt{XOR} non si ottengono le sequenze di lunghezza massima.

\begin{table}[h!]
    \centering
    \subfloat[\label{tab:pseudo}\texttt{XOR} collegato $ Q_{2} $ e $ Q_{3} $.]{
        \begin{tabular}{cccc}
          \toprule
          $ Q_{0} $ & $ Q_{1} $ & $ Q_{2} $ & $ Q_{3} $ \\ \midrule
          1 & 1 & 1 & 1 \\
          0 & 1 & 1 & 1 \\
          0 & 0 & 1 & 1 \\
          0 & 0 & 0 & 1 \\
          1 & 0 & 0 & 0 \\
          0 & 1 & 0 & 0 \\
          0 & 0 & 1 & 0 \\
          1 & 0 & 0 & 1 \\
          1 & 1 & 0 & 0 \\
          0 & 1 & 1 & 0 \\
          1 & 0 & 1 & 1 \\
          0 & 1 & 0 & 1 \\
          1 & 0 & 1 & 0 \\
          1 & 1 & 0 & 1 \\
          1 & 1 & 1 & 0 \\
          1 & 1 & 1 & 1 \\
          \bottomrule
        \end{tabular}
    }
    \hspace{2cm}
    \subfloat[\label{tab:pseudocusu}\texttt{XOR} collegato a $ Q_{0} $ e $ Q_{3} $.]{
        \begin{tabular}{cccc}
          \toprule
          $ Q_{0} $ & $ Q_{1} $ & $ Q_{2} $ & $ Q_{3} $ \\ \midrule
          1 & 1 & 1 & 1 \\
          0 & 1 & 1 & 1 \\
          1 & 0 & 1 & 1 \\
          0 & 1 & 0 & 1 \\
          1 & 0 & 1 & 0 \\
          1 & 1 & 0 & 1 \\
          0 & 1 & 1 & 0 \\
          0 & 0 & 1 & 1 \\
          1 & 0 & 0 & 1 \\
          0 & 1 & 0 & 0 \\
          0 & 0 & 1 & 0 \\
          0 & 0 & 0 & 1 \\
          1 & 0 & 0 & 0 \\
          1 & 1 & 0 & 0 \\
          1 & 1 & 1 & 0 \\
          1 & 1 & 1 & 1 \\
          \bottomrule
        \end{tabular}
    }
    \caption{sequenze ``pseudocasuali'' attese usando la porta \texttt{XOR}.}
\end{table}


\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.8]{img/5b}
    \caption{\label{fig:randomGenerator}acquisizione dei 4 output $ \{Q_{i}\} $ in funzione del tempo del generatore di sequenze pseudo-casuali.}
\end{figure}

\dichiarazione

\end{document}
