#!/usr/bin/env python3

import numpy as np
from uncertainties import unumpy as unp
from uncertainties import correlated_values
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from lab import Data
nvs = unp.nominal_values
sds = unp.std_devs

c = 299792458
h_e_exact = 4.13566766e-15


def retta(x, m, q):
    return m*x + q


plt.style.use('../style.yml')
d = Data()
datasets = range(1, 5)

for metodo in ['a', 'b']:
    V0s = np.array([
        d.get('dataset', f'd{dataset}', f'V0{metodo}')
        for dataset in datasets
    ])

    lambdas = np.array([
        d.get('dataset', f'd{dataset}', 'lambda')
        for dataset in datasets
    ])
    f = c/lambdas

    opt, cov = curve_fit(
        retta, nvs(f), nvs(V0s),
        sigma=sds(V0s)
    )

    _opt = correlated_values(opt, cov)

    print(f'=== Metodo {metodo} ===')
    print(f'h/e fit: {_opt[0]}')
    print(f'h/e fit/exact: {_opt[0]/h_e_exact}')
    d.push(_opt[0], r'volt\second', f'metodo-{metodo}', 'he')

    X = np.array([min(nvs(f)), max(nvs(f))])
    plt.plot(
        X, retta(X, *opt)
    )
    plt.errorbar(
        nvs(f), nvs(V0s),
        xerr=sds(f), yerr=sds(V0s),
        linestyle=''
    )
    plt.title(rf'Metodo \texttt{{{metodo.upper()}}}')
    plt.xlabel('$ f $\\ [Hz]')
    plt.ylabel('$ V_0 $\\ [V]')

    plt.savefig(f'img/h-e-fit-{metodo}.png', bbox_inches='tight')
    # plt.show()
    plt.close()

d.save()
