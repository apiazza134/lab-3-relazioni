\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\preambolo["../globals-fisica.yml"]

\begin{document}
\maketitle
\renewcommand{\abstractname}{Nota}
\begin{abstract}
    Questa esperienza è stata svolta a casa, collaborando in maniera telematica, tramite utilizzo di dati degli anni precedenti a causa della chiusura dei laboratori in seguito sospensione dell'attività didattica.
\end{abstract}

\section{Descrizione della misure}
Lo scopo dell'esperienza è la stima del rapporto tra la costante di Planck e la carica dell'elettrone $ h/e $. A tale fine si sfrutta l'effetto fotoelettrico, secondo cui un elettrone può essere estratto da un metallo assorbendo un fotone con energia superiore al lavoro di estrazione $ W_{0} $ e venire emesso con energia cinetica data dalla conservazione dell'energia
\[ K = h f - W_{0} \]
dove $ f $ è la frequenza della radiazione elettromagnetica incidente sul metallo. Gli elettroni estratti per effetto fotoelettrico verranno chiamati \emph{fotoelettroni}. \\

L'apparato sperimentale è schematizzato in Figura~\ref{fig:apparato-sperimentale}. Sul catodo di una fotocella incide un fascio luminoso che, se ha frequenza $ f \geq W_{0}/h $, libera elettroni.
Alla cella è collegato un generatore di tensione continua che genera tra catodo e anodo un campo elettrico concorde alla direzione di moto dei fotoelettroni, dimodoché questi siano rallentati. In serie alla fotocella abbiamo un pico-amperometro per misurare la corrente che scorre nel circuito e in parallelo al generatore un voltmetro.

Se $ V $ è la tensione applicata dal generatore, idealmente si presentano 3 situazioni
\begin{itemize}
    \item se $ h  f < W_{0} $ non riusciamo ad estrarre fotoelettroni e $ I = 0 $;
    \item se $ W_{0} < h f < W_{0} + eV $ riusciamo ad estrarre fotoelettroni ma è comunque $ I = 0 $ in quanto gli elettroni non riescono a superare la differenza di potenziale tra anodo e catodo;
    \item se $ h f > W_{0} + eV $ riusciamo ad estrarre fotoelettroni e questi hanno energia sufficiente a raggiungere l'anodo, per cui misuriamo $ I > 0 $.
\end{itemize}
Ora nell'esperienza la grandezza che riusciamo a variare in modo continuo non è $  f $ ma $ V $: osserviamo quindi che esiste un differenza di potenziale critica
\begin{equation}\label{eq:V0}
    V_{0} = \frac{h}{e}  f - \frac{W_{0}}{e}
\end{equation}
che discrimina tra i due regimi $ I = 0 $ e $ I > 0 $. Il \emph{metodo del potenziale frenante} consiste quindi nel variare la tensione del generatore e determinare il particolare valore di $ V $ al quale si osserva la transizione tra i due regimi, ripetere questa procedura per diverse frequenze e stimare $ h/e $ da un \emph{fit} lineare sulla~\eqref{eq:V0}.

Il valore atteso con cui confrontare i risultati è
\begin{equation}
    \del{\frac{h}{e}}\ped{exp} = \SI{4.14e-15}{\volt\second}.
\end{equation}

Nella realizzazione in laboratorio del \emph{setup} sperimentale sono presenti tuttavia alcuni effetti di cui è necessario tenere conto e che rendono meno diretta la stima di $ h/e $. In particolare:
\begin{itemize}
    \item la luce incidente non è monocromatica ma la frequenza voluta viene selezionata a partire da una lampada a spettro continuo tramite un filtro interferenziale (Figura~\ref{fig:luce}) con una larghezza di banda $ \Delta \lambda = \SI{10}{\nano\meter} $ centrata attorno ad un valore nominale $ \lambda_{0} $. Si terrà conto di questo fatto considerando la lunghezza d'onda della luce incidente come $ \lambda = \lambda_{0} \pm \Delta \lambda /2 $;
    \item la corrente fatta dai fotoelettroni è in realtà sovrapposta ad altre correnti che sono presenti anche quando $ V > V_{0} $ dovuta alla sovrapposizione di numerosi effetti quali:
    \begin{enumerate}
        \item emissione di fotoelettroni dall'anodo, dovuta al sistema di illuminazione del catodo che può colpire incidentalmente anche l'anodo (Figura~\ref{fig:luce});
        \item emissione di elettroni dall'anodo per effetto termoionico;
        \item connessione ohmica tra gli elettrodi dovuta al non perfetto isolamento delle guaine di rivestimento dei cavi.
    \end{enumerate}
    La dipendenza di tale corrente dalla tensione fornita dal generatore viene assunta lineare
    \begin{equation}\label{eq:fit-oscuro}
        I\ped{inv} = bV + I_{0},
    \end{equation}
    dove i parametri $ b $ e $ I_{0} $ non dipendono chiaramente dalla lunghezza d'onda della luce incidente, ma solo dalle caratteristiche fisiche della fotocella.
\end{itemize}

\begin{figure}[h!]
    \centering
    \def\scale{0.3}
    \subfloat[\label{fig:apparato-sperimentale}Circuito.]{
        \includegraphics[scale=\scale]{img/apparato-sperimentale}
    } \\
    \subfloat[\label{fig:luce}Schema del sistema di illuminazione del catodo.]{
        \includegraphics[scale=\scale]{img/luce}
    }
    \caption{apparato sperimentale.}
\end{figure}

Complessivamente ci aspettiamo una dipendenza della corrente dal potenziale della forma
\[
    I = f(V) \theta(V_{0} - V) + I\ped{inv}
\]
dove $ f(V) $ è una funzione che descrive l'andamento della corrente prodotta dai fotoelettroni. Assumendo una distribuzione di Fermi-Dirac per gli elettroni della banda di conduzione si ricava che $ {I\ped{photo} \propto (V_{0} - V)^{3/2}} $: chiaramente tale modello è un po' rozzo in quanto non si tiene conto dell'interazione con i fotoni e si è assunta una distribuzione a elettroni liberi. Possiamo tuttavia considerare un modello efficace della forma
\begin{equation}\label{eq:modello-magico}
    I(V) = a (V_{0} - V)^{\alpha} \theta(V_{0} - V) + b V + I_{0}
\end{equation}
dove $ a $ e $ \alpha $ sono parametri che verranno determinati tramite \emph{fit} e che non riusciamo a mettere esplicitamente in legame con i parametri fisici del sistema.

\section{Dati sperimentali}
Si riportano in Figura~\ref{fig:dati} il plot dei dati sperimentali forniti $ V - I $ per le 4 lunghezze d'onda utilizzate.

\begin{figure}[h!]
    \centering
    \foreach \x in {1,...,4} {
        \subfloat{\includegraphics[scale=0.5]{img/dataset-\x.png}}
    }
    \caption{\label{fig:dati}dati sperimentali di $ I $ in funzione di $ V $.}
\end{figure}

\section{Metodo \texttt{A}}\label{sec:metodoA}
Dato che vogliamo determinare la differenza di potenziale di \emph{cutoff} $ V_{0} $ relativa all'effetto fotoelettrico degli elettroni liberati dal \emph{catodo}, si è proceduto dapprima a depurare i dati dalle correnti non attribuibili all'effetto fotoelettrico in questione.

Assumendo la dipendenza lineare~\eqref{eq:fit-oscuro}, si è quindi effettuato un \emph{fit} sui soli dati con $ I \leq 0 $. Si è ottenuto come parametri di best fit per i 4 dataset forniti:
\begin{center}
    \begin{tabular}{ ccc } \toprule
      $ \lambda\ [\si{ \nano\meter }] $ & $ b\ [\si{ \ohm^{-1} }] $ & $ I_{0}\ [\si{ \ampere }] $ \\ \midrule
      \SI[multi-part-units=single]{ 450+-5 }{ \nothing } & \SI[multi-part-units=single]{ -31+-4 }{ \pico\nothing } & \SI[multi-part-units=single]{ 34+-5 }{ \pico\nothing } \\
      \SI[multi-part-units=single]{ 499+-5 }{ \nothing } & \SI[multi-part-units=single]{ -27+-4 }{ \pico\nothing } & \SI[multi-part-units=single]{ 23+-5 }{ \pico\nothing } \\
      \SI[multi-part-units=single]{ 546+-5 }{ \nothing } & \SI[multi-part-units=single]{ -15+-3 }{ \pico\nothing } & \SI[multi-part-units=single]{ 8+-3 }{ \pico\nothing } \\
      \SI[multi-part-units=single]{ 577+-5 }{ \nothing } & \SI[multi-part-units=single]{ -10+-2 }{ \pico\nothing } & \SI[multi-part-units=single]{ 3+-2 }{ \pico\nothing }\\ \bottomrule
    \end{tabular}
\end{center}
Questi parametri dovrebbero in linea di principio essere indipendenti dalla lunghezza d'onda, cosa chiaramente non verificata data la mutua incompatibilità (eccetto per alcune coppie di valori). La dipendenza dalla lunghezza d'onda di tali parametri, come spiegato più in dettaglio nella sezione~\ref{sec:commenti}, potrebbe essere causata dalla corrente di fotoelettroni emessi dall'anodo.

Si riportano in Figura~\ref{fig:fit-oscuro} i dati sperimentali nella regione $ I \leq 0 $ con la relativa retta di \emph{best-fit}.

La retta di \emph{best-fit} è stata quindi sottratta a tutte le correnti, in modo da depurare i dati lasciando solo le correnti dovute all'effetto fotoelettrico che vogliamo analizzare.
Il valore di $ V_{0} $ è stato quindi determinato come l'ascissa del ``primo'' punto sperimentale per cui $ I - I\ped{inv}\ap{fit} $ fosse compatibile con zero, entro una certa tolleranza $\delta I$.

La soglia di tolleranza va determinata in base alle condizioni sperimentali in cui sono stati presi i dati e alla stabilità della misura fatta dal picoamperometro. Sul manuale di quest'ultimo è riportata per il fondo-scala più piccolo (\SI{2}{\nano\ampere}) una risoluzione di \SI{0.1}{\pico\ampere} e un accuratezza del $0.4\% + 4 \; \mathrm{digit} $: essendo quindi errore nominale molto minore di quello attribuito ai dati sperimentali forniti in tale range di corrente, si è assunto che l'errore riportato pari a \SI{1}{\pico\ampere} fosse una stima ragionevole dell'accuratezza reale nella misura delle correnti.

Si è pertanto scelto di porre $ \delta I = \SI{1}{\pico\ampere} $. Si riportano in Figura~\ref{fig:fit-purified} i dati depurati al netto della ``corrente oscura'', con in evidenza le rette $ I = 0 $ e $ I = \delta I $. Si è ottenuto
\begin{center}
    \begin{tabular}{ ccc } \toprule
      $ \lambda\ [\si{ \nano\meter }] $ & $ f\ [\si{ \tera\hertz }] $ & $ V_{0}\ [\si{ \volt }] $ \\ \midrule
      \SI[multi-part-units=single]{ 577+-5 }{ \nothing } & \SI[multi-part-units=single]{ 520+-5 }{ \nothing } & \SI[multi-part-units=single]{ 721+-4 }{ \milli\nothing } \\
      \SI[multi-part-units=single]{ 546+-5 }{ \nothing } & \SI[multi-part-units=single]{ 549+-5 }{ \nothing } & \SI[multi-part-units=single]{ 802+-4 }{ \milli\nothing } \\
      \SI[multi-part-units=single]{ 499+-5 }{ \nothing } & \SI[multi-part-units=single]{ 601+-6 }{ \nothing } & \SI[multi-part-units=single]{ 971+-5 }{ \milli\nothing } \\
      \SI[multi-part-units=single]{ 450+-5 }{ \nothing } & \SI[multi-part-units=single]{ 666+-7 }{ \nothing } & \SI[multi-part-units=single]{ 1.194+-0.006 }{ \nothing }\\ \bottomrule
    \end{tabular}
\end{center}
Su questi dati si è effettuato un \emph{fit} lineare; in accordo con la~\eqref{eq:V0} il coefficiente angolare è proprio il rapporto cercato $ h/e $. Il grafico con relativo \emph{fit} è riportato in Figura~\ref{fig:he-a}. Si è ottenuto
\begin{equation}\label{eq:risultato-A}
    \del{\frac{h}{e}}\ped{\texttt{A}} = \SI[]{ 3.23+-0.09e-15 }{ \volt\second }
\end{equation}
che è confrontabile, seppur non compatibile, con il valore atteso.

\section{Metodo \texttt{B}}\label{sec:metodoB}
Si è assunto il modello dato dalla~\eqref{eq:modello-magico} e si è direttamente fatto un \emph{fit} sui dati sperimentali lasciando come parametri liberi $ a $, $ V_{0} $, $ \alpha $, $ b $ e $ I_{0} $. I relativi grafici sono riportati in Figura~\ref{fig:fit-modello-magico}. Si sono ottenuti come parametri di \emph{best-fit}:

\begin{center}
    \begin{tabular}{ ccccccc } \toprule
      $ \lambda\ [\si{ \nano\meter }] $ & $ f\ [\si{ \tera\hertz }] $ & $ a\ [\si{ \ampere/\volt^{\alpha} }] $ & $ V_{0}\ [\si{ \volt }] $ & $ \alpha $ & $ b\ [\si{ \ohm^{-1} }] $ & $ I_{0}\ [\si{ \ampere }] $ \\ \midrule
      \SI[multi-part-units=single]{ 577+-5 }{ \nothing } & \SI[multi-part-units=single]{ 520+-5 }{ \nothing } & \SI[multi-part-units=single]{ 2.28+-0.03 }{ \nano\nothing } & \SI[multi-part-units=single]{ 834+-7 }{ \milli\nothing } & \num{ 2.87+-0.04e0 } & \SI[multi-part-units=single]{ -4+-1 }{ \pico\nothing } & \SI[multi-part-units=single]{ -5+-1 }{ \pico\nothing } \\
      \SI[multi-part-units=single]{ 546+-5 }{ \nothing } & \SI[multi-part-units=single]{ 549+-5 }{ \nothing } & \SI[multi-part-units=single]{ 1.34+-0.04 }{ \nano\nothing } & \SI[multi-part-units=single]{ 943+-14 }{ \milli\nothing } & \num{ 2.77+-0.07e0 } & \SI[multi-part-units=single]{ -6+-3 }{ \pico\nothing } & \SI[multi-part-units=single]{ -3+-3 }{ \pico\nothing } \\
      \SI[multi-part-units=single]{ 499+-5 }{ \nothing } & \SI[multi-part-units=single]{ 601+-6 }{ \nothing } & \SI[multi-part-units=single]{ 1.00+-0.04 }{ \nano\nothing } & \SI[multi-part-units=single]{ 1.12+-0.03 }{ \nothing } & \num{ 2.5+-0.1e0 } & \SI[multi-part-units=single]{ -3+-9 }{ \pico\nothing } & \SI[multi-part-units=single]{ -6+-11 }{ \pico\nothing } \\
      \SI[multi-part-units=single]{ 450+-5 }{ \nothing } & \SI[multi-part-units=single]{ 666+-7 }{ \nothing } & \SI[multi-part-units=single]{ 585+-19 }{ \pico\nothing } & \SI[multi-part-units=single]{ 1.32+-0.03 }{ \nothing } & \num{ 2.2+-0.1e0 } & \SI[multi-part-units=single]{ -9+-10 }{ \pico\nothing } & \SI[multi-part-units=single]{ 4+-14 }{ \pico\nothing }\\ \bottomrule
    \end{tabular}
\end{center}
Come si è visto per il \hyperref[sec:metodoA]{metodo \texttt{A}}, i parametri $ b $ e $ I_{0} $ dipendono dalla frequenza ma ora risultano tra di loro compatibili, mentre i valori ottenuti per $ V_{0} $ non sono compatibili con i rispettivi ottenuti in precedenza. Tuttavia tale discrepanza è attribuibile all'arbitrarietà della scelta di $\delta I$ che, nonostante non influenzi troppo la determinazione di $h/e$, si riflette in un offset globale su $V_{0}$ di $\sim\SI{130}{\milli\volt}$ tra i due metodi. \\

Come prima, si è fatto un \emph{fit} di $ V_{0} $ al variare della frequenza $ f $. Il grafico con relativo \emph{fit} è riportato in Figura~\ref{fig:he-b}. Si è ottenuto
\[
    \del{\frac{h}{e}}\ped{\texttt{B}} = \SI[]{ 3.40+-0.09e-15 }{ \volt\second }
\]
che non è, anche in questo caso, compatibile con il valore atteso ma è compatibile con il valore ottenuto nel \hyperref[eq:risultato-A]{metodo \texttt{A}}.

\begin{figure}[p]
    \centering
    \foreach \x in {1,...,4} {
        \subfloat{\includegraphics[scale=0.5]{img/A-oscura-\x.png}}
    }
    \subfloat{\includegraphics[scale=0.6]{img/A-oscura-all.png}}
    \caption{\label{fig:fit-oscuro}dati e \emph{fit} relativi alla ``corrente oscura'' per le diverse lunghezze d'onda e sovrapposizione degli stessi. È evidente la dipendenza dalla lunghezza d'onda.}
\end{figure}

\begin{figure}[p]
    \centering
    \foreach \x in {1,...,4} {
        \subfloat{\includegraphics[scale=0.5]{img/A-purified-\x.png}}
    }
    \caption{\label{fig:fit-purified}dati al netto della ``corrente oscura''. Sono inoltre evidenziate le rette $ I = 0 $ e $ I = \SI{1}{\pico\ampere} $.}
\end{figure}
\begin{figure}[p]
    \centering
    \includegraphics[scale=0.6]{img/h-e-fit-a.png}
    \caption{\label{fig:he-a}\emph{fit} di $ V_0 $ in funzione della frequenza $ f $ della luce incidente.}
\end{figure}

\begin{figure}[p]
    \centering
    \foreach \x in {1,...,4} {
        \subfloat{\includegraphics[scale=0.5]{img/modello-magico-fit-\x.png}}
    }
    \caption{\label{fig:fit-modello-magico}dati e \emph{fit} con il modello~\eqref{eq:modello-magico}.}
\end{figure}
\begin{figure}[p]
    \centering
    \includegraphics[scale=0.6]{img/h-e-fit-b.png}
    \caption{\label{fig:he-b}\emph{fit} di $ V_0 $ in funzione della frequenza $ f $ della luce incidente.}
\end{figure}

\clearpage
\section{Errori sistematici e interpretazione dei parametri}\label{sec:commenti}
La fotocella \texttt{Leybold 55877} utilizzata per l'esperienza è costituita da un anodo in platino, una spira di dimensioni $\SI{30}{\milli \meter }$ in diametro e da un catodo in potassio, materiale caratterizzato da un lavoro di estrazione molto basso in quanto metallo alcalino, che è un arco delle dimensioni di $\SI{40 }{mm }$. Tali caratteristiche diventano rilevanti al fine di capire i possibili errori sistematici che affliggono le misure.

\subsection{Effetto fotoelettrico sull'anodo}
Il raggio di luce monocromatica viene focalizzato dal diaframma a iride così da incidere sul catodo della fotocellula. I fotoelettroni emessi dal catodo si spostano poi verso l'anodo dando luogo a una una corrente.

Tuttavia parte della luce incidente potrebbe incidere sull'anodo dando luogo a una corrente inversa: purché minoritaria, questa diventa più rilevante per valori prossimi a $V\ped{0}$ in quanto i fotoelettroni emessi dall'anodo vengono accelerati dalla differenza di potenziale. Per $ V > V_{0} $ la corrente prodotta dai fotoelettroni emessi dal catodo si annulla e la corrente inversa dell'anodo è misurabile.

Questo effetto è limitabile modificando l'apertura del diaframma. Inoltre cambiando filtro e dunque cambiando lunghezza d'onda, aree differenti del catodo potrebbero essere illuminate: questo effetto è da evitare il più possibile essendo il lavoro di estrazione $W\ped{0}$ dipendente dalle caratteristiche di omogeneità della superficie del materiale.

\subsection{Effetto termoionico e connessione ohmica tra gli elettrodi}
A differenza dell'effetto precedentemente citato ci sono due effetti che non possono essere soppressi.
\begin{itemize}
    \item L'effetto termoionico si ha sia sul catodo che sull'anodo e diventa pià significativo all'aumentare della tensione di \emph{bias}. La corrente $ I\ped{inv} $ misurata per $ V > V\ped{0}$ è principalmente attribuibile a questo contributo e dovrebbe essere osservabile (e quindi quantificabile in modo più preciso) anche in assenza di illuminazione;

    \item I cavi coassiali uscenti dalla scatola metallica in cui è contenuto l'apparato sono connessi al catodo e all'anodo e poi esternamente collegati rispettivamente in parallelo al voltmetro e in serie al picoamperometro. La scatola metallica scherma dal rumore elettronico, ma le guaine di rivestimento dei cavi non sono perfettamente isolanti e questo può comportare una ulteriore corrente inversa tra gli elettrodi, che per via dell'ordine di grandezza delle correnti in gioco, diventa non trascurabile.
\end{itemize}

\subsection{Considerazioni sul metodo \texttt{A}}
Nel \hyperref[sec:metodoA]{metodo \texttt{A}} si determina il valore della corrente inversa interpolando le misure nella regione $I \leq 0 $ con un modello lineare. I parametri $ b $ e $ I_{0} $ non dovrebbero dipendere dalla lunghezza d'onda della radiazione incidente: considerando un modello del tipo equazione di Shockley per un diodo a giunzione per la fotocellula, il modello lineare dovrebbe essere uno sviluppo al primo ordine dell'equazione caratteristica nel range considerato, con $ b $ parametro nell'esponenziale e $ I_{0} $ valore asintotico della corrente.

Tuttavia dalla~\eqref{fig:fit-oscuro} è possibile notare come $ I\ped{inv} $ abbia in realtà andamenti diversi al variare della lunghezza d'onda: per le lunghezze d'onda minori, quindi per fotoni più energetici, il potenziale di frenamento $ V_{0} $ è più elevato e dunque la corrente tende più lentamente al valore asintotico. Questo comporta una deviazione significativa dall'andamento lineare e dunque i parametri del \emph{fit} manifestano necessariamente una dipendenza dalla lunghezza d'onda impiegata.

\subsection{Considerazioni sul metodo \texttt{B}}
Nel \hyperref[sec:metodoB]{metodo \texttt{B}} si adopera l'equazione~\eqref{eq:modello-magico} e in tale caso i parametri $ b $ e $ I_{0} $ risultano compatibili. Nella zona di transizione $ V\sim V_{0} $, il parametro $ a $ tiene conto della dipendenza dalla lunghezza d'onda: dai parametri di \emph{best-fit} si nota infatti che $ a $ cambia significativamente al variare della lunghezza d'onda ed è monotona crescente nella lunghezza d'onda.

L'origine fisica del parametro $\alpha$ nella legge di potenza può essere compresa descrivendo, in prima approssimazione, il gas di elettroni nella banda di conduzione del metallo alcalino di cui è costituito il catodo come un gas degenere di Fermi: ovviamente questo modello considera gli elettroni nel metallo come liberi e non tiene conto delle successive interazioni dei fotoelettroni con il bulbo della fotocella. Si ottengono infatti valori maggiori di $ 3/2 $, gli $ \alpha $ di \emph{best-fit} non risultano compatibili tra loro e risultano monotoni crescenti nella lunghezza d'onda.

\dichiarazione

\end{document}
