#!/usr/bin/env python3

import numpy as np
from uncertainties import unumpy as unp
from uncertainties import correlated_values
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from lab import Data, Table
nvs = unp.nominal_values
sds = unp.std_devs


def retta(x, m, q):
    return m*x + q


plt.style.use('../style.yml')

d = Data()

datasets = [1, 2, 3, 4]
color = {1: 'black', 2: 'tab:green', 3: 'tab:blue', 4: 'tab:purple'}

I_cutoff = 0
I_soglia = 1e-12
V0s = []

fig_all, ax_all = plt.subplots()

ms = []
qs = []
V0s = []

for dataset in datasets:
    V, dV, I, dI = np.loadtxt(f'data/data{dataset}.txt', unpack=True)

    V = unp.uarray(V, dV)
    I = unp.uarray(I, dI)*1e-12

    # Fitto la "corrente oscura"
    mask = (nvs(I) <= I_cutoff)
    not_mask = [not i for i in mask]

    opt, cov = curve_fit(
        retta, nvs(V[mask]), nvs(I[mask]),
        sigma=sds(I[mask])
    )

    m, q = correlated_values(opt, cov)
    print(f'=== Dataset {dataset} ===')
    print(f'm: {m:.2u}')
    print(f'q: {q:.2u}')
    ms.append(m)
    qs.append(q)

    # Disegno le robe
    X = np.array([min(nvs(V[mask])),
                  max(nvs(V[mask]))])

    fig, ax = plt.subplots()

    for plot in ax, ax_all:
        plot.plot(X, retta(X, *opt), color='red')
        plot.errorbar(
            nvs(V[mask]), nvs(I[mask]),
            xerr=sds(V[mask]), yerr=sds(I[mask]),
            linestyle='', color=color[dataset]
        )

        plot.set_xlabel('$ V $ [V]')
        plot.set_ylabel('$ I $ [A]')

    _lambda = d.get('dataset', f'd{dataset}', 'lambda').n * 1e9
    ax.set_title(rf"$ \lambda = \SI{{ {_lambda:.0f} }}{{\nano\meter}} $")
    fig.savefig(f'img/A-oscura-{dataset}.png', bbox_inches='tight')

    # Tolgo la corrente magica
    fig, ax = plt.subplots()
    I_purified = I - retta(nvs(V), *opt)
    ax.errorbar(
        nvs(V), nvs(I_purified),
        xerr=sds(V), yerr=sds(I_purified),
        linestyle='', color='black'
    )

    # calcolo V0
    cutoff = 0.7
    mask = (V < cutoff*max(nvs(V)) + (1 - cutoff)*min(nvs(V)))
    arg_V0 = np.argmin(abs(I_purified[mask] - I_soglia))
    V0 = V[mask][arg_V0]
    V0s.append(V0)
    d.push(V0, 'volt', 'dataset', f'd{dataset}', 'V0a')
    print(f'V0 = {V0}')

    # Disegno la linea magica
    X = np.array([min(nvs(V)), max(nvs(V))])
    ax.plot(X, 2*[I_soglia], color='red')
    ax.plot(X, 2*[0], color='red')

    ax.set_xlabel('$ V $ [V]')
    ax.set_ylabel('$ I $ [A]')
    ax.set_title(rf"$ \lambda = \SI{{ {_lambda:.0f} }}{{\nano\meter}} $")
    fig.savefig(f'img/A-purified-{dataset}.png', bbox_inches='tight')

fig_all.savefig('img/A-oscura-all.png', bbox_inches='tight')

# Faccio le tabelle
tab = Table()
lambdas = np.array([d.get('dataset', f'd{dataset}', 'lambda') * 1e9 for dataset in datasets])
tab.pushColumn(r'\lambda', r'nano\meter', lambdas)
tab.pushColumn('b', r'ohm^{-1}', ms)
tab.pushColumn('I_{0}', 'ampere', qs)
d.recursivePush(tab, 'metodo-a', 'tab-fit')

tab = Table()
c = 299792458
tab.pushColumn(r'\lambda', r'nano\meter', lambdas)
tab.pushColumn(r'f', r'tera\hertz', c/lambdas*1e-3)
tab.pushColumn('V_{0}', 'volt', V0s)
tab.sortRows((1,))
d.recursivePush(tab, 'metodo-a', 'tab-V0')

d.save()
