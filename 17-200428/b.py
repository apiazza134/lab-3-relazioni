#!/usr/bin/env python3

import numpy as np
from uncertainties import unumpy as unp
from uncertainties import correlated_values
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from lab import Data, Table
nvs = unp.nominal_values
sds = unp.std_devs


def modello(x, a, V0, alpha, b, I0):
    return (a*(V0 - x)**alpha if x < V0 else 0) + b*x + I0


modello = np.vectorize(modello)

plt.style.use('../style.yml')

d = Data()

datasets = [1, 2, 3, 4]
pars = []

for dataset in datasets:
    V, dV, I, dI = np.loadtxt(f'data/data{dataset}.txt', unpack=True)

    V = unp.uarray(V, dV)
    I = unp.uarray(I, dI)*1e-12

    init = (1, 1, 1, 1, 1)
    opt, cov = curve_fit(
        modello, nvs(V), nvs(I),
        sigma=sds(I), p0=init
    )

    _opt = correlated_values(opt, cov)
    pars.append(_opt)

    print(f'=== Dataset {dataset} ===')
    print(f'alpha: {_opt[2]}')
    print(f'V0: {_opt[1]}')
    print(f'b: {_opt[3]}')
    print(f'I0: {_opt[4]}')

    d.push(_opt[1], 'volt', 'dataset', f'd{dataset}', 'V0b')

    # Disegno le robe
    plt.errorbar(
        nvs(V), nvs(I), xerr=sds(V), yerr=sds(I),
        linestyle='', color='black'
    )

    X = np.linspace(min(nvs(V)), max(nvs(V)))
    plt.plot(X, modello(X, *opt))

    plt.xlabel('$ V $ [V]')
    plt.ylabel('$ I $ [A]')
    _lambda = d.get('dataset', f'd{dataset}', 'lambda').n * 1e9
    plt.title(rf"$ \lambda = \SI{{ {_lambda:.0f} }}{{\nano\meter}} $")
    plt.savefig(f'img/modello-magico-fit-{dataset}.png', bbox_inches='tight')
    # plt.show()
    plt.close()

# Faccio la tabella
tab = Table()
c = 299792458

pars = np.array(pars).transpose()
lambdas = np.array([d.get('dataset', f'd{dataset}', 'lambda') * 1e9 for dataset in datasets])

tab.pushColumn(r'\lambda', r'nano\meter', lambdas)
tab.pushColumn(r'f', r'tera\hertz', c/lambdas*1e-3)
tab.pushColumn('a', r'ampere/\volt^{\alpha}', pars[0])
tab.pushColumn('V_{0}', 'volt', pars[1])
tab.pushColumn(r'\alpha', None, pars[2])
tab.pushColumn('b', 'ohm^{-1}', pars[3])
tab.pushColumn('I_{0}', 'ampere', pars[4])
tab.sortRows((1,))
d.recursivePush(tab, 'metodo-b', 'tab-fit')

d.save()
