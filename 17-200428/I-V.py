#!/usr/bin/env python3

import numpy as np
from uncertainties import unumpy as unp
import matplotlib.pyplot as plt
from lab import Data

plt.style.use('../style.yml')

d = Data()
datasets = [1, 2, 3, 4]
for dataset in datasets:
    V, dV, I, dI = np.loadtxt(f'data/data{dataset}.txt', unpack=True)

    V = unp.uarray(V, dV)
    I = unp.uarray(I, dI)*1e-12

    plt.errorbar(
        unp.nominal_values(V), unp.nominal_values(I),
        xerr=unp.std_devs(V), yerr=unp.std_devs(I),
        linestyle='', color='black'
    )
    plt.xlabel('$ V $ [V]')
    plt.ylabel('$ I $ [A]')
    _lambda = d.get('dataset', f'd{dataset}', 'lambda').n * 1e9
    plt.title(rf"$ \lambda = \SI{{ {_lambda:.0f} }}{{\nano\meter}} $")
    plt.savefig(f'img/dataset-{dataset}.png', bbox_inches='tight')
    plt.close()
