#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit

import sys

dataset = sys.argv[1]
# dataset = 'B01'

print("==== " + dataset + " ===")

d = 1e6  # passo reticolare in nm
D = {'B01': (227, 3), 'B09': (305, 3)}  # cm distanza dal muro [B01, B04]

# Data load: h in cm
x, h, dh = np.loadtxt(f'data/data{dataset}.txt', unpack=True)

D, dD = D[dataset]

y = np.sin((np.pi/2) - np.arctan(h/D))

Dy = np.cos((np.pi/2) - np.arctan(h/D)) * \
    (np.sqrt((dh/D)**2 + (dD*h/D**2)**2))/(1 + (h/D)**2)


# Fit
def ff(x, a, b):
    return -a*x + b


init = [0.00065, 0.7]
sigma = Dy
w = 1/sigma**2
pars, covm = curve_fit(ff, x, y, init, sigma, absolute_sigma=False)
chi2 = ((w*(y-ff(x, *pars))**2)).sum()
ndof = len(y) - 2

a = pars[0]
vara = covm[0, 0]**0.5
b = pars[1]
varb = covm[1, 1]**0.5
corrab = covm[0, 1]/((covm[1, 1]*covm[0, 0])**0.5)

print(pars, covm.diagonal()**0.5)
print(corrab)
print(chi2, ndof)

# Lambda
lbd = a*d
Dl = vara*d

print("lambda: ", lbd, "+-", Dl, " nm")

# Plot
rr = np.linspace(min(x), max(x), 10000)

plt.style.use("../style.yml")
fig, axes = plt.subplots(2, 1, sharex=True,
                         gridspec_kw={'hspace': 0, 'height_ratios': [5, 2]})

axes[0].errorbar(x, y, Dy, linestyle='', color='black', elinewidth=1)
axes[0].plot(rr, ff(rr, *pars), color='blue')
axes[0].set_ylabel("$ \\sin{\\theta} $")

axes[1].plot(x, (y-ff(x, *pars))/Dy, linestyle='--', marker='.', color='black')
axes[1].plot(rr, np.zeros(len(rr)), color='gray')
axes[1].set_xlabel("$ m $")
axes[1].set_ylabel("res. norm.")

plt.savefig('img/fit' + dataset + '.png')
# plt.show()
