#!/bin/bash

# Prende come singolo argomento la cartella della relazione
mainDir=$1

# Elaboro i dati da inserire nel tex
cd $mainDir
mkdir -p img
mkdir -p tab
./elaborate.py data.yml || exit 1

# Compilo il tex e gli cambio il nome
latexmk -lualatex -interaction=nonstopmode -shell-escape main.tex || exit 2
mv main.pdf ../$PDF_NAME
