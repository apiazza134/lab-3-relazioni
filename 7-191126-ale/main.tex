\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\counterwithin{table}{section}
\counterwithin{figure}{section}
\counterwithin{equation}{section}

\preambolo
\author{Alessandro Piazza}

\begin{document}
\maketitle

\section{Misura del loop-gain}
L'OpAmp utilizzato è un \texttt{TL081}, alimentato con tensioni
\[ V\ped{CC} = \sivar{1}{VCC} \qquad V\ped{EE} = \sivar{1}{VEE} \]
I diodi utilizzati sono dei \texttt{1N4148}.

Si riportano di seguito i valori rilevanti dei componenti utilizzati per la rete di feedback positivo dipendente dalla frequenza
\begin{align*}
  R_{1} &= \sivar{1}{R1} & R_{2} &= \sivar{1}{R2} \\
  C_{1} &= \sivar{1}{C1} & C_{2} &= \sivar{1}{C2}
\end{align*}
mentre per la rete di feedback negativo per l'amplificatore non invertente si sono misurati
\[ R_{3} = \sivar{1}{R3} \qquad R_{4} = \sivar{1}{R4} \qquad R_{5} = \sivar{1}{R5} \]
Il potenziometro ha una resistenza massima tra il pin centrale e un altro pin pari a $ R\ped{P} = \sivar{1}{Rpot} $. Di seguito indicheremo con $ R = \alpha R\ped{P} $ (con $ 0 \leq \alpha \leq 1 $) la resistenza tra il pin centrale e il pin collegato alla resistenza $ R_{4} $.

\subsection{Funzione di loop-gain}
La funzione di loop-gain è data da
\[
  \tilde{L}(\omega) = A(j\omega) \beta(j\omega)
\]
L'amplificazione del ramo di feedback $ A $  è data da
\begin{equation} \label{eq:loop-gain-A}
  A = 1 + \frac{\alpha R\ped{P} + R_{3} + R_{4}}{(1-\alpha) R\ped{P} + R_{5}} = \frac{R\ped{P} + R_{3} + R_{4} + R_{5}}{R\ped{P} + R_{5} -\alpha R\ped{P}}
\end{equation}
indipendente dalla frequenza, in cui abbiamo trascurato il ruolo dei diodi essendo $ V\ped{S} $ ``piccolo'' rispetto alla tensione caratteristica di ``attivazione'' di questi ($ \sim \SI{0.7}{\volt} $). Il fattore $ \beta $ è dato dal rapporto di partizione
\begin{equation} \label{eq:loop-gain-beta}
  \beta(s) = \frac{Z_{2}}{Z_{1} + Z_{2}}
\end{equation}
con
\[
  Z_{1} = R_{1} + \frac{1}{sC_{1}} = R_{1} \frac{s + \omega_{1}}{s} \qquad Z_{2} = \frac{R_{2}}{1 + sC_{2} R_{2}} = R_{2} \frac{\omega_{2}}{s + \omega_{2}}
\]
ove $ \omega_{i} = 1/(R_{i} C_{i}) $. Più esplicitamente
\begin{equation}
  \beta(s) = \frac{1}{1 + \frac{Z_{1}}{Z_{2}}} = \frac{1}{1 + \frac{R_{1}}{R_{2}} \frac{(s + \omega_{1})(s + \omega_{2})}{\omega_{2} s}} = \frac{R_{2}}{R_{1}} \frac{\omega_{2} s}{\omega_{1}\omega_{2} + s^{2} + \left[\omega_{1} + \omega_{2} \left(1 + \frac{R_2}{R_{1}}\right)\right] s}
\end{equation}
che per valori puramente immaginari di $ s $ diventa
\begin{equation}
  \beta(j\omega) = \frac{R_{2}}{R_{1}} \frac{j \omega_{2} \omega}{\left(\omega_{1}\omega_{2} - \omega^{2}\right) + j \left[\omega_{1} + \omega_{2} \left(1 + \frac{R_2}{R_{1}}\right)\right] \omega}\, .
\end{equation}
Essendo $ A $ reale, la pulsazione in cui si ha sfasamento nullo si trova imponendo che $ \beta(j\omega) $ sia reale, ovvero
\begin{equation}\label{eq:loop-gain-freq}
  \omega_{1} \omega_{2} - {\bar{\omega}}^{2} = 0 \quad \Rightarrow \quad \bar{\omega} = \sqrt{\omega_{1} \omega_{2}}
\end{equation}
A tale frequenza si ha
\[
  \tilde{L}(\bar{\omega}) = A \frac{R_{2}}{R_{1}} \frac{1}{1 + \frac{R_{2}}{R_{1}} + \frac{\omega_{1}}{\omega_{2}}}
\]
Nel caso in cui si avesse $ R_{1} = R_{2} = R $ e $ C_{1} = C_{2} = C $, da cui $ \omega_{1} = \omega_{2} = \omega_{0} $, il loop-gain assume la forma più semplice
\begin{equation} \label{eq:loop-gain-approx}
  \tilde{L}(\omega) = A \frac{j \omega/\omega_{0}}{\left(1 - \omega^{2}/\omega_{0}^{2}\right) + 3j \omega/\omega_{0}}
\end{equation}
e la pulsazione a sfasamento nullo è $ \bar{\omega} = \omega_{0} $ e per tale frequenza si ha più semplicemente $ L(\bar{\omega}) = \vert{\tilde{L}(\bar{\omega})}\vert =  A / 3 $.

\subsection{Dipendenza del loop-gain dalla frequenza}
Si fissa la posizione del potenziometro a $ R = \sivar{2}{R} $, in modo tale che sia $ \alpha \simeq 0.44 $. Si collega al terminale ``+'' dell'OpAmp il generatore di funzioni e si invia in ingresso un segnale sinusoidale di ampiezza
\[
  V\ped{S} = \sivar{2}{VS}
\]
e se ne varia la frequenza nel range $ \SI{500}{\hertz}-\SI{3}{\kilo\hertz} $.

Si misurano con l'oscilloscopio la frequenza $ f $ del segnale in ingresso, le ampiezze picco-picco di $ V\ped{S} $ e $ V\ped{A} $, e con i cursori la differenza temporale $ \Delta t $ tra il punto di intersezione di $ V\ped{S}(t) $ e di $ V\ped{A}(t) $ con la linea di riferimento di terra quando i regnali risultano crescenti\footnote{A tale scopo è stato necessario accoppiare gli ingressi dell'oscilloscipio in \texttt{AC} a causa della presenza di un offset di circa \SI{150}{\milli\volt} nel segnale prodotto dal generatore.}. Da tali misura si deduce $ L = V\ped{A}/V\ped{S} $ e lo sfasamento tra i segnali $ \phi = 2\pi f \Delta t $ (pari al modulo e alla fase della funzione di loop-gain complessa). \\

Si riportano in Tabella~\ref{tab:loop-gain} i valori misurati e le grandezze derivate. In Figura~\ref{fig:loop-gain-Bode} si riporta il diagramma di Bode per $ L $ e il Figura~\ref{fig:loop-gain-Fase} la dipendenza della differenza di fase dalla frequenza.

\begin{table}[h!]
  \centering
  \get{2}{tab}
  \caption{misure relative e grandezze derivate al loop-gain.}
  \label{tab:loop-gain}
\end{table}

Nelle stesse figura si riportano inoltre le curve attese per $ \abs{L} $ e dello sfasamento calcolate numericamente con Python facendo uso del pacchetto \texttt{cmath}. Le curve in questione sono riprodotte usando come parametri (resistenze e condensatori) i valori centrali delle misure. In particolare abbiamo graficato il modulo e la fase della funzione di loop-gain complessa $ \tilde{L}(\omega) $. \\

Nel particolare circuito realizzato non conosciamo in modo sufficientemente preciso i valori dei componenti utilizzati, in particolare della capacità: pertanto il grafici riportati in Figura~\ref{fig:loop-gain-Bode} e Figura~\ref{fig:loop-gain-Fase} hanno solo scopo di confronto qualitativo con le misure.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.75]{img/loopgainBode}
  \caption{diagramma di Bode per il loop-gain.}
  \label{fig:loop-gain-Bode}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.75]{img/loopgainfase}
  \caption{sfasamento in funzione della frequenza in carta semi-logaritmica.}
  \label{fig:loop-gain-Fase}
\end{figure}

\subsection{Commenti}
\begin{itemize}
\item A causa dell'errore del 4\% sulle capacità, per stimare la frequenza a sfasamento nullo attesa usiamo la \eqref{eq:loop-gain-freq} da cui
  \[
    \bar{f}\ped{exp} = \frac{1}{2\pi\sqrt{R_{1} C_{1} R_{2} C_{2}}} =  \sivar{2}{fexp}.
  \]
  Possiamo stimare la frequenza a sfasamento nullo in due modi diversi.
  \begin{enumerate}
  \item Si effettua un \textit{fit} numerico (funzione \texttt{curve\_fit} del pacchetto \texttt{scipy} di Python con opzione \texttt{absolute\_sigma=True}) dei dati con un modello basato sulla \eqref{eq:loop-gain-approx}
    \[
      y = \frac{a f}{\sqrt{\left(1 - f^{2}/f_{0}^{2}\right)^{2} + 9 f^{2}/f_{0}^{2}}}
    \]
    che non tiene conto della differenza tra i valori delle resistenza e delle capacità ma è più semplice e dipendente solo da 2 parametri. I parametri di \textit{best fit} ottenuti sono
    \[
      a = \sivar{2}{afit} \qquad f_{0} = \bar{f}\ped{fit} = \sivar{2}{ffit} \qquad \mathrm{corr} = \get{2}{corr} \quad \chi^{2}/\mathrm{dof} = \sivar{2}{chi2_ndof}
    \]
  \item Si cambia la frequenza del generatore fino a quando sull'oscilloscopio non si osserva una sovrapposizione dei due segnali in coincidenza della loro intersezione con la linea di massa. Si ottiene
    \[
      \bar{f}\ped{osc} = \sivar{2}{f0}
    \]
    dove l'incertezza è stata attribuita tenendo conto della difficoltà nel determinare sperimentalmente tale frequenza a causa dello ``spessore'' delle tracce dell'oscilloscopio.
  \end{enumerate}
  In entrambi i casi si ottiene una stima di $ \bar{f} $ compatibile con il valore atteso. Di seguito quando ci riferiremo a tale frequenza intenderemo $ \bar{f} = \bar{f}\ped{osc} $.
\item Fissata la frequenza a $ \bar{f} $ e l'ampiezza del segnale in ingresso a $ \sivar{2}{VS} $, si osserva che l'ampiezza del segnale in uscita aumenta all'aumentare della resistenza del potenziometro del ramo di feedback $ R = \alpha R\ped{pot} $. In particolare
  \begin{align*}
    \alpha &\sim 0 & V\ped{A} &= \sivar{2}{VAmin} \\
    \alpha &\sim 0.5 & V\ped{A} &= \sivar{2}{VA} \\
    \alpha &\sim 1 & V\ped{A} &= \sivar{2}{VAmax}
  \end{align*}
  Tale comportamento è spiegabile considerando il fatto che il modulo del loop-gain $ L = A \abs{\beta} $ dipende da $ \alpha $ solo tramite $ A $\footnote{Supponendo, come verificato nel nostro caso, di essere sufficientemente al di sotto della soglia di conduzione dei diodi.} secondo l'equazione~\ref{eq:loop-gain-A} che è monotona crescente in $ \alpha $ per $ \alpha \in [0, 1] $.
\item Fissata la frequenza del segnale in ingresso a $ \bar{f} $, si osserva una diminuzione del loop-gain all'aumentare dell'ampiezza del segnale in ingresso. Tale diminuzione è dovuta alla presenza dei diodi che per segnali in ingresso tali da farli andare in conduzione, introducono effetti non lineari nel circuito (visibili in Figura~\ref{fig:loop-gain-ampiezzagrossa}) e limitano l'ampiezza di $ V\ped{A} $.

  \begin{figure}[h!]
    \centering
    \includegraphics[scale=0.7,angle=90]{data/2.JPG}
    \caption{risposta non lineare del circuito per ampiezze troppo ``grosse'' del segnali in ingresso. $ V\ped{S} $ è in \texttt{CH1}, $ V\ped{A} $ è in \texttt{CH2}.}
    \label{fig:loop-gain-ampiezzagrossa}
  \end{figure}
\end{itemize}

\section{Oscillatore a Ponte di Wien}
Si disconnette il generatore del terminale ``--'' dell'OpAmp e vi si collega il punto A.
\subsection{Dipendenza dalla posizione del potenziometro}
In $ V\ped{out} $ si osserva una differenza di potenziale oscillante la cui ampiezza e forma dipende dalla posizione del potenziometro. In Figura~\ref{fig:wien-potenziometro} si riportano alcune acquisizioni del segnale per diverse posizioni del trimmer dell'oscilloscopio.

\begin{itemize}
\item Se $ R \leq R\ped{min} $ non si osserva segnale in uscita. Il valore di tale soglia è molto incerto a causa della difficoltà nel regolare in modo fine la posizione del potenziometro e al fatto che vicino a tale soglia, variazioni minime nella posizione del potenziometro causano rapide variazioni in $ V\ped{out} $. Indicativamente si ha $ \SI{5}{\kilo\ohm}\leq R\ped{min} \leq \SI{6}{\kilo\ohm} $, ovvero $ 0.5 \leq \alpha \leq 0.6 $
\item Al di sopra della soglia si osserva in un intervallo di regolazione del potenziometro, un segnale sinusoidale in $ V\ped{out} $. In tale intervallo, l'ampiezza picco-picco di $ V\ped{out} $ risulta crescente all'aumentare di $ R $ in modo non lineare.
\item Più si aumenta $ R $, più il segnale comincia a presentare distorsioni dovute all'effetto non lineare dei diodi.
\item Quando $ V\ped{out} $ raggiunge i $ \SI{27.8}{\volt} $ picco-picco, al solito il segnale si satura a causa del \emph{voltage swing} dell'OpAmp come si può vedere in Figura~\ref{fig:wien-potenziometro-max}
\end{itemize}

La presenza di un limite inferiore a $ R = \alpha R\ped{pot} $ è giustificata dallo studio della stabilità del sistema in base alla funzione di loop-gain. Infatti il sistema in assenza dei diodi dovrebbe auto-oscillare solo nel caso in cui sia soddisfatta \emph{esattamente} la condizione di Barkhausen per cui $ L = 1 \Rightarrow \beta = 1/3, A = 3 $\footnote{Se supponiamo di essere nel caso $ R_{1} = R_{2} $ e $ C_{1} = C_{2} $}. Ora la condizione $ \beta = 1/3 $ è indipendente dalla posizione del potenziometro e abbiamo già visto che $ A $ risulta (per piccoli segnali) crescente in $ \alpha $.

Pertanto se $ \alpha $ è troppo piccolo e tale che $ A < 3 $ allora si ha $ L < 1 $ e quindi, da quanto sappiamo sulla teoria generale sui sistemi con feedback, l'inviluppo di $ V\ped{out} $ decade esponenzialmente nel tempo. Mentre se $ \alpha $ è tale che $ A \gtrsim 3 $, allora $ L \gtrsim 1 $ e  l'inviluppo di $ V\ped{out} $ cresce esponenzialmente; tuttavia tale crescita esponenziale è limitata sia dal \emph{voltage swing} dell'operazionale, ma soprattutto dalla presenza dei diodi, il cui ruolo sarà esaminato più in dettaglio nella sotto-sezione~\ref{subsec:wien-diodi}, che pone un limite dipendente da $ \alpha $. \\

Si osserva tuttavia che la frequenza delle oscillazioni risulta pressochè indipendente dalla posizione del potenziometro pari a
\[
  f = \sivar{4}{f},
\]
con leggere deviazioni che si presentano in contemporanea con le distorsioni non lineari del segnale. La frequenza del segnale è compatibile con la frequenza a sfasamento nullo $ \bar{f} = \sivar{2}{f0} $ determinata in precedenza.

\begin{figure}[h!]
  \centering
  \subfloat[$ R = \SI{6.23 \pm 0.05}{\kilo\ohm} $]{\includegraphics[scale=0.6]{data/3-5.png}}
  \subfloat[$ R = \SI{6.30 \pm 0.05}{\kilo\ohm} $]{\includegraphics[scale=0.6]{data/3-4.png}}
  \subfloat[$ R = \SI{7.12 \pm 0.06}{\kilo\ohm} $]{\includegraphics[scale=0.6]{data/3-3.png}}
  \hfill
  \subfloat[$ R = \SI{8.64 \pm 0.07}{\kilo\ohm} $]{\includegraphics[scale=0.7]{data/3-2.png}}
  \subfloat[$ R = \SI{9.81 \pm 0.08}{\kilo\ohm} $]{\includegraphics[scale=0.7]{data/3-1.png}\label{fig:wien-potenziometro-max}}
  \caption{segnale in uscita dall'oscillatore a ponte di Wien per diversi valori di $ R $}
  \label{fig:wien-potenziometro}
\end{figure}

\subsection{Loop-gain dell'oscillatore}
Si fissa il potenziometro a $ R = \sivar{5}{R} $, poco sopra l'innesco dell'oscillazione. In corrispondenza di tale posizione del ``trimmer'' si misura l'ampiezza picco-picco
\[
  V\ped{out} = \sivar{5}{Vout}.
\]
Per la misura del loop-gain si disconnette il punto A dal terminale ``--'' dell'OpAmp e vi si connette nuovamente il generatore $ V\ped{S} $ per la misura del loop-gain. Il segnale sinusoidale in ingresso ha frequenza pari a quella delle auto-oscillazioni e ampiezza picco-picco
\[
  V\ped{S} = \sivar{5}{VS} \qquad f = \sivar{4}{f}
\]
La differenza di potenziale nel punto A ha ampiezza picco-picco
\[
  V\ped{A} = \sivar{5}{VA}
\]
Da tali misure si ottengono le seguenti stime per l'amplificazione del non-invertente, il fattore di partizione e del modulo del loop-gain
\[
  A = \frac{V\ped{out}}{V\ped{S}} = \sivar{5}{A} \qquad
  \frac{1}{\abs{\beta}} = \frac{V\ped{A}}{V\ped{out}} = \sivar{5}{betainv} \quad
  L = \frac{V\ped{A}}{V\ped{S}} = \sivar{5}{L}
\]
compatibili con i valori attesi $ A\ped{exp} = 3 $ e $ L\ped{exp} = A\abs{\beta} = 1 $.

\subsection{Funzione dei diodi}\label{subsec:wien-diodi}
In assenza dei diodi, come già detto in precedenza, la risposta del sistema non sarebbe ``stabile'' e controllabile. Se la posizione del trimmer è tale da avere $ L < 1 $, il segnale prodotto dalle auto-oscillazioni del sistema $ V\ped{out} $ decade esponenzialmente, mentre se $ L > 1 $, $ V\ped{out} $ cresce esponenzialmente. Naturalmente tale crescita è limitata dal massimo valore della differenza di potenziale erogabile da un OpAmp, che nel nostro caso è pari a $ \pm\SI{13.6}{\volt} $ (il \emph{voltage swing} quando alimentato a $ \pm\SI{15}{\volt} $ dal datasheet). Il segnale in uscita è riportato in Figura~\ref{fig:wien-nodiodi-out}, mentre in Figura~\ref{fig:wien-nodiodi-innseco} si può osservare l'innesco del segnale, la crescita esponenziale e poi la saturazione.

\begin{figure}[h!]
  \centering
  \subfloat[\label{fig:wien-nodiodi-out}]{\includegraphics[scale=0.9]{data/6-nodiodi-2.png}}
  \subfloat[\label{fig:wien-nodiodi-innseco}]{\includegraphics[scale=0.9]{data/6-nodiodi-3.png}}
  \caption{auto-oscillazioni in assenza dei diodi.}
\end{figure}

I diodi hanno la funzione di limitare l'ampiezza massima delle auto-oscillazioni. Tale limitazione è dovuta all'introduzione di componenti non lineari nel circuito (i diodi lavorano in conduzione) che rendono difficile dare una descrizione analitica e quantitativa del comportamento del circuito.

Limitandoci a una descrizione più qualitativa, supponendo di essere nel caso in cui $ L > 1 $, l'inviluppo del segnale $ V\ped{out} $ cresce esponenzialmente. Ad un certo istante la differenza di potenziale ai capi dei diodi è sufficiente a farli andare in conduzione\footnote{Più precisamente uno un conduzione e l'altro i interdizione nei rispettivi semi-periodi.}. Supponendo che l'equazione~\eqref{eq:loop-gain-A} sia ancora una stima ragionevole del guadagno dell'invertente\footnote{Di fatto stiamo considerando solo il caso di piccole deviazioni della resistenza $ R_{\parallel} $ da $ R_{3} $} nella forma
\[
  A = \frac{R\ped{P} + R_{\parallel} + R_{4} + R_{5}}{R\ped{P} + R_{5} -\alpha R\ped{P}}
\]
dove $ R_{\parallel} $ è la resistenza ``efficace'' del parallelo tra $ R_{3} $ e i diodi. Quando i diodi sono in conduzione, questo abbassa il valore efficace di $ R_{\parallel} $ portando quindi ad una diminuzione del guadagno. La presenza dei diodi ha quindi l'effetto di introdurre un effetto di feedback negativo che stabilizza il segnale ad una data ampiezza (dipendente dalla posizione del potenziometro).

Per valori ``grandi'' di $ V\ped{out} $, ma ancora minori del \emph{voltage swing}, non è più lecito utilizzare la formula per $ A $ a causa della risposta fortemente non lineare dei diodi; l'effetto netto della loro presenza è sempre quello di abbassare la resistenza ``efficace'' del ramo di feedback, ma la forma d'onda in uscita non sarà più sinusoidale. \\

In Figura~\ref{fig:wien-diodi-innseco} si riporta l'acquisizione fatta con l'oscilloscopio del processo di innesco in presenza dei diodi. A differenza di quanto osservato in Figura~\ref{fig:wien-nodiodi-innseco}, qui la stabilizzazione avviene a una differenza di potenziale definita $ R $ e si ha una transizione più smussata.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=1.2]{data/6-2}
  \caption{innesco delle auto-oscillazioni in presenza dei diodi}
  \label{fig:wien-diodi-innseco}
\end{figure}

\dichiarazione
\end{document}