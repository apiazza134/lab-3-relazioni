#!/usr/bin/env python3

from uncertainties import unumpy as unp
from uncertainties import correlated_values
import numpy as np
import matplotlib.pyplot as plt
import cmath
from scipy.optimize import curve_fit

from lab import (
    Data, loadtxtSorted, Table, cov2corr, measureMeanUncertaintyArray
)

plt.style.use('../style.yml')

# Carico i parametri del circuito
d = Data()
R1 = d.get('1', 'R1').nominal_value
R2 = d.get('1', 'R2').nominal_value
R3 = d.get('1', 'R3').nominal_value
R4 = d.get('1', 'R4').nominal_value
R5 = d.get('1', 'R5').nominal_value
Rpot = d.get('1', 'Rpot').nominal_value
C1 = d.get('1', 'C1').nominal_value
C2 = d.get('1', 'C2').nominal_value

fexp = 1/(2*np.pi*unp.sqrt(d.get('1', 'R1')*d.get('1', 'R2')
                           * d.get('1', 'C1')*d.get('1', 'C2')))

d.push(fexp, 'hertz', '2', 'fexp')

# Carico i dati dal txt
f, df, VS, divVS, VA, divVA, tau, dtau = loadtxtSorted('./data/2.txt')

f = unp.uarray(f, df)
VS = measureMeanUncertaintyArray(VS, divVS)
VA = measureMeanUncertaintyArray(VA, divVA)
tau = unp.uarray(tau, dtau)

L = VA/VS
phi = - 2*np.pi*f*tau

# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{S}', 'volt', VS)
tab.pushColumn('V\\ped{A}', 'volt', VA)
tab.pushColumn('\\Delta t', 'second', tau)
tab.pushColumn('\\abs{L}', None, L)
tab.pushColumn('\\phi', 'radian', phi)

d.pushTable(tab, '2', 'tab')


# Fit
def Z1(f):
    f1 = 1/(2*np.pi*R1*C1)
    return R1 * (1 - 1j*f1/f)


def Z2(f):
    f2 = 1/(2*np.pi*R2*C2)
    return R2 / (1 + 1j*f/f2)


def beta(f):
    return 1/(1 + Z1(f)/Z2(f))


def Lexp(f):
    alpha = 0.44349228
    RF = R4 + R3 + alpha*Rpot
    RT = R5 + (1-alpha)*Rpot
    return (1 + RF/RT)*abs(beta(f))


def phase(f):
    return np.array(list(
        map(lambda x: cmath.phase(x), list(beta(f)))
    ))


def Lfit(f, a, f0):
    return abs(a * 1j*f / (1 - f**2/f0**2 + 3j*f/f0))


init = (1, 1e3)
opt, cov = curve_fit(
    Lfit, unp.nominal_values(f), unp.nominal_values(L),
    p0=init, sigma=unp.std_devs(L), absolute_sigma=True
)
chi2 = (((unp.nominal_values(L) - Lfit(unp.nominal_values(f), *opt))/unp.std_devs(L))**2).sum()

opt = correlated_values(opt, cov)
d.push(opt[0], 'second', '2', 'afit')
d.push(opt[1], 'hertz', '2', 'ffit')
d.push(opt[0]*opt[1], None, '2', 'Afit')
d.recursivePush(f'{cov2corr(cov)[0][1]:.2f}', '2', 'corr')
d.recursivePush(f'{chi2:.2f}/{len(f)}', '2', 'chi2_ndof')

x = np.logspace(np.log10(5e2), np.log10(3e3))

L = 20*unp.log10(L)

# Plot Bode
plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(L),
    xerr=unp.std_devs(f), yerr=unp.std_devs(L),
    linestyle='', color='black', elinewidth=0.8
)
plt.plot(x, 20*np.log10(Lexp(x)), color='red', linewidth=0.5)

plt.xscale('log')
plt.xlabel(r'$ f \ [\mathrm{Hz}] $')
plt.ylabel(r'$ L \ [\mathrm{dB}] $')

plt.savefig('./img/loopgainBode.png')
plt.close()

# Plot fase

# Balck magic sul plot
diff = (unp.nominal_values(phi) - phase(unp.nominal_values(f)))
diff = diff.mean()

plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(phi),
    xerr=unp.std_devs(f), yerr=unp.std_devs(phi),
    linestyle='', color='black', elinewidth=0.8
)

plt.plot(x, phase(x) + diff, color='red', linewidth=0.5)
plt.xscale('log')
plt.xlabel(r'$ f \ [\mathrm{Hz}] $')
plt.ylabel(r'$ \phi \ [\mathrm{rad}] $')

plt.savefig('./img/loopgainFase.png')
plt.close()

# Salvo tutto
d.save()
