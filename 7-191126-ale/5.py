#!/usr/bin/env python

from lab import Data

d = Data()

A = d.get('5', 'Vout') / d.get('5', 'VS')
L = d.get('5', 'VA') / d.get('5', 'VS')
binv = d.get('5', 'Vout') / d.get('5', 'VA')

d.push(A, None, '5', 'A')
d.push(L, None, '5', 'L')
d.push(binv, None, '5', 'betainv')

d.save()
