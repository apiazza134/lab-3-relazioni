#!/usr/bin/env python3

from uncertainties import unumpy as unp
import numpy as np
import matplotlib.pyplot as plt
import cmath

from lab import Data, loadtxtSorted, Table

d = Data()
plt.style.use('../style.yml')

# 7.d
f, df, tau, dtau, scala = loadtxtSorted("data/7d.txt")

cusu = (scala/250 + 1e-4*(tau) + 0.4e-9)*1e3

f = unp.uarray(f, df)
tau = unp.uarray(tau, dtau)

phi = 2*np.pi*f*tau


# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('\\tau', 'second', tau)
tab.pushColumn('\\phi', 'radian', phi)
d.pushTable(tab, '7.d', 'tab')

d.save()


plt.errorbar(
    unp.nominal_values(f), unp.nominal_values(phi),
    xerr=unp.std_devs(f), yerr=unp.std_devs(phi),
    linestyle=''
)

X = np.logspace(
    np.log10(unp.nominal_values(f).min()),
    np.log10(unp.nominal_values(f).max())
)


def fase(f, fT1, fT2):
    return -1*np.array(
        list(map(cmath.phase, 1/((1 + 1j*f/fT1)*(1 - 1j*fT2/f))))
    )


def faseEsatta(f, R1, R2, C1, C2):
    f1 = 1/(2*np.pi*R1*C1)
    A1 = 1/(1 + 1j*f/f1)

    f2 = 1/(2*np.pi*R2*C2)
    A2 = 1/(1 - 1j*f2/f)

    A = A1*A2/(A1*A2*R1/R2 + 1)
    return -1*np.array(
        list(map(cmath.phase, A))
    )


R1 = d.get('5.a', 'R1')
C1 = d.get('5.a', 'C1')
R2 = d.get('6.a', 'R2')
C2 = d.get('6.a', 'C2')

plt.plot(
    X, faseEsatta(
        X,
        R1.nominal_value, R2.nominal_value,
        C1.nominal_value, C2.nominal_value
    ),
    linewidth=0.5
)

plt.xscale('log')
plt.xlabel('$\\log_{10} (f/1 \\mathrm{Hz})$')
plt.ylabel('$\\Delta\\phi \\ [\\mathrm{rad}]$')
plt.savefig("img/passaBandaFase.png")
plt.close()
