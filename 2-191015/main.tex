\documentclass[a4paper, 11pt]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\renewcommand{\thesubsection}{\arabic{subsection}}
\counterwithin{table}{subsection}
\counterwithin{figure}{subsection}
\counterwithin{equation}{subsection}

\preambolo

\begin{document}
\maketitle

\section*{Filtro passa--basso}

\subsection{Progettazione del circuito}
\begin{enumerate}[label=(\alph*)]
    \item Si riporta in Figura~\ref{fig:passaBasso} lo schema circuitale del passa--basso.
    \begin{figure}[h!]
        \centering
        \begin{circuitikz}
            \draw (0,2) node[ocirc, label=$ V\ped{in} $]{}
            to[R=$R_{1}$] (3,2)
            to[C=$ C_{1} $] (3,0) node[ground]{};
            % \node[ground] at (2,0){};
            \draw (3,2) to (4,2) node[ocirc, label=$ V\ped{out} $]{};
        \end{circuitikz}
        \caption{\label{fig:passaBasso} filtro passa--basso.}
    \end{figure}

    \item Come frequenza di taglio teorica abbiamo scelto $ f\ped{T, teo} = \SI{2}{\kilo\hertz} $, così da attenuare il segnale a \SI{2}{\kilo\hertz} di un fattore $ 1/\sqrt{2} \simeq 0.7 $ e quello a \SI{20}{\kilo\hertz} di un fattore $ 1/\sqrt{101} \simeq 0.1 $, per avere un fattore di soppressione di circa 7. Tale scelta è stata motivata dalle seguenti considerazioni:
    \begin{itemize}
        \item Posto $ f_{1} = \SI{2}{\kilo\hertz} $ e $ f_{2} = \SI{20}{\kilo\hertz} $ il fattore di soppressione è della forma
        \[
            \mathfrak{A}^{2}(f\ped{T}) \coloneqq \frac{\abs{A(f_{1})}^{2}}{\abs{A(f_{2})}^{2}} = \frac{f\ped{T}^{2} + f_{2}^{2}}{f\ped{T}^{2} + f_{1}^{2}}
        \]
        che è una funzione monotona decrescente in $ f\ped{T} \geq \SI{0}{\hertz} $ e presenta massimo in $ f\ped{T} = \SI{0}{\hertz} $ dove $ \mathfrak{A}(\SI{0}{\hertz}) = f_{2} / f_{1} $;
        \item tuttavia se si scegliesse $ f\ped{T} = \SI{0}{\hertz} $ (oltre all'impossibilità nella realizzazione pratica) si avrebbe $ A(f) = 0 $ per ogni frequenza, cioè il circuito non si comporterebbe come passa--basso;
        \item risulta quindi difficile esprimere la condizione voluta in termini matematicamente precisi: $ f\ped{T} $ dovrebbe essere scelta il più ``piccola'' possibile, ma non minore di $ f_{1} $ per ridurre attenuazioni non volute del segnale a $ \SI{2}{\kilo\hertz} $ ma ``sufficientemente'' minore di $ f_{2} $ per poter attenuare il segnale a $ \SI{20}{\kilo\hertz} $. Pertanto $ f\ped{T} = f_{1} $ ci è sembrato un giusto compromesso.
    \end{itemize}
    \item Affinché il comportamento del passa--basso non venga perturbato dalla presenza del carico a valle $ R\ped{L} = \SI{100}{\kilo\ohm} $ l'impedenza in uscita dal circuito $ Z\ped{out} $ deve essere trascurabile rispetto a quella del carico:
    \[
        \abs{Z\ped{out}} = \abs{\left(\frac{1}{R_{1}} + j\omega C_{1}\right)^{-1}} \ll R\ped{L}
        \quad \Rightarrow \quad
        R_{1} \ll R\ped{L} \sqrt{1 + \omega^{2} R_{1}^{2} C_{1}^{2}} = R\ped{L} \sqrt{1 + \left(\frac{f}{f\ped{T}}\right)^{2}}.
    \]
    Dunque ci basta imporre che
    \[
        R_{1} \ll \SI{100}{\kilo\ohm}  \ \sqrt{1 + \left(\frac{\SI{2}{\kilo\hertz}}{\SI{2}{\kilo\hertz}}\right)^{2}} \simeq \SI{141}{\kilo\ohm}.
    \]
    Abbiamo quindi scelto $ R\ped{1, teo} = \SI{8.2}{\kilo\ohm} $.
    \item Nota la frequenza di taglio e $ R_{1} $ si ricava
    \[
        C\ped{1, teo} = \frac{1}{2\pi R\ped{1, teo} f\ped{T, teo}} = \SI{9.7}{\nano\farad}.
    \]
    \item Abbiamo misurato
    \[ R_{1} = \sivar{1.e}{R1} \qquad C_{1} = \sivar{1.e}{C1} \]
    da cui si ottiene la frequenza di taglio attesa
    \begin{equation}\label{eq:passaBassoTaglioAttesa}
        f\ped{T, exp} = \sivar{1.e}{fT1}.
    \end{equation}
    Le attenuazioni attese sono invece
    \begin{equation}\label{eq:passaBassoAttenuazioniAttese}
        A\ped{exp}(\SI{2}{\kilo\hertz}) = \sivar{1.e}{A1} \qquad A\ped{exp}(\SI{20}{\kilo\hertz}) =  \sivar{1.e}{A2}
    \end{equation}
\end{enumerate}

\subsection{Risposta in frequenza e diagramma di Bode}
\begin{enumerate}[label=(\alph*)]
    \item In Tabella~\ref{tab:passaBassoBode} sono riportate le attenuazioni in funzione delle frequenze. Si osserva che le attenuazioni a $ f \simeq \SI{2}{\kilo\hertz} $ e a $ f \simeq \SI{20}{\kilo\hertz} $ ottenute dalle misure sono compatibili con quelle attese~\eqref{eq:passaBassoAttenuazioniAttese}.

    \item Si è misurato
    \[ f\ped{T,A} = \sivar{2.b}{fTA} \]
    che è in accordo con la~\eqref{eq:passaBassoTaglioAttesa}.

    \item Facendo un fit con una retta nel diagramma di Bode, per i soli punti con frequenza maggiore di $ \sim \SI{5}{\kilo\hertz} $ (arbitrariamente scelta), si sono ottenuti come parametri ottimali
    \[
        m = \sivar{2.b}{m} \qquad
        q = \sivar{2.b}{q}
    \]
    La pendenza è compatibile con quella attesa di $ \SI{-20}{\deci\bel/decade} $.
    Lo zero della retta di \emph{best fit} è legato alla frequenza di taglio tramite
    \[ f\ped{T,B} = 10^{-q/m} = \sivar{2.b}{fTrette} \]
    dove si è tenuto conto della correlazione tra i parametri di \emph{best fit}. Il valore ottenuto è nuovamente in accordo con le misure prima ottenute.
    Il diagramma di Bode e la retta di \emph{best fit} sono riportati in Figura~\ref{fig:passaBassoBode}.
    \item Tutti i valori ottenuti per la frequenza di taglio, tramite misura dei valori dei componenti \texttt{RC}, tramite individuazione del fattore di attenuazione $ \SI{-3}{\deci\bel} $ e tramite \emph{fit} dell'andamento di $ A $ ad alte frequenze, risultano mutuamente compatibili e con incertezze comparabili. Come si ci poteva aspettare, l'incertezza più bassa è stata ottenuta come risultato del \emph{fit} (il valore numerico dipende da più misure).
\end{enumerate}

\begin{table}[h!]
    \centering
    \tab{2.b}{tab}
    \caption{\label{tab:passaBassoBode} attenuazione del filtro passa--basso \texttt{RC} in funzione della frequenza.}
\end{table}

\begin{figure}[h!]
    \centering
    \includegraphics[scale=0.85]{img/passaBassoBode.png}
    \caption{\label{fig:passaBassoBode} diagramma di Bode per il filtro passa--basso \texttt{RC}. In rosso le frequenze alte, in verde la retta di \emph{best fit} e l'asse $ A[\si{\decibel}] = 0 $ fino alla frequenza di taglio.}
\end{figure}

\subsection{Risposta ad un'onda quadra}
La risposta del circuito ad un'onda quadra è un segnale a ``pinna di squalo''. Il tempo di discesa (definito come il tempo impiegato dal segnale per passare dal 90\% del massimo al 10\%) previsto dal modello è dato da $ t\ped{disc} = RC \, \log{9} = \log{9} / (2\pi f\ped{T}) $\footnote{Con $ \log $ si intende il logaritmo in base $ e $.}. Utilizzando i cursori si ottiene $ t\ped{disc} = \sivar{3.z}{tdCur} $ (con errore dominato dall'incertezza nella determinazione dell'intersezione tra il segnale ed livello 10\% o 90\%) da cui si ottiene una stima della frequenza di taglio \[ f\ped{T} = \sivar{3.z}{fTCur} \] compatibile con quanto trovato precedentemente.

La misura del tempo di salita può anche essere effettuata direttamente tramite l'apposita funzione dell'oscilloscopio. In tale caso si ottiene $ t\ped{disc} = \sivar{3.z}{tdMis} $. Da tale valore si ha $ f\ped{T} = \sivar{3.z}{fTMis} $.

\subsection{Commenti}
\begin{enumerate}[label=(\alph*)]
    \item L'impedenza in ingresso nel circuito è
    \[ Z\ped{in}(f) = R \left( 1 - j \frac{f\ped{T}}{f} \right) \]
    dove, al solito, $ f\ped{T} = 1/(2\pi RC) $.
    A bassa frequenza ($ f \ll f\ped{T} $) il termine con la frequenza di taglio domina sull'altro e quindi si ha
    \[ Z\ped{in}(f) \simeq -jR \frac{f\ped{T}}{f} \]
    cioè, come ci si aspetta, il filtro si comporta come un circuito aperto (per via del condensatore, la cui impedenza diverge per $ f \to \SI{0}{\hertz} $). Ad alta frequenza ($ f \gg f\ped{T} $), invece, avviene l'opposto e quindi abbiamo
    \[ Z\ped{in}(f) \simeq R \]
    cioè il filtro si comporta come se il condensatore non ci fosse. Alla frequenza di taglio, infine,
    \[ Z\ped{in}(f\ped{T}) = R (1-j). \]
    \item Nel caso in cui si aggiunga una resistenza di carico $ R\ped{L} $ in parallelo al condensatore, l'impedenza in ingresso diventa
    \[ Z\ped{in} = \frac{R+R\ped{L} + j \dfrac{f}{f\ped{T}}R\ped{L}}{1 + j \dfrac{f}{f\ped{T}} \dfrac{R\ped{L}}{R}}. \]

    Se $ R\ped{L} \simeq \SI{100}{\kilo\ohm} $ vale $ R\ped{L} \gg R $ e quindi la precedente diventa
    \[ Z\ped{in} \simeq \frac{R\ped{L} \left(1 + j \dfrac{f}{f\ped{T}}\right)}{1+j \dfrac{f}{f\ped{T}}\dfrac{R\ped{L}}{R}}. \]
    Nel caso $ f \ll f\ped{T} $ questa è approssimativamente $ R\ped{L} $ (il condensatore è come un circuito aperto e la resistenza dominante è quella di carico). Per $ f \gg f\ped{T} $, invece, $ Z\ped{in} $ è approssimativamente $ R $ (il condensatore è come un cortocircuito per cui l'unica resistenza che conta è $ R $).
    Se $ R\ped{L} \simeq \SI{10}{\kilo\ohm} $, $ R $ e $ R\ped{L} $ sono comparabili e quindi non possiamo trascurare alcun termine senza sapere nulla sul rapporto $ f/f\ped{T} $.
    Se però $ f \ll f\ped{T} $ allora si possono trascurare i termini che moltiplicano le $ j $ e quindi $ R\ped{in} \simeq R + R\ped{L} $. Al contrario, se $ f \gg f\ped{T} $, $ Z\ped{in} \simeq R $.
    Osserviamo che l'effetto della presenza della resistenza di carico $ R\ped{L} $ è di cambiare il comportamento del filtro a ``basse frequenze'' (sempre intese nel senso di molto minori della frequenza di taglio $ f\ped{T} $), facendo quindi deviare il filtro dal suo comportamento ideale.
    Al contrario, ad alte frequenze il comportamento è ``universale''. Indipendentemente dalla resistenza di carico, si può trascurare l'impedenza del condensatore $ 1/(2\pi j f C) \simeq 0 $ e sostituire quest'ultimo con un cortocircuito. In questo modo l'unica resistenza ``vista'' dal circuito è $ R $ e l'impedenza in ingresso non può dipendere da altri parametri.
\end{enumerate}



\section*{Filtro passa--basso}

\subsection{Filtro \texttt{RC} passa--basso}
\begin{enumerate}[label=(\alph*)]
    \item Resistenza e capacità misurate con il multimetro digitale:
    \begin{equation}\label{eq:R1C1multimetro}
        R_{1} = \sivar{5.a}{R1} \qquad  C_{1} = \sivar{5.a}{C1}
    \end{equation}
    La frequenza di taglio attesa è quindi $ f\ped{T1, exp} = \sivar{5.a}{fT1} $.
    \item Alla frequenza $ f = \sivar{5.b}{fMax} \ll f\ped{T1, exp} $ abbiamo misurato $ V\ped{in}\ap{max} = \sivar{5.b}{VinMax} $ e $ V\ped{out}\ap{max} = \sivar{5.b}{VoutMax} $ da cui
    \[ A_{1}\ap{max} = \sivar{5.b}{Amax} \]
    La frequenza di taglio, misurata approssimativamente in corrispondenza di un guadagno di $ \SI{-3}{\decibel} $, è di
    \[ f\ped{T1} = \sivar{5.b}{fT}. \]
\end{enumerate}

\subsection{Filtro \texttt{RC} passa--alto}
\begin{enumerate}[label=(\alph*)]
    \item Resistenza e capacità misurate con il multimetro digitale:
    \begin{equation}\label{eq:R2C2multimetro}
        R_{2} = \sivar{6.a}{R2} \qquad  C_{2} = \sivar{6.a}{C2}
    \end{equation}
    La frequenza di taglio attesa è quindi $ f\ped{T2, exp} = \sivar{6.a}{fT2} $.
    \item Alla frequenza $ f = \sivar{6.b}{fMax} \gg f\ped{T2, exp} $ abbiamo misurato $ V\ped{in}\ap{max} = \sivar{6.b}{VinMax} $ e $ V\ped{out}\ap{max} = \sivar{6.b}{VoutMax} $ da cui
    \[ A_{2}\ap{max} = \sivar{6.b}{Amax} \]
    La frequenza di taglio, misurata approssimativamente in corrispondenza di un guadagno di $ \SI{-3}{\decibel} $, è di
    \[ f\ped{T2} = \sivar{6.b}{fT}. \]
\end{enumerate}

\subsection{Risposta in frequenza del passa--banda e diagramma di Bode}

In Tabella~\ref{tab:passaBanda} si riportano le misure delle ampiezza picco-picco dei segnali in entrata $ V\ped{in} $ e in uscita $ V\ped{out} $ dal passa--basso al variare della frequenza $ f $. Nella stessa tabella è riportato anche l'attenuazione del segnale $ A = V\ped{out}/V\ped{in} $.

In Figura~\ref{fig:passaBandaBode} riportiamo l'attenuazione in decibel $ A[\si{\decibel}] = 20 \log_{10} A $ in funzione della frequenza in scala semi-logaritmica. Sovrapposti ai dati sperimentali vi sono le curve di \emph{best fit} richieste nel punto~\ref{itm:passaBandaFit}.

\begin{enumerate}[label=(\alph*)]
    \item \label{itm:passaBandaFit} Da un punto di vista modellistico, supponendo di avere $ f\ped{T1} \ll f\ped{T2} $ e di poter considerare indipendenti i due sotto-circuiti, ci aspettiamo un andamento del tipo
    \begin{equation}\label{eq:passaBandaIndip}
        \abs{A} \simeq \abs{A_{1}} \abs{A_{2}} \simeq
        \begin{cases}
            f/f\ped{T2} & f \ll f\ped{T2} \ll f\ped{T1} \\
            1           & f\ped{T2} \ll f \ll f\ped{T1} \\
            f\ped{T1}/f & f\ped{T2} \ll f\ped{T1} \ll f \\
        \end{cases}
    \end{equation}
    e quindi un andamento lineare nelle tre regioni nel diagramma di Bode (in particolare costante nella regione intermedia). Abbiamo effettuato un \emph{fit} numerico lineare dei dati (Figura~\ref{fig:passaBassoBode}) separatamente per basse frequenze ed alte frequenza rispetto alle frequenze di taglio dei due sotto-circuiti. Abbiamo \emph{arbitrariamente} deciso di considerare come ``basse'' frequenze sotto i \SI{100}{\hertz} e come ``alte'' frequenze sopra di \SI{30}{\kilo\hertz}. In tali regimi la funzione di \emph{fit} utilizzata è per le basse frequenze
    \[
        A[\si{\decibel}] = 20\log_{10} A \simeq 20\log_{10}{f} - 20 \log_{10}{f\ped{T2}} = m_{2} x + q_{2}
    \]
    e similmente per le alte frequenze
    \[
        A[\si{\decibel}] = 20\log_{10} A \simeq -20\log_{10}{f} + 20 \log_{10}{f\ped{T1}} = m_{1} x + q_{1}
    \]
    Si riportano di seguito i parametri di \emph{best fit} ottenuti per alte frequenze e la matrice di correlazione:
    \[
        m_{1} = \sivar{7.a}{malto} \qquad q_{1} = \sivar{7.a}{qalto} \qquad
        \mathrm{corr} = \get{7.a}{corralto}
    \]
    La pendenza non risulta compatibile quella attesa di $ \SI{-20}{\deci\bel/decade} $, seppur prossima a tale valore. Lo zero della retta di best fit è legato alla frequenza di taglio tramite
    \[ f\ped{T1} = 10^{-q_{1}/m_{1}} = \sivar{7.a}{fTrettealto} \]
    dove si è tenuto conto della correlazione tra i parametri di best fit.
    Invece a basse frequenze si ottiene
    \[
        m_{2} = \sivar{7.a}{mbasso} \qquad q_{2} = \sivar{7.a}{qbasso} \qquad
        \mathrm{corr} = \get{7.a}{corrbasso}
    \]
    La pendenza è compatibile con quella attesa di $ \SI{20}{\deci\bel/decade} $.
    Lo zero della retta di best fit è legato alla frequenza di taglio tramite
    \[ f\ped{T2} = 10^{-q_{2}/m_{2}} = \sivar{7.a}{fTrettebasso} \]
    dove l'errore, al solito, è stato calcolando a partire dalla matrice di covarianza.

    \item Dai valori ottenuti e dal grafico dei dati sperimentali possiamo fare le seguenti osservazioni.
    \begin{itemize}
        \item L'approssimazione di sotto-circuiti indipendenti non modellizza in maniera sufficiente il comportamento reale del circuito. Ciò è particolarmente evidente nella zone $ f\ped{T2} \leq f \leq f\ped{T1} $ in cui l'attenuazione non è unitaria (come ci si aspetterebbe da $ A_{1}\ap{max} A_{2}\ap{max} \simeq 0.98 $) ma vale al massimo \[ A_{0} = \sivar{7.a}{Amax} \] compatibile con il fatto che in realtà $ A_{0} \simeq 1/2 $.
        \item Infatti se $ A_{1} $ e $ A_{2} $ sono le risposte in frequenza del passa--basso e del passa-alto, la risposta in frequenza esatta del passa--banda è data da
        \begin{equation}\label{eq:passaBandaExact}
            A = \frac{A_{1} A_{2}}{A_{1} A_{2} \dfrac{R_1}{R_{2}} + 1}.
        \end{equation}
        Nel nostro caso $ R_{1} \simeq R_{2} $ e al centro della banda passante ($ f\ped{T2} \ll f \ll f\ped{T1} $) si ha $ A_{1} \simeq A_{2} \simeq 1 $, da cui si ottiene la stima di $ A_{0} $ del punto precedente.
        \item Di conseguenza non è più vero che a basse ed alte frequenze la risposta in frequenza è data dalla~\eqref{eq:passaBandaIndip} e infatti le frequenze di taglio ottenute non sono compatibili con quelle ottenute separatamente nell'analisi del passa--basso e del passa-alto.
        \item D'altra parte, una ragionevole richiesta per assicurare l'indipendenza dei due circuiti è che si abbia $ \abs{Z\ped{out, 1}} \ll \abs{Z\ped{in,2}} $ ad ogni frequenza (e indipendentemente da questa). Nel caso in questione
        \[
            \abs{Z\ped{out, 1}} = \abs{\frac{R_{1}}{1 + j \omega R_{1} C_{1}}}^{2} = \frac{R_{1}^{2}}{1 + \omega^{2} R_{1}^{2} C_{1}^{2}}
            \quad
            \abs{Z\ped{in, 2}}^{2} = \abs{\frac{1 + j \omega R_{2} C_{2}}{j\omega C_{2}}}^{2} = \frac{1 + \omega^{2} R_{2}^{2} C_{2}^{2}}{\omega^{2} C_{2}^{2}}
        \]
        Ora $ \abs{Z\ped{out, 1}} \leq 1/(\omega C_{1}) $ (e si ha $ \abs{Z\ped{out, 1}} \simeq 1/(\omega C_{1}) $ per $ f \gg f\ped{T,1} $) e $ \abs{Z\ped{in, 2}} \geq 1/(\omega C_{2}) $ (e si ha $ \abs{Z\ped{out, 1}} \simeq 1/(\omega C_{2}) $ per $ f \ll f\ped{T,2} $). Una possibile condizione da imporre che garantisca l'indipendenza dei due circuiti è quindi
        \[
            \abs{Z\ped{out, 1}} \leq \frac{1}{\omega C_{2}} \ll \frac{1}{\omega C_{2}} \leq \abs{Z\ped{in, 2}} \qquad \Rightarrow \qquad C_{2} \ll C_{1}.
        \]
        Ma con le scelte fatte si ha invece $ C_{2} \simeq 10 \times C_{1} $.


        \item Il diagramma di Bode mantiene l'andamento lineare nei due regimi.
        \item Le frequenze di intersezione tra la retta di \emph{best fit} nelle regioni a bassa e alta frequenza con la retta orizzontale $ A = A_{0} $ nel digramma di Bode sono invece
        \[
            f\ped{L} = \sivar{7.a}{fL} \qquad f\ped{H} = \sivar{7.a}{fH}
        \]
    \end{itemize}

    \item Essendo $ \abs{A_{1}}, \abs{A_{2}} \leq 1 $, scegliendo $ R_{1} \ll R_{2} $ si ottene dall'equazione~\eqref{eq:passaBandaExact} che $ A \simeq A_{1} A_{2} $.

    \item In Tabella~\ref{tab:passaBandaFase} si riportano i dati di sfasamento in funzione della frequenza in termini di distanza temporale $ \tau $ e di angolo. In Figura~\ref{fig:passaBandaFase}, oltre ai dati sperimentali, è raffigurato il grafico dello sfasamento esatto (cioè senza approssimare i due filtri come agenti in modo indipendente, ricavato dall'equazione~\eqref{eq:passaBandaExact}) in funzione della frequenza, ottenuto facendo uso del pacchetto Python \texttt{cmath}. Per il calcolo si sono adoperati i valori di $ R_{1,2}, C_{1,2} $ misurati con il multimetro (equazioni~\eqref{eq:R1C1multimetro} e~\eqref{eq:R2C2multimetro}).
    Le incertezze sulle misure dei tempi sono dominate dalla difficoltà nell'individuare (con i cursori) le intersezioni del segnale, che, soprattutto ad alte frequenze, non era stabile o ben definito, con l'asse orizzontale.

    Si osserva un buon accordo per tutte le frequenze inferiori a $ {\sim\SI{e5}{\hertz}} $; per frequenze superiori si osserva che lo sfasamento inizia a decrescere, indizio che la modellizzazione del circuito inizia a non essere valida a quelle frequenze, probabilmente perché ci si sta avvicinando al valore della banda passante dell'oscilloscopio (da manuale $ \sim\SI{6}{\mega\hertz} $ quando si usano sonde \texttt{1x}, come nel nostro caso, in cui si sono usati dei cavi coassiali senza attenuazione) e quindi l'impedenza ``di carico'' in serie al passa--banda non può essere trascurata, come si è fatto nel calcolo dello sfasamento atteso.
\end{enumerate}

\begin{table}[p]
    \centering
    \tab{7.a}{tab}
    \caption{\label{tab:passaBanda} $ V\ped{in} $ e $ V\ped{out} $ picco-picco in funzione della frequenza e conseguente attenuazione $ A $.}
\end{table}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.85]{img/passaBandaBode.png}
    \caption{\label{fig:passaBandaBode} diagramma di Bode per il passa--basso. In rosso i punti a $ f \leq \SI{100}{\hertz} $, in blu i punti a $ f \geq \SI{30}{\kilo\hertz} $, in verde le rette di \emph{best fit} e la retta orizzontale in corrispondenza del guadagno massimo.}
\end{figure}

\begin{table}[p]
    \centering
    \tab{7.d}{tab}
    \caption{\label{tab:passaBandaFase} sfasamento in funzione della frequenza in termini di distanza temporale $ \tau $ e di angolo $ \Delta\phi $.}
\end{table}

\begin{figure}[p]
    \centering
    \includegraphics[scale=0.8]{img/passaBandaFase.png}
    \caption{\label{fig:passaBandaFase} sfasamento tra $ V\ped{in} $ e $ V\ped{out} $ in funzione della frequenza.}
\end{figure}

\dichiarazione

\end{document}
