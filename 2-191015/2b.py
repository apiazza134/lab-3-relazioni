#!/usr/bin/env python3

from uncertainties import unumpy as unp
from uncertainties import correlated_values
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from lab import Data, loadtxtSorted, Table, measureMeanUncertaintyArray

d = Data()

f, df, Vin, VinDiv, Vout, VoutDiv = loadtxtSorted('data/2z.txt')

f = unp.uarray(f, df)
Vin = measureMeanUncertaintyArray(Vin, VinDiv)
Vout = measureMeanUncertaintyArray(Vout, VoutDiv)

A = Vout/Vin
logf = unp.log10(f)
Adb = 20*unp.log10(A)  # decibel


# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A', 'decibel', Adb)
d.pushTable(tab, '2.b', 'tab')


# Frequenze alte (> 5100 Hz perché sì)
logfalte = logf[logf > 3.71]
Adbalte = Adb[logf > 3.71]

# Frequenze basse
logfbasse = logf[logf < 3.71]
Adbbasse = Adb[logf < 3.71]


# Fit dei log delle cose
def retta(x, m, q):
    return m*x + q


init = (-20, 20*7.57)
opt, cov = curve_fit(
    retta, unp.nominal_values(logfalte), unp.nominal_values(Adbalte),
    p0=init, sigma=unp.std_devs(Adbalte)
)
# scrivo i parametri ottimali come valori correlati
opt = correlated_values(opt, cov)

# frequenza di taglio ottenuta come intersezione tra le rette
# (la retta orizzontale è stata presa come l'asse x)
logft = -opt[1]/opt[0]
ft = 10**logft

# salvo i risultati
d.push(opt[0], 'decibel', '2.b', 'm')
d.push(opt[1], 'decibel', '2.b', 'q')
d.push(ft, 'hertz', '2.b', 'fTrette')

d.save()


# Grafici
plt.style.use('../style.yml')

plt.errorbar(
    unp.nominal_values(logfbasse), unp.nominal_values(Adbbasse),
    xerr=unp.std_devs(logfbasse), yerr=unp.std_devs(Adbbasse),
    linestyle='', color='black'
)
plt.errorbar(
    unp.nominal_values(logfalte), unp.nominal_values(Adbalte),
    xerr=unp.std_devs(logfalte), yerr=unp.std_devs(Adbalte),
    linestyle='', color='red'
)

X = np.linspace(logft.nominal_value, unp.nominal_values(logfalte).max())
Y = retta(X, *(unp.nominal_values(opt)))
plt.plot(X, Y, color='green', linewidth=0.5)

X = np.linspace(unp.nominal_values(logf).min(), logft.nominal_value)
plt.plot(X, [0]*len(X), color='green', linewidth=0.5)

plt.savefig("img/passaBassoBode.png")
plt.close()
