#!/usr/bin/env python3

import numpy as np
from uncertainties.umath import sqrt as usqrt
from lab import Data

d = Data()

# 1
R1 = d.get('1.e', 'R1')
C1 = d.get('1.e', 'C1')

fT1 = 1/(2*np.pi*R1*C1)

A1 = 1/usqrt(1 + (2e3/fT1)**2)
A2 = 1/usqrt(1 + (20e3/fT1)**2)

d.push(fT1, 'hertz', '1.e', 'fT1')
d.push(A1, None, '1.e', 'A1')
d.push(A2, None, '1.e', 'A2')

# 3
tdCur = d.get('3.z', 'tdCur')
tdMis = d.get('3.z', 'tdMis')

fTCur = np.log(9) / (2*np.pi * tdCur)  # np.log è il logaritmo naturale
fTMis = np.log(9) / (2*np.pi * tdMis)

d.push(fTCur, 'hertz', '3.z', 'fTCur')
d.push(fTMis, 'hertz', '3.z', 'fTMis')

# 5.a
R1 = d.get('5.a', 'R1')
C1 = d.get('5.a', 'C1')

fT1 = 1/(2*np.pi*R1*C1)
d.push(fT1, 'hertz', '5.a', 'fT1')

# 5.b
VinMax = d.get('5.b', 'VinMax')
VoutMax = d.get('5.b', 'VoutMax')

Amax = VoutMax/VinMax

d.push(Amax, None, '5.b', 'Amax')

# 6.a
R2 = d.get('6.a', 'R2')
C2 = d.get('6.a', 'C2')

fT2 = 1/(2*np.pi*R2*C2)

d.push(fT2, 'hertz', '6.a', 'fT2')

# 6.b
VinMax = d.get('6.b', 'VinMax')
VoutMax = d.get('6.b', 'VoutMax')

Amax = VoutMax/VinMax

d.push(Amax, None, '6.b', 'Amax')

d.save()
