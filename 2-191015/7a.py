#!/usr/bin/env python3

from uncertainties import unumpy as unp
from uncertainties import correlated_values
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from lab import (
    Data, loadtxtSorted, measureMeanUncertaintyArray, Table,
    matrixToTex, cov2corr
)

d = Data()
plt.style.use('../style.yml')

f, df, Vin, VinDiv, Vout, VoutDiv = loadtxtSorted("./data/7z.txt")

f = unp.uarray(f, df)
Vin = measureMeanUncertaintyArray(Vin, VinDiv)
Vout = measureMeanUncertaintyArray(Vout, VoutDiv)

A = Vout/Vin
logf = unp.log10(f)
Adb = 20*unp.log10(A)  # decibel

# Tabella
tab = Table()
tab.pushColumn('f', 'hertz', f)
tab.pushColumn('V\\ped{in}', 'volt', Vin)
tab.pushColumn('V\\ped{out}', 'volt', Vout)
tab.pushColumn('A', 'decibel', Adb)
d.pushTable(tab, '7.a', 'tab')

# Frequenze alte (> 30 kHz perché sì)
logfalte = logf[logf > np.log10(3e4)]
Adbalte = Adb[logf > np.log10(3e4)]

# Frequenze basse (< 100 Hz perché sì)
logfbasse = logf[logf < 2]
Adbbasse = Adb[logf < 2]

# Frequenze mancanti
logfcentro = logf[(logf > 2) & (logf < np.log10(3e4))]
Adbcentro = Adb[(logf > 2) & (logf < np.log10(3e4))]

indexMax = np.argmax(unp.nominal_values(A))

d.push(A[indexMax], None, '7.a', 'Amax')

# Grafici dei dati sperimentali
plt.errorbar(
    unp.nominal_values(logfbasse), unp.nominal_values(Adbbasse),
    xerr=unp.std_devs(logfbasse), yerr=unp.std_devs(Adbbasse),
    linestyle='', color='red'
)

plt.errorbar(
    unp.nominal_values(logfcentro), unp.nominal_values(Adbcentro),
    xerr=unp.std_devs(logfcentro), yerr=unp.std_devs(Adbcentro),
    linestyle='', color='black'
)

plt.errorbar(
    unp.nominal_values(logfalte), unp.nominal_values(Adbalte),
    xerr=unp.std_devs(logfalte), yerr=unp.std_devs(Adbalte),
    linestyle='', color='blue'
)


def retta(x, m, q):
    return m*x + q


# ALTA FREQUENZA: fit dei log delle cose
init = (-20, 20*7.57)
opt, cov = curve_fit(
    retta, unp.nominal_values(logfalte), unp.nominal_values(Adbalte),
    p0=init, sigma=unp.std_devs(Adbalte)
)
opt = correlated_values(opt, cov)

logft = -opt[1]/opt[0]
ft = 10**logft
logfH = (Adb[indexMax] - opt[1])/opt[0]
fH = 10**logfH

d.push(opt[0], 'decibel/decade', '7.a', 'malto')
d.push(opt[1], 'decibel', '7.a', 'qalto')
d.push(ft, 'hertz', '7.a', 'fTrettealto')
d.push(fH, 'hertz', '7.a', 'fH')
d.recursivePush(
    matrixToTex(cov2corr(cov)), '7.a', 'corralto'
)


# Grafico
X = np.linspace(logft.nominal_value, unp.nominal_values(logfalte).max())
Y = retta(X, *(unp.nominal_values(opt)))
plt.plot(X, Y, color='green', linewidth=0.5)  # stampo la retta fittata


# BASSA FREQUENZA: fit dei log delle cose
init = (20, -20*np.log10(400))
opt, cov = curve_fit(
    retta, unp.nominal_values(logfbasse), unp.nominal_values(Adbbasse),
    p0=init, sigma=unp.std_devs(Adbbasse)
)
opt = correlated_values(opt, cov)

logft = -opt[1]/opt[0]
ft = 10**logft
logfL = (Adb[indexMax] - opt[1])/opt[0]
fL = 10**logfL

d.push(opt[0], 'decibel/decade', '7.a', 'mbasso')
d.push(opt[1], 'decibel', '7.a', 'qbasso')
d.push(ft, 'hertz', '7.a', 'fTrettebasso')
d.push(fL, 'hertz', '7.a', 'fL')
d.recursivePush(
    matrixToTex(cov2corr(cov)), '7.a', 'corrbasso'
)


# Grafico
X = np.linspace(unp.nominal_values(logfbasse).min(), logft.nominal_value)
Y = retta(X, *(unp.nominal_values(opt)))
plt.plot(X, Y, color='green', linewidth=0.5)  # stampo la retta fittata


# FREQUENZE CENTRALI: grafico della retta orizzontale
X = np.linspace(unp.nominal_values(logfbasse).max(),
                unp.nominal_values(logfalte).min())
plt.plot(X, [unp.nominal_values(Adb).max()] *
         len(X), color='green', linewidth=0.5)

# salvo il grafico (bellissimo)
plt.savefig("img/passaBandaBode.png")
plt.close()

d.save()
