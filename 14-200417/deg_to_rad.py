#!/usr/bin/env python3

from uncertainties import ufloat

from lab import Data
from lab.utils import degstring_to_rad

d = Data()

for dataset in ['A06', 'B04']:
    cusu = d.data['dataset'][f'{dataset}-deg']
    for key, angles in cusu['Hg'].items():
        angle = ufloat(degstring_to_rad(angles[0]),
                       degstring_to_rad(angles[1]))
        d.push(angle, 'radian', 'dataset', dataset, 'Hg', key)

    for ordine in ['righe1', 'righe2']:
        angles = []
        for angle in cusu[ordine]:
            angles.append([
                degstring_to_rad(angle[0]),
                degstring_to_rad(angle[1]),
                'radian'
            ])
        d.recursivePush(angles, 'dataset', dataset, ordine)
        d.recursivePush(cusu[f'n{ordine}'], 'dataset', dataset, f'n{ordine}')


for dataset in ['A06']:
    cusu = d.data['dataset'][f'{dataset}-deg']
    angle = ufloat(degstring_to_rad(cusu['beta0'][0]),
                   degstring_to_rad(cusu['beta0'][1]))
    d.push(angle, 'radian', 'dataset', dataset, 'beta0')

d.save()
