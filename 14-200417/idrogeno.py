#!/usr/bin/env python3

from lab import Data
from uncertainties import ufloat
from uncertainties import unumpy as unp
import numpy as np
import matplotlib.pyplot as plt
# from lab.utils import degstring_to_rad
from scipy.optimize import curve_fit
from lab import Table

data = Data()
datasets_decenti = list(set(data.elab_data['dataset'].keys()) - set(['A06-deg', 'B04-deg']))

# Computo gli angoli theta_d
alpha = {}
theta_d = {}
beta_tab = {}
for dataset in ['A06', 'B05', 'B06']:
    d = data.elab_data['dataset'][dataset]
    beta0 = data.get('dataset', dataset, 'beta0')

    alpha[dataset] = {}
    beta_tab[dataset] = {}
    for ordine in ['righe1', 'righe2']:
        beta = unp.uarray(np.array([b[0] for b in d[ordine]]),
                          np.array([b[1] for b in d[ordine]]))
        alpha[dataset][ordine] = beta - beta0
        beta_tab[dataset][ordine] = beta

for dataset in ['B04']:
    d = data.elab_data['dataset'][dataset]

    alpha[dataset] = {}
    for ordine in ['righe1', 'righe2']:
        alpha[dataset][ordine] = unp.uarray(np.array([a[0] for a in d[ordine]]),
                                            np.array([a[1] for a in d[ordine]]))

for dataset in datasets_decenti:
    theta_i = data.get('dataset', dataset, 'theta-i')

    theta_d[dataset] = {}
    for ordine in ['righe1', 'righe2']:
        theta_d[dataset][ordine] = np.pi - theta_i - alpha[dataset][ordine]


# Fit e plot
def wavelength(m, d, theta_i, theta_d):
    return d/m*(unp.sin(theta_i) - unp.sin(theta_d))


def modello(n, R):
    return 1/(R*(1/4-1/(n**2)))


Resatto = 10973731.56816

plt.style.use("../style.yml")

for dataset in ['B05', 'B06']:
    print('=================')
    print(f'== Dataset {dataset} ==')
    print('=================')
    d = data.get('dataset', dataset, 'd')

    theta_i = data.get('dataset', dataset, 'theta-i')

    theta_dM1 = theta_d[dataset]['righe1']
    theta_dM2 = theta_d[dataset]['righe2']

    print(f'theta_i {theta_i}\ntheta_d')
    for i in theta_dM1:
        print(f'{i.nominal_value:.3f} +- {i.std_dev:.3f}')
    for i in theta_dM2:
        print(f'{i.nominal_value:.3f} +- {i.std_dev:.3f}')

    N1 = np.array(data.elab_data['dataset'][dataset]['nrighe1'])
    N2 = np.array(data.elab_data['dataset'][dataset]['nrighe2'])

    lambdaM1 = wavelength(1, d, theta_i, theta_dM1)
    lambdaM2 = wavelength(2, d, theta_i, theta_dM2)

    plt.errorbar(N1, unp.nominal_values(lambdaM1)*1e9, yerr=unp.std_devs(lambdaM1)*1e9,
                 linestyle='', color='black', elinewidth=1)
    plt.errorbar(N2, unp.nominal_values(lambdaM2)*1e9, yerr=unp.std_devs(lambdaM2)*1e9,
                 linestyle='', color='red', elinewidth=1)

    # faccio il fit di merda
    init = [Resatto]

    xfit = np.concatenate([N1, N2])
    yfit = np.concatenate([unp.nominal_values(lambdaM1), unp.nominal_values(lambdaM2)])
    yerr = np.concatenate([unp.std_devs(lambdaM1), unp.std_devs(lambdaM2)])

    opt, cov = curve_fit(
        modello,
        xfit, yfit,
        p0=init, sigma=yerr
    )

    opt = ufloat(opt, np.sqrt(cov))

    print(f'Best fit: {opt}')

    data.push(opt, 'meter^{-1}', 'dataset', dataset, 'R')

    X = np.linspace(min(N1), max(N1))
    plt.plot(X, modello(X, opt.nominal_value)*1e9, color='blue')
    # plt.plot(X, modello(X, Resatto)*1e9, color='red')

    plt.xlabel('$ n $')
    plt.ylabel('$ \\lambda $\ [nm]')
    plt.xticks(np.arange(min(N1), max(N1)+1, step=1))
    plt.title(f'Dataset {dataset}')
    plt.savefig(f'img/fit-{dataset}.png')
    # plt.show()
    plt.close()

    # Faccio la tabella
    tab = Table()

    tab.pushColumn('m', None, len(lambdaM1)*[1] + len(lambdaM2)*[2])
    tab.pushColumn('n', None, np.array(xfit))
    tab.pushColumn('\\beta', 'radian', np.concatenate([beta_tab[dataset]['righe1'], beta_tab[dataset]['righe2']]))
    tab.pushColumn('\\theta_{d}', 'radian', np.concatenate([theta_dM1, theta_dM2]))
    tab.pushColumn('\\lambda', 'nano\\meter', np.concatenate([lambdaM1, lambdaM2]))
    tab.sortRows((0, 1))
    data.recursivePush(tab, 'dataset', dataset, 'tab')

data.save()
