\documentclass[10pt,a4paper]{article}
\usepackage{../style/style}
\usepackage{../style/common-parts}

\newcommand{\vartab}[3]{\directlua{texprintStripNL(data['#1']['#2']['#3'])}}
\preambolo["../globals-fisica.yml"]

\begin{document}
\maketitle
\renewcommand{\abstractname}{Nota}
\begin{abstract}
    Questa esperienza è stata svolta a casa, collaborando in maniera telematica, tramite utilizzo di dati degli anni precedenti a causa della chiusura dei laboratori in seguito sospensione dell'attività didattica.
\end{abstract}

\section{Descrizione della misura}\label{sec:misura}
Lo scopo dell'esperienza è la misura della costante di Rydberg a partire dallo spettro dell'atomo di idrogeno.

In prima approssimazione lo spettro dell'atomo di idrogeno ha livelli energetici della forma
\begin{equation}
    E_{n} = - \frac{R h c}{n^{2}}
\end{equation}
dove $ n\in\N\setminus\{0\} $, $ R $ è la costante di Rydberg, $ h $ è la costante di Planck e $ c $ è la velocità della luce. La lunghezza d'onda della luce emessa in una transizione $ n_{1} \to n_{2} $ è quindi
\begin{equation}
    \frac{1}{\lambda_{n_{1}, n_{2}}} = R \left(\frac{1}{n_{2}^{2}} - \frac{1}{n_{1}^{2}}\right), \qquad n_{1} > n_{2}
\end{equation}
Pertanto, se si misura la lunghezza d'onda relativa ad una transizione nota, è possibile ricavare la costante di Rydberg. Per esempio, si vede che le righe di emissione nelle transizioni $ n \to 2 $ (serie di Balmer)
\begin{equation}\label{eq:Balmer}
    \frac{1}{\lambda_{n}} = R \left(\frac{1}{4} - \frac{1}{n^2}\right), \qquad n \geq 3
\end{equation}
sono nel visibile e pertanto sono quelle che verranno usate per la determinazione di $ R $. \\

A tale scopo si utilizza la luce prodotta da una lampada ad idrogeno che viene collimata (tramite un telescopio) e fatta incidere su di un reticolo di diffrazione. L'$ m $-esimo ordine di diffrazione di un fascio monocromatico di lunghezza d'onda $ \lambda $ che incide su un reticolo di diffrazione di passo $ d $ con angolo $ \theta\ped{i} $ viene emesso ad un angolo $ \theta_{\mathrm{d}, m} $ (angoli rispetto alla normale al piano del reticolo) che soddisfa la relazione
\begin{equation}\label{eq:diffrazione}
    m\lambda = d\left(\sin{\theta\ped{i}} - \sin{\theta_{\mathrm{d}, m}}\right),
    \qquad m\in\Z.
\end{equation}
Misurando con uno spettroscopio l'angolo di incidenza e quello di diffrazione ad un ordine noto, possiamo ottenere una stima della lunghezza d'onda della luce incidente. Se, come nel caso dell'atomo di idrogeno, la luce incidente non è monocromatica, le varie componenti vengono diffratte ad angoli diversi in base alla lunghezza d'onda. Essendo luce nel visibile possiamo distinguere le varie righe diffratte in base al ``colore'' e ottenere così una stima delle lunghezze d'onda associate alle transizioni della serie di Balmer. \\

La misura degli angoli di diffrazione viene fatta tramite uno spettroscopio a reticolo di diffrazione, schematizzato in Figura~\ref{fig:spettroscopio}.\\
Si fissa un angolo di $ \beta_{0} $ (angolo della direzione del fascio rispetto allo zero dello spettroscopio) rispetto al quale si misurano gli angoli $ \alpha = \beta - \beta_{0} $. L'angolo di incidenza $ \theta\ped{i} = \theta\ped{d,0} $ è legato a quello misurato $ \alpha\ped{d,0} $ (angolo del raggio riflesso rispetto alla direzione di incidenza del fascio  sul reticolo) da
\[
    \theta\ped{i} = \theta\ped{d,0}
    = \frac{\pi - \alpha\ped{d,0}}{2}
    = \frac{\pi + \beta_{0} - \beta\ped{d,0}}{2}.
\]
L'angolo del raggio diffratto $ \theta\ped{d} $ è legato a quello misurato $ \alpha\ped{d} $ (angolo del raggio diffratto rispetto alla direzione di incidenza del fascio  sul reticolo) da
\[
    \theta\ped{d} = \pi - \theta\ped{i} - \alpha\ped{d} = \frac{\pi}{2} + \frac{\alpha\ped{i}}{2} - \alpha\ped{d}
    = \frac{\pi + \beta_{0} + \beta\ped{d,0}}{2} - \beta\ped{d}.
\]
La geometria degli angoli è riportata in Figura~\ref{fig:angoli}.

\begin{figure}[h!]
    \centering
    \subfloat[\label{fig:spettroscopio}Spettroscopio a diffrazione]{
      \includegraphics[scale=0.4]{img/schema-spettroscopio.pdf}
    } \hspace{1cm}
    \subfloat[\label{fig:angoli}Geometria per la misura degli angoli]{
      \includegraphics[scale=0.4]{img/schema-angoli.png}
    }
    \caption{}
\end{figure}

\section{Determinazione del passo reticolare}\label{sec:passo-reticolare}
Si stima il passo del reticolo di diffrazione usato tramite la misura degli angoli di diffrazione di ordine $ m = 0 $ (riflessione) e $ m = 1 $ (primo ordine di diffrazione) prodotti da una lampada al mercurio, di cui è noto lo spettro. Con le convenzioni dette nella Sezione~\ref{sec:misura}, i dati che abbiamo utilizzato sono
\begin{align*}
  \texttt{B05}: && \beta_{0} &= \SI[]{ 2.944+-0.001 }{ \radian } && \beta\ped{d,0} = \SI[]{ 4.014+-0.001 }{ \radian } && \beta\ped{d,1} = \SI[]{ 4.842+-0.001 }{ \radian } \\
  \texttt{B06}: && \beta_{0} &= \SI[]{ 2.9451+-0.0004 }{ \radian } && \beta\ped{d,0} = \SI[]{ 3.6931+-0.0004 }{ \radian } && \beta\ped{d,1} = \SI[]{ 4.6090+-0.0002 }{ \radian }
\end{align*}
da cui si ricavano
\begin{align*}
  \texttt{B05}: && \theta\ped{i} &= \theta\ped{d,0} = \SI[]{ 1.0358+-0.0007 }{ \radian } && \theta\ped{d,1} = \SI[]{ 208+-2 }{ \milli\radian } \\
  \texttt{B06}: && \theta\ped{i} &= \theta\ped{d,0} = \SI[]{ 1.1968+-0.0003 }{ \radian } && \theta\ped{d,1} = \SI[]{ 280.9+-0.5 }{ \milli\radian }
\end{align*}
Usando la relazione~\eqref{eq:diffrazione} con $ m=1 $ possiamo ricavare il passo reticolare $ d = \lambda/(\sin\theta\ped{i} - \sin\theta\ped{d}) $ nota la lunghezza d'onda della luce usata. Per la lampada a mercurio utilizzata possiamo assumere che la lunghezza d'onda della luce emessa sia $ \lambda = \SI{ 546.1}{ \nano\meter } $. I valori ottenuti sono quindi i seguenti
\[
    d_{\texttt{B05}} = \SI[]{ 835+-2 }{ \nano\meter } \qquad
    d_{\texttt{B06}} = \SI[]{ 835.4+-0.7 }{ \nano\meter }
\]


\section{Determinazione delle righe di emissione dell'idrogeno}
In Tabella~\ref{tab:datiB05} e Tabella~\ref{tab:datiB06} si riportano i dati sperimentali relativi a $ \beta $ in funzione dell'ordine di diffrazione $ m $ e del numero quantico $ n $, con riferimento alla~\eqref{eq:Balmer}, assieme ai valori ottenuti per gli angoli di diffrazione $ \theta\ped{d} $ e alle corrispondenti lunghezze d'onda $ \lambda $.
Gli angoli sono stati calcolati come riportato nella Sezione~\ref{sec:misura}, mentre per la lunghezza d'onda si è usata la~\eqref{eq:diffrazione}
\[ \lambda = \frac{d}{m} \del{\sin\theta\ped{i} -\sin\theta_{\mathrm{d},m}} \]
dove l'angolo $ \theta\ped{i} $ è quello riportato nella Sezione~\ref{sec:passo-reticolare}.\\

\begin{table}[h!]
    \centering
    \vartab{dataset}{B05}{tab}
    \caption{\label{tab:datiB05}dataset \texttt{B05}.}
\end{table}

\begin{table}[h!]
    \centering
    \vartab{dataset}{B05}{tab}
    \caption{\label{tab:datiB06}dataset \texttt{B06}.}
\end{table}
Si è quindi effettuato un \emph{fit} con il modello a un parametro libero (la costante di Rydberg)
\[ \lambda(n) = \frac{1}{R} \del{\frac{1}{4} - \frac{1}{n^2}}^{-1}. \]
Si sono ottenute, nei due casi, come parametro di best fit, le seguenti stime della costante di Rydberg
\[
    R\ped{\texttt{B05}} = \SI[]{ 10.970+-0.006 }{ \mega\meter^{-1} } \qquad
    R\ped{\texttt{B06}} =  \SI[]{ 10.95+-0.01 }{ \mega\meter^{-1} }
\]
compatibili rispettivamente entro 1 e 3 deviazioni standard, con il valore atteso
\[ R = \SI{10.973}{\mega\meter^{-1}}. \]
Riportiamo in Figura~\ref{fig:fit-idrogeno} i dati sperimentali e le relative curve di \emph{best-fit}.

\begin{figure}[p]
    \centering
    \def\scale{0.8}
    \subfloat[\label{fig:fitB05}Dataset \texttt{B05}]{
      \includegraphics[scale=\scale]{img/fit-B05.png}
    }\\
    \subfloat[\label{fig:fitB06}Dataset \texttt{B06}]{
      \includegraphics[scale=\scale]{img/fit-B06.png}
    }
    \caption{\label{fig:fit-idrogeno}dati sperimentali e curve di \emph{best-fit} per la lunghezza d'onda $ \lambda $ delle righe spettrali in funzione del numero quantico $ n $. In nero i dati relativi a $ m = 1 $, in rosso quelli relativi a $ m = 2 $.}
\end{figure}

Sottolineiamo, tuttavia, che nelle Tabelle~\ref{tab:datiB05} e~\ref{tab:datiB06} e in Figura~\ref{fig:fit-idrogeno} non sono riportati i dati relativi ad alcune righe osservate ma non attribuibili allo spettro atomico dell'idrogeno, nemmeno tenendo conto dello splitting dei livelli dovuto alla struttura fine\footnote{
  Riguardo a questo splitting, il gruppo \texttt{B06} ha effettivamente misurato alcuni doppietti, come si può vedere in Tabella~\ref{tab:datiB06}, dove sono riportati più valori per gli stessi $ m $ e $ n $. Questi sono stati attributi allo stesso $ n $ perché il modello da cui si ottiene la~\eqref{eq:Balmer} considera degeneri i livelli appartenenti a un doppietto.
}. Non essendo stati presenti alla presa dei dati e non sapendo in quali condizioni siano state prese le misure, possiamo solo ipotizzare che le linee in questione possano essere attribuibili a impurità presenti all'interno della lampada utilizzata, ovvero a luce ambientale del laboratorio.

\dichiarazione

\end{document}
