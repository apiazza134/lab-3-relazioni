#!/usr/bin/env python3

from uncertainties.umath import sin
from math import pi

from lab import Data

d = Data()

# Computo gli angoli alpha
for dataset in ['B05', 'B06']:
    beta0 = d.get('dataset', dataset, 'beta0')
    beta_ord0 = d.get('dataset', dataset, 'Hg', 'beta-ord-0')
    beta_ord1 = d.get('dataset', dataset, 'Hg', 'beta-ord-1')

    alpha_ord0 = beta_ord0 - beta0
    alpha_ord1 = beta_ord1 - beta0
    d.push(alpha_ord0, 'radian', 'dataset', dataset, 'Hg', 'alpha-ord-0')
    d.push(alpha_ord1, 'radian', 'dataset', dataset, 'Hg', 'alpha-ord-1')


# Computo gli angoli di rifrazione e il passo reticolare
lbd = d.get('lambdaHg')
datasets = list(set(d.elab_data['dataset'].keys()) - set(['A06-deg', 'B04-deg']))
for dataset in datasets:
    alpha_ord0 = d.get('dataset', dataset, 'Hg', 'alpha-ord-0')
    alpha_ord1 = d.get('dataset', dataset, 'Hg', 'alpha-ord-1')

    theta_i = (pi - alpha_ord0)/2
    theta_d = pi - theta_i - alpha_ord1
    passo_ret = lbd/(sin(theta_i) - sin(theta_d))

    d.push(theta_i, 'radian', 'dataset', dataset, 'theta-i')
    d.push(theta_d, 'radian', 'dataset', dataset, 'Hg', 'theta-d')
    d.push(passo_ret, 'meter', 'dataset', dataset, 'd')

d.save()
