#!/usr/bin/env python3

from lab.data import Data
import os

if os.system('python3 deg_to_rad.py') | \
   os.system('python3 mercurio.py') | \
   os.system('python3 idrogeno.py'):
    exit(1)

Data().toyaml()
