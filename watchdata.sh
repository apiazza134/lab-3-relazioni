#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

print_help() {
    cat << EOF

Usage: $0 scriptname filename
Uses 'inotifywait' to sleep until 'filename' has been modified.

Inspired by https://bitbucket.org/denilsonsa/small_scripts/src/default/sleep_until_modified.sh

EOF
}

# check dependencies
if ! type inotifywait &>/dev/null ; then
    echo "You are missing the inotifywait dependency. Install the package inotify-tools (apt-get install inotify-tools)"
    exit 1
fi

# parse_parameters:
while [[ "$1" == -* ]] ; do
    case "$1" in
	-h|-help|--help)
	    print_help
	    exit
	    ;;
	--)
	    #echo "-- found"
	    shift
	    break
	    ;;
	*)
	    echo "Invalid parameter: '$1'"
	    exit 1
	    ;;
    esac
done

if [ "$#" != 2 ] ; then
	echo "Incorrect number of parameters. Use --help for usage instructions."
	exit 1
fi

SCRIPTNAME=$1
FILENAME=$2

while inotifywait -e close_write,moved_to,create $FILENAME; do
    ./$SCRIPTNAME $FILENAME;
done
